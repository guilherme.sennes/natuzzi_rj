/**
 * Created by foreverpinetree@gmail.com on 2016-08-20.
 */
!(function($)
{
    "use strict";

    $(function()
    {
		$('input.script-code').each(function () {
            var $input = $(this);
            var mode = 'javascript';
            var editDiv = $('<div>', {
                height:'600px',
                'class': $input.attr('class') + ' custom-script-editor'
            }).insertBefore($input);
            $input.css('visibility', 'hidden');
            var editor = ace.edit(editDiv[0]);
            editor.renderer.setShowGutter(false);
			editor.setShowPrintMargin(false);
            editor.getSession().setValue($input.val());
            editor.getSession().setMode("ace/mode/" + mode);
            editor.setTheme("ace/theme/monokai");
            
            $input.closest('form').submit(function () {
                $input.val(editor.getSession().getValue());
            });

            $input.closest('form').find('.script-save-btn').on('click', function(){
                $(this).closest('form').submit();
            });

            setTimeout(function(){
                $('body').addClass('custom-script-ready');
            }, 100);
        });

        var optionsExportLimit = false;
        $('.export-script-btn').on('click', function(evt){
            evt.preventDefault();

            if(optionsExportLimit)
            {
                alert('Don\'t export so offen! Just wait a few seconds!');
                return;
            }

            optionsExportLimit = true;
            setTimeout(function(){
                optionsExportLimit = false;
            }, 5000);

            $('#script-options-form').ajaxSubmit({
                type:"post",
                url:ajaxurl,
                data:{
                    action: 'export_custom_script'
                },
                success:function(data){
                    var obj = JSON.parse(data);
                    var status = obj.status;

                    /**
                     * return status: 
                     * 1: Success.
                     * 0: Failed, unexpected error.
                     * -1: Not allowed.
                     * -2: In CD, export too often.
                     */

                    if(status == '1')
                    {
                        var url = obj.url;
                        if(url)
                        {
                            var a = document.createElement('a');
                            a.download = 'custom-script.js';
                            a.setAttribute('href', url);
                            a.click();
                        }
                    }
                    else if(status == '0')
                    {
                        alert("Failed, unexpected error!");
                    }
                    else if(status == '-1')
                    {
                        alert("Error, you don't have access to this page!");
                    }
                    else if(status == '-2')
                    {
                        alert('Don\'t export so offen! Just wait a few seconds!');
                    }
                }
            });
        });

        $('.script-btns-checkbox-item input[type="checkbox"]').on('change', function(evt) {
            if($(this).prop('checked'))
            {
                $(this).val('1');
            }
            else
            {
                $(this).val('0');
            }
        }).each(function(){
            if($(this).val() === '1')
            {
                $(this).prop('checked', true);
            }
            else
            {
                $(this).prop('checked', false);
            }
        });
	});
    
}(jQuery));