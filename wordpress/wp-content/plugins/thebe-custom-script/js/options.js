/**
 * Created by foreverpinetree@gmail.com on 2016-08-20.
 */
!(function($)
{
    "use strict";

    $(function()
    {
        // ------------------ options ------------------
        $('.option-item.custom-script input.option-value[data-editor]').each(function () {
            var $input = $(this);
            var mode = $input.data('editor') || 'javascript';
            var editDiv = $('<div>', {
                height:'250px',
                'class': $input.attr('class') + ' custom-script-editor'
            }).insertBefore($input);
            $input.css('visibility', 'hidden');
            var editor = ace.edit(editDiv[0]);
            editor.renderer.setShowGutter(false);
            editor.$blockScrolling = Infinity;
			editor.setShowPrintMargin(false);
            editor.getSession().setValue($input.val());
            editor.getSession().setMode("ace/mode/" + mode);
            editor.setOption('wrap', 'free');
            editor.setTheme("ace/theme/monokai");
            editor.on('change', function(){
                $input.val(editor.getSession().getValue());
            })

            this.__editor = editor;
        });

        $('.option-item.custom-script .option-item-tab').on('click', function(evt){
            var $this = $(this);
            var $parent = $this.parent();
            var type = $this.data('type');
            
            $parent.children('.option-item-tab').removeClass('active');
            $this.addClass('active');
            
            $parent.siblings('.editor-container.active').removeClass('active');

            var $editorContainer = $parent.siblings('[data-type="' + type + '"]');
            $editorContainer.addClass('active');
            var input = $editorContainer.find('input.option-value[data-editor]').get(0);
            if(input)
            {
                var editor = input.__editor;
                setTimeout(function(){
                    if(editor)
                    {
                        editor.renderer.updateFull();
                    }
                }, 100);
            }

            var ls = window.localStorage;
            if(ls)
            {
                var key = window.location.search || window.location.href;
                ls.setItem(key, type);
            }
        });

        var ls = window.localStorage;
        if(ls)
        {
            var key = window.location.search || window.location.href;
            var type = ls.getItem(key);
            if(type)
            {
                $('.option-item.custom-script').find('.option-item-tab[data-type="' + type + '"]').trigger('click');
            }
        }
        
	});
    
}(jQuery));