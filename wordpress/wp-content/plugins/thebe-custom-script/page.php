<?php 
	if (!current_user_can('manage_options'))
	{
		wp_die(esc_html__("You don't have access to this page", 'thebe-custom-script'));
	}
	
    ?>
    <div class="custom-script-loading"></div>
    <div class="custom-script-wrap" style="display:none">
        <span class="script-page-title"><?php esc_html_e('Custom Script', 'thebe-custom-script'); ?></span>
        <span class="script-page-des"><?php esc_html_e('Enter your custom Javascript (just like Google Analytics) here.', 'thebe-custom-script'); ?></span>
        <form method="post" action="options.php" id="script-options-form">
            <?php
                settings_fields('thebe_custom_script_fields');
            ?>
            <input class="script-code" type="hidden" name="<?php echo esc_attr( THEBE_CUSTOM_SCRIPT_OPTION_NAME ); ?>" value="<?php echo esc_attr( get_option( THEBE_CUSTOM_SCRIPT_OPTION_NAME, '' ) ); ?>" />
            <div class="script-btns">
                <div class="script-save-btn script-btn"><?php esc_attr_e('Save Changes', 'thebe-custom-script'); ?></div>
                <div class="export-script-btn script-btn"><?php esc_attr_e('Export', 'thebe-custom-script'); ?></div>
                <div class="script-btns-checkbox-item">
                    <label>
                        <input type="checkbox" name="<?php echo esc_attr( THEBE_CUSTOM_SCRIPT_USE_OPTIONS_NAME ); ?>" value="<?php echo esc_attr( THEBE_CUSTOM_SCRIPT_USE_OPTIONS ); ?>"/>
                        <span>
                            <?php esc_attr_e('Enabled on Page/Post/Project/Product', 'thebe-custom-script'); ?>
                            <i title="<?php esc_attr_e('If checked, an option "Custom Script" will be added into the setting of Page/Post/Project/Product.', 'thebe-custom-script'); ?>"></i>
                        </span>
                    </label>
                </div>
            </div>
        </form>
    </div>