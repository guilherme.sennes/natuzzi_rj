<?php
/**
 * @author foreverpinetree@gmail.com
 * site: https://www.3theme.com
 **/
 
class Thebe_Custom_Script_Option_Manager
{
	static public function add_custom_box()
	{
		add_meta_box(
			'thebe_custom_script',
			esc_html__( 'Custom Script', 'thebe-custom-script' ),
			array('Thebe_Custom_Script_Option_Manager', 'inner_page_setting'),
			array('post','project', 'page', 'product'),
			'normal',
			'high'
		);
	}
	 
	//output page data
	static public function inner_page_setting( $post )
	{
		wp_nonce_field( 'thebe_custom_script_options_mgr_action_page', 'thebe_custom_script_options_mgr_page' );

		$css = get_post_meta( $post->ID, THEBE_CUSTOM_SCRIPT_META_CSS, true );
        $js = get_post_meta( $post->ID, THEBE_CUSTOM_SCRIPT_META_JS, true );

        ?>
        <div class="option-item custom-script">
            <ul class="custom-script-tabs">
                <li class="option-item-tab active" data-type="css"><i></i><?php esc_html_e( 'CSS', 'thebe-custom-script' ); ?></li>
                <li class="option-item-tab" data-type="js"><i></i><?php esc_html_e( 'JavaScript', 'thebe-custom-script' ); ?></li>
            </ul>
            <div class="editor-container active" data-type="css">
                <input class="option-value" type="hidden" data-editor="css" name="<?php echo esc_attr( THEBE_CUSTOM_SCRIPT_META_CSS ); ?>" value="<?php echo esc_attr( $css ); ?>" />
            </div>
            <div class="editor-container" data-type="js">
                <input class="option-value" type="hidden" data-editor="javascript" name="<?php echo esc_attr( THEBE_CUSTOM_SCRIPT_META_JS ); ?>" value="<?php echo esc_attr( $js ); ?>" />
            </div>
        </div>
        <?php
	}
	
	//save data
	static public function save_postdata( $post_id )
	{
		//check if user can edit the page/post
		if( isset($_POST['post_type']) && $_POST['post_type'] === "post" )
		{
			if( !current_user_can('edit_post', $post_id) )
			{
                return;
            }
		}
		else 
		{
			if( !current_user_can('edit_page', $post_id) )
			{
                return;
            }
		}

		if( !isset($_POST['thebe_custom_script_options_mgr_page']) )
		{
			return;
		}

		if( isset($_POST['thebe_custom_script_options_mgr_page']) && ! wp_verify_nonce($_POST['thebe_custom_script_options_mgr_page'], 'thebe_custom_script_options_mgr_action_page') )
		{
            return;
        }

        if( isset( $_POST[THEBE_CUSTOM_SCRIPT_META_CSS] ) )
        {
            $css_value = $_POST[THEBE_CUSTOM_SCRIPT_META_CSS];
            add_post_meta($post_id, THEBE_CUSTOM_SCRIPT_META_CSS, $css_value, true) or update_post_meta($post_id, THEBE_CUSTOM_SCRIPT_META_CSS, $css_value);
        }

        if( isset( $_POST[THEBE_CUSTOM_SCRIPT_META_JS] ) )
        {
            $js_value = $_POST[THEBE_CUSTOM_SCRIPT_META_JS];
            add_post_meta($post_id, THEBE_CUSTOM_SCRIPT_META_JS, $js_value, true) or update_post_meta($post_id, THEBE_CUSTOM_SCRIPT_META_JS, $js_value);
        }
	}
}

add_action( 'admin_head', array('Thebe_Custom_Script_Option_Manager', 'add_custom_box'), 11 );
add_action( 'save_post', array('Thebe_Custom_Script_Option_Manager', 'save_postdata'), 10, 3 );

?>