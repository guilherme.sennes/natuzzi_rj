<?php
/*
Plugin Name: Thebe Custom Script
Plugin URI: http://3theme.com
Description: This plugin enables customizing Javascript.
Version: 1.0.1
Author: foreverpinetree@gmail.com
Author URI: http://foreverpinetree.com/
License: GPLv2 or later
Text Domain: thebe-custom-script
*/

/**
 * return status: 
 * 1: Success.
 * 0: Failed, unexpected error.
 * -1: Not allowed.
 * -2: In CD, export too often.
 */

if ( !defined( 'ABSPATH' ) )
{
	exit;
}

if ( ! defined( 'THEBE_CUSTOM_SCRIPT_OPTION_NAME' ) ) 
{
    define( 'THEBE_CUSTOM_SCRIPT_OPTION_NAME', 'thebe_custom_script' );
}

if ( ! defined( 'THEBE_CUSTOM_SCRIPT_USE_OPTIONS_NAME' ) ) 
{
    define( 'THEBE_CUSTOM_SCRIPT_USE_OPTIONS_NAME', 'thebe_custom_script_use_options' );
}

if ( ! defined( 'THEBE_CUSTOM_SCRIPT_VERSION' ) ) 
{
    define( 'THEBE_CUSTOM_SCRIPT_VERSION', '1.0.1' );
}

if ( ! defined( 'THEBE_CUSTOM_SCRIPT_META_CSS' ) ) 
{
    define( 'THEBE_CUSTOM_SCRIPT_META_CSS', '_thebe_custom_script_meta_css' );
}

if ( ! defined( 'THEBE_CUSTOM_SCRIPT_META_JS' ) ) 
{
    define( 'THEBE_CUSTOM_SCRIPT_META_JS', '_thebe_custom_script_meta_js' );
}

if ( ! defined( 'THEBE_CUSTOM_SCRIPT_USE_OPTIONS' ) ) 
{
    define( 'THEBE_CUSTOM_SCRIPT_USE_OPTIONS', get_option( THEBE_CUSTOM_SCRIPT_USE_OPTIONS_NAME, '0' ) === '1' ? '1' : '0' );
}

add_action('admin_menu', 'thebe_custom_script_plugin');
function thebe_custom_script_plugin()
{
	if ( !defined( 'THEBE_THEME_NAME' ) || THEBE_THEME_NAME !== 'thebe' )
	{
		return;
	}

	load_plugin_textdomain( 'thebe-custom-script', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

	add_option( THEBE_CUSTOM_SCRIPT_OPTION_NAME, '', '', 'yes' );
    add_option( THEBE_CUSTOM_SCRIPT_USE_OPTIONS_NAME, '', '', 'yes' );

    register_setting( 'thebe_custom_script_fields', THEBE_CUSTOM_SCRIPT_OPTION_NAME );
    register_setting( 'thebe_custom_script_fields', THEBE_CUSTOM_SCRIPT_USE_OPTIONS_NAME );
    
    if( THEBE_CUSTOM_SCRIPT_USE_OPTIONS === '1' )
    {
        include_once(plugin_dir_path( __FILE__ ) . 'metabox.php');

        // css
	    wp_enqueue_style( 'pt-custom-script-options', plugin_dir_url(__FILE__).'css/options.css', false, THEBE_CUSTOM_SCRIPT_VERSION, 'all' );

        // js
        wp_enqueue_script( 'pt-custom-script-ace-js', plugin_dir_url(__FILE__) . 'js/ace/ace.js', array(), THEBE_CUSTOM_SCRIPT_VERSION, 'all' );
        wp_enqueue_script( 'pt-custom-script-options-js', plugin_dir_url(__FILE__) . 'js/options.js', array(), THEBE_CUSTOM_SCRIPT_VERSION, 'all' );
    }
	
    add_theme_page(esc_html__('Custom Script', 'thebe-custom-script'), esc_html__('Custom Script', 'thebe-custom-script'), 'administrator', 'custom_script', 'thebe_output_custom_script_page');
}

/**
 * Output script page
 */
function thebe_output_custom_script_page()
{
    include_once(plugin_dir_path( __FILE__ ) . 'page.php');
	
	// css
	wp_enqueue_style( 'pt-custom-script-style', plugin_dir_url(__FILE__).'css/style.css', array(), THEBE_CUSTOM_SCRIPT_VERSION, 'all' );

	// js
	wp_enqueue_script('jquery-form');
	wp_enqueue_script( 'pt-custom-script-ace-js', plugin_dir_url(__FILE__) . 'js/ace/ace.js', array(), THEBE_CUSTOM_SCRIPT_VERSION, 'all' );
    wp_enqueue_script( 'pt-custom-script-js', plugin_dir_url(__FILE__) . 'js/main.js', array(), THEBE_CUSTOM_SCRIPT_VERSION, 'all' );
}

/**
 * WP-AJAX export script
 */
add_action('wp_ajax_export_custom_script', 'thebe_export_custom_script');
function thebe_export_custom_script()
{
	if(is_admin())
	{
		$export_time = get_option('thebe_custom_script_export_time', 0);
		if($export_time === FALSE)
		{
			add_option('thebe_custom_script_export_time', time(), '', 'yes');
		}
		else
		{
			$offset_time = time() - $export_time;
			if($offset_time < 5)
			{
				exit('{"status":-2}');//in CD (export too offten)
			}

			update_option('thebe_custom_script_export_time', time());
		}

		$js_data = get_option( THEBE_CUSTOM_SCRIPT_OPTION_NAME, '' );

		$file_name = 'scrpit-'.date('Ymd').'.js';

		$dir = plugin_dir_path( __FILE__ ) .'export-data/';
		wp_mkdir_p($dir);

		global $wp_filesystem;
		WP_Filesystem();
		$wp_filesystem->put_contents($dir.$file_name, $js_data);
		
		exit('{"status":1, "url":"'.esc_url(plugin_dir_url(__FILE__).'export-data/'.$file_name).'"}');
	}
	exit('{"status":0}');
}


/**
 * WP-AJAX export options warnning if user no login
 */
add_action('wp_ajax_nopriv_export_custom_script', 'thebe_export_custom_script_nopriv');
function thebe_export_custom_script_nopriv()
{
	exit('{"status":-1}');
}


if( THEBE_CUSTOM_SCRIPT_USE_OPTIONS === '1' )
{
    if( !function_exists( 'thebe_custom_script_output_script' ) )
    {
        function thebe_custom_script_output_script()
        {
            $post_id = get_queried_object_id();
            if( $post_id !== 0 )
            {
                $css = get_post_meta( $post_id, THEBE_CUSTOM_SCRIPT_META_CSS, true );
                $js = get_post_meta( $post_id, THEBE_CUSTOM_SCRIPT_META_JS, true );

                //Output all the styles
	            wp_add_inline_style('thebe-style', $css);

                $js = '!(function($){' . PHP_EOL . $js . PHP_EOL . '})(jQuery);';
                wp_add_inline_script( 'thebe-main', $js, 'before' );
            }
        }
    }
    add_action( 'wp_enqueue_scripts', 'thebe_custom_script_output_script', 11 );
}


?>