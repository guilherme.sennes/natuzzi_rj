<?php
/**
 * @author foreverpinetree@gmail.com
 * site: http://www.foreverpinetree.com
 **/
 
class Thebe_Generate_HTML
{
	static public function handle($box_type, $module_name, $data, $value, $is_basic = false, $class = '', $use_post_format = false)
	{
		$var = $data['var'];
		$module_label = isset($data['label']) ? $data['label'] : $var;
		$placeholder = isset($data['placeholder']) ? $data['placeholder'] : '';
		$default = isset($data['default']) ? $data['default'] : '';
		$type = $data['type'];
		
		$hide_des = isset($data['hide_des']) ? $data['hide_des'] : false;
		$des = $hide_des ? "" : (isset($data['des']) ? $data['des'] : "");

		$extend_class = isset($data['class']) ? $data['class'].' ' : "";

		if($class != '')
		{
			$extend_class .= $class . ' ';
		}

		if($is_basic)
		{
			$extend_class .= 'basic-option-item ';

			$name = $module_name."_".$var;
		}
		else
		{
			$name = $module_name."_".$var;
		}
		
		$help = isset($data['help']) ? $data['help'] : "";

		$params = isset($data['params']) ? $data['params'] : array();

		$post_format = '';
		if($use_post_format && $params && isset($params['post_format']))
		{
			$post_format = implode(',', $params['post_format']);
			$extend_class .= 'option-item-hide post-format-filter ';
		}
		
		if($value == null)
		{
			$value = $default;
		}

		$d_index = strrpos($var, "setting_box_title");
		if(is_int($d_index))
		{
			?>
			<h3 class="setting_box_title <?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var); ?> option-item" data-format="<?php echo esc_attr($post_format); ?>">
				<?php thebe_kses_content($des); ?>
			</h3>
			<?php
			return;
		}
		
		$d_index = strrpos($var, "delimiter");
		if(is_int($d_index))
		{
			?>
			<div class="custom_delimiter <?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var); ?> option-item" data-format="<?php echo esc_attr($post_format); ?>">
				<?php thebe_kses_content($des); ?>
				<hr/>
			</div>
			<?php
			return;
		}
		
		if($type == "title")
		{
			?>
			<div class="<?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var); ?> option-title option-item" data-format="<?php echo esc_attr($post_format); ?>">
				<p><?php echo esc_html($module_label); ?></p>
				<em><?php thebe_kses_content($des); ?></em>
			</div>
			<?php
		}
		else if($type == "text")
		{
			?>
			<div class="<?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var); ?> option-item" data-format="<?php echo esc_attr($post_format); ?>">
			<?php
			if($module_label != '') 
			{
				?><strong><?php echo esc_html($module_label); ?>:</strong><?php
			}
			?>
			<div class="option-content-wrap">
				<input class="<?php echo esc_attr($module_name.'_custom_'.$box_type.'_input '.$module_name.'_custom_'.$box_type.'_box option-input-field'); ?>" type="text" id="<?php echo esc_attr($module_name.'_'.$var); ?>" name="<?php echo esc_attr($name); ?>" value="<?php echo esc_attr($value); ?>" placeholder="<?php echo esc_attr($placeholder); ?>"/>
				<span class="pt-option-item-des"><?php thebe_kses_content($des); ?></span>
				<?php
				if( $help != '' )
				{
					?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php
				}
				?>
			</div>
			</div>
			<?php
		}
		else if($type == "textarea")
		{
			$use_mce = isset($params['use_mce']) ? $params['use_mce'] : false;
			$need_shortcode = isset($params['need_shortcode']) ? $params['need_shortcode'] : false;
			
			$rows = isset($data['rows']) ? $data['rows'] : 5;
			
			?>
			<div class="<?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var.' option-item'); ?>" data-id="<?php echo esc_attr($module_name.'_'.$var); ?>" data-format="<?php echo esc_attr($post_format); ?>">
			<?php
			if($module_label != '') 
			{
				?>
				<strong class="<?php echo ($need_shortcode ? 'use-mce-textarea' : 'unuse-mce-textarea'); ?>"><?php echo esc_html($module_label); ?>:</strong>
				<?php
			}

			?><div class="option-content-wrap"><?php
			
				if($use_mce)
				{
					wp_editor(wp_specialchars_decode($value, ENT_QUOTES), $module_name.'_'.$var, array(
							'textarea_rows' => 10,
							'media_buttons' => $need_shortcode,
							'wpautop' => false
						)
					);
				}
				else
				{
					?>
					<textarea class="<?php echo esc_attr($module_name.'_custom_'.$box_type.'_input '.$module_name.'_custom_'.$box_type.'_box option-textarea'); ?>" rows="<?php echo esc_attr( $rows ); ?>" cols="120" id="<?php echo esc_attr($module_name.'_'.$var); ?>" name="<?php echo esc_attr($name); ?>" placeholder="<?php echo esc_attr($placeholder); ?>"><?php echo esc_html($value); ?></textarea>
					<?php
				}
				if($des != '')
				{
					?>
					<span class="pt-option-item-des"><?php thebe_kses_content($des); ?></span>
					<?php
				}

				if( $help != '' )
				{
					?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php
				}
				?>
				</div>
			</div>
			<?php
		}
		else if($type == "popup_textarea")
		{
			?>
			<div class="<?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var.' option-item'); ?>" data-id="<?php echo esc_attr($module_name.'_'.$var); ?>" data-format="<?php echo esc_attr($post_format); ?>">
			<?php

			if($module_label != '') 
			{
				?>
				<strong class=""><?php echo esc_html($module_label); ?>:</strong>
				<?php
			}

			$btn_name = isset($params['btn_name']) ? $params['btn_name'] : __('Edit', 'thebe-options-manager');
			$textarea_w = isset($params['w']) ? $params['w'] : 800;
			$textarea_h = isset($params['h']) ? $params['h'] : 500;
			$use_mce = isset($params['use_mce']) ? $params['use_mce'] : false;
			$need_shortcode = isset($params['need_shortcode']) ? $params['need_shortcode'] : false;

			?>
			<div class="option-content-wrap">
				<div class="popup-textarea-open-btn button button-primary button-large"><?php echo esc_html($btn_name); ?></div>
				<div class="popup-textarea-container <?php echo esc_attr( 'popup_tc_'.$module_name.'_'.$var ); ?>">
					<div class="popup-textarea-content" style="width:<?php echo esc_attr($textarea_w); ?>px;height:<?php echo esc_attr($textarea_h); ?>px;">
						<?php
						if($use_mce)
						{
							wp_editor(wp_specialchars_decode($value, ENT_QUOTES), $module_name.'_'.$var, array(
									'textarea_rows' => 10,
									'media_buttons' => $need_shortcode,
									'wpautop' => false
								)
							);
						}
						else
						{
							?>
							<textarea class="<?php echo esc_attr($module_name.'_custom_'.$box_type.'_input '.$module_name.'_custom_'.$box_type.'_box option-popup-textarea'); ?>" id="<?php echo esc_attr($module_name.'_'.$var); ?>" name="<?php echo esc_attr($name); ?>" placeholder="<?php echo esc_attr($placeholder); ?>"><?php echo esc_html($value); ?></textarea>
							<?php
						}
						?>
						<div class="popup-textarea-close-btn"><span><?php esc_html_e('Finish', 'thebe-options-manager'); ?></span></div>
					</div>
				</div>
				<?php
				if($des != '')
				{
					?>
					<span class="pt-option-item-des"><?php thebe_kses_content($des); ?></span>
					<?php
				}

				if( $help != '' )
				{
					?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php
				}
				?>
			</div>
			</div>
			<?php
		}
		else if($type == "shortcode")
		{
			$btn_labels = array(
				'edit' => esc_html__('Edit', 'thebe'),
				'duplicate' => esc_html__('Duplicate', 'thebe'),
				'add' => esc_html__('Add after it', 'thebe'),
				'delete' => esc_html__('Delete', 'thebe'),
			);

			?>
			<div class="<?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var.' option-item'); ?>" data-id="<?php echo esc_attr($module_name.'_'.$var); ?>" data-format="<?php echo esc_attr($post_format); ?>" data-button-labels="<?php echo esc_attr( json_encode($btn_labels) ); ?>">
			<?php
			if($module_label != '') 
			{
				?>
				<strong class="<?php echo ($need_shortcode ? 'use-mce-textarea' : 'unuse-mce-textarea'); ?>"><?php echo esc_html($module_label); ?>:</strong>
				<?php
			}

			?>
			<div class="option-content-wrap">
				<div class="shortcode-header">
					<div class="shortcode-entry-buttons">
						<div class="insert-shortcode-button shortcode-button"><?php esc_html_e( 'Insert Shortcode', 'thebe' ); ?></div>
					</div>
					<ul class="sc-mode-switch">
						<li class="sc-mode-visual selected"><?php esc_html_e( 'Visual', 'thebe' ); ?></li>
						<li class="sc-mode-text"><?php esc_html_e( 'Text', 'thebe' ); ?></li>
					</ul>
				</div>
				<div class="shortcode-list" data-edit-info="<?php echo esc_attr( 'Double click to edit.', 'thebe' ); ?>" tabindex="-1"></div>
				<textarea class="<?php echo esc_attr($module_name.'_custom_'.$box_type.'_input '.$module_name.'_custom_'.$box_type.'_box shortcode-text'); ?>" id="<?php echo esc_attr($module_name.'_'.$var); ?>" name="<?php echo esc_attr($name); ?>"><?php echo esc_html($value); ?></textarea>
				<?php
				if($des != '')
				{
					?>
					<span class="pt-option-item-des"><?php thebe_kses_content($des); ?></span>
					<?php
				}

				if( $help != '' )
				{
					?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php
				}
				?>
				</div>
			</div>
			<?php
		}
		else if($type == "icon")
		{
			?>
			<div class="<?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var.' option-item'); ?>" data-format="<?php echo esc_attr($post_format); ?>">
			<?php
			if($module_label != '')
			{
				?><strong><?php echo esc_html($module_label)?>:</strong><?php
			}
			?>
			<input class="<?php echo esc_attr($module_name.'_custom_'.$box_type.'_input '.$module_name.'_custom_'.$box_type.'_box option-input-field icon-input'); ?>" type="text" id="<?php echo esc_attr($module_name.'_'.$var); ?>" name="<?php echo esc_attr($name); ?>" value="<?php echo esc_attr($value); ?>" placeholder="<?php echo esc_attr($placeholder)?>"/>
			<div class="icons-insert-button icons-btn-pe" data-id="pe" title="<?php echo esc_attr__('Pe icon 7', 'thebe-options-manager'); ?>"></div>
			<div class="icons-insert-button icons-btn-fontawesome" data-id="fontawesome" title="<?php echo esc_attr__('FontAwesome', 'thebe-options-manager'); ?>"></div>
			<span class="pt-option-item-des"><?php thebe_kses_content($des); ?></span>
			<?php
			if( $help != '' )
			{
				?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php
			}
			?>
			</div>
			<?php
		}
		else if($type == 'checkbox')
		{
			$not_save = isset($param['not_save']) ? $param['not_save'] : '0';

			$only_available = isset($data['only_available']) ? $data['only_available'] : null;
			$available_value = $only_available != null ? json_encode($only_available) : '';

			$bind_body_class_prefix = isset($data['body_class_prefix']) ? $data['body_class_prefix'] : '';
			if($bind_body_class_prefix != '')
			{
				$extend_class .= 'bind-body-class ';
			}

			?>
			<div class="<?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var.' option-item checkbox-item'); ?>" data-format="<?php echo esc_attr($post_format); ?>">
			<?php
			if($module_label != '') 
			{
				?>
				<strong><?php echo esc_html($module_label); ?>:</strong>
				<?php
			}
			?>
			<div class="option-content-wrap">
				<input class="<?php echo esc_attr($module_name.'_custom_'.$box_type.'_input '.$module_name.'_custom_'.$box_type.'_box'); ?>" type="hidden" <?php echo ($not_save == '1' ? '' : 'name="'.esc_attr($name).'"'); ?> value="<?php echo esc_attr($value); ?>" data-available="<?php echo esc_attr($available_value); ?>" data-prefix="<?php echo esc_attr($module_name); ?>" data-bodyclass-prefix="<?php echo esc_attr($bind_body_class_prefix); ?>" />
				<input class="<?php echo esc_attr($module_name.'_custom_'.$box_type.'_input '.$module_name.'_custom_'.$box_type.'_box option-input-field pt-checkbox'); ?>" type="checkbox" id="<?php echo esc_attr($module_name.'_'.$var); ?>" <?php echo ( $value == '1' ? 'checked' : '' ); ?>/>
				<span class="pt-option-item-des"><?php thebe_kses_content($des); ?></span>

				<?php
				if( $help != '' )
				{
					?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php
				}
				?>
			</div>
			</div>
			<?php
		}
		else if($type == "select")
		{
			$choices = $data['options'];
			if($choices == NULL || !is_array($choices) || count($choices) < 1) return;
			$use_preview = isset($data['use_preview']) && $data['use_preview'] == true;
			$remove_preview_prefix = isset($data['remove_path_prefix']) && $data['remove_path_prefix'] == true;
			$preview_class = $use_preview ? 'select-previewable ' : '';
			$outer_preview_class = $use_preview ? ' has-pt-select' : '';
			$only_available = isset($data['only_available']) ? $data['only_available'] : null;
			$available_value = $only_available != null ? json_encode($only_available) : '';
			$bind_body_class_prefix = isset($data['body_class_prefix']) ? $data['body_class_prefix'] : '';
			if($bind_body_class_prefix != '')
			{
				$extend_class .= 'bind-body-class ';
			}

			?>
			<div class="<?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var.' option-item'.$outer_preview_class); ?>" data-format="<?php echo esc_attr($post_format); ?>">
			<?php
			if($module_label != '') 
			{
				?>
				<strong><?php echo esc_html($module_label); ?>:</strong>
				<?php
			}

			$dropdown_class = $use_preview ? 'pt-select ' : '';
			?>
			<div class="option-content-wrap">
				<select class="<?php echo esc_attr($dropdown_class.$preview_class.$module_name.'_custom_'.$box_type.'_input '.$module_name.'_custom_'.$box_type.'_box'); ?>" id="<?php echo esc_attr($module_name.'_'.$var); ?>" name="<?php echo esc_attr($name); ?>" data-available="<?php echo esc_attr($available_value); ?>" data-prefix="<?php echo esc_attr($module_name); ?>" data-bodyclass-prefix="<?php echo esc_attr($bind_body_class_prefix); ?>">
				<?php
				foreach ($choices as $choice_key=>$choice_value)
				{
					$preview_id = $remove_preview_prefix ? $var.'_'.$choice_key : $module_name.'_'.$var.'_'.$choice_key;
					
					if($choice_key == (string)$value)
					{
						?><option <?php echo ($use_preview ? 'data-previewid="'.esc_attr($preview_id).'"' : ''); ?> selected="selected" value="<?php echo esc_attr($choice_key); ?>"><?php echo esc_html($choice_value); ?></option><?php
					}
					else
					{
						?><option <?php echo ($use_preview ? 'data-previewid="'.esc_attr($preview_id).'"' : ''); ?> value="<?php echo esc_attr($choice_key); ?>"><?php echo esc_html($choice_value); ?></option><?php
					}
				}
				?>
				</select>
				<span class="pt-option-item-des"><?php thebe_kses_content($des); ?></span>

				<?php
				if( $help != '' )
				{
					?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php
				}
				?>
			</div>
			</div>
			<?php
		}
		else if($type == "image_select")
		{
			$choices = $data['options'];
			if($choices == NULL || !is_array($choices) || count($choices) < 1) return;
			$remove_preview_prefix = isset($data['remove_path_prefix']) && $data['remove_path_prefix'] == true;
			$only_available = isset($data['only_available']) ? $data['only_available'] : null;
			$available_value = $only_available != null ? json_encode($only_available) : '';

			$bind_body_class_prefix = isset($data['body_class_prefix']) ? $data['body_class_prefix'] : '';
			if($bind_body_class_prefix != '')
			{
				$extend_class .= 'bind-body-class ';
			}

			$img_prefix = isset($data['img_prefix']) ? $data['img_prefix'] : '';

			if( $des != '' )
			{
				$extend_class .= 'has-option-des ';
			}

			$image_type = isset($data['image_type']) ? $data['image_type'] : 'png';
			?>
			<div class="<?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var.' option-item pt-image-select'); ?>" data-format="<?php echo esc_attr($post_format); ?>" data-img-type="<?php echo esc_attr( $image_type ); ?>">
			<?php
			if($module_label != '') 
			{
				?>
				<strong><?php echo esc_html($module_label); ?>:</strong>
				<?php
			}
			?>
			<span class="pt-option-item-des"><?php thebe_kses_content($des); ?></span>
			<?php
			if( $help != '' )
			{
				?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php
			}
			?>
			<input class="<?php echo esc_attr($module_name.'_custom_'.$box_type.'_input '.$module_name.'_custom_'.$box_type.'_box image_select_input'); ?>" type="hidden" id="<?php echo esc_attr($module_name.'_'.$var); ?>" name="<?php echo esc_attr($name); ?>" value="<?php echo esc_attr($value); ?>" data-bodyclass-prefix="<?php echo esc_attr($bind_body_class_prefix); ?>" data-available="<?php echo esc_attr($available_value); ?>" data-prefix="<?php echo esc_attr($module_name); ?>"/>
			<ul class="image-select-list">
			<?php
			foreach ($choices as $choice_value)
			{
				if( $img_prefix )
				{
					$preview_id = $img_prefix.'_'.$choice_value;
				}
				else
				{
					$preview_id = $remove_preview_prefix ? $var.'_'.$choice_value : $module_name.'_'.$var.'_'.$choice_value;
				}
				
				if($choice_value == (string)$value)
				{
					?><li class="selected" data-value="<?php echo esc_attr($choice_value); ?>" data-img-name="<?php echo esc_attr($preview_id); ?>"></li><?php
				}
				else
				{
					?><li data-value="<?php echo esc_attr($choice_value); ?>" data-img-name="<?php echo esc_attr($preview_id); ?>"></li><?php
				}
			}
			?>
			</ul>
			</div>
			<?php
		}
		else if($type == "color")
		{
			?>
			<div class="<?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var.' option-item'); ?>" data-format="<?php echo esc_attr($post_format); ?>">
			<?php
			if($module_label != '') 
			{
				?>
				<strong><?php echo esc_html($module_label); ?>:</strong>
				<?php
			}
			?>
			<input class="color-field <?php echo esc_attr($module_name.'_custom_'.$box_type.'_input '.$module_name.'_custom_'.$box_type.'_box'); ?>" type="text" data-alpha="true" data-custom-width="50px" id="<?php echo esc_attr($module_name.'_'.$var); ?>" name="<?php echo esc_attr($name); ?>" value="<?php echo esc_attr($value); ?>"/>
			<span class="pt-option-item-des"><?php thebe_kses_content($des); ?></span>

			<?php
			if( $help != '' )
			{
				?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php
			}
			?>
			</div>
			<?php
		}
		else if($type == "image")
		{
			?>
			<div class="<?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var.' option-item'); ?>" data-format="<?php echo esc_attr($post_format); ?>">
			<?php
			if($module_label != '') 
			{
				?>
				<strong><?php echo esc_html($module_label); ?>:</strong>
				<?php
			}
			?>
			<div class="option-content-wrap">
				<input class="<?php echo esc_attr($module_name.'_custom_'.$box_type.'_input '.$module_name.'_custom_'.$box_type.'_box image_box_uploader_input'); ?>" type="hidden" id="<?php echo esc_attr($module_name.'_'.$var); ?>" name="<?php echo esc_attr($name); ?>" value="<?php echo esc_attr($value); ?>" />
				<div class="option-item-upload-button button-primary single-image-upload"><?php esc_html_e('Upload', 'thebe-options-manager'); ?></div>
				<span class="uploader-image-des"><?php thebe_kses_content($des); ?></span>

				<?php
				if( $help != '' )
				{
					?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php
				}

				$img_id = $value == $default ? '' : $value;

				$empty = "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";
				$img_url = $empty;

				if($img_id != '')
				{
					$img_obj = wp_get_attachment_image_src( $img_id, 'thumbnail' );
					if($img_obj)
					{
						$img_url = $img_obj[0];
					}
				}

				$img_extend_class = '';
				if( $img_url == $empty )
				{
					$img_extend_class = 'empty';
				}
				?>

				<div class="uploader-image-container single-image-input single-image-upload-delete <?php echo esc_attr( $img_extend_class ); ?>" style="display:<?php echo ($img_url == '' ? "none" : "block") ?>">
					<span class="single-image-upload-delete-button upload-delete-button"></span>
					<img class="uploader-image-preview" alt="<?php esc_attr_e('Image preview', 'thebe-options-manager'); ?>" src="<?php echo esc_url($img_url); ?>" data-empty="<?php echo esc_attr($empty); ?>"/>
				</div>
			</div>
			</div>
			<?php
		}
		else if($type == "imagelist")
		{
			?>
			<div class="<?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var.' option-item'); ?>" data-format="<?php echo esc_attr($post_format); ?>">

			<?php
			if($module_label != '') 
			{
				?>
				<strong><?php echo esc_html($module_label); ?>:</strong>
				<?php
			}
			?>
			<div class="option-content-wrap">
				<input class="<?php echo esc_attr($module_name.'_custom_'.$box_type.'_input '.$module_name.'_custom_'.$box_type.'_box image_box_uploader_input image-list-target-input'); ?>" type="hidden" id="<?php echo esc_attr($module_name.'_'.$var); ?>" name="<?php echo esc_attr($name); ?>" value="<?php echo esc_attr($value); ?>"/>
				
				<div class="option-item-upload-button button-primary imagelist-upload"><?php esc_html_e('Add image', 'thebe-options-manager'); ?></div>
				<span class="uploader-image-des"><?php thebe_kses_content($des); ?></span>

				<?php
				if( $help != '' )
				{
					?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php
				}
				?>

				<div class="image-list-container sortable">
				<?php

				$rids = $value == $default ? '' : $value;
				$rid_arr = explode(",", $rids);
				if($rid_arr != false)
				{
					$len = count($rid_arr);
					for($i = 0; $i < $len; $i ++)
					{
						$img_url = '';

						$img_id = $rid_arr[$i];
						if($img_id != '')
						{
							$img_obj = wp_get_attachment_image_src( $img_id, 'thumbnail' );
							if($img_obj)
							{
								$img_url = $img_obj[0];
							}
						}
						else
						{
							$img_id = '';
						}

						if($img_url != '')
						{
							?>
							<div class="uploader-image-container image-list imagelist-upload-delete" data-rid="<?php echo esc_attr($img_id); ?>">
								<span class="imagelist-upload-delete-button upload-delete-button"></span>
								<img class="uploader-image-preview uploader-image-list" alt="<?php esc_attr_e('Image preview', 'thebe-options-manager'); ?>" src="<?php echo esc_url($img_url); ?>"/>
							</div>
							<?php
						}
					}
				}
				?>
				</div>
			</div>
			</div>
			<?php
		}
		else if($type == "category")
		{
			?>
			<div class="<?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var.' option-item'); ?>" data-format="<?php echo esc_attr($post_format); ?>">

			<?php
			if($module_label != '') 
			{
				?>
				<strong><?php echo esc_html($module_label); ?>:</strong>
				<?php
			}
			?>

			<div class="option-content-wrap">

				<input class="<?php echo esc_attr($module_name.'_custom_'.$box_type.'_input '.$module_name.'_custom_'.$box_type.'_box option-input-field'); ?>" type="hidden" id="<?php echo esc_attr($module_name.'_'.$var); ?>" name="<?php echo esc_attr($name); ?>" value="<?php echo esc_attr($value); ?>"/>

				<?php

				$data_from = isset($data['data']) ? $data['data'] : "";
				$disable_all = isset($data['disable_all']) ? $data['disable_all'] : false;
				?>
				<div class="checkbox-group">
				<?php
					if(!$disable_all)
					{
						?><div class="mc-option-all mc-option-item"><input type="checkbox" data-value="" id="<?php echo esc_attr($module_name.'_check_for_all'); ?>" /><label for="<?php echo esc_attr($module_name.'_check_for_all'); ?>"><?php esc_html_e('All', 'thebe-options-manager'); ?></label></div><?php
					}

					$post_cats = array();
					$project_cats = array();

					if($data_from == 'post')
					{
						$post_cats = get_categories( array( 'taxonomy' => 'category', 'hide_empty' => false) );
					}
					else if($data_from == 'project')
					{
						$project_cats = get_categories( array( 'taxonomy' => 'project_cat', 'hide_empty' => false) );
					}
					else
					{
						$post_cats = get_categories( array( 'taxonomy' => 'category', 'hide_empty' => false) );
						$project_cats = get_categories( array( 'taxonomy' => 'project_cat', 'hide_empty' => false) );
					}

					$cat_index = 1;

					foreach($post_cats as $cat)
					{
						$cat_for_id = $module_name.'_check_for_'.$cat_index;
						$cat_index ++;

						?>
						<div class="mc-option-item mc-post">
							<input type="checkbox" data-value="<?php echo esc_attr($cat->name); ?>" id="<?php echo esc_attr($cat_for_id); ?>" />
							<label for="<?php echo esc_attr($cat_for_id); ?>"><?php echo esc_html($cat->name); ?></label>
						</div>
						<?php
					}

					foreach($project_cats as $cat)
					{
						$cat_for_id = $module_name.'_check_for_'.$cat_index;
						$cat_index ++;

						?>
						<div class="mc-option-item mc-project">
							<input type="checkbox" data-value="<?php echo esc_attr($cat->name); ?>" id="<?php echo esc_attr($cat_for_id); ?>" />
							<label for="<?php echo esc_attr($cat_for_id); ?>"><?php echo esc_html($cat->name); ?></label>
						</div>
						<?php
					}
				?>
				</div>
				<span class="pt-option-item-des"><?php thebe_kses_content($des); ?></span>

				<?php
				if( $help != '' )
				{
					?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php
				}
				?>
				</div>
			</div>
			<?php
		}
		else if($type == "image-group")
		{
			?>
			<div class="<?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var.' option-item'); ?>" data-format="<?php echo esc_attr($post_format); ?>">

			<?php
			if($module_label != '') 
			{
				?>
				<strong><?php echo esc_html($module_label); ?>:</strong>
				<?php
			}
			?>
			<div class="option-content-wrap">
				<div class="image-group">
					<input class="<?php echo esc_attr($module_name.'_custom_'.$box_type.'_input '.$module_name.'_custom_'.$box_type.'_box option-input-field image-group-data'); ?>" type="hidden" id="<?php echo esc_attr($module_name.'_'.$var); ?>" name="<?php echo esc_attr($name); ?>" value="<?php echo esc_attr($value); ?>"/>
					<div class="image-group-open-btn button button-primary button-large"><?php esc_html_e('Image Manager', 'thebe-options-manager'); ?></div>
					<div class="image-group-item-edit">
						<div class="image-group-item-edit-image-prev"></div>
						<div class="image-group-item-edit-image-next"></div>
						<div class="image-group-item-edit-image-close"></div>
						<div class="image-group-item-edit-content">
							<div class="image-group-item-edit-image image-group-item detail-item"></div>
							<div class="image-group-item-edit-image-btn"></div>
							<div class="image-group-item-edit-extend">
							<?php
								$group_extend = isset($data['extend']) ? $data['extend'] : null;
								if( $group_extend )
								{
									foreach( $group_extend as $key_name => $group_item )
									{
										$group_item_label = $group_item['label'];
										$group_item_default = isset($group_item['default']) ? $group_item['default'] : '';
										$group_item_help = isset($group_item['help']) ? $group_item['help'] : '';

										switch( $group_item['type'] )
										{
											case 'text':
												?>
												<span class="ig-extend-item" data-type="text" data-key="<?php echo esc_attr( $key_name ); ?>">
													<span><?php echo esc_html( $group_item_label ); ?></span>
													<input class="ig-extend-target" type="text" value="<?php echo esc_attr( $group_item_default ); ?>" data-default="<?php echo esc_attr( $group_item_default ); ?>"/>
													<?php
														if( $group_item_help != '' )
														{
															?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($group_item_help); ?>"></span><?php
														}
													?>
												</span>
												<?php
												break;
											case 'textarea':
												$group_item_cols = isset($group_item['cols']) ? $group_item['cols'] : 10;
												$group_item_rows = isset($group_item['rows']) ? $group_item['rows'] : 5;

												?>
												<span class="ig-extend-item" data-type="textarea" data-key="<?php echo esc_attr( $key_name ); ?>">
													<span><?php echo esc_html( $group_item_label ); ?></span>
													<textarea class="ig-extend-target" data-default="<?php echo esc_attr( $group_item_default ); ?>" cols="<?php echo esc_attr( $group_item_cols ); ?>" rows="<?php echo esc_attr( $group_item_rows ); ?>"><?php echo esc_html( $group_item_default ); ?></textarea>
													<?php
														if( $group_item_help != '' )
														{
															?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($group_item_help); ?>"></span><?php
														}
													?>
												</span>
												<?php
												break;
											case 'color':
												?>
												<span class="ig-extend-item" data-type="color" data-key="<?php echo esc_attr( $key_name ); ?>">
													<span><?php echo esc_html( $group_item_label ); ?></span>
													<input class="ig-extend-target color-field" data-custom-width="50px" type="text"  value="<?php echo esc_attr( $group_item_default ); ?>" data-default="<?php echo esc_attr( $group_item_default ); ?>"/>
													<?php
														if( $group_item_help != '' )
														{
															?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($group_item_help); ?>"></span><?php
														}
													?>
												</span>
												<?php
												break;
											case 'checkbox':
												?>
												<span class="ig-extend-item" data-type="checkbox" data-key="<?php echo esc_attr( $key_name ); ?>">
													<span><?php echo esc_html( $group_item_label ); ?></span>
													<input class="ig-extend-target pt-checkbox" type="checkbox" <?php echo ($group_item_default == '1' ? 'checked' : ''); ?> data-default="<?php echo esc_attr( $group_item_default ); ?>"/>
													<?php
														if( $group_item_help != '' )
														{
															?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($group_item_help); ?>"></span><?php
														}
													?>
												</span>
												<?php
												break;
											case 'select':
													$use_preview = isset($group_item['use_preview']) && $group_item['use_preview'] == true;
													$preview_class = $use_preview ? 'select-previewable ' : '';
													$outer_preview_class = $use_preview ? ' has-pt-select' : '';
													$dropdown_class = $use_preview ? 'pt-select ' : '';
												?>
												<span class="ig-extend-item<?php echo esc_attr( $outer_preview_class ); ?>" data-type="select" data-key="<?php echo esc_attr( $key_name ); ?>">
													<span><?php echo esc_html( $group_item_label ); ?></span>
													<select class="ig-extend-target <?php echo esc_attr( $preview_class.$dropdown_class ); ?>" value="<?php echo esc_attr( $group_item_default ); ?>" data-default="<?php echo esc_attr( $group_item_default ); ?>">
													<?php
														$group_item_options = isset($group_item['options']) ? $group_item['options'] : '';
														if( $group_item_options )
														{
															foreach ($group_item_options as $choice_key=>$choice_value)
															{
																$preview_id = $var.'_'.$choice_key;

																if($choice_key == (string)$group_item_default)
																{
																	?><option <?php echo ($use_preview ? ' data-previewid="pt_ig_item_'.esc_attr($preview_id).'"' : ''); ?> selected="selected" value="<?php echo esc_attr($choice_key); ?>"><?php echo esc_html($choice_value); ?></option><?php
																}
																else
																{
																	?><option <?php echo ($use_preview ? ' data-previewid="pt_ig_item_'.esc_attr($preview_id).'"' : ''); ?> value="<?php echo esc_attr($choice_key); ?>"><?php echo esc_html($choice_value); ?></option><?php
																}
															}
														}
													?>
													</select>
													<?php
														if( $group_item_help != '' )
														{
															?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($group_item_help); ?>"></span><?php
														}
													?>
												</span>
												<?php
												break;
										}
									}
								}
							?>
							</div>
						</div>
					</div>
					<div class="image-group-content">
						<div class="image-group-header">
							<div class="image-group-mode-btns">
								<div class="image-group-images-btn image-group-button"><?php esc_html_e('Images', 'thebe-options-manager'); ?></div>
								<?php
									$hide_cat = isset($data['hide_cat']) ? $data['hide_cat'] : false;
									if( !$hide_cat )
									{
										?>
										<div class="image-group-categories-btn image-group-button"><?php esc_html_e('Categories', 'thebe-options-manager'); ?></div>
										<?php
									}
								?>
								<div class="image-group-cancel-btn image-group-button"><?php esc_html_e('Cancel', 'thebe-options-manager'); ?></div>
								<div class="image-group-close-btn image-group-button"><?php esc_html_e('Confirm', 'thebe-options-manager'); ?></div>
							</div>
							<div class="image-group-detail-btns">
								<span class="image-group-category-info">
									<i class="fa fa-info-circle" aria-hidden="true"></i>
									<span><?php esc_html_e('Manager the categories via Drag &amp; Drop.', 'thebe-options-manager'); ?></span>
								</span>
								<div class="image-group-add-category image-group-button"><?php esc_html_e('Add Category', 'thebe-options-manager'); ?></div>
								<div class="image-group-add-images image-group-button"><?php esc_html_e('Add Images', 'thebe-options-manager'); ?></div>
								<img src="<?php echo esc_url( THEBE_THEME_URL . '/data/images/gallery-tips.png' ); ?>" class="image-group-images-tips" alt="<?php esc_attr_e('Tips', 'thebe-options-manager'); ?>"/>
							</div>
						</div>
						<div class="image-group-images"></div>
						<div class="image-group-categories">
							<div class="image-group-category image-group-category-all">
								<div class="image-group-category-header">
									<input class="image-group-input image-group-uncategorized" value=""/>
								</div>
								<div class="image-group-items"></div>
							</div>
							<div class="image-group-details"></div>
						</div>
					</div>
				</div>
				<span class="pt-option-item-des"><?php thebe_kses_content($des); ?></span>

				<?php
				if( $help != '' )
				{
					?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php
				}
				?>
			</div>
			</div>
			<?php
		}
		else if($type == "group")
		{
			$group_class = isset($data['group_class']) ? $data['group_class'] : '';
			if( $des != '' )
			{
				$extend_class .= 'has-option-des ';
			}

			?>
			<div class="<?php echo esc_attr($extend_class.$module_name.'_custom_'.$box_type.'_box '.$module_name.'_'.$var.' option-item pt-group-root'); ?>" data-format="<?php echo esc_attr($post_format); ?>">

				<div class="pt-group">
				
					<input type="hidden" name="<?php echo esc_attr($name); ?>" class="<?php echo esc_attr($module_name.'_custom_'.$box_type.'_input'); ?> pt-group-value" value="<?php echo esc_attr($value) ; ?>"/>
					
					<strong><?php echo esc_html($module_label); ?></strong>
					<span class="uploader-image-des"><?php thebe_kses_content($des); ?></span>

					<?php
					if( $help != '' )
					{
						?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php
					}
					?>

					<div class="pt-group-container">
						<div class="pt-group-item <?php echo esc_attr($group_class); ?> hidden" style="display:none">
							<div class="intro"></div>
							<span class="pt-group-item-label"><?php esc_html_e('Item', 'thebe-options-manager'); ?></span>
							<button class="pt-group-item-delete" type="button"><?php esc_html_e('Delete', 'thebe-options-manager'); ?></button>
							
							<?php
								$items = isset($data['items']) ? $data['items'] : null;
								if($items != null)
								{
									foreach($items as $item)
									{
										$item_type = $item['type'];
										$item_var = $item['var'];
										$item_default = isset($item['default']) ? $item['default'] : '';
										$item_extend_class = isset($item['class']) ? ' '.$item['class'] : '';
										$item_class = 'pt-option-item-'.$item_var.$item_extend_class;
										$item_label = isset($item['label']) ? $item['label'] : $item_var;
										$item_des = isset($item['des']) ? $item['des'] : '';
										$item_help = isset($item['help']) ? $item['help'] : '';
										$item_params = isset($item['params']) ? $item['params'] : '';
										
										switch($item_type)
										{
											case 'text':
												?>
												<div class="pt-group-option-item <?php echo esc_attr($item_class); ?>">
													<?php
														if($item_label != '') 
														{
															?><strong><?php echo esc_html($item_label); ?>:</strong><?php
														}
													?>
													<input class="pt-group-option-data" data-name="<?php echo esc_attr($item_var); ?>" type="text" value="<?php echo esc_attr($item_default); ?>"/>
													<span class="pt-option-item-des"><?php thebe_kses_content($item_des); ?></span>
													<?php
														if( $item_help != '' )
														{
															?><span class="pt-admin-help-<?php echo esc_attr($item_type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($item_help); ?>"></span><?php
														}
													?>
												</div>
												<?php
												break;
											case 'color':
												?>
												<div class="pt-group-option-item <?php echo esc_attr($item_class); ?>">
													<?php
														if($item_label != '') 
														{
															?><strong><?php echo esc_html($item_label); ?>:</strong><?php
														}
													?>
													<input class="pt-group-option-data group-color-field" data-alpha="true" data-custom-width="50px" data-name="<?php echo esc_attr($item_var); ?>" type="text" value="<?php echo esc_attr($item_default); ?>"/>
													<span class="pt-option-item-des"><?php thebe_kses_content($item_des); ?></span>
													<?php
														if( $item_help != '' )
														{
															?><span class="pt-admin-help-<?php echo esc_attr($item_type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($item_help); ?>"></span><?php
														}
													?>
												</div>
												<?php
												break;
											case 'icon':
												?>
												<div class="pt-group-option-item <?php echo esc_attr($item_class); ?>">
													<?php
														if($item_label != '') 
														{
															?><strong><?php echo esc_html($item_label); ?>:</strong><?php
														}
													?>
													<input class="pt-group-option-data icon-input" data-name="<?php echo esc_attr($item_var); ?>" type="text" value="<?php echo esc_attr($item_default); ?>"/>
													<div class="icons-insert-button icons-btn-pe" data-id="pe" title="<?php echo esc_attr__('Pe icon 7', 'thebe-options-manager'); ?>"></div>
													<div class="icons-insert-button icons-btn-fontawesome" data-id="fontawesome" title="<?php echo esc_attr__('FontAwesome', 'thebe-options-manager'); ?>"></div>
													<span class="pt-option-item-des"><?php thebe_kses_content($item_des); ?></span>
													<?php
														if( $item_help != '' )
														{
															?><span class="pt-admin-help-<?php echo esc_attr($item_type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($item_help); ?>"></span><?php
														}
													?>
												</div>
												<?php
												break;
											case 'textarea':
												$item_cols = isset($item['cols']) ? $item['cols'] : 10;
												$item_rows = isset($item['rows']) ? $item['rows'] : 3;
												?>
												<div class="pt-group-option-item <?php echo esc_attr($item_class); ?>">
													<?php
														if($item_label != '') 
														{
															?><strong><?php echo esc_html($item_label); ?>:</strong><?php
														}
													?>
													<textarea class="pt-group-option-data" data-name="<?php echo esc_attr($item_var); ?>" cols="<?php echo esc_attr( $item_cols ); ?>" rows="<?php echo esc_attr( $item_rows ); ?>"><?php echo esc_html($item_default); ?></textarea>
													<span class="pt-option-item-des"><?php thebe_kses_content($item_des); ?></span>
													<?php
														if( $item_help != '' )
														{
															?><span class="pt-admin-help-<?php echo esc_attr($item_type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($item_help); ?>"></span><?php
														}
													?>
												</div>
												<?php
												break;
											case 'select':

												$use_preview = isset($item['use_preview']) && $item['use_preview'] == true;
												$preview_class = $use_preview ? 'select-previewable ' : '';
												$outer_preview_class = $use_preview ? 'has-pt-select ' : '';
												$dropdown_class = $use_preview ? 'pt-select pt-group-select ' : '';
												$only_available = isset($item['only_available']) ? $item['only_available'] : null;
												$available_value = $only_available != null ? json_encode($only_available) : '';

												?>
												<div class="pt-group-option-item <?php echo esc_attr($outer_preview_class.$item_class); ?>">
													<?php
														if($item_label != '') 
														{
															?><strong><?php echo esc_html($item_label); ?>:</strong><?php
														}
													?>
													<select class="<?php echo esc_attr($dropdown_class.$preview_class); ?>pt-group-option-data" data-name="<?php echo esc_attr($item_var); ?>" data-available="<?php echo esc_attr($available_value); ?>" data-type="group">
													<?php
														$item_options = $item['options'];
														foreach ($item_options as $choice_key=>$choice_value)
														{
															$preview_id = $item_var.'_'.$choice_key;

															if($choice_key == (string)$item_default)
															{
																?>
																<option <?php echo ($use_preview ? ' data-previewid="pt_group_item_'.esc_attr($preview_id).'"' : ''); ?> value="<?php echo esc_attr($choice_key); ?>" selected="selected"><?php echo esc_html($choice_value); ?></option>
																<?php
															}
															else
															{
																?>
																<option <?php echo ($use_preview ? ' data-previewid="pt_group_item_'.esc_attr($preview_id).'"' : ''); ?> value="<?php echo esc_attr($choice_key); ?>"><?php echo esc_html($choice_value); ?></option>
																<?php
															}
															
														}
													?>
													</select>
													<span class="pt-option-item-des"><?php thebe_kses_content($item_des); ?></span>
													<?php
														if( $item_help != '' )
														{
															?><span class="pt-admin-help-<?php echo esc_attr($item_type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($item_help); ?>"></span><?php
														}
													?>
												</div>
												<?php
												break;
											case 'image_select':

												$only_available = isset($item['only_available']) ? $item['only_available'] : null;
												$available_value = $only_available != null ? json_encode($only_available) : '';
												$image_type = isset($item['image_type']) ? $item['image_type'] : 'png';

												?>
												<div class="pt-group-option-item <?php echo esc_attr($item_class); ?> pt-image-select pt-group-image-select" data-img-type="<?php echo esc_attr( $image_type ); ?>">
													<?php
														if($item_label != '') 
														{
															?><strong><?php echo esc_html($item_label); ?>:</strong><?php
														}
													?>
													<span class="pt-option-item-des"><?php thebe_kses_content($item_des); ?></span>
													<?php
														if( $item_help != '' )
														{
															?><span class="pt-admin-help-<?php echo esc_attr($item_type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($item_help); ?>"></span><?php
														}
													?>
													<input class="pt-group-option-data image_select_input" data-name="<?php echo esc_attr($item_var); ?>" type="hidden" value="<?php echo esc_attr($item_default); ?>" data-available="<?php echo esc_attr($available_value); ?>" data-type="group"/>
													<ul class="image-select-list">
													<?php
														$item_options = $item['options'];
														foreach ($item_options as $choice_value)
														{
															$preview_id = $item_var.'_'.$choice_value;
															
															if($choice_value == (string)$item_default)
															{
																?><li class="selected" data-value="<?php echo esc_attr($choice_value); ?>" data-img-name="<?php echo esc_attr($preview_id); ?>"></li><?php
															}
															else
															{
																?><li data-value="<?php echo esc_attr($choice_value); ?>" data-img-name="<?php echo esc_attr($preview_id); ?>"></li><?php
															}
														}
													?>
													</ul>
												</div>
												<?php
												break;
											case 'checkbox':
												$only_available = isset($item['only_available']) ? $item['only_available'] : null;
												$available_value = $only_available != null ? json_encode($only_available) : '';

												?>
												<div class="pt-group-option-item checkbox-item <?php echo esc_attr($item_class); ?>">
													<?php
														if($item_label != '')
														{
															?><strong><?php echo esc_html($item_label); ?>:</strong><?php
														}
													?>
													<input class="pt-group-option-data" data-name="<?php echo esc_attr($item_var); ?>" type="hidden" value="<?php echo esc_attr($item_default); ?>" data-available="<?php echo esc_attr($available_value); ?>" data-type="group"/>
													<input class="pt-group-checkbox" type="checkbox"/>
													<label class="pt-option-item-des"><?php thebe_kses_content($item_des); ?></label>
													<?php
														if( $item_help != '' )
														{
															?><span class="pt-admin-help-<?php echo esc_attr($item_type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($item_help); ?>"></span><?php
														}
													?>
												</div>
												<?php
												break;
											case 'image':
												?>
												<div class="pt-group-option-item <?php echo esc_attr($item_class); ?>">
													<?php
														if($item_label != '') 
														{
															?><strong><?php echo esc_html($item_label); ?>:</strong><?php
														}

														$empty = "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";
													?>
													<input class="pt-group-option-data" type="hidden" data-name="<?php echo esc_attr($item_var); ?>" data-bindto="image&uploader-image-preview&src" value="<?php echo esc_attr($item_default); ?>"/>
													<div class="option-item-upload-button button-primary single-image-upload"><?php esc_html_e('Upload', 'thebe-options-manager'); ?></div>
													<span class="uploader-image-des"><?php thebe_kses_content($item_des); ?></span>
													<?php 
														if( $item_help != '' )
														{
															?><span class="pt-admin-help-<?php echo esc_attr($item_type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($item_help); ?>"></span><?php
														}
													?>
													<div class="uploader-image-container single-image-input single-image-upload-delete empty" style="display:none;">
														<span class="single-image-upload-delete-button upload-delete-button"></span>
														<img class="uploader-image-preview" alt="<?php esc_attr_e('Image preview', 'thebe-options-manager'); ?>" src="<?php echo esc_attr($empty); ?>" data-empty="<?php echo esc_attr($empty); ?>"/>
													</div>
												</div>
												<?php
											break;
										}
									}
								}
							?>
						</div>
					</div>
					<div class="pt-option-add-button button-primary"><?php esc_html_e('Add Item', 'thebe-options-manager'); ?></div>
				</div>
			</div>
			<?php
		}
	}
}
?>