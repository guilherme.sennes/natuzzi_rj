<?php
/**
 * @author foreverpinetree@gmail.com
 * site: https://www.3theme.com
 **/
 
class Thebe_Option_Manager
{
	static private $SAVE_PREFIXES = array( 
		'portfolio_', 
		/*'gallery_', */
		'blog_', 
		/*'contact_', */
		'default_', 
		'landing_', 
		'shortcode_', 
		'pt_post_', 
		'pt_project_', 
		/*'pt_product_', */
		'pt_sc_',
		/* 'shop_' */
	);
	static private $BASIC_SAVE_PREFIX = 'pt_basic_';
	static private $BG_SAVE_PREFIX = 'pt_bg_';
	static private $SEO_SAVE_PREFIX = 'pt_seo_';

	static public $PAGE_OPTIONS = array( 'thebe_page_basic_setting', 'thebe_page_setting', 'thebe_page_bg_setting', 'thebe_shortcode_insert'/* , 'thebe_seo_setting' */ );
	static public $POST_OPTIONS = array( 'thebe_post_setting', 'thebe_shortcode_insert', 'thebe_page_bg_setting'/* , 'thebe_seo_setting' */ );
	static public $PROJECT_OPTIONS = array( 'thebe_project_setting', 'thebe_shortcode_insert', 'thebe_page_bg_setting'/* , 'thebe_seo_setting' */ );
	static public $PRODUCT_OPTIONS = array( 'thebe_product_setting', 'thebe_page_bg_setting'/* , 'thebe_seo_setting' */ );
	
	static public function thebe_add_custom_box()
	{
		add_meta_box(
			'thebe_post_setting',
			esc_html__( 'Post setting', 'thebe-options-manager' ),
			array('Thebe_Option_Manager', 'thebe_inner_post_setting'),
			'post',
			'normal',
			'high'
		);
		
		add_meta_box(
			'thebe_project_setting',
			esc_html__( 'Project setting', 'thebe-options-manager' ),
			array('Thebe_Option_Manager', 'thebe_inner_project_setting'),
			'project',
			'normal',
			'high'
		);

		/* add_meta_box(
			'thebe_product_setting',
			esc_html__( 'Product setting', 'thebe-options-manager' ),
			array('Thebe_Option_Manager', 'thebe_inner_product_setting'),
			'product',
			'normal',
			'core'
		); */
		
		add_meta_box(
			'thebe_page_basic_setting',
			esc_html__( 'Page title', 'thebe-options-manager' ),
			array('Thebe_Option_Manager', 'thebe_inner_page_basic_setting'),
			'page',
			'normal',
			'high'
		);

		add_meta_box(
			'thebe_page_setting',
			esc_html__( 'Page setting', 'thebe-options-manager' ),
			array('Thebe_Option_Manager', 'thebe_inner_page_setting'),
			'page',
			'normal',
			'high'
		);

		add_meta_box(
			'thebe_page_bg_setting',
			esc_html__( 'Page background setting', 'thebe-options-manager' ),
			array('Thebe_Option_Manager', 'thebe_inner_page_bg_setting'),
			array('post','project', 'page', 'product'),
			'normal',
			'high'
		);

		add_meta_box(
			'thebe_shortcode_insert',
			esc_html__( 'Shortcode Modules', 'thebe-options-manager' ),
			array('Thebe_Option_Manager', 'thebe_inner_shortcode'),
			array('post','project', 'page'),
			'normal',
			'high'
		);

		add_meta_box(
			'thebe_seo_setting',
			esc_html__( 'SEO setting', 'thebe-options-manager' ),
			array('Thebe_Option_Manager', 'thebe_inner_seo_setting'),
			array('post','project', 'page', 'product'),
			'normal',
			'high'
		);
	}
	 
	//output post data
	static public function thebe_inner_post_setting( $post )
	{
		wp_nonce_field( 'thebe_options_mgr_action_post', 'thebe_options_mgr_post' );
		$value = get_post_meta( $post->ID, '_thebe_meta_post_page', true );
		$params = explode("&", $value);
		$data = array();
		foreach($params as $param)
		{
			$arr = explode("=", $param);
			if(count($arr) > 1)
			{
				$data[urldecode($arr[0])] = stripcslashes(urldecode($arr[1]));
			}
		}
		
		$options = Thebe_Post_Options::$data;
		foreach($options as $option_key=>$option_value)
		{
			if($option_value == NULL) continue;
			foreach($option_value as $item)
			{
				$var = $item['var'];
				$item_value = isset($data[$option_key."_".$var]) ? $data[$option_key."_".$var] : NULL;
				Thebe_Generate_HTML::handle("post", $option_key, $item, $item_value, false, 'post-option-item', true);
			}
		}
	}
	
	//output project data
	static public function thebe_inner_project_setting( $post )
	{
		wp_nonce_field( 'thebe_options_mgr_action_project', 'thebe_options_mgr_project' );
		$value = get_post_meta( $post->ID, '_thebe_meta_post_page', true );
		$params = explode("&", $value);
		$data = array();
		foreach($params as $param)
		{
			$arr = explode("=", $param);
			if(count($arr) > 1)
			{
				$data[urldecode($arr[0])] = stripcslashes(urldecode($arr[1]));
			}
		}
		
		$options = Thebe_Project_Options::$data;
		foreach($options as $option_key=>$option_value)
		{
			if($option_value == NULL) continue;
			foreach($option_value as $item)
			{
				$var = $item['var'];
				$item_value = isset($data[$option_key."_".$var]) ? $data[$option_key."_".$var] : NULL;
				Thebe_Generate_HTML::handle("post", $option_key, $item, $item_value, false, 'project-option-item', true);
			}
		}
	}


	//output product data
	static public function thebe_inner_product_setting( $post )
	{
		wp_nonce_field( 'thebe_options_mgr_action_product', 'thebe_options_mgr_product' );
		$value = get_post_meta( $post->ID, '_thebe_meta_post_page', true );
		$params = explode("&", $value);
		$data = array();
		foreach($params as $param)
		{
			$arr = explode("=", $param);
			if(count($arr) > 1)
			{
				$data[urldecode($arr[0])] = stripcslashes(urldecode($arr[1]));
			}
		}
		
		$options = Thebe_Product_Options::$data;
		foreach($options as $option_key=>$option_value)
		{
			if($option_value == NULL) continue;
			foreach($option_value as $item)
			{
				$var = $item['var'];
				$item_value = isset($data[$option_key."_".$var]) ? $data[$option_key."_".$var] : NULL;
				Thebe_Generate_HTML::handle("post", $option_key, $item, $item_value, false, 'product-option-item', true);
			}
		}
	}
	
	//output page data
	static public function thebe_inner_page_setting( $post )
	{
		wp_nonce_field( 'thebe_options_mgr_action_page', 'thebe_options_mgr_page' );
		$value = get_post_meta( $post->ID, '_thebe_meta_post_page', true );
		$params = explode("&", $value);
		$data = array();
		foreach($params as $param)
		{
			$arr = explode("=", $param);
			if(count($arr) > 1)
			{
				$data[urldecode($arr[0])] = stripcslashes(urldecode($arr[1]));
			}
		}
		
		$options = Thebe_Page_Options::$data;
		foreach($options as $option_key=>$option_value)
		{
			if($option_value == NULL) continue;
			foreach($option_value as $item)
			{
				$var = $item['var'];
				$item_value = isset($data[$option_key."_".$var]) ? $data[$option_key."_".$var] : NULL;
				Thebe_Generate_HTML::handle("page", $option_key, $item, $item_value, false, '', false);
			}
		}
	}

	static public function thebe_inner_page_basic_setting( $post )
	{
		wp_nonce_field( 'thebe_options_mgr_action_basic', 'thebe_options_mgr_basic' );
		$value = get_post_meta( $post->ID, '_thebe_meta_page_basic', true );
		$params = explode("&", $value);
		$data = array();
		foreach($params as $param)
		{
			$arr = explode("=", $param);
			if(count($arr) > 1)
			{
				$data[urldecode($arr[0])] = stripcslashes(urldecode($arr[1]));
			}
		}

		$options = Thebe_Page_Options::$basic_data;
		foreach($options as $option_key=>$option_value)
		{
			if($option_value == NULL) continue;
			foreach($option_value as $item)
			{
				$var = $item['var'];
				$item_value = isset($data[$option_key."_".$var]) ? $data[$option_key."_".$var] : NULL;
				Thebe_Generate_HTML::handle("page", $option_key, $item, $item_value, true, '', false);
			}
		}
	}

	static public function thebe_inner_page_bg_setting( $post )
	{
		wp_nonce_field( 'thebe_options_mgr_action_bg', 'thebe_options_mgr_bg' );
		$value = get_post_meta( $post->ID, '_thebe_meta_page_bg', true );
		$params = explode("&", $value);
		$data = array();
		foreach($params as $param)
		{
			$arr = explode("=", $param);
			if(count($arr) > 1)
			{
				$data[urldecode($arr[0])] = stripcslashes(urldecode($arr[1]));
			}
		}

		$options = Thebe_Global_Options::$bg_data;
		foreach($options as $option_key=>$option_value)
		{
			if($option_value == NULL) continue;
			foreach($option_value as $item)
			{
				$var = $item['var'];
				$item_value = isset($data[$option_key."_".$var]) ? $data[$option_key."_".$var] : NULL;
				Thebe_Generate_HTML::handle(get_post_type(), $option_key, $item, $item_value, true, '', false);
			}
		}
	}

	static public function thebe_inner_seo_setting( $post )
	{
		wp_nonce_field( 'thebe_options_mgr_action_seo', 'thebe_options_mgr_seo' );
		$value = get_post_meta( $post->ID, '_thebe_meta_seo', true );
		$params = explode("&", $value);
		$data = array();
		foreach($params as $param)
		{
			$arr = explode("=", $param);
			if(count($arr) > 1)
			{
				$data[urldecode($arr[0])] = stripcslashes(urldecode($arr[1]));
			}
		}

		$options = Thebe_Global_Options::$seo_data;
		foreach($options as $option_key=>$option_value)
		{
			if($option_value == NULL) continue;
			foreach($option_value as $item)
			{
				$var = $item['var'];
				$item_value = isset($data[$option_key."_".$var]) ? $data[$option_key."_".$var] : NULL;
				Thebe_Generate_HTML::handle(get_post_type(), $option_key, $item, $item_value, true, '', false);
			}
		}
	}
	
	static public function thebe_inner_shortcode( $post )
	{
		wp_nonce_field( 'thebe_options_mgr_action_shortcode', 'thebe_options_mgr_shortcode' );
		$value = get_post_meta( $post->ID, '_thebe_meta_post_page', true );
		$params = explode("&", $value);
		$data = array();
		foreach($params as $param)
		{
			$arr = explode("=", $param);
			if(count($arr) > 1)
			{
				$data[urldecode($arr[0])] = stripcslashes(urldecode($arr[1]));
			}
		}

		$options = Thebe_Shortcode_Options::$data;
		foreach($options as $option_key=>$option_value)
		{
			if($option_value == NULL) continue;
			foreach($option_value as $item)
			{
				$var = $item['var'];
				$item_value = isset($data[$option_key."_".$var]) ? $data[$option_key."_".$var] : NULL;
				Thebe_Generate_HTML::handle(get_post_type(), $option_key, $item, $item_value, false, 'shortcode-option-item', false);
			}
		}
	}
	
	//save data
	static public function thebe_save_postdata( $post_id )
	{
		 //check if user can edit the page/post
		if (isset($_POST['post_type']) && $_POST['post_type'] == "post")
		{
			if(!current_user_can('edit_post', $post_id))
				return;
		}
		else 
		{
			if(!current_user_can('edit_page', $post_id))
				return;
		}

		if( !isset($_POST['thebe_options_mgr_post']) && !isset($_POST['thebe_options_mgr_project']) && !isset($_POST['thebe_options_mgr_page'])
		 && !isset($_POST['thebe_options_mgr_shortcode']) && !isset($_POST['thebe_options_mgr_basic']) && !isset($_POST['thebe_options_mgr_bg']) 
		 && !isset($_POST['thebe_options_mgr_seo']) && !isset($_POST['thebe_options_mgr_product']) )
		{
			return;
		}

		//check if the user want to change this value
		if(isset($_POST['thebe_options_mgr_post']) && ! wp_verify_nonce($_POST['thebe_options_mgr_post'], 'thebe_options_mgr_action_post'))
		  return;
		if(isset($_POST['thebe_options_mgr_project']) && ! wp_verify_nonce($_POST['thebe_options_mgr_project'], 'thebe_options_mgr_action_project'))
		  return;
		if(isset($_POST['thebe_options_mgr_product']) && ! wp_verify_nonce($_POST['thebe_options_mgr_product'], 'thebe_options_mgr_action_product'))
		  return;
		if(isset($_POST['thebe_options_mgr_page']) && ! wp_verify_nonce($_POST['thebe_options_mgr_page'], 'thebe_options_mgr_action_page'))
		  return;
		if(isset($_POST['thebe_options_mgr_basic']) && ! wp_verify_nonce($_POST['thebe_options_mgr_basic'], 'thebe_options_mgr_action_basic'))
		  return;
		if(isset($_POST['thebe_options_mgr_bg']) && ! wp_verify_nonce($_POST['thebe_options_mgr_bg'], 'thebe_options_mgr_action_bg'))
		  return;
		if(isset($_POST['thebe_options_mgr_seo']) && ! wp_verify_nonce($_POST['thebe_options_mgr_seo'], 'thebe_options_mgr_action_seo'))
		  return;
		if(isset($_POST['thebe_options_mgr_shortcode']) && ! wp_verify_nonce($_POST['thebe_options_mgr_shortcode'], 'thebe_options_mgr_action_shortcode'))
		  return;

		$save_value = "";
		$save_basic = "";
		$save_bg = "";
        $save_seo = "";
        
        $save_prefixes = apply_filters( 'thebe_option_save_prefixes', self::$SAVE_PREFIXES );
		
		foreach($_POST as $key=>$value)
		{
			if(self::checkIsInterestedToSaveValue($key, $save_prefixes))
			{
				$save_value = $save_value.urlencode($key)."=".urlencode(esc_attr($value)).'&';
			}
			elseif(self::checkIsInterestedToSaveBasic($key))
			{
				$save_basic = $save_basic.urlencode($key)."=".urlencode(esc_attr($value)).'&';
			}
			elseif(self::checkIsInterestedToSaveBg($key))
			{
				$save_bg = $save_bg.urlencode($key)."=".urlencode(esc_attr($value)).'&';
			}
			elseif(self::checkIsInterestedToSaveSEO($key))
			{
				$save_seo = $save_seo.urlencode($key)."=".urlencode(esc_attr($value)).'&';
			}
		}
		
		add_post_meta($post_id, '_thebe_meta_post_page', $save_value, true) or update_post_meta($post_id, '_thebe_meta_post_page', $save_value); 
		add_post_meta($post_id, '_thebe_meta_page_basic', $save_basic, true) or update_post_meta($post_id, '_thebe_meta_page_basic', $save_basic); 
		add_post_meta($post_id, '_thebe_meta_page_bg', $save_bg, true) or update_post_meta($post_id, '_thebe_meta_page_bg', $save_bg); 
		add_post_meta($post_id, '_thebe_meta_seo', $save_seo, true) or update_post_meta($post_id, '_thebe_meta_seo', $save_seo); 
	}
	
	static private function checkIsInterestedToSaveValue($type, $prefixes)
	{
		foreach($prefixes as $value)
		{
			$index = strpos($type, $value);
			if($index !== false  && $index == 0)
			{
				return true;
			}
		}
		
		return false;
	}

	static private function checkIsInterestedToSaveBasic($type)
	{
		$index = strpos($type, self::$BASIC_SAVE_PREFIX);
		if($index !== false && $index == 0)
		{
			return true;
		}
		
		return false;
	}

	static private function checkIsInterestedToSaveBg($type)
	{
		$index = strpos($type, self::$BG_SAVE_PREFIX);
		if($index !== false && $index == 0)
		{
			return true;
		}
		
		return false;
	}

	static private function checkIsInterestedToSaveSEO($type)
	{
		$index = strpos($type, self::$SEO_SAVE_PREFIX);
		if($index !== false && $index == 0)
		{
			return true;
		}
		
		return false;
	}
}

add_action( 'admin_head', array('Thebe_Option_Manager', 'thebe_add_custom_box'));
add_action( 'save_post', array('Thebe_Option_Manager', 'thebe_save_postdata'), 10, 3);

/**
 * The page/post/project/shortcode options should not be hidden.
 **/
add_filter( 'hidden_meta_boxes', 'thebe_handle_meta_boxes', 10, 2 );
function thebe_handle_meta_boxes( $hidden, $screen ) {

	$page_options = Thebe_Option_Manager::$PAGE_OPTIONS;
	$post_options = Thebe_Option_Manager::$POST_OPTIONS;
	$project_options = Thebe_Option_Manager::$PROJECT_OPTIONS;
	$product_options = Thebe_Option_Manager::$PRODUCT_OPTIONS;

    $post_type= $screen->id;
    switch ($post_type)
    {
       	case 'page':
       		foreach( $page_options as $option_name )
       		{
       			$index = array_search( $option_name, $hidden );
       			if( $index !== false )
       			{
       				array_splice( $hidden, $index, 1 );
       			}
       		}
       		break;
       	case 'post':
       		foreach( $post_options as $option_name )
       		{
       			$index = array_search( $option_name, $hidden );
       			if( $index !== false )
       			{
       				array_splice( $hidden, $index, 1 );
       			}
       		}
       		break;
       	case 'project':
       		foreach( $project_options as $option_name )
       		{
       			$index = array_search( $option_name, $hidden );
       			if( $index !== false )
       			{
       				array_splice( $hidden, $index, 1 );
       			}
       		}
       		break;
       	case 'product':
       		foreach( $product_options as $option_name )
       		{
       			$index = array_search( $option_name, $hidden );
       			if( $index !== false )
       			{
       				array_splice( $hidden, $index, 1 );
       			}
       		}
       		break;
    }

    return $hidden;
}


add_filter( 'get_user_option_managenav-menuscolumnshidden', 'thebe_nav_menus_columns_hidden' );
function thebe_nav_menus_columns_hidden( $hidden )
{
	if( !is_array($hidden) )
	{
		return $hidden;
	}

	$menu_options = array('description', 'image');
	foreach( $menu_options as $option_name ) 
	{
		$index = array_search( $option_name, $hidden );
		if( $index !== false )
		{
			array_splice( $hidden, $index, 1 );
		}
	}
	
    return $hidden;
}

?>