<?php
/*
Plugin Name: Thebe Options Manager
Plugin URI: https://3theme.com
Description: This plugin manages options for Pages, Posts and Projects.
Version: 1.0.3
Author: foreverpinetree@gmail.com
Author URI: http://foreverpinetree.com/
License: GPLv2 or later
Text Domain: thebe-options-manager
*/

if ( !defined( 'ABSPATH' ) )
{
    exit;
}

add_action('admin_init', 'thebe_options_namager_init');
function thebe_options_namager_init()
{
    if ( !defined( 'THEBE_THEME_NAME' ) || THEBE_THEME_NAME !== 'thebe' )
    {
        return;
    }
    load_plugin_textdomain( 'thebe-options-manager', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

    require_once plugin_dir_path( __FILE__ ) . '/inc/option-generate-html.php';
	require_once plugin_dir_path( __FILE__ ) . '/inc/option-manager.php';
}

?>