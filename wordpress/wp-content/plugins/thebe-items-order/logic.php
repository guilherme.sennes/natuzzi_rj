<?php

if ( !defined( 'ABSPATH' ) )
{
    exit;
}

class Thebe_Items_Order
{
    private $type = '';
    private $PLUGIN_DIR_URL = '';
    private $TYPE_INTERESTS = array( 'project', 'post' );

	public function __construct()
	{
        $this->PLUGIN_DIR_URL = plugin_dir_url(__FILE__);

		add_filter( 'pre_get_posts', array( $this, 'pre_get_posts' ) );

        add_action( 'admin_init', array($this, 'set_current_screen' ));
        add_action( 'current_screen', array($this, 'current_screen') );

        add_action( 'wp_ajax_Thebe_Items_Order/update', array($this, 'update') );
	}

	private function get_url()
    {
        return add_query_arg('post_type', $this->type, admin_url('edit.php'));
    }

    public function current_screen($screen)
    {
        $this->type = $GLOBALS['typenow'];

        if( !in_array( $this->type, $this->TYPE_INTERESTS ) || get_current_screen()->base !== 'edit' )
        {
            return;
        }

        add_action( 'admin_print_styles-edit.php', array($this, 'admin_print_styles') );
        add_action( 'admin_print_scripts-edit.php', array($this, 'admin_print_scripts') );

        add_filter( "manage_{$screen->post_type}_posts_columns", array( $this, 'get_columns' ) );
        add_action( "manage_{$screen->post_type}_posts_custom_column", array( $this, 'render_column' ), 10, 2 );
    }

    public function set_current_screen()
    {
        if (defined('DOING_AJAX') && isset($_POST['screen_id'])) {
            convert_to_screen($_POST['screen_id'])->set_current_screen();
        }
    }

	public function get_columns($columns)
    {
        $title = sprintf(
            '<a href="%1$s">'.
            '<span class="dashicons dashicons-sort"></span>'.
            '</a>'.
            '<span class="title">%2$s</span>',
            esc_url($this->get_url()),
            esc_html__('Sorting', 'thebe-items-order')
        );

        return array('thebe-items-order' => $title) + $columns;
    }

	public function render_column( $column_name, $post_id )
	{
		if ( $column_name === 'thebe-items-order' )
		{
			$post = get_post( $post_id );
			$post_order = $post->menu_order;

			printf(
		        '<i class="items-order" data-id="%1$s" data-order="%2$s"></i>',
		        absint( $post_id ),
		        absint( $post_order )
	        );
		}
	}

	private function should_order()
	{
    	return apply_filters("Thebe_Items_Order/should_order",
		    !is_admin() || ( is_admin() && !isset($_GET['orderby']) )
	    );
    }

	public function pre_get_posts( &$q )
	{
		add_filter( 'posts_orderby', array( $this, 'posts_orderby' ) );
	}

	public function posts_orderby( $orderby )
	{
		global $wpdb;
		if ( $this->should_order() && strpos( $orderby, 'menu_order' ) === false )
		{
			$orderby = "$wpdb->posts.menu_order ASC,$orderby";
		}

		return $orderby;
	}

	public function update()
    {
        check_ajax_referer("Thebe_Items_Order/update");

        $error = new WP_Error();

        $ids = isset($_POST['ids']) ? $_POST['ids'] : array();
        $order = isset($_POST['order']) ? intval($_POST['order']) : 0;
        $objectnow = isset($_POST['objectnow']) ? $_POST['objectnow'] : '';

        if (!$order)
        {
            $error->add(
                'invalid_order',
                esc_html__('Invalid order index.', 'thebe-items-order')
            );
        }

        $msgs = $error->get_error_messages();

        if( empty($msgs) )
        {
            $redirect = $this->uploadLogic($ids, $order, $objectnow) ? '' : $this->get_url();

            exit(json_encode(array(
                'success' => 1,
                'redirect' => $redirect
            )));
        }
        else
        {
            exit(json_encode(array(
                'success' => 0,
                'message' => $msgs,
            )));
        }
    }

    /**
     *  @return true ? update : reset
     **/
    private function uploadLogic( $ids, $order, $objectnow )
	{
		global $wpdb;

		if ( empty( $ids ) )
		{
			$wpdb->update(
				$wpdb->posts, array( 'menu_order' => 0 ), array( 'post_type' => $objectnow )
			);

			return false;
		}
		else
		{
			foreach ( $ids as $id )
			{
				if ( $id > 0 )
				{
					$wpdb->update(
						$wpdb->posts, array( 'menu_order' => $order ++ ), array( 'ID' => $id )
					);
				}
			}
		}

		return true;
	}

    public function admin_print_styles()
    {
        wp_enqueue_style( 'thebe-items-order-style', $this->PLUGIN_DIR_URL.'css/style.css', array(), false, 'all');
    }

    public function admin_print_scripts()
    {
	    global $wp_query;

        wp_enqueue_script( 'thebe-items-order', $this->PLUGIN_DIR_URL.'js/main.js', array('jquery-ui-sortable'), false, true);

        $params = apply_filters('Thebe_Items_Order/ajax_params', array(
            '_ajax_nonce' => wp_create_nonce("Thebe_Items_Order/update"),
            'action' => "Thebe_Items_Order/update",
            'objectnow' => $this->type
        ));

        $custom_js = 'window.__pt_items_order = '.json_encode($params).';';
        wp_add_inline_script( 'thebe-items-order', $custom_js, 'before' );
    }

}

new Thebe_Items_Order();
