!(function($, win)
{
    var globalData = win.__pt_items_order || {};

	$(function()
	{
		var currentPage = $('.tablenav.top .pagination-links .current-page').val() || 1;
    	var perPage = $('#adv-settings .screen-per-page').val();

    	$('#the-list').sortable({
            cursor: 'move',
            axis: 'y',
            distance: 10,
            helper: 'clone',
            placeholder: 'items-order-drop-target',
            start:function( event, ui ){
                $('.items-order-drop-target').css('width', $('#the-list').css('width'));
            },
            update: function( event, ui ){
                var ids = [], $eles = $('#the-list i.items-order');
                for(var i = 0; i < $eles.length; i ++)
                {
                    ids.push($($eles[i]).data('id'));
                }

                var $item = ui.item;
                if($item)
                {
                    $item.addClass('is-loading');
                }

        		var obj = {
                    ids: ids,
                    order: perPage * (currentPage - 1) + 1,
                    action: globalData.action || '',
                    _ajax_nonce: globalData._ajax_nonce || '',
                    objectnow: globalData.objectnow || ''
                };

                $.post(window.ajaxurl, obj, function(str){
                    var data = JSON.parse(str);
                    if(data.success)
                    {
                        if(data.redirect)
                        {
                            window.location.href = data.redirect
                        }
                    }
                    else
                    {
                        alert('Failed! Please try again!');
                        window.location.reload();
                    }

                    setTimeout(function(){
                        if($item)
                        {
                            $item.removeClass('is-loading');
                        }
                    }, 100);
                })
            }
        });

        $(document).ajaxSend(function(e, xhr, o, undefined){
            if(-1 == o.data.indexOf('screen_id=') && undefined != window.pagenow){
                o.data += '&screen_id='+window.pagenow
            }
        });
	});
})(jQuery, window);