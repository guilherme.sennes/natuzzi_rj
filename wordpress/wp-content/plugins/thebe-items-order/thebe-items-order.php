<?php
/*
Plugin Name: Thebe Items Order
Plugin URI: http://3theme.com
Description: This plugin enables managing the order of Ports and Projects.
Version: 1.0.0
Author: foreverpinetree@gmail.com
Author URI: http://foreverpinetree.com/
License: GPLv2 or later
Text Domain: thebe-items-order
*/

if ( !defined( 'ABSPATH' ) )
{
	exit;
}

if ( defined( 'THEBE_ENABLE_ITEMS_ORDER' ) && !THEBE_ENABLE_ITEMS_ORDER )
{
	exit;
}

add_action( 'plugins_loaded', 'thebe_items_order_init' );
if( !function_exists( 'thebe_items_order_init' ) )
{
	function thebe_items_order_init()
	{

		$enable = get_option('thebe_enable_items_order', '1');
		if( $enable === '0' )
		{
			return;
		}
		
		load_plugin_textdomain( 'thebe-items-order', false, basename( dirname( __FILE__ ) ) . '/languages' );

		require_once 'logic.php';
	}
}
