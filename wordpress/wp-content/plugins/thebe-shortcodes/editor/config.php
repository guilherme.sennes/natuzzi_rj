<?php

/* Mix-box Config --- */
$thebe_shortcode_configs['mix_box'] = array(
	'title' => esc_html__('Mix box', 'thebe-shortcodes'),
	'id' => 'ptsc-mix_box-shortcode',
	'template' => '[mix_box {{attributes}}]{{child_shortcode}}[/mix_box]',
	'visible' => true,
	'params' => array(
		'title_img' => array(
			'std' => '',
			'type' => 'image',
			'label' => esc_html__('Title image', 'thebe-shortcodes'),
			'desc' => ''
		),
		'title' => array(
			'std' => '',
            'type' => 'text',
			'label' => esc_html__('Title', 'thebe-shortcodes'),
			'desc' => ''
		),
		'subtitle' => array(
			'std' => '',
            'type' => 'text',
			'label' => esc_html__('Subtitle', 'thebe-shortcodes'),
			'desc' => ''
        ),
        'title_center' => array(
			'std' => '0',
			'type' => 'checkbox',
			'label' => esc_html__('Title center', 'thebe-shortcodes'),
			'desc' => ''
		),
		'content_center' => array(
			'std' => '0',
			'type' => 'checkbox',
			'label' => esc_html__('Content text center', 'thebe-shortcodes'),
			'desc' => ''
		),
		'wide' => array(
			'std' => '0',
			'type' => 'checkbox',
			'label' => esc_html__('Wide', 'thebe-shortcodes'),
			'desc' => ''
		),
		'append_class' => array(
			'std' => '',
			'type' => 'text',
			'label' => esc_html__('Class', 'thebe-shortcodes'),
			'desc' => esc_html__('You can set a class to this shortcode so that you can write CSS according to this class.', 'thebe-shortcodes'),
		),
	),
	'child_shortcode' => array(
    	'params' => array(
    		'module_width' => array(
	        	'std' => '1',
				'type' => 'select',
				'label' => esc_html__('Item width', 'thebe-shortcodes'),
				'options' => array(
					'0.25' => esc_html__('25%', 'thebe-shortcodes'),
					'0.3333' => esc_html__('33.33%', 'thebe-shortcodes'),
					'0.4' => esc_html__('40%', 'thebe-shortcodes'),
					'0.5' => esc_html__('50%', 'thebe-shortcodes'),
					'0.6' => esc_html__('60%', 'thebe-shortcodes'),
					'0.6666' => esc_html__('66.66%', 'thebe-shortcodes'),
					'0.75' => esc_html__('75%', 'thebe-shortcodes'),
					'1' => esc_html__('100%', 'thebe-shortcodes'),
				),
				'desc' => '',
			),

			'item_head_type' => array(
				'std' => 'images',
				'type' => 'select',
				'label' => esc_html__('Head element', 'thebe-shortcodes'),
				'options' => array(
					'images' => esc_html__('Images', 'thebe-shortcodes'),
					'icon' => esc_html__('Icon', 'thebe-shortcodes'),
				),
				'only_available' => array(
					'images' => array('item_img_list'),
					'icon' => array('item_head_icon', 'item_head_icon_color'),
				),
				'desc' => '',
			),

    		'item_img_list' => array(
				'std' => '',
	            'type' => 'imagelist',
				'label' => esc_html__('Images', 'thebe-shortcodes'),
				'desc' => ''
			),
			
			'item_head_icon' => array(
				'std' => '',
				'type' => 'icon',
				'label' => esc_html__('Head icon', 'thebe-shortcodes'),
				'desc' => ''
			),
			'item_head_icon_color' => array(
				'std' => '',
				'type' => 'color',
				'label' => esc_html__('Head icon color', 'thebe-shortcodes'),
				'desc' => ''
			),

			'item_title' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('Title', 'thebe-shortcodes'),
				'desc' => ''
	        ),
	        'item_title_size' => array(
	        	'std' => '3',
				'type' => 'select',
				'label' => esc_html__('Title size', 'thebe-shortcodes'),
				'options' => array(
					'0' => esc_html__('H1(large)', 'thebe-shortcodes'),
					'1' => esc_html__('H1', 'thebe-shortcodes'),
					'2' => esc_html__('H2', 'thebe-shortcodes'),
					'3' => esc_html__('H3', 'thebe-shortcodes'),
					'4' => esc_html__('H4', 'thebe-shortcodes'),
					'5' => esc_html__('H5', 'thebe-shortcodes'),
					'6' => esc_html__('H6', 'thebe-shortcodes'),
				),
				'desc' => '',
			),
	        'item_title_color' => array(
				'std' => '',
	            'type' => 'color',
	            'extend_class' => 'advanced',
				'label' => esc_html__('Title color', 'thebe-shortcodes'),
				'desc' => ''
	        ),
	        'item_subtitle' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('Subtitle', 'thebe-shortcodes'),
				'desc' => ''
	        ),
			'content' => array(
				'std' => '',
	            'type' => 'textarea',
				'label' => esc_html__('Description', 'thebe-shortcodes'),
				'desc' => ''
			),

			'item_text_color' => array(
				'std' => '',
				'type' => 'select',
				'extend_class' => 'advanced',
				'label' => esc_html__('Text color', 'thebe-shortcodes'),
				'options' => array(
					'none' => esc_html__('Default', 'thebe-shortcodes'),
					'white' => esc_html__('White', 'thebe-shortcodes'),
					'black' => esc_html__('Black', 'thebe-shortcodes'),
				),
				'desc' => ''
	        ),

	        'item_bg_color' => array(
				'std' => '',
	            'type' => 'color',
	            'extend_class' => 'advanced',
				'label' => esc_html__('Background color', 'thebe-shortcodes'),
				'desc' => ''
	        ),

			'item_footer_type' => array(
	        	'std' => '1',
				'type' => 'select',
				'label' => esc_html__('Footer element', 'thebe-shortcodes'),
				'options' => array(
					'1' => esc_html__('None', 'thebe-shortcodes'),
					'2' => esc_html__('Button', 'thebe-shortcodes'),
					'3' => esc_html__('Contact form', 'thebe-shortcodes'),
					'4' => esc_html__('Google map', 'thebe-shortcodes'),
					'5' => esc_html__('Social', 'thebe-shortcodes'),
				),
				'only_available' => array(
					'2' => array('item_link_text', 'item_link', 'item_link_new_tab'),
					'3' => array('item_contact7_shortcode'),
					'4' => array('lat', 'lng', 'zoom', 'marker_title', 'marker_lat', 'marker_lng', ),
					'5' => array('item_link_new_tab', 'social1', 'social_link1', 'social2', 'social_link2', 'social3', 'social_link3'),
				),
				'desc' => '',
			),

			'item_link_text' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('Button text', 'thebe-shortcodes'),
				'desc' => ''
	        ),
	        'item_link' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('Button link', 'thebe-shortcodes'),
				'desc' => ''
	        ),
			'item_link_new_tab' => array(
				'std' => '0',
				'type' => 'checkbox',
				'label' => esc_html__('New tab (link open mode)', 'thebe-shortcodes'),
				'desc' => ''
			),

			'item_contact7_shortcode' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('Contact7 shortcode', 'thebe-shortcodes'),
				'desc' => esc_html__('Paste the contact7 shortcode to here.', 'thebe-shortcodes'),
	        ),

	        'lat' => array(
				'std' => '0',
	            'type' => 'text',
				'label' => esc_html__('Latitude', 'thebe-shortcodes'),
				'desc' => ''
	        ),
	        'lng' => array(
				'std' => '0',
	            'type' => 'text',
				'label' => esc_html__('Longitude', 'thebe-shortcodes'),
				'desc' => ''
	        ),
	        'zoom' => array(
				'std' => '8',
	            'type' => 'text',
				'label' => esc_html__('Zoom', 'thebe-shortcodes'),
				'desc' => ''
	        ),
	        'marker_title' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('Marker title', 'thebe-shortcodes'),
				'desc' => ''
	        ),
	        'marker_lat' => array(
				'std' => '0',
	            'type' => 'text',
				'label' => esc_html__('Marker latitude', 'thebe-shortcodes'),
				'desc' => ''
	        ),
	        'marker_lng' => array(
				'std' => '0',
	            'type' => 'text',
				'label' => esc_html__('Marker longitude', 'thebe-shortcodes'),
				'desc' => ''
	        ),

	        'social1' => array(
				'std' => '',
	            'type' => 'icon',
				'label' => esc_html__('Social 1', 'thebe-shortcodes'),
				'desc' => ''
	        ),
	        'social_link1' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('Social link 1', 'thebe-shortcodes'),
				'desc' => ''
	        ),
	        'social2' => array(
				'std' => '',
	            'type' => 'icon',
				'label' => esc_html__('Social 2', 'thebe-shortcodes'),
				'desc' => ''
	        ),
	        'social_link2' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('Social link 2', 'thebe-shortcodes'),
				'desc' => ''
	        ),
	        'social3' => array(
				'std' => '',
	            'type' => 'icon',
				'label' => esc_html__('Social 3', 'thebe-shortcodes'),
				'desc' => ''
	        ),
	        'social_link3' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('Social link 3', 'thebe-shortcodes'),
				'desc' => ''
	        ),

	        'advanced' => array(
	            'type' => 'advanced_button',
				'label' => esc_html__('Show advanced settings', 'thebe-shortcodes'),
				'desc' => ''
	        ),
		),
		'template' => '[mix_box_item {{attributes}}]{{content}}[/mix_box_item]',
        'clone_button' => esc_html__('Add Item', 'thebe-shortcodes')
	)
);

/* Slider Config --- */
$thebe_shortcode_configs['slider'] = array(
	'title' => esc_html__('Slider', 'thebe-shortcodes'),
	'id' => 'ptsc-slider-shortcode',
	'template' => '[slider {{attributes}}]{{child_shortcode}}[/slider]',
	'visible' => true,
	'params' => array(
		'autoplay' => array(
			'std' => '1',
			'type' => 'checkbox',
			'label' => esc_html__('Autoplay', 'thebe-shortcodes'),
			'desc' => ''
		),
		'autoplay_video' => array(
			'std' => '1',
			'type' => 'checkbox',
			'label' => esc_html__('Autoplay video', 'thebe-shortcodes'),
			'desc' => '',
		),
		'duration' => array(
			'std' => '5000',
            'type' => 'text',
			'label' => esc_html__('Duration', 'thebe-shortcodes'),
			'desc' => ''
        ),
        'transition_mode' => array(
        	'std' => '1',
			'type' => 'select',
			'label' => esc_html__('Transition mode', 'thebe-shortcodes'),
			'options' => array(
				'1' => esc_html__('Movement', 'thebe-shortcodes'),
				'2' => esc_html__('Fade', 'thebe-shortcodes'),
			),
			'desc' => '',
		),
        'display_type' => array(
        	'std' => 'boxed',
			'type' => 'select',
			'label' => esc_html__('Display type', 'thebe-shortcodes'),
			'options' => array(
				'fullscreen' => esc_html__('Wide', 'thebe-shortcodes'),
				'boxed' => esc_html__('Boxed', 'thebe-shortcodes'),
			),
			'desc' => '',
        ),
        'small_dots' => array(
			'std' => '0',
			'type' => 'checkbox',
			'label' => esc_html__('Use small dots (buttons)', 'thebe-shortcodes'),
			'desc' => ''
		),
		'text_position' => array(
			'std' => 't-center',
			'type' => 'select',
			'label' => esc_html__('Text position', 'thebe-shortcodes'),
			'options' => array(
				't-center' => esc_html__('Center', 'thebe-shortcodes'),
				't-bottom' => esc_html__('Bottom', 'thebe-shortcodes'),
			),
			'desc' => ''
        ),
		'header_color' => array(
			'std' => 'white',
			'type' => 'select',
			'label' => esc_html__('Header color', 'thebe-shortcodes'),
			'options' => array(
				'white' => esc_html__('White', 'thebe-shortcodes'),
				'black' => esc_html__('Black', 'thebe-shortcodes'),
			),
			'help'=>'header_color?w=600&h=400&ox=0&oy=-180&bc=#eeeeee', 
			'desc' => ''
        ),
		'height_type' => array(
        	'std' => 'standard',
			'type' => 'select',
			'label' => esc_html__('Height', 'thebe-shortcodes'),
			'options' => array(
				'standard' => esc_html__('Standard', 'thebe-shortcodes'),
				'short' => esc_html__('Short', 'thebe-shortcodes'),
			),
			'desc' => '',
        ),
        'mobile_type' => array(
			'std' => '0',
			'type' => 'select',
			'label' => esc_html__('Display for mobile', 'thebe-shortcodes'),
			'options' => array(
				'0' => esc_html__('Default', 'thebe-shortcodes'),
				'1' => esc_html__('Image list', 'thebe-shortcodes'),
				'2' => esc_html__('Fullscreen', 'thebe-shortcodes'),
			),
			'desc' => '',
		),
		'append_class' => array(
			'std' => '',
			'type' => 'text',
			'label' => esc_html__('Class', 'thebe-shortcodes'),
			'desc' => esc_html__('You can set a class to this shortcode so that you can write CSS according to this class.', 'thebe-shortcodes'),
		),
	),
	'child_shortcode' => array(
    	'params' => array(
    		'item_use_video' => array(
				'std' => '0',
				'type' => 'checkbox',
				'label' => esc_html__('Use video', 'thebe-shortcodes'),
				'only_available' => array(
					'1' => array('item_video_link', 'item_video_type', 'item_video_volume')
				),
				'desc' => ''
			),
			'item_video_type' => array(
				'type' => 'select',
				'label' => esc_html__('Video type', 'thebe-shortcodes'),
				'options' => array(
					'1' => esc_html__('*.mp4', 'thebe-shortcodes'),
					'2' => esc_html__('Youtube', 'thebe-shortcodes'),
					'3' => esc_html__('Vimeo', 'thebe-shortcodes')
				),
				'desc' => '',
			),
			'item_video_link' => array(
				'std' => '',
				'type' => 'text',
				'label' => esc_html__('Video link', 'thebe-shortcodes'),
				'desc' => ''
			),
			'item_video_volume' => array(
				'std' => '0',
				'type' => 'text',
				'label' => esc_html__('Video volume', 'thebe-shortcodes'),
				'desc' => ''
			),
			'item_img' => array(
	            'std' => '',
	            'type' => 'image',
				'label' => esc_html__('Item image', 'thebe-shortcodes'),
				'desc' => ''
	        ),
			'item_title' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('Title', 'thebe-shortcodes'),
				'desc' => ''
			),
	        'item_title_size' => array(
	        	'std' => '1',
				'type' => 'select',
				'label' => esc_html__('Title size', 'thebe-shortcodes'),
				'options' => array(
					'0' => esc_html__('H1(large)', 'thebe-shortcodes'),
					'1' => esc_html__('H1', 'thebe-shortcodes'),
					'2' => esc_html__('H2', 'thebe-shortcodes'),
					'3' => esc_html__('H3', 'thebe-shortcodes'),
					'4' => esc_html__('H4', 'thebe-shortcodes'),
					'5' => esc_html__('H5', 'thebe-shortcodes'),
					'6' => esc_html__('H6', 'thebe-shortcodes'),
				),
				'desc' => '',
			),
			'item_intro' => array(
				'std' => '',
	            'type' => 'textarea',
				'label' => esc_html__('Intro', 'thebe-shortcodes'),
				'desc' => ''
			),
			'item_link_text' => array(
				'std' => '',
				'type' => 'text',
				'label' => esc_html__('Button text', 'thebe-shortcodes'),
				'desc' => ''
			),
	        'item_link' => array(
				'std' => '#',
				'type' => 'text',
				'label' => esc_html__('Button link', 'thebe-shortcodes'),
				'desc' => ''
			),
			'item_link_new_tab' => array(
				'std' => '0',
				'type' => 'checkbox',
				'label' => esc_html__('New tab (link open mode)', 'thebe-shortcodes'),
				'desc' => ''
			),
        ),
        'template' => '[slider_item {{attributes}}]{{content}}[/slider_item]',
        'clone_button' => esc_html__('Add Item', 'thebe-shortcodes')
    )
);

/* Pricing-table Config --- */
$thebe_shortcode_configs['pricing_table'] = array(
	'title' => esc_html__('Pricing table', 'thebe-shortcodes'),
	'id' => 'ptsc-pricing_table-shortcode',
	'template' => '[pricing_table {{attributes}}]{{child_shortcode}}[/pricing_table]',
	'visible' => true,
	'params' => array(
		'title_img' => array(
			'std' => '',
			'type' => 'image',
			'label' => esc_html__('Title image', 'thebe-shortcodes'),
			'desc' => ''
		),
		'title' => array(
			'std' => '',
            'type' => 'text',
			'label' => esc_html__('Title', 'thebe-shortcodes'),
			'desc' => ''
		),
		'subtitle' => array(
			'std' => '',
            'type' => 'text',
			'label' => esc_html__('Subtitle', 'thebe-shortcodes'),
			'desc' => ''
        ),
		'wide' => array(
			'std' => '0',
			'type' => 'checkbox',
			'label' => esc_html__('Wide', 'thebe-shortcodes'),
			'desc' => ''
		),
		'append_class' => array(
			'std' => '',
			'type' => 'text',
			'label' => esc_html__('Class', 'thebe-shortcodes'),
			'desc' => esc_html__('You can set a class to this shortcode so that you can write CSS according to this class.', 'thebe-shortcodes'),
		),
	),
	'child_shortcode' => array(
    	'params' => array(
    		'module_width' => array(
	        	'std' => '0.3333',
				'type' => 'select',
				'label' => esc_html__('Item width', 'thebe-shortcodes'),
				'options' => array(
					'0.25' => esc_html__('25%', 'thebe-shortcodes'),
					'0.3333' => esc_html__('33.33%', 'thebe-shortcodes'),
					'0.4' => esc_html__('40%', 'thebe-shortcodes'),
					'0.5' => esc_html__('50%', 'thebe-shortcodes'),
					'0.6' => esc_html__('60%', 'thebe-shortcodes'),
					'0.6666' => esc_html__('66.66%', 'thebe-shortcodes'),
					'0.75' => esc_html__('75%', 'thebe-shortcodes'),
					'1' => esc_html__('100%', 'thebe-shortcodes'),
				),
				'desc' => '',
			),
    		'item_highlight_color' => array(
				'std' => '',
	            'type' => 'color',
				'label' => esc_html__('Highlight color', 'thebe-shortcodes'),
				'desc' => ''
	        ),
			'item_text_color' => array(
				'std' => '',
				'type' => 'select',
				'label' => esc_html__('Text color', 'thebe-shortcodes'),
				'options' => array(
					'none' => esc_html__('Default', 'thebe-shortcodes'),
					'white' => esc_html__('White', 'thebe-shortcodes'),
					'black' => esc_html__('Black', 'thebe-shortcodes'),
				),
				'desc' => ''
	        ),
	        'item_bg_color' => array(
				'std' => '',
	            'type' => 'color',
				'label' => esc_html__('Background color', 'thebe-shortcodes'),
				'desc' => ''
	        ),

			'hr1' => array(
				'type' => 'delimiter',
			),

			'item_price_symbol' => array(
				'std' => '$',
	            'type' => 'text',
				'label' => esc_html__('Price symbol', 'thebe-shortcodes'),
				'desc' => ''
	        ),
			'item_price' => array(
				'std' => '100',
	            'type' => 'text',
				'label' => esc_html__('Price', 'thebe-shortcodes'),
				'desc' => ''
	        ),
			'item_title' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('Title', 'thebe-shortcodes'),
				'desc' => ''
	        ),
			'content' => array(
				'std' => '',
				'type' => 'textarea',
				'label' => esc_html__('Intro', 'thebe-shortcodes'),
				'rows' => 5,
				'desc' => '',
			),

			'item_list_text_1' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('List text 1', 'thebe-shortcodes'),
				'desc' => '',
	        ),
	        'item_list_text_2' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('List text 2', 'thebe-shortcodes'),
				'desc' => '',
	        ),
	        'item_list_text_3' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('List text 3', 'thebe-shortcodes'),
				'desc' => '',
	        ),
	        'item_list_text_4' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('List text 4', 'thebe-shortcodes'),
				'desc' => '',
	        ),
	        'item_list_text_5' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('List text 5', 'thebe-shortcodes'),
				'desc' => '',
	        ),
	        'item_list_text_6' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('List text 6', 'thebe-shortcodes'),
				'desc' => esc_html__('If you want to add icon, just add HTML tag like <i class="fa fa-check"></i>.', 'thebe-shortcodes'),
	        ),

	        'hr2' => array(
				'type' => 'delimiter',
			),

			'item_footer_type' => array(
	        	'std' => '1',
				'type' => 'select',
				'label' => esc_html__('Footer element', 'thebe-shortcodes'),
				'options' => array(
					'1' => esc_html__('None', 'thebe-shortcodes'),
					'2' => esc_html__('Button', 'thebe-shortcodes'),
				),
				'only_available' => array(
					'2' => array('item_link_text', 'item_link', 'item_link_new_tab'),
				),
				'desc' => '',
			),

			'item_link_text' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('Button text', 'thebe-shortcodes'),
				'desc' => ''
	        ),
	        'item_link' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('Button link', 'thebe-shortcodes'),
				'desc' => ''
	        ),
			'item_link_new_tab' => array(
				'std' => '0',
				'type' => 'checkbox',
				'label' => esc_html__('New tab (link open mode)', 'thebe-shortcodes'),
				'desc' => ''
			),
		),
		'template' => '[pricing_table_item {{attributes}}]{{content}}[/pricing_table_item]',
        'clone_button' => esc_html__('Add Item', 'thebe-shortcodes')
	)
);

/* Banner Config --- */
$thebe_shortcode_configs['banner'] = array(
	'title' => esc_html__('Banner', 'thebe-shortcodes'),
	'id' => 'ptsc-banner-shortcode',
	'template' => '[banner {{attributes}}]{{content}}[/banner]',
	'visible' => true,
	'params' => array(
		'style' => array(
			'std' => '1',
			'type' => 'image_select',
			'label' => esc_html__('Banner style', 'thebe-shortcodes'),
			'desc' => '',
			'options' => array('1', '2', '3'),
		),
		'reverse' => array(
			'std' => '0',
			'type' => 'checkbox',
			'label' => esc_html__('Reverse', 'thebe-shortcodes'),
			'desc' => ''
		),
		'parallax' => array(
			'std' => '0',
			'type' => 'checkbox',
			'label' => esc_html__('Parallax', 'thebe-shortcodes'),
			'desc' => ''
		),
		'text_color' => array(
			'std' => 'white',
			'type' => 'select',
			'label' => esc_html__('Text color', 'thebe-shortcodes'),
			'options' => array(
				'white' => esc_html__('White', 'thebe-shortcodes'),
				'black' => esc_html__('Black', 'thebe-shortcodes'),
			),
			'desc' => ''
        ),
        'hr1' => array(
			'type' => 'delimiter',
		),

		'img' => array(
			'std' => '',
			'type' => 'image',
			'label' => esc_html__('Banner image', 'thebe-shortcodes'),
			'desc' => ''
		),
		'title' => array(
			'std' => '',
            'type' => 'text',
			'label' => esc_html__('Title', 'thebe-shortcodes'),
			'desc' => ''
        ),
        'title_size' => array(
        	'std' => '2',
			'type' => 'select',
			'label' => esc_html__('Title size', 'thebe-shortcodes'),
			'desc' => '',
			'options' => array(
				'0' => esc_html__('H1(large)', 'thebe-shortcodes'),
				'1' => esc_html__('H1', 'thebe-shortcodes'),
				'2' => esc_html__('H2', 'thebe-shortcodes'),
				'3' => esc_html__('H3', 'thebe-shortcodes'),
				'4' => esc_html__('H4', 'thebe-shortcodes'),
				'5' => esc_html__('H5', 'thebe-shortcodes'),
				'6' => esc_html__('H6', 'thebe-shortcodes'),
			)
		),
		'content' => array(
			'std' => '',
			'type' => 'textarea',
			'label' => esc_html__('Content', 'thebe-shortcodes'),
			'desc' => '',
		),
		'btn_text' => array(
			'std' => '',
			'type' => 'text',
			'label' => esc_html__('Button text', 'thebe-shortcodes'),
			'desc' => ''
		),
		'btn_link' => array(
			'std' => '#',
			'type' => 'text',
			'label' => esc_html__('Button link', 'thebe-shortcodes'),
			'desc' => ''
		),
		'btn_link_new_tab' => array(
			'std' => '0',
			'type' => 'checkbox',
			'label' => esc_html__('New tab (link open mode)', 'thebe-shortcodes'),
			'desc' => ''
		),
		'hr2' => array(
			'type' => 'delimiter',
		),

		'bg_img' => array(
			'std' => '',
			'type' => 'image',
			'label' => esc_html__('Background image', 'thebe-shortcodes'),
			'desc' => ''
		),
		'bg_color' => array(
			'std' => '',
			'type' => 'color',
			'label' => esc_html__('Background color', 'thebe-shortcodes'),
			'desc' => ''
		),
		'bg_video_type' => array(
			'type' => 'select',
			'label' => esc_html__('Background video', 'thebe-shortcodes'),
			'options' => array(
				'0' => esc_html__('None', 'thebe-shortcodes'),
				'1' => esc_html__('*.mp4', 'thebe-shortcodes'),
				'2' => esc_html__('Youtube', 'thebe-shortcodes'),
				'3' => esc_html__('Vimeo', 'thebe-shortcodes')
			),
			'only_available' => array(
				'1' => array( 'bg_video_link' ),
				'2' => array( 'bg_video_link' ),
				'3' => array( 'bg_video_link' ),
			),
			'desc' => '',
		),
		'bg_video_link' => array(
			'std' => '',
			'type' => 'text',
			'label' => esc_html__('Video link', 'thebe-shortcodes'),
			'desc' => ''
		),
		'append_class' => array(
			'std' => '',
			'type' => 'text',
			'label' => esc_html__('Class', 'thebe-shortcodes'),
			'desc' => esc_html__('You can set a class to this shortcode so that you can write CSS according to this class.', 'thebe-shortcodes'),
		),
	)
);


/* Text carousel Config --- */
$thebe_shortcode_configs['text_carousel'] = array(
	'title' => esc_html__('Text carousel', 'thebe-shortcodes'),
	'id' => 'ptsc-text_carousel-shortcode',
	'template' => '[text_carousel {{attributes}}]{{child_shortcode}}[/text_carousel]',
	'visible' => true,
	'params' => array(
		'title_img' => array(
			'std' => '',
			'type' => 'image',
			'label' => esc_html__('Title image', 'thebe-shortcodes'),
			'desc' => ''
		),
		'title' => array(
			'std' => '',
            'type' => 'text',
			'label' => esc_html__('Title', 'thebe-shortcodes'),
			'desc' => ''
		),
		'subtitle' => array(
			'std' => '',
            'type' => 'text',
			'label' => esc_html__('Subtitle', 'thebe-shortcodes'),
			'desc' => ''
        ),
        'title_center' => array(
			'std' => '0',
			'type' => 'checkbox',
			'label' => esc_html__('Title center', 'thebe-shortcodes'),
			'desc' => ''
		),
		'append_class' => array(
			'std' => '',
			'type' => 'text',
			'label' => esc_html__('Class', 'thebe-shortcodes'),
			'desc' => esc_html__('You can set a class to this shortcode so that you can write CSS according to this class.', 'thebe-shortcodes'),
		),
	),
	'child_shortcode' => array(
    	'params' => array(
    		'item_img' => array(
				'std' => '',
	            'type' => 'image',
				'label' => esc_html__('Image', 'thebe-shortcodes'),
				'desc' => ''
	        ),
			'item_title' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('Title', 'thebe-shortcodes'),
				'desc' => ''
	        ),
	        'item_title_size' => array(
	        	'std' => '3',
				'type' => 'select',
				'label' => esc_html__('Title size', 'thebe-shortcodes'),
				'options' => array(
					'0' => esc_html__('H1(large)', 'thebe-shortcodes'),
					'1' => esc_html__('H1', 'thebe-shortcodes'),
					'2' => esc_html__('H2', 'thebe-shortcodes'),
					'3' => esc_html__('H3', 'thebe-shortcodes'),
					'4' => esc_html__('H4', 'thebe-shortcodes'),
					'5' => esc_html__('H5', 'thebe-shortcodes'),
					'6' => esc_html__('H6', 'thebe-shortcodes'),
				),
				'desc' => '',
			),
	        'item_subtitle' => array(
				'std' => '',
	            'type' => 'text',
				'label' => esc_html__('Subtitle', 'thebe-shortcodes'),
				'desc' => ''
	        ),
			'content' => array(
				'std' => '',
	            'type' => 'textarea',
				'label' => esc_html__('Description', 'thebe-shortcodes'),
				'desc' => ''
			),
		),
		'template' => '[text_carousel_item {{attributes}}]{{content}}[/text_carousel_item]',
        'clone_button' => esc_html__('Add Item', 'thebe-shortcodes')
	)
);


/* Gallery Config --- */
$thebe_shortcode_configs['pt_gallery'] = array(
	'title' => esc_html__('Gallery', 'thebe-shortcodes'),
	'id' => 'ptsc-pt_gallery-shortcode',
	'template' => '[pt_gallery {{attributes}}]{{content}}[/pt_gallery]',
	'visible' => true,
	'params' => array(
		'title_img' => array(
			'std' => '',
			'type' => 'image',
			'label' => esc_html__('Title image', 'thebe-shortcodes'),
			'desc' => ''
		),
		'title' => array(
			'std' => '',
            'type' => 'text',
			'label' => esc_html__('Title', 'thebe-shortcodes'),
			'desc' => ''
		),
		'subtitle' => array(
			'std' => '',
            'type' => 'text',
			'label' => esc_html__('Subtitle', 'thebe-shortcodes'),
			'desc' => ''
        ),
        'title_center' => array(
			'std' => '0',
			'type' => 'checkbox',
			'label' => esc_html__('Title center', 'thebe-shortcodes'),
			'desc' => ''
		),
		'wide' => array(
			'std' => '0',
			'type' => 'checkbox',
			'label' => esc_html__('Wide', 'thebe-shortcodes'),
			'desc' => ''
		),

		'hr1' => array(
			'type' => 'delimiter',
		),

		'img_list' => array(
			'std' => '',
			'type' => 'image_info_list',
			'meta' => array(
				'title' => array( 'type' => 'text', 'title' => esc_html__('Item title', 'thebe-shortcodes'), 'std' => '' ),
				'link' => array('type' => 'text', 'title' => esc_html__('Item link', 'thebe-shortcodes'), 'std' => '' ),
				'link_new_tab' => array( 'type' => 'checkbox', 'title' => esc_html__('New tab (link open mode)', 'thebe-shortcodes'), 'std' => '0' ),

                'use_video' => array( 'type' => 'checkbox', 'title' => esc_html__('Use video', 'thebe-shortcodes'), 'std' => '0' ),
                'video_type' => array('type' => 'select', 'title' => esc_html__('Video type', 'thebe-shortcodes'), 
                    'options' => array(
                        '1' => esc_html__('*.mp4', 'thebe-shortcodes'),
                        '2' => esc_html__('Youtube', 'thebe-shortcodes'),
                        '3' => esc_html__('Vimeo', 'thebe-shortcodes')
                    ),
                    'std' => '1' 
                ),
                'video_link' => array('type' => 'text', 'title' => esc_html__('Video link', 'thebe-shortcodes'), 'std' => '' ),
			),
			'label' => esc_html__('Images', 'thebe-shortcodes'),
			'desc' => '',
		),
		'cols' => array(
			'std' => '2',
			'type' => 'image_select',
			'label' => esc_html__('Columns', 'thebe-shortcodes'),
			'options' => array('2', '3', '4', '5'),
			'desc' => ''
		),
		'thumb_type' => array(
			'std' => 'auto',
			'type' => 'image_select',
			'label' => esc_html__('Thumb type', 'thebe-shortcodes'),
			'options' => array('auto', 'h', 'v', 's'),
			'desc' => ''
		),
		'gap' => array(
			'std' => 'none',
			'type' => 'select',
			'label' => esc_html__('Item gap', 'thebe-shortcodes'),
			'options' => array(
				'none' => esc_html__('None', 'thebe-shortcodes'),
				'smallest' => esc_html__('Smallest', 'thebe-shortcodes'),
				'small' => esc_html__('Small', 'thebe-shortcodes'),
				'large' => esc_html__('Large', 'thebe-shortcodes'),
			),
			'desc' => ''
		),
		'use_lightbox' => array(
			'std' => '1',
			'type' => 'checkbox',
			'label' => esc_html__('Lightbox', 'thebe-shortcodes'),
			'desc' => ''
		),

		'append_class' => array(
			'std' => '',
			'type' => 'text',
			'label' => esc_html__('Class', 'thebe-shortcodes'),
			'desc' => esc_html__('You can set a class to this shortcode so that you can write CSS according to this class.', 'thebe-shortcodes'),
		),
	)
);

?>