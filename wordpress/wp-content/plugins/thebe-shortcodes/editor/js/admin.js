/**
 * Created by foreverpinetree@gmail.com on 2016-08-20.
 */
!(function($, win)
{
    "use strict";

    var ShortcodeGenerator = function()
    {
        this.buttonClass = 'ptsc-insert-shortcode-button';
        this.$scContainer = $('#ptsc-shortcode-manager');

        this._currentView = null;

        this._editData = null;
        this._openMode = '';
        this._editShortcodeObj = null;

        this.currentShorcodeId = '';

        this._currentChildCount = 0;

        this.isOpen = false;

        this._isSelectItemEnabled = true;

        this._actionCallbackHandler = null;
        this._actionCallbackParams = null;

        this._actionByTagHandler = null;
        this._cancelByTagHandler = null;

        this._isPreviewOpen = false;

        this._keepPureString = true;

        this.settings = {
            "mix_box":{
                "id":"ptsc-mix_box-shortcode",
                "child":"mix_box_item",
                "title":"Mix box"
            },
            "slider":{
                "id":"ptsc-slider-shortcode",
                "child":"slider_item",
                "title":"Slider"
            },
            "pricing_table":{
                "id":"ptsc-pricing_table-shortcode",
                "child":"pricing_table_item",
                "title":"Pricing table"
            },
            "banner":{
                "id":"ptsc-banner-shortcode",
                "child":"",
                "title":"Banner"
            },
            "text_carousel":{
                "id":"ptsc-text_carousel-shortcode",
                "child":"text_carousel_item",
                "title":"Text carousel"
            },
            'pt_gallery':{
                'id':'ptsc-pt_gallery-shortcode',
                'child':'',
                'title':'Gallery'
            }
        };

        this.shortcodeNames = [];
        for(var shortcodeName in this.settings)
        {
            this.shortcodeNames.push(shortcodeName);
        }
    }

    var p = ShortcodeGenerator.prototype;

    p.init = function()
    {
        $('#select-ptsc-shortcode').val('');

        // remove slider when editing post/project --------------------------------------------------
        !(function(){
        	if(['post', 'project'].indexOf(win.pagenow) > -1)
	        {
	    		var $slider = $($('.ptsc-shortcode-select').find('.pt-select-list').children('[data-value="ptsc-slider-shortcode"]').get(0));
	        	var scIndex = $slider.index();
	        	var $option = $($('.ptsc-shortcode-select').find('select').children().get(scIndex));

	        	$slider.remove();
	        	$option.remove();
	        }
        })();

        var $shortcodes = $('.ptsc-shortcode-type').hide(),
            $title = $('#ptsc-shortcode-title');

        // Show the selected shortcode input fields
        $('#select-ptsc-shortcode').change(function () {
            var id = $(this).val();
            if(ptShortcodeMgr.currentShorcodeId == id)
            {
                return;
            }

            ptShortcodeMgr.reset();

            var text = $(this).find('option:selected').text();

            ptShortcodeMgr.currentShorcodeId = $(this).val();
            if(ptShortcodeMgr.currentShorcodeId == '')
            {
                $('#ptsc-items-container').hide();
                $('#ptsc-shortcode-wrap').hide();
            }
            else
            {
                $('#ptsc-items-container').show();
                $('#ptsc-shortcode-wrap').show();
            }

            var $list = $('#' + ptShortcodeMgr.currentShorcodeId);
            var existsChild = $list.hasClass('ptsc-exists-child');
            if(existsChild)
            {
                $('#ptsc-items-container>.child-item-container').show();
            }
            else
            {
                $('#ptsc-items-container>.child-item-container').hide();
            }

            $shortcodes.hide();
            $title.text(text);
            $list.show().find('table:not(.hidden) .sc-color-field').wpColorPicker();
            ptShortcodeMgr.resizeTB();

            ptShortcodeMgr.selectShortcodeItem(-1);

            ptShortcodeMgr.requestAnimationFrame(win.ptShortcodeMgr.initView);
        });

        $('.' + ptShortcodeMgr.buttonClass).on('click', function(evt){
            ptShortcodeMgr.insertShortcode();
        });

        $('#ptsc-cancel-shortcode-insert').on('click', function(evt){
            if(ptShortcodeMgr._cancelByTagHandler)
            {
                ptShortcodeMgr._cancelByTagHandler();
            }

            ptShortcodeMgr.onClose();
        });

        $('.ptsc-exit-button').on('click', function(evt){
            if(ptShortcodeMgr._cancelByTagHandler)
            {
                ptShortcodeMgr._cancelByTagHandler();
            }

            ptShortcodeMgr.onClose();
        });

        this.$scContainer.on('click', function(evt){
            if(evt.target.id == 'ptsc-shortcode-manager')
            {
                evt.stopPropagation();
                ptShortcodeMgr.onClose();
            }
        });

        $('.ptsc-add-item-button').on('click', function(evt) {
            evt.preventDefault();
            
            var $list = $('#' + ptShortcodeMgr.currentShorcodeId);
            var el = $list.find('div.ptsc-sortable');

            ptShortcodeMgr.cloneContent(el);
            ptShortcodeMgr.resizeTB();
        });

        // Make content sortable using the $ UI Sortable method
        $('.child-item-container>ul.item-list').sortable({
            items: '.child-shortcode-item',
            distance : 30
        });
        
        $('.selectpicker.pt-select>.pt-select-list>.list-item').hover(
            function(evt){
                if($(evt.target).index() == 0) return;
                
                var id = $(evt.target).data('value');
                if(!id) return;
                var url = win.__pt_root_url + '/data/images/shortcode/'+ id + '.png';

                var x = Math.floor($(this).offset().left - $(win).scrollLeft());
                var y = Math.floor($(this).offset().top - $(win).scrollTop());

                if(win.pt_admin)
                {
                    if(pt_admin.isRTL)
                    {
                        x = $(win).width() - (x - $(evt.target).closest('.pt-select-list').width() + 225 - 5);
                    }
                    else
                    {
                        x = x + 225;
                    }

                    pt_admin.showPreviewImage(url, x, y, 250, 130);
                }
            },
            function(evt){
                if($(evt.target).index() == 0) return;
                
                if(win.pt_admin)
                {
                    pt_admin.hidePreviewImage();
                }
            }
        );

        $(win).on('keyup', function (evt){
            if(evt.keyCode == 27)
            {
                if(ptShortcodeMgr._isPreviewOpen)
                {
                    ptShortcodeMgr.closeShortcodePreview();
                }
            }
        });

        $('.insert-shortcode-button.shortcode-button').on('click', function(evt){
            ptShortcodeMgr._currentView = $(this).closest('.shortcode-option-item');
            ptShortcodeMgr.onOpen();
        });

        $('#ptsc-items-container>.main-item').on('click', function(evt){
            ptShortcodeMgr.selectShortcodeItem(-1);
        });

        $('#ptsc-preview-shortcode').on('click', this.onShortcodePreview.bind(this));

        $('.preview-button-list>.ptsc-btn').on('click', function(evt){
            if($(this).hasClass('preview-close-btn'))
            {
                ptShortcodeMgr.closeShortcodePreview();
            }
        });

        $('#ptsc-preview-warning').on('click', function(evt){
            alert(this.title);
        });

        var $mainShortcode = this.$scContainer.find('table[data-type="main_shortcode"]');

        $mainShortcode.find('.pt-select-sc.select-previewable').ptSelect();
        this.$scContainer.find('.list-item[data-previewid]').hover(
            function(evt){
                pt_admin && pt_admin.onOverPreview(evt.target);
            },
            function(evt){
                pt_admin && pt_admin.hidePreviewImage();
            }
        );

        $mainShortcode.find('.pt-sc-image-select').ptImageSelect();
        $mainShortcode.find('input.type-color').wpColorPicker();

        var $allMainInputs = $mainShortcode.find('select,input.thebe-sc-form-checkbox,.pt-image-select input[type="hidden"]');
        $allMainInputs.on('change', function(evt){
            this._setTargetOptionAvailable(evt.currentTarget);
        }.bind(this));

        this.$scContainer.find('.shortcode-item.type-image-info-list').ptscImageInfoList();

        // ---------------------------------------------------------------------------------

        $('.shortcode-option-item').each(function(i, item){
            this._updateVisualItems($(item), $(item).find('.shortcode-text').val());
        }.bind(this));

        $('.shortcode-option-item .shortcode-list').sortable({
            axis: "y",
            distance: 10
        });
        $('.shortcode-option-item .shortcode-list').disableSelection();
        $('.shortcode-option-item .shortcode-list').on('sortupdate', this._onSortShortcodeItem.bind(this));

        $('.sc-mode-switch>li').on('click', function(evt){
            $('.sc-mode-switch>li').removeClass('selected');

            var $this = $(evt.currentTarget);
            $this.addClass('selected');

            var $view = $this.closest('.shortcode-option-item');

            if($this.hasClass('sc-mode-visual'))
            {
                $view.removeClass('is-text');
                this._updateVisualItems($view);
            }
            else
            {
                $view.addClass('is-text');
            }
        }.bind(this));

        function getActiveShortcodeItem ()
        {
            var activeElement = document.activeElement, activeItem;
            if(activeElement)
            {
                var $items = $('.shortcode-option-item');
                var len = $items.length;
                for(var i = 0; i < len; i ++)
                {
                    if($.contains($items[i], activeElement))
                    {
                        activeItem = $items[i];
                        break;
                    }
                }
            }
            return activeItem;
        }

        $(win).on('keyup', function(evt){
            if(evt.keyCode === 46) //Delete
            {
                var activeItem = getActiveShortcodeItem();
                if(activeItem)
                {
                    var $view = $(activeItem);
                    var $target = $view.find('.shortcode-list-item:focus');
                    if($target.length > 0)
                    {
                        $target.remove();

                        var data = this._updateDataFromVisualList($view);
                        this._updateVisualItems($view, data);
                    }
                }
            }
        }.bind(this));

        document.addEventListener('copy', function(e){
            var activeItem = getActiveShortcodeItem();
            if(activeItem)
            {
                var $target = $(activeItem).find('.shortcode-list-item:focus');
                if($target.length > 0)
                {
                    var json = $target.data('json');
                    if(json)
                    {
                        var jsonData = {
                            shortcodeData: json,
                            type: 'shortcode'
                        };

                        e.clipboardData.setData('text/plain', JSON.stringify(jsonData));
                        e.preventDefault();
                    }
                }
            }
        }.bind(this));

        document.addEventListener('paste', function(e){
            var activeItem = getActiveShortcodeItem();
            if(activeItem)
            {
                var pasteData = (e.clipboardData || window.clipboardData).getData('text');
                var jsonData;

                try
                {
                    jsonData = JSON.parse(pasteData);
                }
                catch(e)
                {
                    //
                }

                if(jsonData && jsonData.type === 'shortcode')
                {
                    e.preventDefault();
                    e.stopPropagation();

                    var shortcodeObj = JSON.parse(jsonData.shortcodeData);

                    var $target = $(activeItem).find('.shortcode-list-item:focus');
                    if($target.length > 0)
                    {
                        this.insertShortcodeAfter(shortcodeObj.data.content, $target);
                    }
                    else
                    {
                        this.sendToEditor(shortcodeObj.data.content);
                    }
                }
            }
        }.bind(this));
    }

    p._setTargetOptionAvailable = function(target)
    {
        var $target = $(target);
        var value;

        if(target.type == 'checkbox')
        {
            value = $target.prop('checked') ? '1' : '0';
        }
        else
        {
            value = $target.val();
        }

        var availableObj = $target.data('available');
        if(availableObj)
        {
            this.setOptionAvailable(availableObj, value, $target);
        }
    }

    p.requestAnimationFrame = function(handler)
    {
        if(win.requestAnimationFrame)
        {
            win.requestAnimationFrame(handler);
        }
        else
        {
            setTimeout(handler, 17);
        }
    }

    // Simple function to clone an included template
    p.cloneContent = function(el)
    {
        var $clone = $(el).find('.ptsc-clone-template').clone().removeClass('hidden ptsc-clone-template').removeAttr('id').addClass('ptsc-was-cloned');

        $clone.find('.pt-select-sc.select-previewable').ptSelect( {className:'pt-select-clone'} );
        $clone.find('.list-item[data-previewid]').hover(
            function(evt){
                pt_admin && pt_admin.onOverPreview(evt.target);
            },
            function(evt){
                pt_admin && pt_admin.hidePreviewImage();
            }
        );

        //----------------

        $clone.find('select,input.thebe-sc-form-checkbox,.pt-image-select input[type="hidden"]').each(function(i, target){
            var $target = $(target);
            var value;

            if(target.type == 'checkbox')
            {
                value = $target.prop('checked') ? '1' : '0';
            }
            else
            {
                value = $target.val();
            }

            var availableObj = $target.data('available');
            if(availableObj)
            {
                this.setOptionAvailable(availableObj, value, $target);
            }
        }.bind(this));

        $clone.find('select,input.thebe-sc-form-checkbox,.pt-image-select input[type="hidden"]').on('change', function(evt){
            var target = evt.currentTarget;
            var $target = $(target);
            var value;

            if(target.type == 'checkbox')
            {
                value = $target.prop('checked') ? '1' : '0';
            }
            else
            {
                value = $target.val();
            }

            var availableObj = $target.data('available');
            if(availableObj)
            {
                this.setOptionAvailable(availableObj, value, $target);
            }
        }.bind(this));

        //-------------

        if(win.pt_admin)
        {
            $clone.find('.imagelist-upload').on('click', pt_admin.bindUploadImgListEvent);
            $clone.find('.image-list-container').on('click', function(evt){
                var target = evt.target;
                if($(target).hasClass('imagelist-upload-delete-button'))
                {
                    evt.stopPropagation();
                    pt_admin.removeImgListItem(target.parentNode);
                }
            });
        }
        
        $clone.find(".image-list-container.sortable").sortable();
        $clone.find('.image-list-container.sortable').on('sortupdate', function(evt, ui){
            var $imglist = $(evt.currentTarget);
            var $targetfield = $imglist.parent().children('.image-list-target-input');
            var rids = [];
            $imglist.find('.uploader-image-container').each(function(){
                rids.push($(this).data('rid'));
            });
            $targetfield.val(rids.join(","));
        });

        $clone.find('.thena-sc-form-checkbox,.type-checkbox.target-input').checkboxpicker();

        $clone.find('.pt-sc-image-select').ptImageSelect( {className:'pt-image-select-clone'} );
        
        $clone.find('.pt-admin-help-icon').hover(
            function(evt){
                pt_admin && pt_admin.onOverHelpIcon(evt.currentTarget);
            },
            function(evt){
                pt_admin && pt_admin.hidePreviewImage();
            }
        );

        $clone.find('.sc-color-field').wpColorPicker();

        $clone.find('input.type-color').wpColorPicker();

        $clone.find('.shortcode-item.type-image-info-list').ptscImageInfoList();

        if(win.pt_admin)
        {
            $clone.find('.ptsc-image-upload').on('click', pt_admin.bindSCImageEvent);
            $clone.find('.ptsc-image-upload-delete-button').on('click', pt_admin.bindSCRemoveImgEvent);
        }

        win.IconClassInsertMgr && win.IconClassInsertMgr.listen($clone.get(0));

        $clone.find('.show-advanced-button').on('click', function(evt){
            $clone.toggleClass('show-advanced');
        });
        
        $(el).append($clone);

        $clone.attr('data-index', this._currentChildCount);

        this.addShortcodeItem(this._currentChildCount);

        this._currentChildCount ++;
    }

    p.setOptionAvailable = function(data, value, $valueTarget)
    {
        if(value === undefined || value === null)
        {
            value = '';
        }

        var $itemTarget = $valueTarget.closest('tr.shortcode-item');
        var $itemParent = $itemTarget.parent();

        var listenOptions = [], arr, i, len, str, paramIndex, param, paramArr, paramValues, optionIndex, scInput, $scInput, scValue;
        for(var key in data)
        {
            arr = data[key];
            if(arr)
            {
                len = arr.length;
                for(i = 0; i < len; i ++)
                {
                    str = arr[i];
                    paramIndex = str.indexOf('&');
                    if(paramIndex > -1)
                    {
                        str = str.substring(0, paramIndex);
                    }
                    if(listenOptions.indexOf(str) == -1)
                    {
                        listenOptions.push(str);
                    }
                }
            }
        }
        var currentArr = data[value.toString()];
        if(currentArr)
        {
            len = currentArr.length;
            for(i = 0; i < len; i ++)
            {
                str = currentArr[i];
                paramIndex = str.indexOf('&');//param only supports for Select and Checkbox, and only supports one param.
                if(paramIndex > -1)
                {
                    param = str.substring(paramIndex + 1);
                    str = str.substring(0, paramIndex);
                }
                optionIndex = listenOptions.indexOf(str)
                if(optionIndex >= 0)
                {
                    if(paramIndex > -1)
                    {
                        paramArr = param.split('=');
                        if(paramArr && paramArr.length > 1)
                        {
                            paramValues = paramArr[1].split('|');
							paramValues.forEach(function(value, i){
								if(value === '')
								{
									paramValues.splice(i, 1);
								}
                            });
                            $scInput = $itemParent.find('[data-name="' + paramArr[0] + '"] .thebe-sc-input');
                            scInput = $scInput.get(0);
                            if(scInput && scInput.type == 'checkbox')
                            {
                                scValue = $scInput.prop('checked') ? '1' : '0';
                            }
                            else
                            {
                                scValue = $scInput.val();
                            }

                            if(paramValues.indexOf(scValue) > -1)
                            {
                                listenOptions.splice(optionIndex, 1);
                                $itemParent.find('[data-name="' + str + '"]').removeClass('option-item-hide');
                            }
                        }
                    }
                    else
                    {
                        listenOptions.splice(optionIndex, 1);
                        $itemParent.find('[data-name="' + str + '"]').removeClass('option-item-hide');
                    }
                }
            }
        }
        len = listenOptions.length;
        for(i = 0; i < len; i ++)
        {
            $itemParent.find('[data-name="' + listenOptions[i] + '"]').addClass('option-item-hide');
        }
    }

    /**
     * <0: add shortcode item
     */
    p.addShortcodeItem = function()
    {
        var $itemList = $('#ptsc-items-container>.child-item-container>.item-list');
        var html = '<li class="child-shortcode-item tran-color tran-border" data-index="' + this._currentChildCount + '"><span>Item</span><div class="close-btn"></div></li>';
        var $item = $(html);
        $itemList.append($item);
        $item.on('click', this.onClickShortcodeItem);

        this.selectShortcodeItem(this._currentChildCount);

        $itemList.scrollTop($itemList.get(0).scrollHeight);
    }

    /**
     * <0: main setting, 0-n: child item
     */
    p.selectShortcodeItem = function(index)
    {
        if(!this._isSelectItemEnabled)
        {
            return;
        }

        var $list = $('#' + this.currentShorcodeId);
        var $mainSetting = $list.find('table[data-type="main_shortcode"]');
        var $childSettingList = $list.find('div.ptsc-sortable');
        if(index < 0)
        {
            $mainSetting.show();
            $childSettingList.hide();

            $('#ptsc-items-container>.main-item').addClass('selected');

            var $itemList = $('#ptsc-items-container>.child-item-container>.item-list');
            $itemList.find('li').removeClass('selected');
        }
        else
        {
            $mainSetting.hide();
            $childSettingList.show();

            $('#ptsc-items-container>.main-item').removeClass('selected');

            var $itemList = $('#ptsc-items-container>.child-item-container>.item-list');
            $itemList.find('li').removeClass('selected');

            $itemList.find('[data-index="' + index + '"]').addClass('selected');

            var $childSettings = $childSettingList.find('table:not(.hidden)');
            $childSettings.hide();

            var itemSetting = $childSettingList.find('table[data-index="' + index + '"]');
            $(itemSetting).show();
        }
    }

    p.onClickShortcodeItem = function(evt)
    {
        var $item = $(evt.currentTarget);
        var index = $item.data('index');

        if($(evt.target).hasClass('close-btn'))
        {
            if($item.hasClass('selected'))
            {
                var zIndex = $item.index();
                if($item.parent().children().length == 1)
                {
                    ptShortcodeMgr.selectShortcodeItem(-1);
                }
                else
                {
                    var $preItem = $item.parent().children().eq(zIndex + 1);
                    if($preItem.get(0))
                    {
                        ptShortcodeMgr.selectShortcodeItem($preItem.data('index'));
                    }
                    else
                    {
                        $preItem = $item.parent().children().eq(zIndex - 1);
                        if($preItem.get(0))
                        {
                            ptShortcodeMgr.selectShortcodeItem($preItem.data('index'));
                        }
                        else
                        {
                            ptShortcodeMgr.selectShortcodeItem(-1);
                        }
                    }
                }
            }

            $item.find('.pt-select').disposePtSelect();
            $item.find('.pt-sc-image-select').ptImageSelectCall('dispose');

            win.IconClassInsertMgr && win.IconClassInsertMgr.cancle($item.get(0));

            $item.remove();

            var $list = $('#' + ptShortcodeMgr.currentShorcodeId);
            var $childSettingList = $list.find('div.ptsc-sortable');
            var $itemSetting = $childSettingList.find('table[data-index="' + index + '"]');
            $itemSetting.find('.shortcode-item.type-image-info-list').ptscImageInfoListCall('dispose');
            $itemSetting.remove();

            return;
        }

        ptShortcodeMgr.selectShortcodeItem(index);
    }

    p.sortSettingTables = function(tables)
    {
        var arr = [], table;

        var len = tables.length;
        for(var i = 0; i < len; i ++)
        {
            table = tables[i];
            if(table && $(table).data('type') == 'main_shortcode')
            {
                arr.push(table);
            }
        }

        var $itemList = $('#ptsc-items-container>.child-item-container>.item-list');
        
        $itemList.find('li').each(function(){
            var index = $(this).data('index');
            var len = tables.length;
            for(var i = 0; i < len; i ++)
            {
                table = tables[i];
                if(table && $(table).data('index') == index)
                {
                    arr.push(table);
                }
            }
        });

        return arr;
    }

    p.onOpen = function()
    {
        this.isOpen = true;

        var $select = $('.ptsc-shortcode-select');
        var $btn = $('.' + this.buttonClass);
        var $title = $('#ptsc-shortcode-status-title');
        var $previewBtn = $('#ptsc-preview-shortcode');

        if(this._openMode == 'edit')
        {
            $select.hide();
            $btn.html($btn.data('edit'));

            $title.html($title.data('edit'));
            $title.show();

            $previewBtn.show();
        }
        else if(this._openMode == 'page-builder' || this._openMode == 'pb-edit')
        {
            $select.hide();
            $btn.html($btn.data('pb'));

            $title.html($title.data('pb'));
            $title.show();

            $previewBtn.show();
        }
        else 
        {
            $select.show();
            $btn.html($btn.data('insert'));

            $title.hide();

            $previewBtn.show();
        }

        $('#ptsc-items-container').hide();
        $('#ptsc-shortcode-wrap').hide();

        $(document.body).addClass('shortcode-modal');

        this.$scContainer.show();

        this._isPreviewOpen = false;
    }

    p.onClose = function()
    {
        this.isOpen = false;

        this._actionCallbackHandler = null;
        this._actionCallbackParams = null;

        this._actionByTagHandler = null;
        this._cancelByTagHandler = null;

        this._currentView = null;
        this._editData = null;
        this._openMode = '';
        this._editTag = '';

        this._tempAddItemAfter = null;
        $(document.body).removeClass('shortcode-modal').removeClass('modal-open');

        this.reset();

        $('#select-ptsc-shortcode').val('');

        this.$scContainer.hide();

        this._isPreviewOpen = false;
    }

    p.getShortcode = function() 
    {
        var select = $('#select-ptsc-shortcode').val();
        if(!select)
        {
            return '';
        }

        var type = select.replace('ptsc-', '').replace('-shortcode', ''),
            template = $('#' + select).data('shortcode-template'),
            childTemplate = $('#' + select).data('shortcode-child-template'),
            tables = $('#' + select).find('table').not('.ptsc-clone-template'),
            attributes = '',
            content = '',
            contentToEditor = '',
            mainAttrs = '',
            isMain = true;

        tables = ptShortcodeMgr.sortSettingTables(tables);

        // go over each table, build the shortcode content
        for (var i = 0; i < tables.length; i++)
        {
            var elems = $(tables[i]).find('input:not(.not-save):not(.wp-picker-clear), select:not(.not-save), textarea:not(.not-save)');

            
            isMain = $(tables[i]).data('type') == "main_shortcode";
            // Build an attributes string by mapping over the input
            // fields in a given table.
            attributes = $.map(elems, function(el, index) {
                if(!el) return;

                var $el = $(el);
                var value = $el.val();

                if( el.type == 'checkbox' )
                {
                    value = $el.prop('checked') ? '1' : '0';
                }

                if( $el.data('id') === 'content' )
                {
                    content = value;
                    return '';
                } 
                else if( $el.data('id') === 'last' )
                {
                    if( $el.is(':checked') )
                    {
                        return $el.data('id') + '="true"';
                    } 
                    else
                    {
                        return '';
                    }
                }
                else
                {
                    if(value === null || value === undefined)
                    {
                        value = '';
                    }
                    
                    value = value.replace(/\[/g, win.__sc_code_bracket_l);
                    value = value.replace(/\]/g, win.__sc_code_bracket_r);
                    value = value.replace(/"/g, win.__sc_code_quote);
                    value = value.replace(/\n/g, win.__sc_code_eol);
                    value = value.replace(/</g, win.__sc_code_lt);
                    value = value.replace(/>/g, win.__sc_code_gt);

                    return $el.data('id') + '="' + value + '"';
                }
            });
            attributes = attributes.join(' ').trim();

            // shortcode template
            if( childTemplate )
            {
                if(isMain)
                {
                    mainAttrs += attributes;
                }
                else
                {
                    contentToEditor += childTemplate.replace('{{attributes}}', attributes).replace('{{attributes}}', attributes).replace('{{content}}', content);
                }
            } 
            else 
            {
                contentToEditor += template.replace('{{attributes}}', attributes).replace('{{attributes}}', attributes).replace('{{content}}', content);
            }
        };

        if( childTemplate )
        {
            contentToEditor = template.replace('{{child_shortcode}}', contentToEditor);
        }
        contentToEditor = contentToEditor.replace("{{attributes}}", mainAttrs);

        return contentToEditor;
    }

    p.insertShortcode = function() 
    {
        var select = $('#select-ptsc-shortcode').val();
        if(!select)
        {
            alert('Please select a shortcode type first!');
            return;
        }

        $('.wp-picker-active .sc-color-field').wpColorPicker('close');

        var shortcodeData = this.getShortcode();

        if(this._openMode == 'edit' || this._openMode == 'pb-edit')
        {
            if(this._actionCallbackHandler)
            {
                var params = this._actionCallbackParams || [];
                params.unshift(shortcodeData);
                
                this._actionCallbackHandler.apply(null, params);
            }
            else
            {
                this.replaceToEditor( shortcodeData );
            }
        }
        else if(this._openMode == 'page-builder')
        {
            if(this._actionByTagHandler)
            {
                this._actionByTagHandler(shortcodeData);
            }
        }
        else
        {
            if(this._tempAddItemAfter)
            {
                this.insertShortcodeAfter(shortcodeData, this._tempAddItemAfter);
            }
            else
            {
                this.sendToEditor( shortcodeData );
            }
        }        

        this.onClose();
    }

    p.onShortcodePreview = function()
    {
        this._isPreviewOpen = true;
        $('#ptsc-preview-container').show();
        this.showPreviewLoading(true);

        $.ajax({
            url:ajaxurl,
            type:"post",
            data:{
                action: 'shortcode_preview',
                shortcode: this.getShortcode(),
                type: pagenow
            },
            success:function(data){
                var iframe = $('#ptsc-preview').get(0);
                iframe.src = 'about:blank';
                try
                {
                    iframe.contentWindow.document.open();
                    iframe.contentWindow.document.clear();
                    iframe.contentWindow.document.write(data);  
                    iframe.contentWindow.document.close();
                }
                catch(e)
                {
                    console.log('Iframe error!');
                    return;
                }
            }
        })
    }

    p.closeShortcodePreview = function()
    {
        if(this._isPreviewOpen)
        {
            var iframe = $('#ptsc-preview').get(0);
            iframe.src = 'about:blank';
            try
            {
                iframe.contentWindow.document.open();
                iframe.contentWindow.document.clear();
                iframe.contentWindow.document.close();
            }
            catch(e)
            {
                //
            }
        }

        this._isPreviewOpen = false;
        $('#ptsc-preview-container').hide();
        this.showPreviewLoading(false);
    }

    p.showPreviewLoading = function(value)
    {
        value ? $('#ptsc-preview-loading').show() : $('#ptsc-preview-loading').hide();
    }

    p.reset = function()
    {
        this.resetFields();

        this.currentShorcodeId = '';
        this._currentChildCount = 0;
    }

    // Set the inputs to empty state
    p.resetFields = function()
    {
        $('.uploader-image-preview.shortcode').each(function(){
            $(this).attr('src', $(this).data('empty'));
        });
        $('.thebe-sc-image-upload-input').val('');
        $('.thebe-sc-input.image-list-target-input').val('');
        $('.shortcode-item .image-list-container').empty();
        
        $('.thebe-sc-form-textarea').val('');
        $('.thebe-sc-form-text').val('');
        
        $('#ptsc-shortcode-title').text('');
        $('#ptsc-shortcode-wrap').find('input[type=text]').each(function(){
            var def = $(this).data('default');
            this.value = def === undefined ? '' : def;
        });
        $('#ptsc-shortcode-wrap').find('textarea').text('');
        $('.ptsc-shortcode-type').hide();
        
        $('select.thebe-sc-form-select').each(function(){
            var $option = $(this).children('[data-default="true"]');
            var index = $option.index();
            if(index < 0) index = 0;
            this.selectedIndex = index;
        });
        $('.selectpicker.pt-select,.pt-select-sc.pt-select').resetPtSelect();
        $('.pt-select-sc.pt-select.is-pt-select.pt-select-clone').disposePtSelect();
        $('.pt-sc-image-select:not(.pt-image-select-clone)').ptImageSelectCall('reset');
        $('.pt-sc-image-select.pt-image-select-clone').ptImageSelectCall('dispose');
        
        $('.pt-select-sc.pt-select.is-pt-select.pt-select-clone .list-item[data-previewid]').off('mouseenter mouseover mouseleave mouseout');
        $('.ptsc-was-cloned .pt-admin-help-icon').off('mouseenter mouseover mouseleave mouseout');

        var $mainShortcode = this.$scContainer.find('table[data-type="main_shortcode"]');

        $mainShortcode.find('.shortcode-item.type-image-info-list').ptscImageInfoListCall('reset');
        $mainShortcode.find('.uploader-image-container').addClass('empty');
        var $clone = $('.ptsc-was-cloned');

        $clone.find('.ptsc-image-upload').off('click');
        $clone.find('.ptsc-image-upload-delete-button').off('click');

        $clone.find('.imagelist-upload').off('click');
        $clone.find('.image-list-container').off('click');
        $clone.find('.image-list-container.sortable').off('sortupdate');

        $clone.find('.shortcode-item.type-image-info-list').ptscImageInfoListCall('dispose');
        $('#ptsc-items-container>.child-item-container>ul.item-list').empty();

        win.IconClassInsertMgr && win.IconClassInsertMgr.cancle('.ptsc-was-cloned');

        $clone.remove();

        $('#' + ptShortcodeMgr.currentShorcodeId).find('table:not(.hidden) input.sc-color-field').each(function(){
            var defaultValue = $(this).data('default');
            if(!defaultValue)
            {
                $(this).wpColorPicker('color', '#000000'); 
                $(this).closest('.wp-picker-container').find('.color-alpha').css('background', 'rgba(0, 0, 0, 0)');
                $(this).val('');
            }
            else
            {
                $(this).wpColorPicker('color', defaultValue); 
            }
        });
    }

    // Function to redraw the thickbox for new content
    p.resizeTB = function()
    {
        var ajaxCont = $('#TB_ajaxContent'),
            tbWindow = $('#TB_window');

        ajaxCont.css({
            height: (tbWindow.outerHeight() - 47),
            overflow: 'auto', // IMPORTANT
            width: (tbWindow.outerWidth() - 30)
        });
    }

    p.initView = function()
    {
        var self = ptShortcodeMgr, $list;

        if(!self._editData)
        {
            $list = $('#ptsc-shortcode-wrap>#' + self.currentShorcodeId);
            var $inputs = $list.find('table[data-type="main_shortcode"]').find('select,input.thebe-sc-form-checkbox,.pt-image-select input[type="hidden"]');
            $inputs.each(function(){
                self._setTargetOptionAvailable(this);
            });

            return;
        }

        $list = $('#ptsc-shortcode-wrap>#' + self._editData.id);
        if(!$list)
        {
            return;
        }

        var data = self._editData.data;
        var key, $target, value;
        var $mainList = $list.children('table');
        for(key in data.attrs)
        {
            value = data.attrs[key];
            $target = $mainList.find('.thebe-sc-input[data-id="' + key + '"]');
            self.setInputProperties($target, value);
        }

        if(data.content != undefined)
        {
            $target = $mainList.find('.thebe-sc-input[data-id="content"]');
            self.setInputProperties($target, data.content);
        }

        if(data.children && data.children.length > 0)
        {
            var len = data.children.length, i, obj, $cloneList = $list.find('.ptsc-sortable>table:not(.hidden)'), $childItem;
            for(i = 0; i < len; i ++)
            {
                obj = data.children[i];
                $childItem = $($cloneList.get(i));
                if(!obj || !$childItem)
                {
                    continue;
                }

                for(key in obj.attrs)
                {
                    value = obj.attrs[key];
                    $target = $childItem.find('.thebe-sc-input[data-id="' + key + '"]');
                    self.setInputProperties($target, value);
                }

                if(obj.content != undefined)
                {
                    $target = $childItem.find('.thebe-sc-input[data-id="content"]');
                    self.setInputProperties($target, obj.content);
                }
            }
        }
    }

    p.setInputProperties = function($target, value)
    {
        if(value === undefined)
        {
            value = '';
        }

        value = value.replace(new RegExp(win.__sc_code_bracket_l, 'g'), '[');
        value = value.replace(new RegExp(win.__sc_code_bracket_r, 'g'), ']');
        value = value.replace(new RegExp(win.__sc_code_quote, 'g'), '"');
        value = value.replace(new RegExp(win.__sc_code_eol, 'g'), '\n');
        value = value.replace(new RegExp(win.__sc_code_lt, 'g'), '<');
        value = value.replace(new RegExp(win.__sc_code_gt, 'g'), '>');

        var target = $target.get(0);
        
        $target.val(value);

        var checkAvailable = false;

        if($target.is('select') && $target.hasClass('pt-select'))
        {
            $target.parent().find('.pt-select-list>li[data-value="' + value + '"]').trigger('click');

            checkAvailable = true;
        }
        else if($target.is('select'))
        {
            $target.trigger('change');

            checkAvailable = true;
        }
        else if($target.hasClass('thebe-sc-image-upload-input'))
        {
            if(value)
            {
                if(win.pt_admin)
                {
                    pt_admin.setImageByAttachmentId($target.siblings('.uploader-image-container').removeClass('empty').css('display', 'block').find('img'), value, 'thumbnail', true);
                }
            }
        }
        else if($target.hasClass('sc-color-field'))
        {
            $target.wpColorPicker('color', value); 
        }
        else if($target.hasClass('image-list-target-input'))
        {
            var $imglist = $target.siblings('.image-list-container');
            $imglist.empty();

            if(value != '')
            {
                var arr = value.split(','), rid, $html, len = arr.length;
                for(var i = 0; i < len; i ++)
                {
                    rid = arr[i];
                    $html = $('<div class="uploader-image-container image-list imagelist-upload-delete" data-rid="' + rid + '">' +
                        '<span class="imagelist-upload-delete-button upload-delete-button"></span>' + 
                        '<img class="uploader-image-preview uploader-image-list" alt=""/></div>');
                    pt_admin.setImageByAttachmentId($html.find('img'), rid, 'thumbnail', true);
                    $imglist.append($html);
                }
            }
        }
	    else if($target.hasClass('image-info-list-target-input'))
        {
            var $imglist = $target.siblings('.image-info-items');
            $imglist.empty();

            if(value)
            {
                value = decodeURI(value);

                var arr;

                try
                {
                    arr = JSON.parse(value);
                }
                catch(error)
                {
                    arr = [];
                }

                var itemObj, rid, $html, len = arr.length;
                for(var i = 0; i < len; i ++)
                {
                    itemObj = arr[i];
                    rid = itemObj.rid;

                    $html = $(
                        '<div class="uploader-image-container">' +
                        '<div class="buttons">' +
                        '<span class="edit-button"></span>' + 
                        '<span class="delete-button"></span>' + 
						'</div>' +
                        '<img class="image-preview" alt=""/>' + 
                        '</div>'
                    );

                    $html.data('json', JSON.stringify(itemObj));

                    pt_admin && pt_admin.setImageByAttachmentId($html.find('img'), rid, 'thumbnail', true);
                    $imglist.append($html);
                }
            }
        }
        else if( $target.hasClass('image-select-target-input') )
        {
            $target.trigger('change');

            checkAvailable = true;
        }
        else if( target && target.type == 'checkbox' )
        {
            $target.prop('checked', value == '1');

            checkAvailable = true;
        }

        if(checkAvailable)
        {
            var availableObj = $target.data('available');
            if(availableObj)
            {
                this.setOptionAvailable(availableObj, value, $target);
            }
        }
    }

    p.openByTag = function(tag, mode, callback, cancelCallback)
    {
        this._actionByTagHandler = callback;
        this._cancelByTagHandler = cancelCallback;

        this._openMode = mode;

        this.onOpen(tag);

        var config = this.getShortCodeConfig(tag);
        if(config)
        {
            $('#select-ptsc-shortcode').parent().find('.pt-select-list>li[data-value="' + config.id + '"]').trigger('click');
        }
    }

    p.openWith = function(view, shortcodeObj, callback, params, mode)
    {
        this._actionCallbackHandler = callback;
        this._actionCallbackParams = params;

        var errorStr = 'Shortcode not found!';

        var tag = shortcodeObj.tag;
        if(!tag)
        {
            alert(errorStr);
            return;
        }

        var obj = shortcodeObj.data;
        var codes = obj.content;

        if(!obj || !obj.shortcode)
        {
            alert(errorStr);
            return;
        }

        var config = this.getShortCodeConfig(tag);
        if(!config)
        {
            alert(errorStr);
            return;
        }
        else
        {
            if(config.child)
            {
                var endTagPattern = new RegExp("\\[\\s*\\/" + tag + "\\]");
                var result = codes.match(endTagPattern);
                if(!result)
                {
                    alert('It seems you miss some part of codes!');
                    return;
                }
            }
        }
        
        if(mode)
        {
            this._openMode = mode;
        }
        else
        {
            this._openMode = 'edit';
        }

        this._currentView = view;
        this._editShortcodeObj = shortcodeObj;

        var data = {content:obj.shortcode.content, tag:tag, attrs:{}, children:null};
        var named = obj.shortcode.attrs.named, key;
        for(key in named)
        {
            data.attrs[key] = named[key];
        }

        var index = 0, child, maxDepth = 512, depth = 0;
        if(config.child)
        {
            data.children = [];

            while(true)
            {
                obj = wp.shortcode.next(config.child, codes, index);
                if(!obj || !obj.shortcode)
                {
                    break;
                }

                child = {content:obj.shortcode.content, tag:config.child, attrs:{}};
                named = obj.shortcode.attrs.named;
                for(key in named)
                {
                    child.attrs[key] = named[key];
                }

                data.children.push(child);

                index = obj.index + obj.content.length;

                depth ++;
                if(depth > maxDepth)
                {
                    break;
                }
            }
        }

        this.onOpen(tag);
        $('#select-ptsc-shortcode').parent().find('.pt-select-list>li[data-value="' + config.id + '"]').trigger('click');

        var $list = $('#ptsc-shortcode-wrap>#' + config.id);
        if(!$list)
        {
            return;
        }

        if(data.children && data.children.length > 0)
        {
            var count = $list.find('.ptsc-sortable>table:not(.hidden)').length;
            var childLen = data.children.length - count;
            var $cloneBtn = $('#ptsc-items-container .ptsc-add-item-button');

            this._isSelectItemEnabled = false;

            while(childLen --)
            {
                $cloneBtn.trigger('click');
            }

            this._isSelectItemEnabled = true;
        }
        
        this._editData = { id:config.id, data:data };
    }

    p.getShortCodeStartTag = function(str)
    {
        var pattern = /\[\s*[a-zA-Z_]*/;
        var result = str.match(pattern);
        if(result && result.length > 0)
        {
            str = result[0] || "";
            str = str.replace(/[\s\[]/g, '');
            return str;
        }
        return '';
    }
    
    p.getShortCodeConfig = function(key)
    {
        return this.settings[key] || null;
    }

    p.sendToEditor = function( content, needReplaceAll )
    {
        var $view = this._currentView ? this._currentView : $('.shortcode-option-item').eq(0);
        var $textarea = $view.find('.shortcode-text');
        var value;
        
        if(needReplaceAll)
        {
            value = content;
        }
        else
        {
            value = $textarea.val() + content;
        }

        if(!this._keepPureString)
        {
            value = this._removePureString(value);
        }

        $textarea.val(value);

        $('body').trigger('ptsc_change', [value, $view]);

        this._updateVisualItems($view, value);
    };

    p.replaceToEditor = function( content )
    {
        var shortcodeObj = this._editShortcodeObj;
        if(!shortcodeObj)
        {
            return;
        }

        var $view = this._currentView ? this._currentView : $('.shortcode-option-item').eq(0);
        var $textarea = $view.find('.shortcode-text');
        var value = $textarea.val();
        var strIndex = shortcodeObj.strIndex;
        var str = value.substring(0, strIndex) + content + value.substring(strIndex + shortcodeObj.data.content.length);

        if(!this._keepPureString)
        {
            str = this._removePureString(str);
        }

        $textarea.val(str);

        $('body').trigger('ptsc_change', [str, $view]);

        this._updateVisualItems($view, str);
    };

    // -----------------------------------------------------------------------

    p.insertShortcodeAfter = function(content, $target)
    {
        if(!$target || $target.length < 1)
        {
            this.sendToEditor(content);
            return;
        }

        var $view = $target.closest('.shortcode-option-item');
        var $textarea = $view.find('.shortcode-text');
        var data = $textarea.val();

        var shortcodeObj = JSON.parse($target.data('json'));
        var index = shortcodeObj.strIndex, scContent = shortcodeObj.data.content;
        data = data.substring(0, index) + scContent + content + data.substring(index + scContent.length);
        
        if(!this._keepPureString)
        {
            data = this._removePureString(data);
        }

        $textarea.val(data);

        $('body').trigger('ptsc_change', [data, $view]);

        this._updateVisualItems($view, data);
    }

    p._removePureString = function (shortCodeString)
    {
        var shortcodes = this.parseShortcodes(shortCodeString);
        var value = '';
        shortcodes.forEach(function(obj, i){
            if(obj.type === 'shortcode')
            {
                value += obj.data.content;
            }
        });

        return value;
    }
    
    p.parseShortcodes = function(shortCodeString)
    {
        var shortcodeNames = this.shortcodeNames;
        var str = shortCodeString;

        var shortcodes = [];
        var obj, nameLen = shortcodeNames.length, hasShortcode, maxSteps = 200, index, dataStr = str, indexes = [];
        var i, len;
        while(true)
        {
            hasShortcode = false;
            
            for(i = 0; i < nameLen; i ++)
            {
                obj = wp.shortcode.next(shortcodeNames[i], str, 0);
                if(obj)
                {
                    hasShortcode = true;
                    
                    index = dataStr.indexOf(obj.content);
                    while(index !== undefined && index > -1 && indexes.indexOf(index) > -1)
                    {
                        index = dataStr.indexOf(obj.content, index + 1);
                    }
                    indexes.push(index);
                    shortcodes.push({index: index, data: obj, type: 'shortcode'});
                    str = str.replace(obj.content, '');
                }
            }
            
            if(!hasShortcode)
            {
                break;
            }
            
            maxSteps --;
            if(maxSteps < 0)
            {
                break;
            }
        }

        this._sortArrayByIndex(shortcodes);
        
        if(this._keepPureString)
        {
            len = shortcodes.length;

            var strings = [];

            var scObj, scStr, scIndex, scLen, tempLen = 0;
            for(i = 0; i < len; i ++)
            {
                scObj = shortcodes[i];
                scStr = scObj.data.content;
                scIndex = scObj.index;
                scLen = scStr.length;

                if(scIndex > tempLen)
                {
                    strings.push({index: tempLen, data: shortCodeString.substring(tempLen, scIndex), type: 'string'});
                    tempLen += scIndex;
                }

                tempLen += scLen;
            }

            if(tempLen !== shortCodeString.length)
            {
                strings.push({index: tempLen, data: shortCodeString.substring(tempLen), type: 'string'});
            }

            shortcodes = shortcodes.concat(strings);

            this._sortArrayByIndex(shortcodes);
        }
        
        return shortcodes;
    }

    p._sortArrayByIndex = function(arr)
    {
        var len = arr.length, obj1, obj2, i, j;
        for(i = 0; i < len - 1; i ++)
        {
            for(j = 0; j < len - i - 1; j ++)
            {
                obj1 = arr[j];
                obj2 = arr[j + 1];
                if(obj1.index > obj2.index)
                {
                    arr[j] = obj2;
                    arr[j + 1] = obj1;
                }
            }
        }
    }

    p._updateVisualItems = function($view, shortcodeStr)
    {
        if(!shortcodeStr)
        {
            shortcodeStr = $view.find('.shortcode-text').val();
        }

        var shortcodes = this.parseShortcodes(shortcodeStr)
        var $list = $view.find('.shortcode-list');
        $list.empty();
        var editInfo = $list.data('edit-info');

        shortcodes.forEach(function(obj, i){

            var itemHtml, $item, json;
            if(obj.type === 'string')
            {
                if(this._keepPureString)
                {
                    itemHtml = '<div class="shortcode-list-item type-string">';
                    itemHtml += '<textarea class="string-info" disabled>' + obj.data + '</textarea>';
                    itemHtml += '</div>';
                    $item = $(itemHtml);

                    json = {
                        data: obj.data,
                        index: i,
                        strIndex: obj.index,
                        type: 'string'
                    };
                    $item.data('json', JSON.stringify(json));
                }
            }
            else
            {
                var labels = $view.data('button-labels');

                var tag = obj.data.shortcode.tag;
                var config = this.settings[tag];
                json = {
                    data: obj.data,
                    tag: tag,
                    index: i,
                    strIndex: obj.index,
                    type: 'shortcode'
                };

                itemHtml = '<div class="shortcode-list-item type-shortcode" tabindex="-1">';
                itemHtml += '<img src="' + window.__pt_root_url + '/data/images/shortcode/ptsc-' + tag + '-shortcode.png" alt=""/>';
                itemHtml += '<span class="info">Shortcode - ' + config.title + '</span>';
                itemHtml += '<span class="hint">' + editInfo + '</span>';
                itemHtml += '<div class="shortcode-list-item-btns">';
                itemHtml += '<i class="shortcode-list-item-add" title="' + labels['add'] + '"></i>';
                itemHtml += '<i class="shortcode-list-item-duplicate" title="' + labels['duplicate'] + '"></i>';
                itemHtml += '<i class="shortcode-list-item-delete" title="' + labels['delete'] + '"></i>';
                itemHtml += '<i class="shortcode-list-item-edit" title="' + labels['edit'] + '"></i>';
                itemHtml += '</div>';
                itemHtml += '</div>';
                $item = $(itemHtml);
                $item.data('json', JSON.stringify(json));
            }
            
            $list.append($item);

        }, this);

        var $items = $list.children('.shortcode-list-item.type-shortcode');
        $items.on('dblclick', this._bindItemDblclickEvent.bind(this));

        $items.on('click', function(evt){
            $items.removeClass('selected');
            $(this).addClass('selected');

            this.focus();
        });
        
        $items.find('.shortcode-list-item-add').on('click', this._bindItemAddEvent.bind(this));
        $items.find('.shortcode-list-item-duplicate').on('click', this._bindItemDuplicateEvent.bind(this));
        $items.find('.shortcode-list-item-edit').on('click', this._bindItemEditEvent.bind(this));
        $items.find('.shortcode-list-item-delete').on('click', this._bindItemDeleteEvent.bind(this));
    }

    p._bindItemDuplicateEvent = function (evt)
    {
        var $item = $(evt.currentTarget).closest('.shortcode-list-item');
        var $view = $item.closest('.shortcode-option-item');
        var $textarea = $view.find('.shortcode-text');
        var data = $textarea.val();

        var shortcodeObj = JSON.parse($item.data('json'));
        var index = shortcodeObj.strIndex, content = shortcodeObj.data.content;
        data = data.substring(0, index) + content + content + data.substring(index + content.length);

        if(!this._keepPureString)
        {
            data = this._removePureString(data);
        }
        
        $textarea.val(data);

        $('body').trigger('ptsc_change', [data, $view]);

        this._updateVisualItems($view, data);
    }

    p._bindItemAddEvent = function (evt)
    {
        var $item = $(evt.currentTarget).closest('.shortcode-list-item');
        this._tempAddItemAfter = $item;
        this._currentView = $item.closest('.shortcode-option-item');

        this.onOpen();
    }

    p._bindItemDblclickEvent = function (evt)
    {
        var $item = $(evt.currentTarget);
        var shortcodeObj = JSON.parse($item.data('json'));
        var $view = $item.closest('.shortcode-option-item');

        this.openWith($view, shortcodeObj);
    }

    p._bindItemEditEvent = function (evt)
    {
        var $item = $(evt.currentTarget).closest('.shortcode-list-item');
        var shortcodeObj = JSON.parse($item.data('json'));
        var $view = $item.closest('.shortcode-option-item');

        this.openWith($view, shortcodeObj);
    }

    p._bindItemDeleteEvent = function (evt)
    {
        var $item = $(evt.currentTarget).closest('.shortcode-list-item');
        var $view = $item.closest('.shortcode-option-item');

        $item.remove();

        var data = this._updateDataFromVisualList($view);
        this._updateVisualItems($view, data);
    }

    p._onSortShortcodeItem = function(evt, ui)
    {
    	var $item = $(ui.item);
        var $view = $item.closest('.shortcode-option-item');

        var data = this._updateDataFromVisualList($view);
        this._updateVisualItems($view, data);
    }

    p._updateDataFromVisualList = function ($view)
    {
        if(!$view)
        {
            $view = this._currentView ? this._currentView : $('.shortcode-option-item').eq(0);
        }

        var value = '', keepPureString = this._keepPureString;
        $view.find('.shortcode-list-item').each(function(){
            var json = $(this).data('json');
            if(json)
            {
                var obj = JSON.parse(json);
                if(obj.type === 'string')
                {
                    if(keepPureString)
                    {
                        value += obj.data;
                    }
                }
                else
                {
                    value += obj.data.content;
                }
            }
        });

        var $textarea = $view.find('.shortcode-text');
        $textarea.val(value);

        $('body').trigger('ptsc_change', [value, $view]);

        return value;
    }

    win.ptShortcodeMgr = new ShortcodeGenerator();
})(jQuery, window);