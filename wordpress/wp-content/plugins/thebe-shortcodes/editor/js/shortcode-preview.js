/**
 * Created by foreverpinetree@gmail.com on 2016-08-20.
 */
(function($){
    "use strict";

    var musicPlayer = document.getElementById('music-player');
    if(musicPlayer && musicPlayer.parentNode)
    {
        musicPlayer.parentNode.removeChild(musicPlayer);
    }

    $(function(){

        $(window).on('keyup', function(evt){
            if(evt.keyCode == 27)
            {
                if(window.parent && window.parent.ptShortcodeMgr)
                {
                    window.parent.ptShortcodeMgr.closeShortcodePreview();
                }
            }
        });

        var body = document.querySelector('body');
        body.addEventListener('click', function(evt){
            evt.preventDefault();
            evt.stopPropagation();
        }, true);

        $('footer,header,.footer-gap,nav.hidden-menu,section.popup,.m-header,.header-gap,.loader-layer,.click-layer,.footer-widgets').remove();

        $(window).on('resize', window.__onPreviewResize);
        window.__onPreviewResize();
    });

    $(window).on('load', function(evt){
        window.__onPreviewResize();
        if(window.parent && window.parent.ptShortcodeMgr)
        {
            window.parent.ptShortcodeMgr.showPreviewLoading(false);
        }
    });

    window.__onPreviewResize = function(){
        var $sc = $('body').find('.ptsc');
        var h = $sc.get(0).clientHeight + 60, winHeight = $(window).height();
        if(h < winHeight)
        {
            $sc.css('top', ((winHeight - h) >> 1) + 'px');
        }
        else
        {
            $sc.css('top', '0');
        }
    }
})(jQuery);