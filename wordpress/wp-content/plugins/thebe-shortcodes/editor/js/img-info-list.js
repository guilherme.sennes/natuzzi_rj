/**
 * @author   foreverpinetree@gmail.com
 * @profile  www.3theme.com
 */

!(function($, win)
{
	"use strict";

	var instances = [];
	
	var ImageInfoList = function(element, options)
    {
		instances.push(this);

        this._element = element;
		this._element.__ptscImageInfoList = this;
		this.$element = $(element);

		this._options = options;

		this.$container = this._get('.image-info-list-container');
		this.$content = this._get('.image-info-list-content');
		this.$infoList = this._get('.image-info-items');
		this.$targetInput = this._get('.image-info-list-target-input');

		this._currentItemIndex = -1;
		this._containerParent = null;

		this._isClosed = true;

		this._replaceRid = '';

		this._allowCalls = ['reset', 'dispose'];
		
		this._imgMutipleMedia = null;
		this._imgSingleMedia = null;

		this._init();
	};

	var p = ImageInfoList.prototype;

	p._get = function (queryString)
	{
		return this.$element.find(queryString);
	}

	p._init = function ()
	{
        var $container = this.$container;

		this._get('.image-info-list-upload').on('click', function(evt){
			this._onUploadImgInfoListEvent(evt);
		}.bind(this));

		this._get('.img-info-list-preview-image > .replace-button').on('click', function(evt){
			this._openSingleMediaSelect($container);
		}.bind(this));
	
		this.$infoList.sortable({
			distance: 10
		});
		this.$infoList.on('sortupdate', this._onSortUpdate.bind(this));
		this.$infoList.on('click', function(evt){
			var target = evt.target;
			if($(target).hasClass('delete-button'))
			{
				this._onDelete(evt);
			}
			else if($(target).hasClass('edit-button'))
			{
				this._onEdit(evt);
			}
		}.bind(this));
	
		$container.find('.close-button').on('click', this._onClose.bind(this));
		$container.find('.save-button').on('click', this._onSave.bind(this));

        var metaNames = $container.data('vars');
		if(!Array.isArray(metaNames))
		{
			metaNames = [];
		}

        var removeClassByPrefix = function (element, prefix) {
            if(!element || !prefix)
            {
                return false;
            }

            var names = [];
            element.classList.forEach(function (className) {
                names.push(className);
            });

            names.forEach(function (className) {
                if(className.indexOf(prefix) === 0)
                {
                    element.classList.remove(className);
                }
            });

            return true;
        }

		metaNames.forEach(function(key){
			var $item = $container.find('.image-info-item-node[data-var="' + key + '"]');
            var type = $item.data('type');
            var $target = $item.find('.target-input');
            var $content = this.$content;

            switch(type)
            {
                case 'checkbox':
                    $target.on('change', function (evt){
                        var checked = $(this).prop('checked');
                        var className = 'iil-checkbox-' + key + '-checked';
                        if(checked)
                        {
                            $content.addClass(className);
                        }
                        else
                        {
                            $content.removeClass(className);
                        }
                    });
                    break;
                case 'select':
                    $target.on('change', function (evt){
                        var prefix = 'iil-select-' + key + '-';
                        removeClassByPrefix($content.get(0), prefix);
                        $content.addClass(prefix + $(this).val())
                    });
                    break;
            }
        }, this);
	}

	p._openSingleMediaSelect = function($container)
	{
		var imgSingleMedia = this._imgSingleMedia;

		if(!imgSingleMedia)
		{
			imgSingleMedia = win.wp.media( {multiple:false} );
			var self = this;
			imgSingleMedia.on('select', function(){
				var state = imgSingleMedia.state();
				var selection = state.get('selection');
				if ( ! selection || !selection.models || selection.models.length < 1 ) return;

				var rid = selection.models[0].id;
				self._replaceRid = rid;

				var winTop = $(win).scrollTop();
				setTimeout(function(){
					$(win).scrollTop(winTop);
				}, 0);

				self._setImageByAttachmentId( $container.find('.img-info-list-preview-image'), rid, 'thumbnail', false );
			});
		}
		
		imgSingleMedia.open();

		return false;
	}

	p._onSortUpdate = function (evt, ui)
	{
		var $imglist = $(evt.currentTarget);

		var data = [];
		$imglist.find('.uploader-image-container').each(function(){
			var jsonStr = $(this).data('json');
			if(jsonStr)
			{
				try
				{
					var obj = JSON.parse(jsonStr);
					data.push(obj);
				}
				catch(error)
				{
					//
				}
			}
		});

		this._setTargetValue(JSON.stringify(data));
	}

	p._onDelete = function (evt)
	{
		evt.stopPropagation();
		this._removeSCImgInfoListItem($(evt.target).closest('.uploader-image-container').get(0));
	}

	p._onEdit = function (evt)
	{
		evt.stopPropagation();
	
		var $container = this.$container;
		var $currentItem = $(evt.target).closest('.uploader-image-container');
		var jsonStr = $currentItem.data('json');
		var itemData;

		try
		{
			itemData = JSON.parse(jsonStr || '{}') || {};
		}
		catch(error)
		{
			itemData = {};
		}

		var rid = itemData.rid;
		this._setImageByAttachmentId( $container.find('.img-info-list-preview-image'), rid, 'thumbnail', false );

		var metaNames = $container.data('vars');
		if(!Array.isArray(metaNames))
		{
			metaNames = [];
		}

		metaNames.forEach(function(key){
			var $item = $container.find('.image-info-item-node[data-var="' + key + '"]');

			var type = $item.data('type');
			var $target = $item.find('.target-input');
			var value = itemData[key];
			if(value === undefined)
			{
				value = $item.data('default');
				if(value === undefined)
				{
					value = '';
				}
			}

			switch(type)
			{
				case 'text':
				case 'textarea':
				case 'icon':
					$target.val(value);
					break;
                case 'select':
                    $target.val(value);
                    $target.trigger('change');
                    break;
				case 'checkbox':
					var isChecked = parseInt(value) === 1;
					$target.prop('checked', isChecked);
                    $target.trigger('change');
					break;
				case 'color':
					if(!value)
					{
						$target.wpColorPicker('color', '#000000'); 
						$target.closest('.wp-picker-container').find('.color-alpha').css('background', 'rgba(0, 0, 0, 0)');
						$target.val('');
					}
					else
					{
						$target.wpColorPicker('color', value); 
					}
					break;
			}
		});
		
		this._currentItemIndex = $currentItem.index();
		this._containerParent = $container.parent().get(0);

		$('body').append($container);
		$container.addClass('show');

		var contentEle = this.$content.get(0);
		if(contentEle)
		{
			var contentHeight = contentEle.clientHeight + 30;
			$container.height(contentHeight);
		}

		$('body').addClass('sc-img-info-modal');

		this._isClosed = false;
	}

	p.close = function ()
	{
		this._onClose();
	}

	p._onClose = function (evt)
	{
		if(this._isClosed)
		{
			return;
		}

		this._isClosed = true;

		var $container = this.$container;
		$container.removeClass('show');

		this._replaceRid = '';

		var parent = this._containerParent;
		if(parent)
		{
			$(parent).append($container);
			this._containerParent = null;
		}

		$('body').removeClass('sc-img-info-modal');
	}

	p._onSave = function (evt)
	{
		if(this._isClosed)
		{
			return;
		}

		this._isClosed = true;

		var $container = this.$container;
	
		var parent = this._containerParent;
		if(parent)
		{
			$(parent).append($container);
			this._containerParent = null;
		}

		$container.removeClass('show');

		$('body').removeClass('sc-img-info-modal');
		
		var $currentItem = this.$infoList.children('.uploader-image-container').eq(this._currentItemIndex);
		var jsonStr = $currentItem.data('json');
		var itemData;
		
		try
		{
			itemData = JSON.parse(jsonStr || '{}') || {};
		}
		catch(error)
		{
			itemData = {};
		}
		
		$container.find('.image-info-item-node').each(function(){
			var key = $(this).data('var');
			var type = $(this).data('type');
			var value;

			if(type === 'checkbox')
			{
				value = $(this).find('.target-input').prop('checked') ? 1 : 0;;
			}
			else
			{
				value = $(this).find('.target-input').val();
			}

			itemData[key] = value;
		});

		if(this._replaceRid)
		{
			itemData.rid = this._replaceRid;
			this._setImageByAttachmentId( $currentItem.find('img'), this._replaceRid, 'thumbnail', true );

			this._replaceRid = '';
		}

		$currentItem.data('json', JSON.stringify(itemData));

		var data = [];
		this.$infoList.find('.uploader-image-container').each(function(){
			var jStr = $(this).data('json');
			if(jStr)
			{
				try
				{
					var obj = JSON.parse(jStr);
					data.push(obj);
				}
				catch(error)
				{
					//
				}
			}
		});
		
		this._setTargetValue(JSON.stringify(data));
	}

	p._getTargetValue = function()
	{
		var value = this.$targetInput.val();
		return decodeURI(value);
	}

	p._setTargetValue = function(dataString)
	{
		this.$targetInput.val(encodeURI(dataString));
	}

	p._setImageByAttachmentId = function ($target, rid, size, isImg)
	{
		pt_admin && pt_admin.setImageByAttachmentId( $target, rid, size, isImg );
	}

	p._onUploadImgInfoListEvent = function(evt)
	{
		if(!this._imgMutipleMedia)
		{
			this._imgMutipleMedia = window.wp.media( {multiple:true} );

			var self = this;
			this._imgMutipleMedia.on('select', function(){
				var state = self._imgMutipleMedia.state();
			    var selection = state.get('selection');
				if ( ! selection ) return;
				
				var metaNames = $(evt.currentTarget).siblings('.image-info-list-container').data('vals');
				if(!Array.isArray(metaNames))
				{
					metaNames = [];
				}

				var saveData;
			    var value = self._getTargetValue();
				if(!value)
				{
					saveData = [];
				}
				else
				{
					try
					{
						saveData = JSON.parse(value);
					}
					catch(error)
					{
						saveData = [];
					}
				}

				if(!Array.isArray(saveData))
				{
					saveData = [];
				}

			    var $imglist = self.$infoList;
			    selection.each(function(attachment) {
			    	var rid = attachment.attributes.id || '';

					var obj = {
						rid: rid
					};
					saveData.push(obj);

					self.$container.find('.image-info-item-node').each(function(){
						var key = $(this).data('var');
						var value = $(this).data('default');

						if(value !== undefined)
						{
							obj[key] = value;
						}
					});
					
					var $html = $(
						'<div class="uploader-image-container">' +
						'<div class="buttons">' +
						'<span class="edit-button"></span>' + 
						'<span class="delete-button"></span>' + 
						'</div>' +
						'<img class="image-preview" alt=""/>' + 
						'</div>'
					);

					$html.data('json', JSON.stringify(obj));

					self._setImageByAttachmentId($html.find('img'), rid, 'thumbnail', true);
					$imglist.append($html);
				});
				
				self._setTargetValue(JSON.stringify(saveData));

				var winTop = $(window).scrollTop();
				setTimeout(function(){
					$(window).scrollTop(winTop);
				}, 0);
			});
		}
		this._imgMutipleMedia.open();
		return false;
	}

	p._removeSCImgInfoListItem = function(itemImgContainer)
	{
		var img = itemImgContainer.querySelector('img');
		$(img).parent().removeClass('is-loading');
		if(img)
		{
			var imglist = itemImgContainer.parentNode;
			if(!imglist) return;

			var value = this._getTargetValue();
			if(value)
			{
				var data;
				
				try
				{
					data = JSON.parse(value);
				}
				catch(error)
				{
					data = [];
				}

				var children = $(imglist).find(".uploader-image-container");
				var len = children.length, child, index;
				for(var i = 0; i < len; i ++)
				{
					child = children[i];
					if(child === itemImgContainer)
					{
						index = i;
						break;
					}
				}

				if(index < data.length)
				{
					data.splice(index, 1);

					this._setTargetValue(JSON.stringify(data));

					$(itemImgContainer).remove();
				}
			}
		}
	}

	p.reset = function ()
	{
		this.$infoList.empty();
		this._setTargetValue('');
	}

	p.execute = function(funcName, funcParams)
    {
        if(!funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return;
        }

        if(!Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) === 'function')
        {
            return f.apply(this, funcParams);
        }
	};
	
	p.dispose = function ()
    {
        if(this._element)
        {
            this._element.__ptscImageInfoList = null;
            this._element = null;
		}
		
		var index = instances.indexOf(this);
		if(index > -1)
		{
			instances.splice(index, 1);
		}

		this._isClosed = true;
    };

	$.fn.extend({
        ptscImageInfoList:function (obj) {
            this.each(function(){
                var iil = this.__ptscImageInfoList;
				iil && iil.dispose();
				
				new ImageInfoList(this, obj || {});
            });
            return this;
        },
        ptscImageInfoListCall: function(funcName, funcParams){
            var arr = [];
            this.each(function(){
                var iil = this.__ptscImageInfoList;
                iil && iil.execute(funcName, funcParams);
            });
            return arr;
        }
	});
	
	$('#ptsc-choose-shortcode').on('click', function(evt){
		if(evt.target.id === 'ptsc-choose-shortcode' && $('body').hasClass('sc-img-info-modal'))
		{
			instances.forEach(function(iil){
				iil.close();
			});
		}
	}.bind(this));

 }(jQuery, window));