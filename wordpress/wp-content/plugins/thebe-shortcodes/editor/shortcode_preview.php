<?php

	if(!isset($_POST['shortcode']))
	{
		exit();
	}

	get_header();

	wp_enqueue_style( 'pt-sc-preview-style', PTSC_PLUGIN_URL.'css/shortcode-preview.css', false, '1.0', 'all' );
	wp_enqueue_script('pt-shortcode-preview-js', PTSC_PLUGIN_URL . 'js/shortcode-preview.js', array('jquery'), '', 'all' );

	$shortcode = $_POST['shortcode'];
	$shortcode_type = $_POST['type'];

	?>
	<div class="top-cover"></div>
	<div class="ptsc-list">
		<div class="wrap">
			<?php

			if($shortcode != '')
			{
				echo do_shortcode(htmlspecialchars_decode(stripslashes($shortcode)));
			}
			?>
		</div>
	</div>
	<?php
	get_footer(); 
?>
