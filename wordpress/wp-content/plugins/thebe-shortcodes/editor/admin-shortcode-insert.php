<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Thebe_Shortcode_Admin_Insert class
 */
class Thebe_Shortcode_Admin_Insert
{

	/**
	 * __construct function
	 *
	 * @access public
	 * @return  void
	 */
	public function __construct() {
		add_action( 'admin_footer', array( $this, 'ptsc_popup_html' ), 20 );
	}

	public function ptsc_build_fields($key, $param) 
	{
		$extend_class = isset($param['extend_class']) ? ' '.$param['extend_class'] : '';
		$type = $param['type'];

		if( $type === 'advanced_button' )
		{
			$extend_class .= ' show-advanced-button';
		}
		elseif( $type === 'image_info_list' )
		{
			$extend_class .= ' type-image-info-list';
		}
		
		$html = '<tr data-name="'.esc_attr($key).'" class="shortcode-item'.esc_attr($extend_class).'">';
		
		if( $type === 'advanced_button' )
		{
			$html .= '<td class="sc-label">' . $param['label'] . '</td>';
		}
		elseif($type != 'delimiter')
		{
			$html .= '<td class="sc-label">' . $param['label'] . ':</td>';
		}
		else
		{
			$html .= '<td class="sc-label delimiter"><hr/></td>';
		}
		
		$help = isset($param['help']) ? $param['help'] : '';
		$help_data = $help != '' ? '<span class="pt-admin-help-'.esc_attr($type).' pt-admin-help-icon pt-admin-help-sc" data-id="'.esc_attr(strtolower($help)).'"></span>' : '';
		$std = isset( $param['std'] ) ? $param['std'] : '';
		
		switch($type)
		{
			case 'des' :

				// prepare
				$output = '<td><label class="screen-reader-text">' . $param['label'] . '</label>';
				$output .= '<span class="thecs-sc-form-desc">' . $param['desc'] . '</span>';
				$output .= '</td>';
				
				// append
				$html .= $output;

				break;
				
			case 'advanced_button' :
				// prepare

				$output = '<td><label class="screen-reader-text">' . $param['label'] . '</label>';
				$output .= '</td>';
				
				// append
				$html .= $output;

				break;
			case 'text' :

				// prepare
				$placeholder = isset($param['placeholder']) ? $param['placeholder'] : '';
				if($placeholder != '')
				{
					$placeholder = 'placeholder="' . esc_attr($placeholder) . '"';
				}

				$output = '<td><label class="screen-reader-text">' . $param['label'] . '</label>';
				$output .= '<input type="text" class="thebe-sc-form-text thebe-sc-input" '.$placeholder.' name="' . esc_attr($key) . '" data-id="' . esc_attr(strtolower($key)) . '" value="' . esc_attr($std) . '" data-default="'.esc_attr($std).'" />';
				$output .= '<span class="thebe-sc-form-desc">' . $param['desc'] . '</span>';
				$output .= $help_data;
				$output .= '</td>';
				
				// append
				$html .= $output;

				break;

			case 'icon' :

				// prepare
				$placeholder = isset($param['placeholder']) ? $param['placeholder'] : '';
				if($placeholder != '')
				{
					$placeholder = 'placeholder="' . esc_attr($placeholder) . '"';
				}

				$output = '<td><label class="screen-reader-text">' . $param['label'] . '</label>';
				$output .= '<input type="text" class="thebe-sc-form-icon thebe-sc-input icon-input" '.$placeholder.' name="' . esc_attr($key) . '" data-id="' . esc_attr(strtolower($key)) . '" value="' . esc_attr($std) . '" data-default="'.esc_attr($std).'" />';
				$output .= '<div class="icons-insert-button icons-btn-pe" data-id="pe" title="'.esc_attr__('Pe icon 7', 'thebe-shortcodes').'"></div>';
				$output .= '<div class="icons-insert-button icons-btn-fontawesome" data-id="fontawesome" title="'.esc_attr__('FontAwesome', 'thebe-shortcodes').'"></div>';
				$output .= '<span class="thebe-sc-form-desc">' . $param['desc'] . '</span>';
				$output .= $help_data;
				$output .= '</td>';
				
				// append
				$html .= $output;

				break;

			case 'color' :

				// prepare
				$output = '<td><label class="screen-reader-text">' . $param['label'] . '</label>';
				$output .= '<input type="text" class="sc-color-field thebe-sc-form-color thebe-sc-input" data-alpha="true" data-custom-width="50px" name="' . esc_attr($key) . '" data-id="' . esc_attr(strtolower($key)) . '" value="' . esc_attr($std) . '" data-default="'.esc_attr($std).'" />';
				$output .= '<span class="thebe-sc-form-desc">' . $param['desc'] . '</span>';
				$output .= $help_data;
				$output .= '</td>';
				
				// append
				$html .= $output;

				break;

			case 'textarea' :

				// prepare
				$rows = isset($param['rows']) ? $param['rows'] : 10;
				$cols = isset($param['cols']) ? $param['cols'] : 30;
				
				$output = '<td><label class="screen-reader-text">' . $param['label'] . '</label>';
				$output .= '<textarea rows="'.esc_attr($rows).'" cols="'.esc_attr($cols).'" name="' . esc_attr($key) . '" data-id="' . esc_attr(strtolower($key)) . '" class="thebe-sc-form-textarea thebe-sc-input">' . $std . '</textarea>';
				$output .= '<span class="thebe-sc-form-desc">' . $param['desc'] . '</span>';
				$output .= $help_data;
				$output .= '</td>';

				// append
				$html .= $output;

				break;

			case 'select' :

				// prepare
				
				$previews = isset($param['previews']) ? $param['previews'] : NULL;
				$preview_class = $previews ? 'pt-select select-previewable ' : '';

				$only_available = isset($param['only_available']) ? $param['only_available'] : null;
				$available_value = $only_available != null ? json_encode($only_available) : '';
				
				$output = '<td><label class="screen-reader-text">' . $param['label'] . '</label>';
				$output .= '<select name="' . esc_attr($key) . '" data-id="' . esc_attr(strtolower($key)) . '" data-available="'.esc_attr($available_value).'" class="'.esc_attr($preview_class).'thebe-sc-form-select thebe-sc-input pt-select-sc">';

				foreach( $param['options'] as $value => $option )
				{
					$preview_id = ($previews && isset($previews[$value])) ? ' data-previewid="'.esc_attr($previews[$value]).'"' : '';
					$select_attr = $std == $value ? 'selected="selected" data-default="true"' : '';
					$output .= '<option'.$preview_id.' value="' . esc_attr($value) . '" '.$select_attr.'>' . $option . '</option>';
				}

				$output .= '</select>';
				
				$select_des_class = $previews ? 'pt-select-sc-des ' : '';
				$output .= '<span class="'.esc_attr($select_des_class).'thebe-sc-form-desc">' . $param['desc'] . '</span>';
				$output .= $help_data;
				$output .= '</td>';

				// append
				$html .= $output;

				break;

			case 'image_select' :

				$only_available = isset($param['only_available']) ? $param['only_available'] : null;
				$available_value = $only_available != null ? json_encode($only_available) : '';

				// prepare

				$output = '<td class="pt-image-select pt-sc-image-select"><label class="screen-reader-text">' . $param['label'] . '</label>';
				$output .= '<input type="hidden" name="' . esc_attr($key) . '" data-id="' . esc_attr(strtolower($key)) . '" data-available="'.esc_attr($available_value).'" class="image-select-target-input thebe-sc-input" value="'.esc_attr( $std ).'">';
				$output .= '<span class="thebe-sc-form-desc">' . $param['desc'] . '</span>';
				$output .= $help_data;

				$output .= '<ul class="image-select-list">';

				$option_id_prefix = '';
				if( isset( $param['option_id_prefix'] ) )
				{
					$option_id_prefix = $param['option_id_prefix'].'_';
				}

				foreach( $param['options'] as $value )
				{
					$preview_id = $option_id_prefix.$key.'_'.$value;

					if( $value == $std)
					{
						$output .= '<li data-value="' . esc_attr($value) . '" class="selected" data-img-name="'.esc_attr( $preview_id ).'"></li>';
					}
					else
					{
						$output .= '<li data-value="' . esc_attr($value) . '" data-img-name="'.esc_attr( $preview_id ).'"></li>';
					}
				}

				$output .= '</ul>';
				$output .= '</td>';

				// append
				$html .= $output;

				break;

			case 'checkbox' :
			
				$not_save = isset($param['not_save']) ? $param['not_save'] : '0';
				$not_save_class = $not_save == '1' ? ' not-save' : '';

				$only_available = isset($param['only_available']) ? $param['only_available'] : null;
				$available_value = $only_available != null ? json_encode($only_available) : '';

				// prepare
				$output = '<td><label class="screen-reader-text">' . $param['label'] . '</label>';
				$output .= '<input type="checkbox" name="' . esc_attr($key) . '" data-id="' . esc_attr(strtolower($key)) . '" data-available="'.esc_attr($available_value).'" class="thebe-sc-form-checkbox thebe-sc-input'.esc_attr($not_save_class).'"' . ( $std ? 'checked' : '' ) . '>';
				$output .= '<span class="thebe-sc-checkbox-desc thebe-sc-form-desc">' . $param['desc'] . '</span>';
				$output .= $help_data;
				$output .= '</td>';

				// append
				$html .= $output;
				
				break;
				
			case 'image' :

				$empty = "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";

				// prepare
				$output = '<td><label class="screen-reader-text">' . $param['label'] . '</label>';
				$output .= '<input type="hidden" name="' . esc_attr($key) . '" data-id="' . esc_attr(strtolower($key)) . '" class="thebe-sc-image-upload-input thebe-sc-input">';
				$output .= '<div class="ptsc-image-upload-button button-primary ptsc-image-upload">'.esc_html__('Upload', 'thebe-shortcodes').'</div>';
				$output .= '<div class="uploader-image-container shortcode-input ptsc-image-upload-delete empty" style="display:none;">';
				$output .= '<span class="ptsc-image-upload-delete-button upload-delete-button"></span>';
				$output .= '<img class="uploader-image-preview shortcode" alt="" src="'.esc_attr($empty).'" data-empty="'.esc_attr($empty).'"/></div>';
				$output .= '<span class="thebe-sc-form-desc">' . $param['desc'] . '</span>';
				$output .= $help_data;
				$output .= '</td>';
				
				// append
				$html .= $output;

				break;

			case 'imagelist' :

				$empty = "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";

				// prepare
				$output = '<td><label class="screen-reader-text">' . $param['label'] . '</label>';
				$output .= '<input type="hidden" name="' . esc_attr($key) . '" data-id="' . esc_attr(strtolower($key)) . '" class="image-list-target-input thebe-sc-input">';
				$output .= '<div class="option-item-upload-button button-primary imagelist-upload">'.esc_html__('Add Image', 'thebe-shortcodes').'</div>';
				$output .= '<div class="image-list-container sortable"></div>';
				$output .= '<span class="thebe-sc-form-desc">' . $param['desc'] . '</span>';
				$output .= $help_data;
				$output .= '</td>';
				
				// append
				$html .= $output;

				break;

			case 'image_info_list' :

				// prepare
				$output = '<td><label class="screen-reader-text">' . $param['label'] . '</label>';
				$output .= '<input type="hidden" name="' . esc_attr($key) . '" data-id="' . esc_attr(strtolower($key)) . '" class="image-info-list-target-input thebe-sc-input">';
				$output .= '<div class="option-item-upload-button button-primary image-info-list-upload">'.esc_html__('Add Image', 'thebe-shortcodes').'</div>';
				$output .= $help_data;
				$output .= '<div class="image-info-items">';
				$output .= '</div>';

				$img_info_data = isset($param['meta']) ? $param['meta'] : NULL;

				$img_info_vars = array();
				if( $img_info_data )
				{
					foreach( $img_info_data as $img_info_item_var => $img_info_item_data )
					{
						$img_info_vars[] = $img_info_item_var;
					}
				}

				$output .= '<div class="image-info-list-container" data-vars="'.esc_attr( json_encode( $img_info_vars ) ).'">';
				
				$output .= '<div class="buttons">';
				$output .= '<span class="close-button control-button">'.esc_html( 'Cancel', 'thebe-shortcodes' ).'</span>';
				$output .= '<span class="save-button control-button">'.esc_html( 'Confirm', 'thebe-shortcodes' ).'</span>';
				$output .= '</div>';

				$output .= '<div class="image-info-list-content">';

				$output .= '<div class="img-info-list-preview-image">';
				$output .= '<i class="replace-button"></i>';
				$output .= '</div>';
				
				if( $img_info_data )
				{
					foreach( $img_info_data as $img_info_item_var => $img_info_item_data )
					{
						$img_info_std = isset( $img_info_item_data['std'] ) ? $img_info_item_data['std'] : null;
						$img_info_item_des = isset( $img_info_item_data['des'] ) ? $img_info_item_data['des'] : '';

						$node_class = 'image-info-item-node';
						if( $img_info_item_data['type'] === 'icon' )
						{
							$node_class .= ' icon-selector';
						}

						$default_attr = $img_info_std !== null ? ' data-default="'.esc_attr( $img_info_std ).'"' : '';
						$output .= '<div class="'.esc_attr( $node_class ).'" data-type="'.esc_attr( $img_info_item_data['type'] ).'" data-var="'.esc_attr( $img_info_item_var ).'"'.$default_attr.'>';

						switch ( $img_info_item_data['type'] )
						{
							case 'text':
								$placeholder = isset($img_info_item_data['placeholder']) ? $img_info_item_data['placeholder'] : '';
								if( $placeholder )
								{
									$placeholder = 'placeholder="' . esc_attr($placeholder) . '"';
								}
								
								$output .= '<span class="item-title">'.esc_html( $img_info_item_data['title'] ).'</span>';
								$output .= '<input type="text" class="type-text target-input not-save" '.$placeholder.' value="'.esc_attr( $img_info_std ).'">';
								$output .= '<span class="item-des">'.esc_html( $img_info_item_des ).'</span>';
								break;
							case 'textarea':
								$placeholder = isset($img_info_item_data['placeholder']) ? $img_info_item_data['placeholder'] : '';
								if( $placeholder )
								{
									$placeholder = 'placeholder="' . esc_attr($placeholder) . '"';
								}

								$rows = isset($img_info_item_data['rows']) ? $img_info_item_data['rows'] : 2;
								
								$output .= '<span class="item-title">'.esc_html( $img_info_item_data['title'] ).'</span>';
								$output .= '<textarea class="type-textarea target-input not-save" '.$placeholder.'" rows="'.esc_attr( $rows ).'">'.esc_html( $img_info_std ).'</textarea>';
								$output .= '<span class="item-des">'.esc_html( $img_info_item_des ).'</span>';
								break;
							case 'select':
								$output .= '<span class="item-title">'.esc_html( $img_info_item_data['title'] ).'</span>';
								$output .= '<select class="type-select target-input not-save">';
								$options = isset($img_info_item_data['options']) ? $img_info_item_data['options'] : null;
								if( $options )
								{
									foreach( $options as $value => $option )
									{
										$select_attr = $img_info_std == $value ? 'selected="selected" data-default="true"' : '';
										$output .= '<option value="' . esc_attr($value) . '" '.$select_attr.'>' . $option . '</option>';
									}
								}
								$output .= '</select>';
								$output .= '<span class="item-des">'.esc_html( $img_info_item_des ).'</span>';
								break;
							case 'color':
								$output .= '<span class="item-title">'.esc_html( $img_info_item_data['title'] ).'</span>';
								$output .= '<input type="text" class="type-color target-input not-save" data-alpha="true" data-custom-width="50px" value="' . esc_attr($img_info_std) . '">';
								$output .= '<span class="item-des">'.esc_html( $img_info_item_des ).'</span>';
								break;
							case 'checkbox':
								$output .= '<span class="item-title">'.esc_html( $img_info_item_data['title'] ).'</span>';
								$output .= '<input type="checkbox" class="type-checkbox target-input not-save" ' . ( $img_info_std ? 'checked' : '' ) . '>';
								$output .= '<span class="item-des">'.esc_html( $img_info_item_des ).'</span>';
								break;
							case 'icon':
								$placeholder = isset($img_info_item_data['placeholder']) ? $img_info_item_data['placeholder'] : '';
								if( $placeholder )
								{
									$placeholder = 'placeholder="' . esc_attr($placeholder) . '"';
								}

								$output .= '<span class="item-title">'.esc_html( $img_info_item_data['title'] ).'</span>';
								$output .= '<input type="text" class="type-icon target-input not-save icon-input" '.$placeholder.' value="' . esc_attr($img_info_std) . '">';
								$output .= '<div class="icons-insert-button icons-btn-lnr" data-id="lnr" title="'.esc_attr__('Linearicons', 'thebe-shortcodes').'"></div>';
								$output .= '<div class="icons-insert-button icons-btn-pe" data-id="pe" title="'.esc_attr__('Pe icon 7', 'thebe-shortcodes').'"></div>';
								$output .= '<div class="icons-insert-button icons-btn-fontawesome" data-id="fontawesome" title="'.esc_attr__('FontAwesome', 'thebe-shortcodes').'"></div>';
								$output .= '<span class="item-des">'.esc_html( $img_info_item_des ).'</span>';
								break;
						}

						$output .= '</div>';
					}
				}

				$output .= '</div>';

				$output .= '</div>';
				$output .= '<span class="thebe-sc-form-desc">' . $param['desc'] . '</span>';
				
				$output .= '</td>';
				
				// append
				$html .= $output;

				break;
				
			case 'delimiter' :

				// prepare
				$output = '<td><hr/></td>';
				
				// append
				$html .= $output;

				break;

			default :
				break;
		}
		$html .= '</tr>';

		return $html;
	}

	/**
	 * Popup
	 */
	function ptsc_popup_html() 
	{
		global $pagenow;
		include(PTSC_PLUGIN_DIR . 'config.php');

		// Only run in add/edit page
		if ( in_array( $pagenow, array( 'post.php', 'page.php', 'post-new.php', 'post-edit.php' ) ) ) 
		{
			$custom_js = '!(function($, win){
				win.__sc_dir_url = "'.esc_url( PTSC_PLUGIN_URL ).'";
				win.__sc_wp_version = "'.esc_js( $GLOBALS["wp_version"] ).'";
				win.__sc_wp_version_lt_4_6 = '.(version_compare( $GLOBALS["wp_version"], "4.6-alpha", "<" ) ? 1 : 0).';
				win.__sc_code_bracket_l = "'.esc_js( PTSC_CODE_BRACKET_L ).'";
				win.__sc_code_bracket_r = "'.esc_js( PTSC_CODE_BRACKET_R ).'";
				win.__sc_code_quote = "'.esc_js( PTSC_CODE_QUOTE ).'";
				win.__sc_code_eol = "'.esc_js( PTSC_CODE_EOL ).'";
				win.__sc_code_lt = "'.esc_js(PTSC_CODE_LT).'";
				win.__sc_code_gt = "'.esc_js(PTSC_CODE_GT).'";
				$(function(){
					win.ptShortcodeMgr.init();
				});
			}(jQuery, window));';

			wp_add_inline_script( 'pt-sc-popup-admin-js', $custom_js, 'before' );
			?>

			<div id="ptsc-shortcode-manager">

				<div id="ptsc-choose-shortcode">
					<div id="ptsc-shortcode-header">
						<div id="ptsc-shortcode-status-title" data-edit="<?php esc_attr_e('Edit Shortcode', 'thebe-shortcodes'); ?>" data-pb="<?php esc_attr_e('Edit Content', 'thebe-shortcodes'); ?>"></div>

						<div class="ptsc-exit-button tran-bg-color"><i class="fa fa-times" aria-hidden="true"></i></div>

						<div id="ptsc-preview-container">
							<div class="top-bar">
								<span><?php echo esc_html__('Shortcode Preview', 'thebe-shortcodes').' <i id="ptsc-preview-warning" class="fa fa-info-circle" aria-hidden="true" title="'.esc_html__('Note: after finishing preview please don\'t click the browser back key, you should press the ESC key or just click the Close Button which on the right side to close this preview window.', 'thebe-shortcodes').'"></i>'; ?></span>
								<div class="preview-button-list">
									<div class="preview-close-btn ptsc-btn tran-all"><i class="fa fa-times" aria-hidden="true"></i><span><?php esc_html_e('Close', 'thebe-shortcodes'); ?></span></div>
								</div>
								<div id="ptsc-preview-loading"><i class="fa fa-circle-o-notch fa-spin fa-lg fa-fw"></i><span><?php esc_html_e('Loading...', 'thebe-shortcodes'); ?></span></div>
							</div>
							<iframe id="ptsc-preview"></iframe>
						</div>

						<!-- select shortcode -->
						<div class="ptsc-shortcode-select">
							<select class="selectpicker pt-select" name="ptsc-shortcode" id="select-ptsc-shortcode">
								<option value=""><?php esc_html_e('Select Shortcode', 'thebe-shortcodes'); ?></option>
								<?php foreach( $thebe_shortcode_configs as $shortcode ) {
									if( isset($shortcode['visible']) )
									{
										if( !$shortcode['visible'] ) continue;
									}
									echo '<option data-title="' . esc_attr($shortcode['title']) . '" value="' . esc_attr($shortcode['id']) . '">' . esc_html( $shortcode['title'] ) . '</option>';
								} ?>
							</select>

							<span id="ptsc-shortcode-title"></span>
						</div>

					</div>

					<!-- item list (left panel) -->
					<div id="ptsc-items-container">
						<div class="main-item tran-color tran-border">
							<span><?php esc_html_e('General Settings', 'thebe-shortcodes'); ?></span>
						</div>
						<div class="child-item-container">
							<ul class="item-list"></ul>
							<input class="ptsc-add-item-button" type="button" value="<?php esc_attr_e('Add Item', 'thebe-shortcodes'); ?>" />
						</div>
					</div>

					<!-- shortcode setting (right panel) -->
					<div id="ptsc-shortcode-wrap" class="wrap ptsc-shortcode-wrap">

					<?php

						$html = '';
						$child_exists = false;

						//begin to build each shortcode
						foreach( $thebe_shortcode_configs as $key => $shortcode ) 
						{
							if(isset($shortcode['visible']))
							{
								if($shortcode['visible'] == false || $shortcode['visible'] == '0') 
								{
									continue;
								}
							}
						
							$shortcode_template = ' data-shortcode-template="' . esc_attr($shortcode['template']) . '"';
							$child_exists = array_key_exists('child_shortcode', $shortcode );
							$child_exists_class = '';
							if($child_exists)
							{
								$shortcode_template .= ' data-shortcode-child-template="' . esc_attr($shortcode['child_shortcode']['template']) . '"';
								$child_exists_class = ' ptsc-exists-child';
							}

							$html .= '<div id="' . esc_attr($shortcode['id']) . '" class="ptsc-shortcode-type'.esc_attr($child_exists_class).'" ' . $shortcode_template . '>';

							$html .= '<table data-type="main_shortcode"><tbody>';
							$html .= '<tr class="setting-title"><td class="ptsc-item-title">'.esc_html__('General Settings', 'thebe-shortcodes').'</td><td></td></tr>';
							foreach( $shortcode['params'] as $key => $param )
							{
								$html .= $this->ptsc_build_fields($key, $param);
							}
							$html .= '</tbody></table>';
							
							if($child_exists ) 
							{
								$html .= (isset($shortcode['child_shortcode']['shortcode']) ? $shortcode['child_shortcode']['shortcode'] : null);
								$shortcode['params'] = $shortcode['child_shortcode']['params'];
								$html .= '<div class="ptsc-sortable">';
								$html .= '<table id="clone-' . esc_attr($shortcode['id']) . '" class="hidden ptsc-clone-template"><tbody>';
								$html .= '<tr class="setting-title"><td class="ptsc-item-title">'.esc_html__('Item Settings', 'thebe-shortcodes').'</td><td></td></tr>';
								foreach( $shortcode['params'] as $key => $param )
								{
									$html .= $this->ptsc_build_fields($key, $param);
								}
								$html .= '</tbody></table></div>';
							}

							$html .= '</div>';
						}

						echo $html;
					?>

					</div>

					<div class="submit">
						
						<div class="button-list">
							<div id="ptsc-preview-shortcode" class="ptsc-btn preview-btn tran-all"><?php esc_html_e('Preview', 'thebe-shortcodes'); ?></div>
							<div id="ptsc-insert-shortcode" class="ptsc-btn ptsc-insert-shortcode-button insert tran-all" data-insert="<?php esc_attr_e('Insert Shortcode', 'thebe-shortcodes'); ?>" data-edit="<?php esc_attr_e('Update Shortcode', 'thebe-shortcodes'); ?>" data-pb="<?php esc_attr_e('Done', 'thebe-shortcodes'); ?>"></div>
							<div id="ptsc-cancel-shortcode-insert" class="ptsc-btn ptsc-cancel-shortcode-insert cancle tran-all"><?php esc_html_e('Cancel', 'thebe-shortcodes'); ?></div>
							
						</div>
					</div>

				</div>
			</div>
		<?php
		}
	}
}

new Thebe_Shortcode_Admin_Insert();