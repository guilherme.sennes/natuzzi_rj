<?php
	/*
	* -------------------------------------------
	* ----------   shortcodes list   ------------
	* @author: foreverpinetree@gmail.com
	* @version: 1.0
	*/


	//---------------------------------------
	//-----------    mix-box    -------------
	//---------------------------------------
	add_shortcode('mix_box', 'thebe_shortcode_mix_box');
	function thebe_shortcode_mix_box($attr, $content)
	{
		extract(shortcode_atts(array(
			'title_img' => '',
			'title' => '',
			'subtitle' => '',
			'title_center' => '0',
			'content_center' => '0',
			'wide' => '0',
			'append_class' => '',
		), thebe_shortcode_parse_attr($attr)));

		$box_class = '';

		if( $title_center === '1' )
		{
			$box_class .= ' t-center';
		}

		if( $content_center === '1' )
		{
			$box_class .= ' content-center';
		}

		if( $wide === '1' )
		{
			$box_class .= ' wide';
		}

		if( $append_class )
		{
			$box_class .= ' '.$append_class;
		}
		
		$html = '<div class="ptsc sc-mixbox'.esc_attr( $box_class ).'">';

		$html .= '<div class="section-title">';

		if( $title_img )
		{
			$img_obj = thebe_get_image(array(
				'rid' => $title_img,
				'max_width' => 1920,
			));

			if( $img_obj )
			{
				$html .= '<img src="'.esc_url( $img_obj['thumb'] ).'" alt="" />';
			}
		}

		if( $title )
		{
			$html .= '<h1 class="title">'.thebe_kses_content( $title, false ).'</h1>';
		}

		if( $subtitle )
		{
			$html .= '<div class="sub-title">'.thebe_kses_content( $subtitle, false ).'</div>';
		}

		$html .= '</div>';
		
		$html .= '<div class="wrap">';
		$html .= do_shortcode( wp_specialchars_decode( stripslashes($content), ENT_QUOTES ) );
		$html .= '</div>';
		
		$html .= '</div>';
		
		return $html;
	}

	//-------------------------------------------------
	//--------------    mix-box-item    ---------------
	//-------------------------------------------------
	add_shortcode('mix_box_item', 'thebe_shortcode_mix_box_item');
	function thebe_shortcode_mix_box_item($attr, $content)
	{
		extract(shortcode_atts(array(
			'item_head_type' => 'images',

			'item_img_list' => '',
			'item_head_icon' => '',
			'item_head_icon_color' => '',

			'item_title' => '',
			'item_title_size' => '3',
			'item_title_color' => '',

			'item_subtitle' => '',

			'item_text_color' => 'none',
			'item_bg_color' => '',

			'item_footer_type' => '1',

			'item_link_text' => '',
			'item_link' => '',
			'item_link_new_tab' => '0',

			'item_contact7_shortcode' => '',

			'lat' => '0',
			'lng' => '0',
			'zoom' => '8',
			'marker_title' => '',
			'marker_lat' => '0',
			'marker_lng' => '0',

			'social1' => '',
			'social_link1' => '',
			'social2' => '',
			'social_link2' => '',
			'social3' => '',
			'social_link3' => '',

			'module_width' => '1',
		), thebe_shortcode_parse_attr($attr)));

		$item_class = 'item';

		if( $item_bg_color )
		{
			$item_class .= ' has-bg-color';
		}

		$html = '<div class="'.esc_attr( $item_class ).'" data-w="'.esc_attr( $module_width ).'"';

		if( $item_text_color !== 'none' )
		{
			$html .= ' data-text-color="'.esc_attr( $item_text_color ).'"';
		}

		$html .= '>';
		$html .= '<div class="inner-wrap">';

		if( $item_bg_color )
		{
			$html .= '<div class="bg-color" data-color="'.esc_attr( $item_bg_color ).'"></div>';
		}

		if( $item_head_type === 'icon' )
		{
			if( $item_head_icon )
			{
				$html .= '<div class="icon" data-color="'.esc_attr( $item_head_icon_color ).'">';
				$html .= '<i class="'.esc_attr( $item_head_icon ).'"></i>';
				$html .= '</div>';
			}
		}
		else
		{
			if( $item_img_list )
			{
				$img_list_arr = explode( ',', $item_img_list );

				$html .= '<div class="img">';

				foreach( $img_list_arr as $img_rid )
				{
					$img_obj = thebe_get_image(array(
						'rid' => $img_rid,
						'max_width' => floatval( $module_width ) < 0.5 ? 600 : 1280,
					));

					if( $img_obj )
					{
						$html .= '<img src="'.esc_url( $img_obj['thumb'] ).'" alt="" />';
					}
				}

				$html .= '</div>';
			}
		}

		if( $item_title || $item_subtitle || $content || 
			( $item_footer_type === '2' && $item_link_text ) || 
			( $item_footer_type === '3' && $item_contact7_shortcode ) || 
			$item_footer_type === '4' || 
			($item_footer_type === '5' && ( $social1 || $social2 || $social3 ) ) 
		)
		{
			$html .= '<div class="text">';

			if( $item_title )
			{
				if( $item_title_size === '0' )
				{
					$html .= '<h1 class="large" data-color="'.esc_attr( $item_title_color ).'"><span>'.thebe_kses_content( $item_title, false ).'</span></h1>';
				}
				else
				{
					$html .= '<h'.esc_html( $item_title_size ).' data-color="'.esc_attr( $item_title_color ).'"><span>'.thebe_kses_content( $item_title, false ).'</span></h'.esc_html( $item_title_size ).'>';
				}
			}

			if( $item_subtitle )
			{
				$html .= '<div class="sub-title">'.thebe_kses_content( $item_subtitle, false ).'</div>';
			}

			if( $content )
			{
				$html .= '<div class="intro">'.do_shortcode( wp_specialchars_decode( stripslashes($content), ENT_QUOTES ) ).'</div>';
			}

			if( $item_footer_type === '1' )
			{
				//
			}
			elseif( $item_footer_type === '2' )
			{
				$item_link_target = $item_link_new_tab === '1' ? '_blank' : '_self';

				$item_link_class = 'btn';
				if( stripos( $item_link, 'ajax=1' ) !== false )
				{
					$item_link_class .= ' ajax-link';
				}

				$html .= '<a class="'.esc_attr( $item_link_class ).'" href="'.esc_url( $item_link ).'" target="'.esc_attr( $item_link_target ).'"><span>'.esc_html( $item_link_text ).'</span></a>';
			}
			elseif( $item_footer_type === '3' )
			{
				if( $item_contact7_shortcode )
				{
					$html .= do_shortcode( shortcode_unautop($item_contact7_shortcode) );
				}
			}
			elseif( $item_footer_type === '4' )
			{
				$map_style = thebe_get_theme_option('thebe_map_style', '');
				$map_marker = thebe_get_theme_option('thebe_map_marker', '');

				$img_obj = thebe_get_image(array(
					'rid' => $map_marker,
				));
		
				$map_marker_url = '';
				if( $img_obj )
				{
					$map_marker_url = $img_obj['thumb'];
				}

				$html .= '<div class="map"><div class="map-container" data-style="'.esc_attr($map_style).'" data-lat="'.esc_attr($lat).'" data-lng="'.esc_attr($lng).'" data-zoom="'.esc_attr($zoom).'" data-markertitle="'.esc_attr($marker_title).'" data-markerlat="'.esc_attr($marker_lat).'" data-markerlng="'.esc_attr($marker_lng).'" data-markericon="'.esc_attr($map_marker_url).'"></div></div>';

				thebe_enqueue_map();
			}
			elseif( $item_footer_type === '5' )
			{
				$html .= '<div class="pt-social"><ul>';

				$item_link_target = $item_link_new_tab === '1' ? '_blank' : '_self';

				if( $social1 )
				{
					$html .= '<li><a href="'.esc_url( $social_link1 ).'" target="'.esc_attr( $item_link_target ).'"><i class="'.esc_attr( $social1 ).'"></i></a></li>';
				}

				if( $social2 )
				{
					$html .= '<li><a href="'.esc_url( $social_link2 ).'" target="'.esc_attr( $item_link_target ).'"><i class="'.esc_attr( $social2 ).'"></i></a></li>';
				}

				if( $social3 )
				{
					$html .= '<li><a href="'.esc_url( $social_link3 ).'" target="'.esc_attr( $item_link_target ).'"><i class="'.esc_attr( $social3 ).'"></i></a></li>';
				}

				$html .= '</ul></div>';
			}

			$html .= '</div>';
		}
		

		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}


	//--------------------------------------
	//-----------    slider    -------------
	//--------------------------------------
	add_shortcode('slider', 'thebe_shortcode_slider');
	function thebe_shortcode_slider($attr, $content)
	{
		extract(shortcode_atts(array(
			'autoplay' => '1',
			'autoplay_video' => '1',
			'duration' => '5000',
			'transition_mode' => '1',
			'display_type' => 'boxed',
			'small_dots' => '0',
			'text_position' => 't-center',
			'header_color' => 'white',
			'height_type' => 'standard',
			'mobile_type' => '0',
			'append_class' => '',
		), thebe_shortcode_parse_attr($attr)));

		$slider_class = 'ptsc sc-slider '.$display_type.' '.$text_position;

		if( $height_type === 'short' )
		{
			$slider_class .= ' h-short';
		}

		if( $small_dots === '1' )
		{
			$slider_class .= ' small-dots';
		}

		if( $mobile_type === '1' )
		{
			$slider_class .= ' mobile-list';
		}
		elseif( $mobile_type === '2' )
		{
			$slider_class .= ' mobile-fullscreen';
		}

		if( $transition_mode === '2' )
		{
			$slider_class .= ' mode-fade';
		}

		if( $append_class )
		{
			$slider_class .= ' '.$append_class;
		}

		$html = '<div class="'.esc_attr( $slider_class ).'" data-autoplay="'.esc_attr( $autoplay ).'" data-duration="'.esc_attr( $duration ).'" data-header-color="'.esc_attr( $header_color ).'" data-video-autoplay="'.esc_attr( $autoplay_video ).'">';

		global $thebe_global_sc_slider_display;
		$thebe_global_sc_slider_display = $display_type;

		$html .= '<div class="wrap">';
		$html .= do_shortcode( wp_specialchars_decode( stripslashes($content), ENT_QUOTES ) );
		$html .= '</div>';

		$html .= '</div>';
		
		return $html;
	}


	//------------------------------------------------
	//--------------    slider-item    ---------------
	//------------------------------------------------
	add_shortcode('slider_item', 'thebe_shortcode_slider_item');
	function thebe_shortcode_slider_item($attr, $content)
	{
		extract(shortcode_atts(array(
			'item_use_video' => '0',
			'item_video_link' => '',
			'item_video_type' => '1',
			'item_video_volume' => '1',
			'item_img' => '',
			'item_title' => '',
			'item_title_size' => '1',
			'item_intro' => '',
			'item_link' => '#',
			'item_link_text' => '',
			'item_link_new_tab' => '0',
		), thebe_shortcode_parse_attr($attr)));

		$video_class = $item_use_video === '1' ? ' has-video' : '';
		$html = '<div class="item '.esc_attr( $video_class ).'">';
		$html .= '<div class="text">';

		if( $item_title )
		{
			if( $item_title_size === '0' )
			{
				$html .= '<h1 class="large">';
			}
			else
			{
				$html .= '<h'.esc_html( $item_title_size ).'>';
			}

			$html .= thebe_kses_content( $item_title, false );
			
			$html .= '</h'.esc_html( $item_title_size === '0' ? '1' : $item_title_size ).'>';
		}

		if( $item_intro )
		{
			$html .= '<p class="intro">'.thebe_kses_content( $item_intro, false ).'</p>';
		}

		if( $item_link_text )
		{
			$item_link_target = $item_link_new_tab === '1' ? '_blank' : '_self';

			$item_link_class = 'small-btn';
			if( stripos( $item_link, 'ajax=1' ) !== false )
			{
				$item_link_class .= ' ajax-link';
			}

			$html .= '<div class="'.esc_attr( $item_link_class ).'"><a href="'.esc_url( $item_link ).'" target="'.esc_attr( $item_link_target ).'">'.esc_html( $item_link_text ).'</a></div>';
		}

		$html .= '</div>';

		global $thebe_global_sc_slider_display;

		$img_obj = thebe_get_image(array(
			'rid' => $item_img,
			'max_width' => $thebe_global_sc_slider_display === 'boxed' ? 1800 : 2200,
		));

		if( $img_obj )
		{
			if( $item_use_video === '1' && $item_video_link )
			{
				$html .= '<div class="img" data-w="'.esc_attr( $img_obj['thumb_width'] ).'" data-h="'.esc_attr( $img_obj['thumb_height'] ).'">';
				$html .= '<div class="bg-full" data-bg="'.esc_url( $img_obj['thumb'] ).'" data-src="'.esc_attr( $item_video_link ).'" data-type="'.esc_attr( $item_video_type ).'" data-volume="'.esc_attr( $item_video_volume ).'"></div>';
				$html .= '</div>';
			}
			else
			{
				$html .= '<div class="img" data-w="'.esc_attr( $img_obj['thumb_width'] ).'" data-h="'.esc_attr( $img_obj['thumb_height'] ).'">';
				$html .= '<div class="bg-full" data-bg="'.esc_url( $img_obj['thumb'] ).'"></div>';
				$html .= '</div>';
			}
		}
		else
		{
			if( $item_use_video === '1' && $item_video_link )
			{
				$html .= '<div class="img" data-w="1280" data-h="720">';
				$html .= '<div class="bg-full" data-src="'.esc_attr( $item_video_link ).'" data-type="'.esc_attr( $item_video_type ).'" data-volume="'.esc_attr( $item_video_volume ).'"></div>';
				$html .= '</div>';
			}
		}

		$html .= '</div>';

		return $html;
	}


	//---------------------------------------------
	//-----------    pricing-table    -------------
	//---------------------------------------------
	add_shortcode('pricing_table', 'thebe_shortcode_pricing_table');
	function thebe_shortcode_pricing_table($attr, $content)
	{
		extract(shortcode_atts(array(
			'title_img' => '',
			'title' => '',
			'subtitle' => '',
			'wide' => '0',
			'append_class' => '',
		), thebe_shortcode_parse_attr($attr)));

		$box_class = 'ptsc sc-mixbox pricing-table t-center content-center';

		if( $append_class )
		{
			$box_class .= ' '.$append_class;
		}

		if( $wide === '1' )
		{
			$box_class .= ' wide';
		}
		
		$html = '<div class="'.esc_attr( $box_class ).'">';

		$html .= '<div class="section-title">';

		if( $title_img )
		{
			$img_obj = thebe_get_image(array(
				'rid' => $title_img,
				'max_width' => 1920,
			));

			if( $img_obj )
			{
				$html .= '<img src="'.esc_url( $img_obj['thumb'] ).'" alt="" />';
			}
		}

		if( $title )
		{
			$html .= '<h1 class="title">'.$title.'</h1>';
		}

		if( $subtitle )
		{
			$html .= '<div class="sub-title">'.$subtitle.'</div>';
		}

		$html .= '</div>';
		
		$html .= '<div class="wrap">';
		$html .= do_shortcode( wp_specialchars_decode( stripslashes($content), ENT_QUOTES ) );
		$html .= '</div>';
		
		$html .= '</div>';
		
		return $html;
	}

	//-------------------------------------------------------
	//--------------    pricing-table-item    ---------------
	//-------------------------------------------------------
	add_shortcode('pricing_table_item', 'thebe_shortcode_pricing_table_item');
	function thebe_shortcode_pricing_table_item($attr, $content)
	{
		extract(shortcode_atts(array(
			'item_highlight_color' => '',
			'item_text_color' => 'none',
			'item_bg_color' => '',

			'item_price_symbol' => '$',
			'item_price' => '100',
			'item_title' => '',
			'item_list_text_1' => '',
			'item_list_text_2' => '',
			'item_list_text_3' => '',
			'item_list_text_4' => '',
			'item_list_text_5' => '',
			'item_list_text_6' => '',

			'item_footer_type' => '1',

			'item_link_text' => '',
			'item_link' => '',
			'item_link_new_tab' => '0',

			'module_width' => '0.3333',
		), thebe_shortcode_parse_attr($attr)));

		$item_class = 'item';

		if( $item_bg_color )
		{
			$item_class .= ' has-bg-color';
		}

		$html = '<div class="'.esc_attr( $item_class ).'" data-w="'.esc_attr( $module_width ).'"';

		if( $item_highlight_color )
		{
			$html .= ' data-color="'.esc_attr( $item_highlight_color ).'"';
		}

		if( $item_text_color !== 'none' )
		{
			$html .= ' data-text-color="'.esc_attr( $item_text_color ).'"';
		}

		$html .= '>';
		$html .= '<div class="inner-wrap">';

		if( $item_bg_color )
		{
			$html .= '<div class="bg-color" data-color="'.esc_attr( $item_bg_color ).'"></div>';
		}

		if( $item_title || $content || ( $item_footer_type === '2' && $item_link_text ) )
		{
			$html .= '<div class="text">';

			if( $item_price_symbol && $item_price )
			{
				$html .= '<div class="price-amount"><i>'.esc_html( $item_price_symbol ).'</i>'.esc_html( $item_price ).'</div>';
			}

			if( $item_title )
			{
				$html .= '<div class="title">'.thebe_kses_content( $item_title, false ).'</div>';
			}

			if( $content )
			{
				$html .= '<div class="sub-title">'.thebe_kses_content( $content, false ).'</div>';
			}

			$html .= '<ul>';

			for( $index = 1; $index <= 6; $index ++ )
			{
				$list_text_key = 'item_list_text_'.$index;
				$list_text = $$list_text_key;
				if( $list_text )
				{
					$html .= '<li>'.thebe_kses_content( $list_text, false ).'</li>';
				}
			}

			$html .= '</ul>';

			if( $item_footer_type === '2' )
			{
				$item_link_target = $item_link_new_tab === '1' ? '_blank' : '_self';

				$item_link_class = 'btn';
				if( stripos( $item_link, 'ajax=1' ) !== false )
				{
					$item_link_class .= ' ajax-link';
				}

				$html .= '<a class="'.esc_attr( $item_link_class ).'" href="'.esc_url( $item_link ).'" target="'.esc_attr( $item_link_target ).'"><span>'.esc_html( $item_link_text ).'</span></a>';
			}

			$html .= '</div>';
		}
		

		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}


	//--------------------------------------
	//-----------    banner    -------------
	//--------------------------------------
	add_shortcode('banner', 'shortcode_banner');
	function shortcode_banner($attr, $content)
	{
		extract(shortcode_atts(array(
			'style' => '1',
			'text_color' => 'white',
			'reverse' => '0',
			'parallax' => '0',
			'bg_img' => '',
			'bg_color' => '',
			'bg_video_type' => '0',
			'bg_video_link' => '',
			'img' => '',
			'title' => '',
			'title_size' => '2',
			'btn_text' => '',
			'btn_link' => '',
			'btn_link_new_tab' => '0',
			'append_class' => '',
		), thebe_shortcode_parse_attr($attr)));

		$box_class = ' style-0'.$style;

		if( $reverse === '1' )
		{
			$box_class .= ' reverse';
		}

		if( $parallax === '1' )
		{
			$box_class .= ' parallax';
		}

		if( $append_class )
		{
			$box_class .= ' '.$append_class;
		}
		
		$html = '<div class="ptsc sc-banner'.esc_attr( $box_class ).'" data-text-color="'.esc_attr( $text_color ).'" data-header-color="'.esc_attr( $text_color ).'">';

		$html .= '<div class="wrap">';
		
		if( $bg_color )
		{
			$html .= '<div class="bg-color" data-color="'.esc_attr( $bg_color ).'"></div>';
		}

		$img_obj = thebe_get_image(array(
			'rid' => $bg_img,
			'max_width' => 2200,
		));

		$bg_img_url = '';
		if( $img_obj )
		{
			$bg_img_url = $img_obj['thumb'];
		}

		if( $bg_img_url )
		{
			if( $bg_video_type !== '0' && $bg_video_link )
			{
				$html .= '<div class="bg-full" data-bg="'.esc_url( $bg_img_url ).'" data-src="'.esc_attr( $bg_video_link ).'" data-type="'.esc_attr( $bg_video_type ).'" data-volume="0"></div>';
			}
			else
			{
				$html .= '<div class="bg-full" data-bg="'.esc_url( $bg_img_url ).'"></div>';
			}
		}
		elseif( $bg_video_type !== '0' && $bg_video_link )
		{
			$html .= '<div class="bg-full" data-src="'.esc_attr( $bg_video_link ).'" data-type="'.esc_attr( $bg_video_type ).'" data-volume="0"></div>';
		}

		$html .= '<div class="inner-wrap">';

		$img_obj = thebe_get_image(array(
			'rid' => $img,
			'max_width' => 1100,
		));

		if( $img_obj )
		{
			$html .= '<div class="img"><img src="'.esc_url( $img_obj['thumb'] ).'" alt="" /></div>';
		}

		if( $title || $content || $btn_text )
		{
			$html .= '<div class="text">';

			if( $title )
			{
				if( $title_size === '0' )
				{
					$html .= '<h1 class="large">'.thebe_kses_content( $title, false ).'</h1>';
				}
				else
				{
					$html .= '<h'.esc_html( $title_size ).'>'.thebe_kses_content( $title, false ).'</h'.esc_html( $title_size ).'>';
				}
			}

			if( $content )
			{
				$html .= '<div class="intro">'.do_shortcode( wp_specialchars_decode( stripslashes($content), ENT_QUOTES ) ).'</div>';
			}

			if( $btn_text )
			{
				$link_target = $btn_link_new_tab === '1' ? '_blank' : '_self';

				$item_link_class = 'btn';
				if( stripos( $btn_link, 'ajax=1' ) !== false )
				{
					$item_link_class .= ' ajax-link';
				}

				$html .= '<a class="'.esc_attr( $item_link_class ).'" href="'.esc_url( $btn_link ).'" target="'.esc_attr( $link_target ).'"><span>'.esc_html( $btn_text ).'</span></a>';
			}

			$html .= '</div>';
		}

		$html .= '</div>'; //end inner-wrap
		$html .= '</div>'; //end wrap
		$html .= '</div>'; //end box
		
		return $html;
	}


	//---------------------------------------------
	//-----------    text carousel    -------------
	//---------------------------------------------
	add_shortcode('text_carousel', 'thebe_shortcode_text_carousel');
	function thebe_shortcode_text_carousel($attr, $content)
	{
		extract(shortcode_atts(array(
			'title_img' => '',
			'title' => '',
			'subtitle' => '',
			'title_center' => '0',
			'content_center' => '0',
			'wide' => '0',
			'append_class' => '',
		), thebe_shortcode_parse_attr($attr)));

		$box_class = '';

		if( $title_center === '1' )
		{
			$box_class .= ' t-center';
		}

		if( $append_class )
		{
			$box_class .= ' '.$append_class;
		}
		
		$html = '<div class="ptsc sc-text-carousel'.esc_attr( $box_class ).'">';

		$html .= '<div class="section-title">';

		if( $title_img )
		{
			$img_obj = thebe_get_image(array(
				'rid' => $title_img,
				'max_width' => 1920,
			));

			if( $img_obj )
			{
				$html .= '<img src="'.esc_url( $img_obj['thumb'] ).'" alt="" />';
			}
		}

		if( $title )
		{
			$html .= '<h1 class="title">'.$title.'</h1>';
		}

		if( $subtitle )
		{
			$html .= '<div class="sub-title">'.$subtitle.'</div>';
		}

		$html .= '</div>';
		
		$html .= '<div class="wrap">';
		$html .= do_shortcode( wp_specialchars_decode( stripslashes($content), ENT_QUOTES ) );
		$html .= '</div>';
		
		$html .= '</div>';
		
		return $html;
	}

	//--------------------------------------------------------
	//--------------    text carousel item    ---------------
	//--------------------------------------------------------
	add_shortcode('text_carousel_item', 'thebe_shortcode_text_carousel_item');
	function thebe_shortcode_text_carousel_item($attr, $content)
	{
		extract(shortcode_atts(array(
			'item_img' => '',
			'item_title' => '',
			'item_title_size' => '3',
			'item_subtitle' => '',
		), thebe_shortcode_parse_attr($attr)));

		$html = '<div class="item">';
		$html .= '<div class="inner-wrap">';


		if( $item_img )
		{
			$img_obj = thebe_get_image(array(
				'rid' => $item_img,
				'max_width' => 1680,
			));

			if( $img_obj )
			{
				$html .= '<div class="img">';
				$html .= '<img src="'.esc_url( $img_obj['thumb'] ).'" alt="" />';
				$html .= '</div>';
			}
		}

		if( $item_title || $item_subtitle || $content ) 
		{
			$html .= '<div class="text">';

			if( $item_title )
			{
				if( $item_title_size === '0' )
				{
					$html .= '<h1 class="large">'.thebe_kses_content( $item_title, false ).'</h1>';
				}
				else
				{
					$html .= '<h'.esc_html( $item_title_size ).'>'.thebe_kses_content( $item_title, false ).'</h'.esc_html( $item_title_size ).'>';
				}
			}

			if( $item_subtitle )
			{
				$html .= '<div class="sub-title">'.esc_html( $item_subtitle ).'</div>';
			}

			if( $content )
			{
				$html .= '<div class="intro">'.do_shortcode( wp_specialchars_decode( stripslashes($content), ENT_QUOTES ) ).'</div>';
			}

			$html .= '</div>';
		}
		

		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}

	//--------------------------------------
	//-----------    Gallery    -------------
	//--------------------------------------
	add_shortcode('pt_gallery', 'shortcode_pt_gallery');
	function shortcode_pt_gallery($attr, $content)
	{
		extract(shortcode_atts(array(
			'title_img' => '',
			'title' => '',
			'subtitle' => '',
			'title_center' => '0',
			'wide' => '0',

			'thumb_type' => 'auto',
			'cols' => '2',
			'gap' => 'none',
			'use_lightbox' => '1',
			'img_list' => '',

			'append_class' => '',
		), thebe_shortcode_parse_attr($attr)));

		$box_class = 'ptsc sc-gallery';

		if( $title_center === '1' )
		{
			$box_class .= ' t-center';
		}

		if( $wide === '1' )
		{
			$box_class .= ' wide';
		}

		if( $gap !== 'none' )
		{
			$box_class .= ' gap-'.$gap;
		}

		$box_class .= ' type-'.$thumb_type;

		if( $use_lightbox === '1' )
		{
			$box_class .= ' lightbox';
		}

		$box_class .= ' pt-col-'.$cols;
		
		if( $append_class )
		{
			$box_class .= ' '.$append_class;
		}

		$html = '<div class="'.esc_attr( $box_class ).'">';

		$html .= '<div class="section-title">';

		if( $title_img )
		{
			$img_obj = thebe_get_image(array(
				'rid' => $title_img,
				'max_width' => 1920,
			));

			if( $img_obj )
			{
				$html .= '<img src="'.esc_url( $img_obj['thumb'] ).'" alt="" />';
			}
		}

		if( $title )
		{
			$html .= '<h1 class="title">'.thebe_kses_content( $title, false ).'</h1>';
		}

		if( $subtitle )
		{
			$html .= '<div class="sub-title">'.thebe_kses_content( $subtitle, false ).'</div>';
		}

		$html .= '</div>';
		
		$html .= '<div class="wrap">';

		if( $img_list )
		{
			$img_list_data = thebe_parse_image_info_list_data( $img_list );

			$img_width_const = array( 1000, 800, 600, 400 );
			foreach( $img_list_data as $list_obj )
			{
				$img_rid = $list_obj['rid'];
				if( !$img_rid ) continue;

                $item_class = 'item';

				$item_title = isset( $list_obj['title'] ) ? $list_obj['title'] : '';
				$item_link = isset( $list_obj['link'] ) ? $list_obj['link'] : '';
				$item_link_new_tab = isset( $list_obj['link_new_tab'] ) ? $list_obj['link_new_tab'] : '0';

                $item_use_video = isset( $list_obj['use_video'] ) ? $list_obj['use_video'] : '0';
                $item_video_type = isset( $list_obj['video_type'] ) ? $list_obj['video_type'] : '1';
                $item_video_link = isset( $list_obj['video_link'] ) ? $list_obj['video_link'] : '';

                $has_video = strval( $item_use_video ) === '1' && $item_video_link;

                if( $has_video )
                {
                    $item_class .= ' has-video';
                }

				$img_obj = thebe_get_image(array(
					'rid' => $img_rid,
					'max_width' => $img_width_const[ intval( $cols ) - 2 ],
				));
				
				if( $img_obj )
				{
					$html .= '<div class="'.esc_attr( $item_class ).'" data-w="'.esc_attr( $img_obj['thumb_width'] ).'" data-h="'.esc_attr( $img_obj['thumb_height'] ).'">';
					$html .= '<div class="inner-wrap">';

					$html .= '<div class="img">';
                    if( $has_video )
                    {
                        $html .= '<div class="bg-full" data-bg="'.esc_url( $img_obj['thumb'] ).'" data-type="'.esc_attr( $item_video_type ).'" data-video-src="'.esc_attr( $item_video_link ).'"></div>';
                    }
                    else
                    {
                        $html .= '<div class="bg-full" data-bg="'.esc_url( $img_obj['thumb'] ).'"></div>';
                    }
					
					$html .= '</div>';

					if( $item_link )
					{
						$item_link_target = strval( $item_link_new_tab ) === '1' ? '_blank' : '_self';

						$item_link_class = 'custom-link';
						if( stripos( $item_link, 'ajax=1' ) !== false )
						{
							$item_link_class .= ' ajax-link';
						}
						
						$html .= '<a class="'.esc_attr( $item_link_class ).'" data-href="'.esc_url( $item_link ).'" target="'.esc_attr( $item_link_target ).'"></a>';
					}

					if( $item_title )
					{
						$html .= '<div class="caption">'.thebe_kses_content( $item_title, false ).'</div>';
					}

					if( $use_lightbox === '1' )
					{
						$html .= '<a class="full" data-src="'.esc_url( $img_obj['image'] ).'"></a>';
					}

					$html .= '</div>';//end inner-wrap
					$html .= '</div>';//end item
				}
			}
		}
		
		
		$html .= '</div>'; //end wrap
		$html .= '</div>'; //end box
		
		return $html;
	}

?>