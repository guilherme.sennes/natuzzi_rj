<?php
/*
Plugin Name: Thebe Shortcodes
Plugin URI: http://3theme.com
Description: This plugin enables adding custom shortcodes, provides Shortcode Generator.
Version: 1.0.7
Author: foreverpinetree@gmail.com
Author URI: http://foreverpinetree.com/
License: GPLv2 or later
Text Domain: thebe-shortcodes
*/

if ( !defined( 'ABSPATH' ) )
{
    exit;
}

if ( ! defined( 'PTSC_PLUGIN_VERSION' ) ) 
{
    define( 'PTSC_PLUGIN_VERSION', '1.0.7' );
}

if ( ! defined( 'PTSC_PLUGIN_DIR' ) ) 
{
    define( 'PTSC_PLUGIN_DIR', plugin_dir_path( __FILE__ ) .'editor/' );
}

if ( ! defined( 'PTSC_PLUGIN_URL' ) ) 
{
    define( 'PTSC_PLUGIN_URL', plugin_dir_url(__FILE__)  .'editor/' );
}


// -> [
if ( ! defined( 'PTSC_CODE_BRACKET_L' ) ) 
{
    define( 'PTSC_CODE_BRACKET_L', '{{ptsc-bkl}}' );
}

// -> ]
if ( ! defined( 'PTSC_CODE_BRACKET_R' ) ) 
{
    define( 'PTSC_CODE_BRACKET_R', '{{ptsc-bkr}}' );
}

// -> "
if ( ! defined( 'PTSC_CODE_QUOTE' ) ) 
{
    define( 'PTSC_CODE_QUOTE', '{{ptsc-qt}}' );
}

// -> \n\r
if ( ! defined( 'PTSC_CODE_EOL' ) ) 
{
    define( 'PTSC_CODE_EOL', '{{ptsc-eol}}' );
}

// -> <
if ( ! defined( 'PTSC_CODE_LT' ) ) 
{
    define( 'PTSC_CODE_LT', '{{ptsc-lt}}' );
}

// -> >
if ( ! defined( 'PTSC_CODE_GT' ) ) 
{
    define( 'PTSC_CODE_GT', '{{ptsc-gt}}' );
}


function thebe_shortcode_parse_attr($attr)
{
    if( !$attr )
    {
        return $attr;
    }

    foreach ($attr as $key => $value)
    {
        if( is_numeric($value) )
        {
            continue;
        }

        $value = preg_replace('/'.PTSC_CODE_BRACKET_L.'/', '[', $value);
        $value = preg_replace('/'.PTSC_CODE_BRACKET_R.'/', ']', $value);
        $value = preg_replace('/'.PTSC_CODE_QUOTE.'/', '"', $value);
        $value = preg_replace('/'.PTSC_CODE_EOL.'/', PHP_EOL, $value);
        $value = preg_replace('/'.PTSC_CODE_LT.'/', '<', $value);
        $value = preg_replace('/'.PTSC_CODE_GT.'/', '>', $value);

        $attr[$key] = $value;
    }
    
    return $attr;
}


add_action('init', 'thebe_shortcode_entry');
function thebe_shortcode_entry()
{
    load_plugin_textdomain( 'thebe-shortcodes', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

    if(is_admin())
    {
        require( plugin_dir_path( __FILE__ ) . '/editor/admin-shortcode-insert.php' );
    }
    
    require( plugin_dir_path( __FILE__ ) . '/list.php' );
}


add_action( 'admin_enqueue_scripts', 'thebe_shortcode_scripts' );
function thebe_shortcode_scripts()
{
    // css
    wp_enqueue_style( 'pt-sc-popup-admin-style', PTSC_PLUGIN_URL.'css/admin.css', false, PTSC_PLUGIN_VERSION );

    // js
    $path = 'js/admin.min.js';

    if( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG )
    {
        $path = 'js/admin.js';
    }

    wp_enqueue_script( 'pt-sc-img-info-list-js', PTSC_PLUGIN_URL . 'js/img-info-list.js', array(), PTSC_PLUGIN_VERSION, true );
    wp_enqueue_script( 'pt-sc-popup-admin-js', PTSC_PLUGIN_URL . $path, array(), PTSC_PLUGIN_VERSION, true );
    
    wp_localize_script( 'jquery', 'thebeShortcodes', array('plugin_folder' => WP_PLUGIN_URL .'/thebe-shortcodes') );
}

?>