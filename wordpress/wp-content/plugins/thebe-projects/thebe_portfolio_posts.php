<?php
/*
Plugin Name: Thebe Projects
Plugin URI: http://3theme.com
Description: This plugin enables adding Project type.
Version: 1.0.1
Author: foreverpinetree@gmail.com
Author URI: http://foreverpinetree.com/
License: GPLv2 or later
Text Domain: thebe-projects
*/

if ( !defined( 'ABSPATH' ) )
{
    exit;
}

///////////////////////////////////////////////////////////
/**
 *  Add Preject post type
 */
add_action('init', 'thebe_add_project_post_type');
function thebe_add_project_post_type()
{
    if( !function_exists('thebe_get_theme_option') )
    {
        return;
    }
    
    load_plugin_textdomain( 'thebe-projects', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

    $labels = array(
        'name' => esc_html__('Projects', 'thebe-projects'),
        'singular_name' => esc_html__('Project', 'thebe-projects'),
        'add_new' => esc_html__('Add New', 'thebe-projects'),
        'add_new_item' => esc_html__('Add New Project', 'thebe-projects'),
        'edit_item' => esc_html__('Edit Item', 'thebe-projects'),
        'new_item' => esc_html__('New Item', 'thebe-projects'),
        'view_item' => esc_html__('View Item', 'thebe-projects'),
        'search_items' => esc_html__('Search Items', 'thebe-projects'),
        'not_found' => esc_html__('Not Found', 'thebe-projects'),
        'not_found_in_trash' => esc_html__('Not Found in Trash', 'thebe-projects'),
        'parent_item_colon' => '',
        'menu_name' => esc_html__('Projects', 'thebe-projects'),
    );

    $project_slug = thebe_get_theme_option('thebe_project_slug', '');
    if($project_slug == '')
    {
        $project_slug = 'project_list';
    }
    
    $args = array(
        'labels' => $labels,
        'description'=> esc_html__('Project Manager', 'thebe-projects'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => $project_slug),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-book',
        'show_in_rest' => true,
        'supports' => array('title','editor','author','thumbnail','comments','post-formats'),
    );    
    register_post_type('project',$args);

    $labels = array(   
        'name' => esc_html__('Project Categories', 'thebe-projects'),
        'singular_name' => esc_html__('Project Category', 'thebe-projects'),
        'search_items' => esc_html__('Search Categories', 'thebe-projects'),
        'popular_items' => esc_html__('Most used', 'thebe-projects'),
        'all_items' => esc_html__('All', 'thebe-projects'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => esc_html__('Edit Item', 'thebe-projects'),
        'update_item' =>esc_html__('Update Item', 'thebe-projects'),
        'add_new_item' => esc_html__('Add New Category', 'thebe-projects'),
        'new_item_name' => esc_html__('New Project Categories', 'thebe-projects'),
        'separate_items_with_commas' => esc_html__('Separate categories with commas', 'thebe-projects'),
        'add_or_remove_items' => esc_html__('Add or remove categoires', 'thebe-projects'),
        'choose_from_most_used' => esc_html__('Choose from the most used categoires', 'thebe-projects'),
        'menu_name' => esc_html__('Categories', 'thebe-projects'),
    );

    $project_taxonomy_slug = thebe_get_theme_option('thebe_project_taxonomy_slug', 'project_cat');
    if( $project_taxonomy_slug == '' )
    {
        $project_taxonomy_slug = 'project_cat';
    }
    
    register_taxonomy(   
        'project_cat',
        array('project'),
        array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'query_var' => true,
            'show_in_rest' => true,
            'rewrite' => array('slug' => $project_taxonomy_slug),
        )   
    );

    $labels = array(   
        'name' => esc_html__('Project Tags', 'thebe-projects'),
        'singular_name' => esc_html__('Project Tag', 'thebe-projects'),
        'search_items' => esc_html__('Search Tags', 'thebe-projects'),
        'popular_items' => esc_html__('Most used', 'thebe-projects'),
        'all_items' => esc_html__('All', 'thebe-projects'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => esc_html__('Edit Item', 'thebe-projects'),
        'update_item' =>esc_html__('Update Item', 'thebe-projects'),
        'add_new_item' => esc_html__('Add New Tag', 'thebe-projects'),
        'new_item_name' => esc_html__('New Project Tags', 'thebe-projects'),
        'separate_items_with_commas' => esc_html__('Separate tags with commas', 'thebe-projects'),
        'add_or_remove_items' => esc_html__('Add or remove tags', 'thebe-projects'),
        'choose_from_most_used' => esc_html__('Choose from the most used tags', 'thebe-projects'),
        'menu_name' => esc_html__('Tags', 'thebe-projects'),
    );

    $project_tag_slug = thebe_get_theme_option('thebe_project_tag_slug', 'project_tag');
    if( $project_tag_slug == '' )
    {
        $project_tag_slug = 'project_tag';
    }
    
    register_taxonomy(   
        'project_tag',
        array('project'),
        array(
            'hierarchical' => false,
            'labels' => $labels,
            'show_ui' => true,
            'query_var' => true,
            'show_in_rest' => true,
            'rewrite' => array('slug' => $project_tag_slug),
        )   
    );
}


////////////////////////////////////////////////////////////////////////////
/**
 * Add a Categories column to Projects edit page.
 */
add_filter( 'manage_project_posts_columns', 'thebe_set_custom_edit_project_columns' );
function thebe_set_custom_edit_project_columns($columns) 
{
    unset( $columns['author'] );
    unset( $columns['comments'] );
    unset( $columns['date'] );

    $columns['author'] = esc_html__( 'Author', 'thebe-projects' );
    $columns['project_category'] = esc_html__( 'Categories', 'thebe-projects' );
    $columns['project_tag'] = esc_html__( 'Tags', 'thebe-projects' );
    $columns['comments'] = '<span class="dashicons dashicons-admin-comments"></span>';
    $columns['date'] = esc_html__( 'Date', 'thebe-projects' );

    return $columns;
}

add_action( 'manage_project_posts_custom_column' , 'thebe_custom_project_column', 10, 2 );
function thebe_custom_project_column( $column, $post_id )
{
    switch ( $column ) 
    {
        case 'project_category' :
            $terms = get_the_term_list( $post_id , 'project_cat' , '' , ',' , '' );
            if ( is_string( $terms ) )
            {
                echo $terms;
            }
            else
            {
                esc_html_e( 'Uncategorized', 'thebe-projects' );
            }
            break;
        case 'project_tag' :
            $terms = get_the_term_list( $post_id , 'project_tag' , '' , ',' , '' );
            if ( is_string( $terms ) )
            {
                echo $terms;
            }
            else
            {
                esc_html_e( '&#8212;', 'thebe-projects' );
            }
            break;

    }
}


////////////////////////////////////////////////////////////////////////////
/**
 * Add link to "At a Glance" dashboard.
 */
if( is_admin() )
{
    add_filter( 'dashboard_glance_items', 'thebe_dashboard_glance_items', 10, 1 );
    function thebe_dashboard_glance_items( $args )
    {
        $post_type = 'project';

        $num_posts = wp_count_posts( $post_type );
        if ( $num_posts && $num_posts->publish )
        {
            $text = _n( '%s '.esc_html__('Project', 'thebe-projects'), '%s '.esc_html__('Projects', 'thebe-projects'), $num_posts->publish );
            $text = sprintf( $text, number_format_i18n( $num_posts->publish ) );
            $post_type_object = get_post_type_object( $post_type );
            if ( $post_type_object && current_user_can( $post_type_object->cap->edit_posts ) )
            {
                $args[] = sprintf( '<a href="edit.php?post_type=%1$s">%2$s</a>', $post_type, $text );
            }
            else
            {
                $args[] = sprintf( '<span>%2$s</span>', $post_type, $text );
            }

        }

        return $args;
    }
}


////////////////////////////////////////////////////////////////////////////
/**
 * Plugin activate hook
 */
register_activation_hook( __FILE__, 'thebe_projects_activate' );
function thebe_projects_activate()
{
    add_option( 'thebe_activated_plugin', 'Thebe-Projects' );
}

?>