/**
 * Created by foreverpinetree@gmail.com on 2018-03-27.
 */
!(function($)
{
    "use strict";

    $(function(){
        $('.detail-list .custom-font-upload').on('change', function(evt){
            var fileName = this.files[0]['name'];
            var ext = fileName.substring(fileName.lastIndexOf('.')) || '';
            var type = $(this).data('type');
            if(ext !== type)
            {
                alert('Please select *' + type + ' file!');
                this.value = '';
                return;
            }

            var $label = $(this).siblings('.font-label');
            $label.val(fileName);

            var $fontName = $(this).parent().siblings('.font-name').children('input.font-name-input');
            var fontName = fileName.substring(0, fileName.lastIndexOf('.'));
            if($fontName.val() === '' && fontName)
            {
                $fontName.val(fontName);
            }
        });

		$('.detail-list .font-browse-button').on('click', function(evt){
            $(this).parent().siblings('.custom-font-upload').trigger('click');
        });

        $('.detail-list .font-delete-button').on('click', function(evt){
            $(this).parent().siblings('.font-label').val('');
            $(this).parent().siblings('.custom-font-upload').val('');
        });
    });
    
    $(window).load(function(){
        setTimeout(function(){
            $('.font-page-header').addClass('is-ready');
            $('.font-list-holder').addClass('is-ready');
        }, 100);
    });

}(jQuery));