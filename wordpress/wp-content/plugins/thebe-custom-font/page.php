<?php 
	if (!current_user_can('manage_options'))
	{
		wp_die(esc_html__("You don't have access to this page", 'thebe-custom-font'));
	}

	$custom_data_str = get_option( THEBE_CUSTOM_FONT_KEY, '' );

	$custom_data = $custom_data_str ? json_decode($custom_data_str, true) : array();
	$fonts_data = isset($custom_data['fonts']) ? $custom_data['fonts'] : array();

	$fonts_count = 5;
	if ( defined( 'THEBE_CUSTOM_FONT_COUNT' ) ) 
	{
	    $fonts_count = THEBE_CUSTOM_FONT_COUNT;
	}

	$upload_directory = wp_upload_dir();
	$upload_dir = untrailingslashit( $upload_directory['basedir'] );
	$upload_url = untrailingslashit( $upload_directory['baseurl'] );

	if( isset( $_POST[THEBE_CUSTOM_FONT_KEY] ) && wp_verify_nonce($_POST['thebe_custom_font'], 'thebe_custom_font_action') )
	{
		include_once(plugin_dir_path( __FILE__ ) . 'post.php');
	}
	
    ?>
    <div class="font-page-header">
    	<span class="font-page-title"><?php esc_html_e('Custom Font', 'thebe-custom-font'); ?></span>
		<div class="font-page-des">
			<span><?php echo wp_kses( __('Fonts will be shown at the top of fonts list which located at "Theme Options &raquo; Fonts" after clicking the Save Changes button.<br/>If you don\'t have *.woff and *.woff2 files, you should use <a href="https://www.fontsquirrel.com/tools/webfont-generator" target="_blank">Font Generator</a> to generate them.', 'thebe-custom-font'), wp_kses_allowed_html( 'post' ) ); ?></span>
		</div>
    </div>
	<form method="post" enctype="multipart/form-data">
		<div class="font-list-holder">
		<?php 
			wp_nonce_field( 'thebe_custom_font_action', 'thebe_custom_font' );

			$fonts_data_count = count($fonts_data);

			for( $font_index = 1; $font_index <= $fonts_count;  $font_index ++ )
			{
				$font_obj = $font_index <= $fonts_data_count ? $fonts_data[$font_index - 1] : array();

				$font_name = isset($font_obj['name']) ? $font_obj['name'] : '';
				$font_woff = isset($font_obj['woff']) ? $font_obj['woff'] : '';
				$font_woff2 = isset($font_obj['woff2']) ? $font_obj['woff2'] : '';

				?>
				<ul class="font-list">
					<li classs="font-list-item">
						<span class="font-title"><?php echo esc_html__('Font', 'thebe-custom-font').' '.$font_index; ?></span>
						<ul class="detail-list">
							<li class="detail-list-item font-name">
								<span><?php esc_html_e('Font name', 'thebe-custom-font'); ?></span>
								<input class="font-name-input" name="font_name_<?php echo $font_index; ?>" value="<?php echo esc_attr( $font_name ); ?>">
							</li>
							<li class="detail-list-item font-woff">
								<span><?php esc_html_e('*.woff', 'thebe-custom-font'); ?></span>
								<input class="font-label" value="<?php echo esc_attr( $font_woff ); ?>" name="woff_label_<?php echo $font_index; ?>" readonly>
								<input class="custom-font-upload" type="file" data-type=".woff" name="file_woff_<?php echo $font_index; ?>">
								<div class="buttons-holder">
									<input class="font-browse-button button-primary" type="button" value="<?php esc_html_e('Browse', 'thebe-custom-font'); ?>">
									<input class="font-delete-button button-secondary" type="button" value="<?php esc_html_e('Delete', 'thebe-custom-font'); ?>">
								</div>
							</li>
							<li class="detail-list-item font-woff2">
								<span><?php esc_html_e('*.woff2', 'thebe-custom-font'); ?></span>
								<input class="font-label" value="<?php echo esc_attr( $font_woff2 ); ?>" name="woff2_label_<?php echo $font_index; ?>" readonly>
								<input class="custom-font-upload" type="file" data-type=".woff2" name="file_woff2_<?php echo $font_index; ?>">
								<div class="buttons-holder">
									<input class="font-browse-button button-primary" type="button" value="<?php esc_html_e('Browse', 'thebe-custom-font'); ?>">
									<input class="font-delete-button button-secondary" type="button" value="<?php esc_html_e('Delete', 'thebe-custom-font'); ?>">
								</div>
							</li>
						</ul>
					</li>
					
				</ul>
				<?php
			}
		?>
		</div>
		<input class="thebe-font-code" type="hidden" name="<?php echo THEBE_CUSTOM_FONT_KEY; ?>" />
		<input class="custom-font-upload-btn button-primary" type="submit" value="<?php esc_attr_e('Save Changes', 'thebe-custom-font'); ?>"/>
	</form>
