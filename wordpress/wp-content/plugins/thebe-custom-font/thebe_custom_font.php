<?php
/*
Plugin Name: Thebe Custom Font
Plugin URI: http://3theme.com
Description: This plugin enables customizing font.
Version: 1.0.0
Author: foreverpinetree@gmail.com
Author URI: http://foreverpinetree.com/
License: GPLv2 or later
Text Domain: thebe-custom-font
*/

if ( defined( 'THEBE_CUSTOM_FONT_INITED' ) ) 
{
    exit; // already existed.
}

define( 'THEBE_CUSTOM_FONT_INITED', true );

if ( !defined( 'THEBE_CUSTOM_FONT_KEY' ) ) 
{
    define( 'THEBE_CUSTOM_FONT_KEY', 'thebe_plugin_custom_font' );
}

add_action( 'admin_menu', 'thebe_custom_font_plugin' );
function thebe_custom_font_plugin()
{
	load_plugin_textdomain( 'thebe-custom-font', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

	add_option( THEBE_CUSTOM_FONT_KEY, '', '', 'yes' );
	
    add_theme_page( esc_html__('Custom Font', 'thebe-custom-font'), esc_html__('Custom Font', 'thebe-custom-font'), 'administrator', 'custom_font', 'thebe_output_custom_font_page' );
}

/**
 * Output font page
 */
function thebe_output_custom_font_page()
{
	include_once( plugin_dir_path( __FILE__ ) . 'page.php' );
	
	// css
	wp_enqueue_style( 'pt-custom-font-style', plugin_dir_url( __FILE__ ).'css/style.css', false, '1.0', 'all' );

	// js
    wp_enqueue_script( 'pt-custom-font-js', plugin_dir_url( __FILE__ ) . 'js/main.js', array(), '', 'all' );
}

add_action( 'admin_enqueue_scripts', 'thebe_admin_custom_font_parse_fonts_handler', 11 );
function thebe_admin_custom_font_parse_fonts_handler($custom)
{
	$custom = apply_filters( 'thebe_custom_font_parse_fonts', '' );
	wp_add_inline_style('thebe-fonts', $custom);

	$custom = '
		.font-page-header:not(.is-ready),
		.font-list-holder:not(.is-ready) {
			display: none;
		}
	'."\n";
	wp_add_inline_style('thebe-admin-custom-style', $custom);
}

/**
 * Filter, add custom data to "Theme Options > Fonts > Font list".
 */
add_filter( 'thebe_custom_font_parse_data', 'thebe_custom_font_parse_data_handler' );
if( !function_exists('thebe_custom_font_parse_data_handler') )
{
	function thebe_custom_font_parse_data_handler( $data = null )
	{
		if( !$data )
		{
			return $data;
		}

		$custom_data_str = get_option( THEBE_CUSTOM_FONT_KEY, '' );

		if( !$custom_data_str )
		{
			return $data;
		}

		$custom_data = $custom_data_str ? json_decode( $custom_data_str, true ) : null;
		$fonts_data = $custom_data && isset( $custom_data['fonts'] ) ? $custom_data['fonts'] : null;

		if( !$fonts_data )
		{
			return $data;
		}

		$fonts = $data->fonts;

		foreach( $fonts_data as $font_obj )
		{
			if( $font_obj['name'] && ( $font_obj['woff'] || $font_obj['woff2'] ) )
			{
				$fonts->{'Custom font - '.$font_obj['name']} = array(
					"variants" => array(),
					"fallback" => "sans-serif",
					"specified" => $font_obj['name'],
				);
			}
		}

		return $data;
	}
}

/**
 * Filter, output inline style.
 */
add_filter( 'thebe_custom_font_parse_fonts', 'thebe_custom_font_parse_fonts_handler' );
if( !function_exists('thebe_custom_font_parse_fonts_handler') )
{
	function thebe_custom_font_parse_fonts_handler( $custom = '' )
	{	
		$custom_data_str = get_option( THEBE_CUSTOM_FONT_KEY, '' );

		if( !$custom_data_str )
		{
			return $custom;
		}

		$custom_data = $custom_data_str ? json_decode( $custom_data_str, true ) : null;
		$fonts_data = $custom_data && isset( $custom_data['fonts'] ) ? $custom_data['fonts'] : null;

		if( !$fonts_data )
		{
			return $custom;
		}

		$font_dir = $custom_data && isset( $custom_data['dir'] ) ? $custom_data['dir'] : '';

		foreach( $fonts_data as $font_obj )
		{
			if( $font_obj['name'] && ( $font_obj['woff'] || $font_obj['woff2'] ) )
			{
				$fonts = array();

				if( $font_obj['woff2'] )
				{
					$fonts[] = 'url("'.$font_dir.$font_obj['woff2'].'") format("woff2")';
				}

				if( $font_obj['woff'] )
				{
					$fonts[] = 'url("'.$font_dir.$font_obj['woff'].'") format("woff")';
				}

				$custom .= '@font-face {
				  font-family: "'.$font_obj['name'].'";
				  src: '.implode( ',', $fonts ).';
				  font-weight: normal;
				  font-style: normal;
				}'."\n";
			}
		}

		return $custom;
	}
}

?>