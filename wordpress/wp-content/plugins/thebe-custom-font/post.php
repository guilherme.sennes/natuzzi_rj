<?php 

	$dist_fonts_dir = $upload_dir.'/thebe_custom_font/';
	$dist_fonts_url = $upload_url.'/thebe_custom_font/';

	$data = array(
		'dir' => $dist_fonts_url,
		'fonts' => array(),
	);
	$all_files = array();

	global $wp_filesystem;
	WP_Filesystem();

	if( !$wp_filesystem->exists($dist_fonts_dir) )
	{
		$wp_filesystem->mkdir($dist_fonts_dir);
	}

	for( $font_index = 1; $font_index <= $fonts_count; $font_index ++ )
	{
		$font_name = isset($_POST['font_name_'.$font_index]) ? $_POST['font_name_'.$font_index] : '';

		$file_woff = isset($_FILES['file_woff_'.$font_index]) ? $_FILES['file_woff_'.$font_index] : null;
		$file_woff2 = isset($_FILES['file_woff2_'.$font_index]) ? $_FILES['file_woff2_'.$font_index] : null;

		$woff_label = isset($_POST['woff_label_'.$font_index]) ? $_POST['woff_label_'.$font_index] : '';
		$woff2_label = isset($_POST['woff2_label_'.$font_index]) ? $_POST['woff2_label_'.$font_index] : '';

		$arr = array(
			'name' => $font_name,
			'woff' => $woff_label,
			'woff2' => $woff2_label,
		);

		if( $file_woff )
		{
			move_uploaded_file($file_woff['tmp_name'], $dist_fonts_dir.$file_woff['name']);
		}
		if( $file_woff2 )
		{
			move_uploaded_file($file_woff2['tmp_name'], $dist_fonts_dir.$file_woff2['name']);
		}

		if( $woff_label )
		{
			$all_files[] = $woff_label;
		}
		if( $woff2_label )
		{
			$all_files[] = $woff2_label;
		}

		$data['fonts'][] = $arr;
	}

	$files = $wp_filesystem->dirlist($dist_fonts_dir);
	foreach( $files as $file )
	{
		if( in_array( $file['name'], $all_files ) )
		{
			continue;
		}

		$path = $dist_fonts_dir.$file['name'];
		if( $wp_filesystem->is_file($path) )
		{
			$wp_filesystem->delete($path);
		}
	}

	// ------------------------------------------------------

	update_option( THEBE_CUSTOM_FONT_KEY, json_encode($data) );

	$custom_data = $data;
	$fonts_data = $data['fonts'];

?>