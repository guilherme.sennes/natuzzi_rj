<?php
/*
 * Template Name: Shortcode
 */
	thebe_prepare_setting(
		array(
			'type' => 'page',
			'template' => 'shortcode',
		)
	);

	get_header();

	if ( post_password_required() )
	{
		?>
		<div class="page-password-required password-required-msg">
		<?php
			the_content();
		?>
		</div>
		<?php
	}
	else
	{
		?>
		<div class="ptsc-list">
			<div class="wrap">
			<?php
				thebe_output_page_title();
				thebe_output_page_shortcode();
			?>
			</div>
		</div>
		<?php
	}
	
	get_footer(); 
?>