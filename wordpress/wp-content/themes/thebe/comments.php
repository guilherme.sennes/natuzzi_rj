<?php
	if ( post_password_required() )
	{
		return;
	}

?>
<div class="comment-root cf">
	<div class="wrap">
	<?php
		$thebe_comment_count = get_comments_number();

		if ( have_comments() )
		{
			?>
			<h5 class="title">
				<span><?php echo esc_html( $thebe_comment_count ); ?></span>
				<?php 
					if( $thebe_comment_count > 1 )
					{
						esc_html_e('Comments', 'thebe');
					}
					else
					{
						esc_html_e('Comment', 'thebe');
					}
				?>
			</h5>
			<ol class="comment-list pings">
				<?php
					wp_list_comments( array(
						'type' => 'pings',
						'style'       => 'ol',
						'short_ping'  => true,
						'avatar_size' => 100,
					) );
				?>
			</ol>
			<ol class="comment-list">
				<?php
					wp_list_comments( array(
						'type' => 'comment',
						'style'       => 'ol',
						'short_ping'  => true,
						'avatar_size' => 100,
						'callback'    => 'thebe_comment_callback'//can be found in inc/theme/comment-functions.php
					) );
				?>
			</ol><!-- .comment-list -->
			<?php thebe_comment_nav(); ?><?php
		}

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && $thebe_comment_count && post_type_supports( get_post_type(), 'comments' ) )
		{
			?><p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'thebe' ); ?></p><?php
		};

		comment_form( array(
			'comment_field'      => '<p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="*'.esc_attr__('Comment', 'thebe').'"></textarea></p>',
			'logged_in_as'       => '',
			'title_reply_before' => '<h5 class="title">',
			'title_reply_after'  => '</h5>',
			'title_reply'        => esc_html__('Leave a Comment', 'thebe'),
			'title_reply_to'     => esc_html__('Leave a Comment to %s', 'thebe'),
			'cancel_reply_link'  => esc_html__('Cancel Reply', 'thebe'),
			'label_submit'       => esc_html__('Post Comment', 'thebe')
		) );
	?>
	</div>
</div>
