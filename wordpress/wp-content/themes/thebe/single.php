<?php
	$thebe_call_by_ajax = isset( $_POST['single_ajax'] ) ? $_POST['single_ajax'] : false;
	$thebe_post_type = get_post_type();
	$thebe_prop_prefix = $thebe_post_type === 'project' ? 'pt_project_' : 'pt_post_';
	$thebe_post_id = get_the_ID();
	$thebe_post_data = thebe_get_post_setting( $thebe_post_id );

	if( !$thebe_call_by_ajax )
	{
		thebe_prepare_setting(
			array(
				'type' => $thebe_post_type,
			)
		);

		$thebe_global_site_vars['use_widget'] = is_active_sidebar('sidebar-right');
		$thebe_global_site_vars['single_type'] = $thebe_post_type;

		get_header();

		?><div class="wrap <?php echo esc_attr( $thebe_post_type ); ?>"><?php
	}
	
	while( have_posts() )
	{
		the_post();
		
		$thebe_banner_type = thebe_get_property( $thebe_post_data, 'banner_type', '1', $thebe_prop_prefix );

		$thebe_video_link = thebe_get_property( $thebe_post_data, 'video_link', '', $thebe_prop_prefix );
		$thebe_video_type = thebe_get_property( $thebe_post_data, 'video_type', '1', $thebe_prop_prefix );
		$thebe_video_volume = thebe_get_property( $thebe_post_data, 'video_volume', '1', $thebe_prop_prefix );

		$thebe_post_is_video = $thebe_banner_type === '3';

		$thebe_holder_class = 'single-inner';

		if( $thebe_post_is_video && $thebe_video_link )
		{
			$thebe_holder_class .= ' video';
		}
		else if( $thebe_banner_type === '2' )
		{
			$thebe_holder_class .= ' lightbox';
		}

		?>
		<div class="<?php echo esc_attr( $thebe_holder_class ); ?>">
		<?php

			require thebe_parse_part_path( '/template-parts/single/content-banner' );
			
			?>
			<div class="single-main-intro">
				<?php
					if( $thebe_post_type === 'post' )
					{
						$thebe_categories = get_the_category();
						if( $thebe_categories && !is_wp_error( $thebe_categories ) && count( $thebe_categories ) > 0 )
						{
							?>
							<div class="category">
								<i><?php esc_html_e('Categories', 'thebe'); ?></i>
								<div>
								<?php 
									foreach( $thebe_categories as $thebe_cat )
									{
										?><a href="<?php echo esc_url( get_category_link( $thebe_cat->term_id ) ); ?>"><?php echo esc_html( $thebe_cat->cat_name ); ?></a><?php
									}
								?>
								</div>
							</div>
							<?php
						}
					}
				?>
				<h2 class="title"><?php the_title(); ?></h2>
				<?php
					if( $thebe_post_type === 'project' )
					{
						$thebe_custom_meta = thebe_parse_group_data(
							thebe_get_property( $thebe_post_data, 'custom_meta', '', $thebe_prop_prefix ),
							array('title' => '', 'des' => '')
						);

						if( count( $thebe_custom_meta ) > 0 )
						{
							?>
							<div class="single-meta">
							<?php
								foreach( $thebe_custom_meta as $thebe_custom_meta_item )
								{
									?>
									<div class="item">
										<i><?php echo esc_html( $thebe_custom_meta_item['title'] ); ?></i>
										<?php
											?><div><?php thebe_kses_content( $thebe_custom_meta_item['des'] ); ?></div><?php
										?>
									</div>
									<?php
								}
							?>
							</div>
							<?php
						}
					}
					else
					{
						?>
						<div class="single-meta">
							<div class="item date">
								<i><?php esc_html_e('Date', 'thebe'); ?></i>
								<div><a href="<?php echo esc_url(get_month_link( get_the_time('Y'), get_the_time('m'), get_the_time('d'))); ?>"><?php the_date(get_option('date_format', 'd M,Y')); ?> </a></div>
							</div>
							<div class="item author">
								<i><?php esc_html_e('Author', 'thebe'); ?></i>
								<div><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta('ID') ) ); ?>"><?php the_author(); ?></a></div>
							</div>
						</div>
						<?php
					}
				?>
			</div>

			<?php
				if( get_the_content() )
				{
					?><div class="text-area single-main-content"><?php the_content(); ?></div><?php
				}
			?>

			<?php
				wp_link_pages('before=<div class="page-links">&after=</div>&next_or_number=next&previouspagelink='.esc_html__('Prev Page', 'thebe').'&nextpagelink='.esc_html__('Next Page', 'thebe'));
			?>

			<?php
				require thebe_parse_part_path( '/template-parts/single/content-extend' );

				$thebe_shortcode_text = thebe_get_property( $thebe_post_data, 'shortCodeText', '', 'pt_sc_' );
				if( $thebe_shortcode_text )
				{
					echo do_shortcode( shortcode_unautop( wp_specialchars_decode( stripslashes( $thebe_shortcode_text ), ENT_QUOTES ) ) );
				}

				$thebe_show_comment = $thebe_post_type == 'project' ? thebe_get_theme_option('thebe_show_project_comment', '0') : thebe_get_theme_option('thebe_show_comment', '1');
				if ( !$thebe_call_by_ajax && $thebe_show_comment != '0' && $thebe_show_comment != '' && ( comments_open() || get_comments_number() ) )
				{
					comments_template();
				}
			?>
			<div class="single-footer">
				<div class="tag-and-share">
					<?php
						if( $thebe_post_type === 'project' )
						{
							$thebe_post_tags = get_the_terms( $thebe_post_id, 'project_tag' );
						}
						else
						{
							$thebe_post_tags = get_the_tags();
						}

						if( $thebe_post_tags && count( $thebe_post_tags ) > 0 )
						{
							?>
							<div class="single-tags">
								<span><?php esc_html_e('Tags', 'thebe'); ?></span>
								<?php
									foreach( $thebe_post_tags as $thebe_post_tag )
									{
										?><a href="<?php echo esc_url( get_tag_link( $thebe_post_tag->term_id ) ); ?>"><?php echo esc_html( $thebe_post_tag->name ); ?></a><?php
									}
								?>
							</div>
							<?php
						}
					?>
					<?php
						$thebe_post_socials_str = thebe_post_socials( $thebe_post_id );
						if( $thebe_post_socials_str != '' )
						{
							?>
							<div class="share">
								<i class="btn"><?php esc_html_e('Share', 'thebe'); ?></i>
								<div class="wrap">
									<?php thebe_kses_content( $thebe_post_socials_str ); ?>
								</div>
							</div>
							<?php
						}
					?>
				</div>
				<?php
					if( $thebe_post_type === 'post' && thebe_get_theme_option('thebe_use_post_footer_info', '1') === '1' )
					{
						$thebe_post_footer_title = thebe_get_theme_option( 'thebe_post_footer_title', esc_html__( 'You can edit this in Dashboard.', 'thebe' ) );
						$thebe_post_footer_subtitle = thebe_get_theme_option( 'thebe_post_footer_subtitle', esc_html__( 'Subtitle', 'thebe' ) );
						$thebe_post_footer_intro = thebe_get_theme_option( 'thebe_post_footer_intro', esc_html__( 'Mauris sit amet mi enim. Donec et leo fringilla nunc tincidunt pulvinar. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris in ultrices dui, ac laoreet sem. Aenean eu sem ante. Morbi sed posuere nunc, nec ullamcorper arcu.', 'thebe' ) );

						$thebe_post_footer_has_text = $thebe_post_footer_title || $thebe_post_footer_intro;
						$thebe_post_footer_class = '';

						if( !$thebe_post_footer_has_text )
						{
							$thebe_post_footer_class = ' only-img';
						}
						?>
						<div class="post-footer-info<?php echo esc_attr( $thebe_post_footer_class ); ?>">
							<div class="wrap">
								<?php
									$thebe_post_footer_img_url = THEBE_THEME_URL.'/data/images/author.jpg';
									$thebe_img_obj = thebe_get_image(array(
										'rid' => thebe_get_theme_option('thebe_post_footer_img', ''),
										'max_width' => 600,
									));
								
									if( $thebe_img_obj )
									{
										$thebe_post_footer_img_url = $thebe_img_obj['thumb'];
									}
								?>
								<div class="img">
									<img src="<?php echo esc_url( $thebe_post_footer_img_url ); ?>" alt="<?php esc_attr_e( 'Post footer', 'thebe' ); ?>">
								</div>
								<?php
								if( $thebe_post_footer_has_text )
								{
									?>
									<div class="text">
									<?php
										if( $thebe_post_footer_title )
										{
											?><div class="title"><h3><?php thebe_kses_content( $thebe_post_footer_title ); ?></h3></div><?php
										}
										
										if( $thebe_post_footer_subtitle )
										{
											?><div class="sub-title"><?php thebe_kses_content( $thebe_post_footer_subtitle ); ?></div><?php
										}

										if( $thebe_post_footer_intro )
										{
											?><div class="intro"><?php thebe_kses_content( $thebe_post_footer_intro ); ?></div><?php
										}
									?>
									</div>
									<?php
								}
								?>
							</div>
						</div>
						<?php
					}
				?>
				<div class="single-nav">
					<?php
						$thebe_prev_post = get_previous_post();
						if( $thebe_prev_post )
						{
							$thebe_pn_permalink = get_permalink( $thebe_prev_post->ID );
							$thebe_pn_post_data = thebe_get_post_setting( $thebe_prev_post->ID );
							$thebe_pn_ajax_bg_color = thebe_get_property( $thebe_pn_post_data, 'ajax_bg_color', '', $thebe_prop_prefix );
							$thebe_pn_ajax_text_color = thebe_get_property( $thebe_pn_post_data, 'ajax_text_color', 'white', $thebe_prop_prefix );
							?>
							<div class="prev ctrl" data-bg-color="<?php echo esc_attr( $thebe_pn_ajax_bg_color ); ?>" data-text-color="<?php echo esc_attr( $thebe_pn_ajax_text_color ); ?>">
								<div class="text">
									<span>
									<?php 
										if( $thebe_post_type === 'project' )
										{
											esc_html_e('Prev Project', 'thebe'); 
										}
										else
										{
											esc_html_e('Prev Post', 'thebe'); 
										}
									?>
									</span>
									<h5><?php echo esc_html( $thebe_prev_post->post_title ); ?></h5>
								</div>
								<a class="full" href="<?php echo esc_url( $thebe_pn_permalink ); ?>"></a>
							</div>
							<?php
						}

						$thebe_next_post = get_next_post();
						if( $thebe_next_post )
						{
							$thebe_pn_permalink = get_permalink( $thebe_next_post->ID );
							$thebe_pn_post_data = thebe_get_post_setting( $thebe_next_post->ID );
							$thebe_pn_ajax_bg_color = thebe_get_property( $thebe_pn_post_data, 'ajax_bg_color', '', $thebe_prop_prefix );
							$thebe_pn_ajax_text_color = thebe_get_property( $thebe_pn_post_data, 'ajax_text_color', 'white', $thebe_prop_prefix );
							?>
							<div class="next ctrl" data-bg-color="<?php echo esc_attr( $thebe_pn_ajax_bg_color ); ?>" data-text-color="<?php echo esc_attr( $thebe_pn_ajax_text_color ); ?>">
								<div class="text">
									<span>
									<?php 
										if( $thebe_post_type === 'project' )
										{
											esc_html_e('Next Project', 'thebe'); 
										}
										else
										{
											esc_html_e('Next Post', 'thebe'); 
										}
									?>
									</span>
									<h5><?php echo esc_html( $thebe_next_post->post_title ); ?></h5>
								</div>
								<a class="full" href="<?php echo esc_url( $thebe_pn_permalink ); ?>"></a>
							</div>
							<?php
						}
					?>
				</div>

				<div class="single-related">
					<h3><?php esc_html_e( 'You Might Also Like:', 'thebe' ); ?></h3>
					<div class="wrap">
					<?php
						$thebe_related_show_num = 3;

						$thebe_related_posts = thebe_get_related_posts_data( $thebe_post_id, $thebe_post_type, $thebe_related_show_num, 1 );
						if( count($thebe_related_posts) === $thebe_related_show_num )
						{
							foreach( $thebe_related_posts as $thebe_related_post )
							{
								$thebe_related_imageurl = $thebe_related_post['imageurl'];
								if( $thebe_related_imageurl == '' )
								{
									continue;
								}
								?>
								<div class="item" data-w="<?php echo esc_attr( $thebe_related_post['w'] ); ?>" data-h="<?php echo esc_attr( $thebe_related_post['h'] ); ?>" data-bg-color="<?php echo esc_attr( $thebe_related_post['bg_color'] ); ?>" data-text-color="<?php echo esc_attr( $thebe_related_post['text_color'] ); ?>">
									<div class="img">
										<div class="bg-full" data-bg="<?php echo esc_url( $thebe_related_imageurl ); ?>"></div>
									</div>
									<a class="full" href="<?php echo esc_url( $thebe_related_post['link'] ); ?>"></a>
									<div class="text">
										<h5><span><?php echo esc_html( $thebe_related_post['title'] ); ?></span></h5>
									</div>
									<?php
										if( count( $thebe_related_post['cats'] ) > 0 )
										{
											?><div class="category"><?php
											foreach( $thebe_related_post['cats'] as $thebe_cat )
											{
												thebe_kses_content( $thebe_cat );
											}
											?></div><?php
										}
									?>
								</div>
								<?php
							}
						}
					?>
					</div>
				</div>
			</div>
		</div>
		<?php
	};

	if( !$thebe_call_by_ajax )
	{
		?></div><?php

		get_footer(); 
	}
	
?>