
!(function($){


	'use strict';

	var isRTL = ($('body').css('direction') || '').toLowerCase() == 'rtl';

	window.onpageshow = function(event){

		if (event && event.persisted){
			$('.site-loader').addClass('load').fadeOut(200);
			$(window).trigger('resize');
		};
		
	};

	var imageQueueLoad = function(imagesQueue){

		var $notShowItems = $(imagesQueue).find('.item:not(.show)');
		var itemLen = $(imagesQueue).find('.item').length;

		if( $notShowItems.length > 0 ){

			var arr = [];
			$notShowItems.each(function(){
				arr.push({ 
					target:this, index: parseInt($(this).data('index')) 
				});
			});


			arr.sort(function(obj1, obj2){
				return obj1.index - obj2.index;
			});


			var $item = $(arr[0].target);
			var $bgTarget = $item.find('.bg-full');
			var bgUrl = $bgTarget.data('bg');


			if( $item.index() == 0 ){
				$item.addClass('first-item')
			}


			if( !$(imagesQueue).hasClass('jump') ){

				$bgTarget.css("backgroundImage",'url('+ bgUrl +')').imagesLoaded( { background: true }, function(instance) {
					var elements = instance.elements;
					$item.addClass('show').find('.pic-loader').remove();
					$bgTarget.removeAttr('data-bg');
					imageQueueLoad(imagesQueue);

					// need cut to singleonce
					if( $(imagesQueue).hasClass('single-header') && !$(imagesQueue).hasClass('raw-proportion') ){
						var bgIndex = $item.index();
						$(imagesQueue).find('.pt-swipe-dots li').eq(bgIndex).css("backgroundImage",'url('+ bgUrl +')')
					}
				})

			}



			if( $item.index() == 0 ){
				setTimeout(function(){

					// if first img still no load 
					if ( !$(imagesQueue).find('.first-item').hasClass('show') ) {

						$(imagesQueue).addClass('jump');

						$(imagesQueue).find('.item:not(.first-item)').each(function(){

							var $this = $(this);
							var $bg_full = $this.find('.bg-full');
							var bgUrl = $bg_full.data('bg');

							$bg_full.css("backgroundImage",'url('+ bgUrl +')').imagesLoaded( { background: true }, function(instance) {
								var elements = instance.elements;
								$this.addClass('show').find('.pic-loader').remove();
								$bg_full.removeAttr('data-bg');

								if( $(imagesQueue).hasClass('single-header') && !$(imagesQueue).hasClass('raw-proportion') ){
									var bgIndex = $this.index();
									$(imagesQueue).find('.pt-swipe-dots li').eq(bgIndex).css("backgroundImage",'url('+ bgUrl +')')
								}
							})

						})

					}

				},8000)
			}



		}else{

			if( $(imagesQueue).data('autoplay') == 1 && $(imagesQueue).find('.item.current.has-video:not(.autoplay):not(.pause),.item.current.autoplay:not(.pause)').length == 0 ){
				$(imagesQueue).ptSwipeCall('setAutoplay',true);
			}


		}

	};



	

	function ptCateNav(){

		if ( $('.category-nav li').length == 0 ) {
			return;
		};

		var $cate = $('.category-nav');
		$cate.append('<div class="cate-nav-ctrl"></div><div class="m-cate-ctrl"></div><div class="cate-popup"><div class="wrap"></div><div class="close-cate-popup"></div></div>');

		
		var cateNavLen =  $('.category-nav li').length;


		$('.category-nav li').each(function(){

			var html = $(this).html();
			var active = ' ';

			if($(this).hasClass('active')){
				active = ' active'
			};

			$('.cate-popup .wrap').append('<div class="cate-item'+active+'">'+html+'</div>');

		});


		function catePopup(){

			var cateListInner_w = $('.category-nav .inner-wrap').width();
			var cateOuter_w = $cate.find('ul').get(0).scrollWidth;

			if ( cateListInner_w < cateOuter_w ) {
				$cate.addClass('over');

				$('.category-nav li').removeClass('hide-point').each(function(){
					var this_w = $(this).width() + 150;
					var this_left = $(this).position().left;
					if( this_left + this_w > cateListInner_w ){
						$(this).addClass('hide-point');
						return false;
					}
				})

			}else{
				$cate.removeClass('over');
			}

		};


		catePopup();
		$(window).resize(catePopup);


		$('.cate-nav-ctrl').on('click',function(){
			$('.cate-popup').fadeIn(300);
		})

		$('.close-cate-popup').on('click',function(){
			$('.cate-popup').fadeOut(300);
		});


	};



	//func for data-bg-image
	$.fn.extend({
		PtBgTrans:function (obj) {
			this.each(function(){

				if ( $(this).data('bg') ) {
					var dataBgUrl = $(this).data('bg');
					$(this).css("backgroundImage",'url('+ dataBgUrl +')').imagesLoaded( { background: true }, function(instance) {
						var elements = instance.elements;
						$(elements).closest('.bg-full').removeAttr('data-bg').addClass('show');
						if ( $(elements).closest('.bg-full').parent().hasClass('title-group')) {
							$(elements).closest('.title-group').find('.pic-loader').remove();
						}
					})
				}

			});
			return this;
		}
	});


	//func for data-bg-image
	$.fn.extend({
		PtListBgTrans:function (obj) {
			this.each(function(){

				var $item = $(this).closest('.item');

				if ( $(this).data('bg') ) {

					var dataBgUrl = $(this).data('bg');
					$(this).css("backgroundImage",'url('+ dataBgUrl +')').imagesLoaded( { background: true }, function(instance) {
						var elements = instance.elements;
						$(elements).closest('.bg-full').addClass('done').removeAttr('data-bg');
						$item.addClass('show').find('.pic-loader').remove();
					})

				}else if( $(this).data('src') ){
					$item.addClass('show').find('.pic-loader').remove();
				}
			});
			return this;
		}
	});



	$.fn.extend({
		PtImgLoaded:function (obj) {
			this.each(function(){
				$(this).imagesLoaded( function(instance) {
					var elements = instance.elements;
					$(elements).closest('.img').addClass('img-loaded').find('.pic-loader').remove();
				})
			});
			return this;
		}
	});



	//func for data-bg-image
	$.fn.extend({
		PtPicListBgTrans:function (obj) {
			this.each(function(){

				if ( $(this).data('bg') ) {

					var dataBgUrl = $(this).data('bg');
					$(this).css("backgroundImage",'url('+ dataBgUrl +')').imagesLoaded( { background: true }, function(instance) {
						var elements = instance.elements;
						$(elements).closest('.bg-full').addClass('done').removeAttr('data-bg');

						var $parent = $(elements).closest('div[class^="img-"]');
						$parent.find('.pic-loader').remove();
					})
				}
			});
			return this;
		}
	});





	function responsive(){


		//mobile-mode
		var isMobile = /Mobi|Tablet|iPad|iPhone|Android/i.test(navigator.userAgent);
		var headerH;
		var footerH;


		if (  window.innerWidth <1024 || isMobile ) {
			$('.pt-header.menu-style-02').removeClass('menu-style-02').addClass('menu-style-01 edited');
			$('.pt-header:not(.menu-open-mode-02)').addClass('menu-open-mode-02 mode-02-edited');
			$('.pic-list.large-text').removeClass('large-text').addClass('large-edited');
			$('.title-group.style-02').removeClass('style-02').addClass('edited-02').addClass('style-01');
			$('.title-group.style-03').removeClass('style-03').addClass('edited-03').addClass('style-01');
			headerH = 70;
			footerH = 36;
			window.siteContentGap = 0;
		}else{
			$('.pt-header.menu-style-01.edited').removeClass('menu-style-01 edited').addClass('menu-style-02');
			$('.pt-header.mode-02-edited').removeClass('menu-open-mode-02 mode-02-edited');
			$('.pic-list.large-edited').addClass('large-text').removeClass('large-edited');
			$('.title-group.edited-02').removeClass('edited-02 style-01').addClass('style-02');
			$('.title-group.edited-03').removeClass('edited-03 style-01').addClass('style-03');
			headerH = 0;
			footerH = 0;
			window.siteContentGap = parseInt( $('.logo').css('left') ) * 2 +20;
		};




		if ( $('#wpadminbar').length==1 ) {
			if (  window.innerWidth > 780 ) {
				window.wp_bar_h = 32
			}else{
				window.wp_bar_h = 46;
			}
		}else{
			window.wp_bar_h = 0;
		};


		window.win_h = $(window).height() - wp_bar_h - headerH;
		window.win_w = window.innerWidth;

		window.siteContentW = win_w - siteContentGap * 2;




		if ( win_w <= win_h ) {
			$('body:not(.win-vertical)').addClass('win-vertical')
		}else{
			$('body.win-vertical').removeClass('win-vertical')
		};


		

		$('.addition-menu,.main-menu,.addition-menu > .wrap').css('max-height',win_h);
		$('.menu-wrap,.pt-popup').css('height', $(window).height() - wp_bar_h );
		$('.pc-mode .pt-header,.pt-popup').css('top',wp_bar_h);
		$('.m-mode .pt-header').css('top','0');
		$('.pt-header.menu-open-mode-01 .menu-bg span').height( Math.ceil( win_h * 0.3333));
		$('.pt-header.menu-open-mode-02 .menu-bg span').css('height','100%');





		$('.title-group.fullscreen').css('min-height',win_h);



		$('.pic-list:not(.grid-list) .item').css({
			'height': win_h
		});



		if ( (win_w / win_h) > 1.4 ) {

			var portfolioH = Math.ceil( win_h * 0.6 );
			if ( portfolioH % 2 != 0 ) {
				portfolioH += 1;
			};

			var portfolioW = Math.ceil(portfolioH / 0.56); 
			if ( portfolioW % 2 != 0 ) {
				portfolioW += 1;
			};

		}else{

			var portfolioW = Math.ceil( win_w * 0.8 );
			if ( portfolioW % 2 != 0 ) {
				portfolioW += 1;
			};

			var portfolioH = Math.ceil( portfolioW * 0.56 );
			if ( portfolioH % 2 != 0 ) {
				portfolioH += 1;
			};


		};
		
			

		$('.p-style-03 .img,.p-style-01 .img').css({
			'height':portfolioH,
			'width':portfolioW
		});

		$('.p-style-04 .inner-wrap').css({
			'height':portfolioH * 1.2,
			'width':portfolioW
		});


		$('.p-style-02 .img-addition').each(function(){

			var box_h = win_h * 0.7;
			var box_w2 = portfolioW;
			var pic_w = $(this).data('w');
			var pic_h = $(this).data('h');
			var box_w = Math.ceil( (box_h*pic_w)/pic_h );
			var box_h2 = Math.ceil( (box_w2*pic_h)/pic_w );




			if ( (win_w / win_h) > 1.4 ) { //horizontal

				if ( pic_w / pic_h < 1.4 ){
					$(this).css({
						'height':box_h,
						'width':box_w
					})
				}else{
					$(this).css({
						'height':box_h2,
						'width':box_w2
					})
				}


			}else{ //vertical

				$(this).css({
					'height':box_h2,
					'width':box_w2
				})


			};

			
		});


		$('.p-style-05 .inner-wrap').css({
			'width':portfolioW
		});






		$('.p-style-03 .img-addition').each(function(){
			var box_w = $(this).width();
			var pic_w = $(this).attr('data-w');
			var pic_h = $(this).attr('data-h');
			var box_h = Math.ceil( (box_w*pic_h)/pic_w );

			if ( box_h > portfolioH *0.85 ) {
				$(this).css({
					'height':portfolioH *0.85
				})
			}else{
				$(this).css({
					'height':box_h
				})
			}
			
		});


		$('.grid-list.type-auto .item').each(function(){
			var box_w = $('.img',this).width();
			var pic_w = $(this).attr('data-w');
			var pic_h = $(this).attr('data-h');
			var box_h = Math.ceil( (box_w*pic_h)/pic_w );
			$('.img',this).css({
				'height':box_h
			})
		});



		$('.blog-list.raw-proportion .item').each(function(){

			var box_w = $(this).children('.inner-wrap').width();
			var pic_w = $(this).attr('data-w');
			var pic_h = $(this).attr('data-h');
			var box_h = Math.ceil( (box_w*pic_h)/pic_w );

			$('.img',this).css({
				'height':box_h
			});

		});






		var headerElemH = $('.header-right').height() + parseInt( $('.header-right').css('top') )*2;
		var sliderBoxedH =  win_h - headerElemH*2;
		var sliderBoxedShortH = $('.sc-slider.boxed.h-short').width()* 0.5;


		$('body.single .single-inner,.default-template-page,body.no-title .blog-list,body.no-title .grid-list').css({
			'margin-top':headerElemH,
			'margin-bottom':headerElemH
		});



		// for fullscreen slider
		if( $('body').hasClass('win-vertical') ){
			$('.sc-slider.fullscreen:not(.h-short):not(.mobile-fullscreen)').css('height',win_w * 0.6);
			$('.sc-slider.fullscreen.h-short:not(.mobile-fullscreen)').css('height',win_w * 0.5);
			$('.sc-slider.fullscreen.mobile-fullscreen:not(.h-short)').css('height', win_h - footerH );
		}else{
			$('.sc-slider.fullscreen:not(.h-short)').css('height',win_h - footerH);
			$('.sc-slider.fullscreen.h-short').css('height',win_h * 0.7 );
		}


		if ( (win_w / win_h) > 1.4 ) {


			$('.sc-slider.boxed:not(.h-short)').css({
				'height': sliderBoxedH,
				'width':sliderBoxedH/0.562,
				'margin-top':headerElemH,
				'margin-bottom':headerElemH
			});

		}else{

			$('.sc-slider.boxed:not(.h-short)').css({
				'width': win_w * 0.9,
				'height':win_w * 0.51,//0.8 * 0.562
				'margin-top':win_w*0.05,
				'margin-bottom':win_w*0.05
			});
		};


		$('.sc-slider.boxed.h-short').css({
			'height': sliderBoxedShortH,
			'margin-top':headerElemH,
			'margin-bottom':headerElemH
		});
		


		var ajaxMarginTop;
		if ( window.innerWidth > 1680 ) {
			ajaxMarginTop = win_h * 0.08 + 1;
		}else{
			ajaxMarginTop = win_w * 0.03;
		}


		$('.ajax-content:not(.ajax-fullscreen) .ajax-target').css({
			'min-height': win_h,
			'margin-top':ajaxMarginTop,
			'top':wp_bar_h
		});


		$('.ajax-content.ajax-fullscreen .ajax-target').css({
			'min-height': win_h,
			'margin-top':ajaxMarginTop,
			'top':wp_bar_h
		});




		var bodyRollH = $('body').get(0).scrollHeight;
		if ( bodyRollH > $(window).height() && $('.p-slider-mode').length == 0 ) {
			$('body').addClass('is-overflow');
		}else{
			$('body').removeClass('is-overflow');
		};


		$('.m-mode .p-style-02.no-addition').css({
			'height':win_w * 0.7
		});





		var banner03MaxH = $('.sc-banner.style-03 .inner-wrap').width()*0.7;
		$('.sc-banner.style-03 .img,.sc-banner.style-03 .text').css('min-height',banner03MaxH);

		var banner03Line = parseInt($('.sc-banner.style-03 .intro').css('line-height'));

		if ( window.innerWidth > 1680 ) {
			$('.sc-banner.style-03 .ptbar-text-wrap').css('max-height',banner03Line*14);
		}else{
			$('.sc-banner.style-03 .ptbar-text-wrap').css('max-height',banner03Line*10);
		}

		if ( $('.p-style-02,.sc-slider:not(.t-bottom) .text').length > 0 ){

			setTimeout(function(){

				$('.p-style-02 .text,.sc-slider:not(.t-bottom) .text').removeClass('hack-blur-v hack-blur-h').each(function(){

					var $text = $(this);

					if ( $text.height() % 2 !== 0 ) {
						$text.addClass('hack-blur-v')
					}
					if ( $text.width() % 2 !== 0 ) {
						$text.addClass('hack-blur-h')
					}
					
				});

			},1000)

		}


	};//responsive-end


	function pt_shortcode(){


		$('.sc-gallery .item:not(.show) .img').append('<abbr class="pic-loader"><i></i><i></i><i></i></abbr>');

		$('.sc-gallery.lightbox a.full:not(.iv-link)').addClass('iv-link');

		$('.sc-gallery .item:not(.link-inited)').each(function(){
			if ( $('a.custom-link',this).length > 0 ) {
				$(this).addClass('has-link')
			};
		}).addClass('link-inited');


		$('.sc-gallery.type-auto .wrap:not(.isotope-inited)').isotope({
			itemSelector: '.item',
			transitionDuration: '0.3s',
			percentPosition: true
		}).isotope().addClass('isotope-inited');


		$('.sc-gallery:not(.type-auto) .wrap:not(.isotope-inited)').isotope({
			itemSelector: '.item',
			layoutMode: 'fitRows',
			transitionDuration: '0.3s'
		}).isotope().addClass('isotope-inited');


		$('.sc-gallery .inner-wrap:not(.hover-inited)').ptElasticHover({
			target:'.bg-full',
			offset:10,
			scale: 1.05
		}).addClass('hover-inited');


		$('.sc-gallery.lightbox:not(.lightbox-inited)').ptImageViewer({
			itemSelector: "a.full",
			fadeTime: 600,
			loop:true,
			skinStyle: 'dark'
		}).addClass('lightbox-inited');


		$('.sc-gallery .item:not(.show) .bg-full').PtListBgTrans();

		$('.sc-gallery .item.has-video .bg-full').after('<div class="call-item-video"></div>');


	}


	function sc_responsive(){

		$('.sc-gallery.type-auto').each(function(){

			var $root  = $(this),
				$items = $('.item',this),
				box_w   = $(this).find('.img').width();

			$items.each(function(){

				var pic_w = $(this).attr('data-w');
				var pic_h = $(this).attr('data-h');
				var box_h = Math.ceil( (box_w*pic_h)/pic_w );

				$('.img',this).css({
					'height':box_h
				});

			})

		})

	}



	function ptSingleSize(){

		var $singleImg = $('.single-header .wrap');
		var singleMainImgH = Math.ceil( $singleImg.width()*0.562 );


		if ( $('.single-inner.lightbox').length == 0  ) {

			if ( $('body > .ajax-fullscreen').length == 0 ) {

				$singleImg.height(singleMainImgH);

			}else if( $('.single-header.raw-proportion').length == 0 ){

				if( $('body.win-vertical').length > 0 ) {
					$singleImg.height( $(window).width() * 0.66 );
				}else{
					$singleImg.height( $(window).height() - wp_bar_h - 50 );
				}

			}
			
		};




		// raw-proportion imgSize of signle
		if ( $('.single-inner:not(.lightbox):not(.video) .single-header.raw-proportion').length > 0 ) {

			$('.single-header .item').each(function(){

				var pic_w = $(this).attr('data-w');
				var pic_h = $(this).attr('data-h');
				var box_h ;

				if ( $('.ajax-fullscreen').length > 0 && (win_w / win_h) > 1.4  ) {
					box_h = win_h - 50
				}else{
					box_h = singleMainImgH
				}

				var box_w = Math.ceil( (box_h*pic_w)/pic_h );


				if ( pic_h / pic_w > 0.7 ) {
					$('.img',this).css({
						'width':box_w,
						'height':box_h
					});
				}else{
					$('.img',this).css({
						'width': $('.single-header').width(),
						'height':box_h
					});
				}

				
				
			})

		};


		$('.single-extend .item').each(function(){
			var box_w = $('.img',this).width();
			var pic_w = $(this).attr('data-w');
			var pic_h = $(this).attr('data-h');
			var box_h = Math.ceil( (box_w*pic_h)/pic_w );

			$('.img',this).css({
				'height':box_h
			})
		});

		var banner03MaxH = $('.single-inner .sc-banner.style-03 .inner-wrap').width()*0.7;
		$('.single-inner .sc-banner.style-03 .img,.single-inner .sc-banner.style-03 .text').css('min-height',banner03MaxH);

		var banner03Line = parseInt($('.single-inner .sc-banner.style-03 .intro').css('line-height'));

		if ( window.innerWidth > 1680 ) {
			$('.single-inner .sc-banner.style-03 .intro').css('max-height',banner03Line*10);
		}else{
			$('.single-inner .sc-banner.style-03 .intro').css('max-height',banner03Line*8);
		}





	};//singleSize



	var ptSingleOnce = function(){


		$('.ajax-content .bg-color').each(function(){
			if ( $(this).data('color') ) {
				var colorCode = $(this).data('color');
				$(this).css('background',colorCode)
			};
		});


		if ( $('.single-header .item').length == 1){
			$('.single-header:not(.single-item)').addClass('single-item');
			var singlePic_w = $('.single-header .item').data('w');
			var singlePic_h = $('.single-header .item').data('h');
			if ( singlePic_h / singlePic_w > 0.7 ) {
				$('.single-header .item').addClass('vertical')
			}
		}

		if ( $('.single-header').hasClass('only-text') && $('body').hasClass('single')){
			$('.single-main-intro').wrap('<div class="text-banner"></div>');
			$('.single-inner').addClass('has-text-banner')
		}


		if ( $('.post-footer-info .img').length > 0 ){
			var imgSrc = $('.post-footer-info .img img').attr('src');
			$('.post-footer-info .img').css('backgroundImage','url('+imgSrc+')');
		}


		$('.single-extend .item:not(.show) .img,.single-related .item:not(.show) .img').append('<abbr class="pic-loader"><i></i><i></i><i></i></abbr>');
		$(".ajax-content .single-inner").find("h1,h2,h3,h4,h5,h6").addClass('h');
		$('.ajax-content a.btn').each(function(){
			if ( $(this).children('span').length == 0 ) {
				$(this).wrapInner('<span></span>')
			}
		});

		if ( !$('.sc-banner.style-03 .intro').parent().hasClass('content') ) {
			$('.single-inner .sc-banner.style-03 .intro').wrap('<div class="ptbar-text-wrap"><div class="content"></div></div>');
		}

		if ( $('.single-inner .single-nav .ctrl').length == 1 ) {
			$('.single-inner .single-nav').addClass('only-one')
		}

		


		$('.single-extend .item').each(function(){
			if ( $('.bg-full',this).data('src')) {
				$(this).addClass('has-video')
			}
		});

		category_popup();

		$('.single-inner:not(.fullscreen) .single-related .item').ptElasticHover({
			target:'.img'
		});




		if ( $('.single-inner:not(.lightbox):not(.video) .single-header .item').length > 0 ) {

			var singleHeaderAutoplay = $('.single-inner .single-header').data('autoplay') ? true : false;
			var singleHeaderDuration = singleHeaderAutoplay ? $('.single-inner .single-header').data('duration') : 0;

			$('.single-header:not(.raw-proportion):not(.single-item) .item').append('<div class="s-btn s-btn-prev"></div><div class="s-btn s-btn-next"></div>');

			$('.single-inner .single-header.raw-proportion:not(.single-item) .wrap').addClass('owl-carousel').owlCarousel({
				loop:true,
				responsiveRefreshRate:10,
				center:true,
				smartSpeed:600,
				navElement:'div',
				autoWidth:true,
				nav:true,
				dots:true,
				items:1,
				rtl:isRTL,
				autoplay: singleHeaderAutoplay,
				autoplayTimeout: singleHeaderDuration
			});



			$('.single-inner .single-header:not(.raw-proportion):not(.single-item)').addClass('pt-slider-root').ptSwipe({
				wrapTarget: '.wrap',
				itemTarget: '.item',
				autoplay: singleHeaderAutoplay,
				duration: singleHeaderDuration,
				speed: 900,
				tweenOutOffset: 0.95,
				ease: 'cubicInOut',
				onInit: function(){
				},
				onSwipeBefore: function(){
					this.$element.find('.item.leaving').stop().animate({opacity:0.4},400,'easeInQuad');
				},
				onSwipeAfter: function(){
					this.$element.find('.item').stop().css('opacity','1');
				}
			});

		};


		if ( $('.single-inner.lightbox .single-header').length > 0 ) {

			var itemLen =  $('.single-header .item').length;

			if ( $('.single-header.type-auto').length == 0)  {

				if (itemLen==1) {
					$('.single-header').attr('data-col-w','1');
				}else if (itemLen==2) {
					$('.single-header').attr('data-col-w','0.5');
				}else if (itemLen % 3 == 0 && itemLen % 4 !== 0) {
					$('.single-header').attr('data-col-w','0.33');
				}else if (itemLen % 5 == 0) {
					$('.single-header').attr('data-col-w','0.2');
				}else{
					$('.single-header').attr('data-col-w','0.25');
				};

			};



			$('.single-header').ptImageViewer({
				itemSelector: ".bg-full",
				fadeTime: 600,
				loop:true,
				skinStyle: 'dark'
			});

			$('.single-header .bg-full').addClass('hoverElems').ptElasticHover({
				scale: 1.08
			});


			if ( $('.single-header').hasClass('type-auto')) {

				var lastRowOption;
				var rowH;
				var itemLen = $('.single-header .item').length;

				if ( $('.single-header').data('last-row')=='style-01' || $('.single-header').data('last-row') == undefined ) {
					lastRowOption = true
				}
				if ( $('.single-header').data('last-row')=='style-02' ) {
					lastRowOption = false
				}

				if ( itemLen < 12 ) {
					rowH = win_h * 0.5
				}else if ( itemLen < 20 ){
					rowH = win_h * 0.4
				}else if ( itemLen < 30 ){
					rowH = win_h * 0.3
				}else{
					rowH = win_h * 0.2
				}

				if(window.__pt_header_force_row_height)
				{
					var rowValue = window.__pt_header_row_height === undefined ? 0.4 : window.__pt_header_row_height;
					rowH = win_h * rowValue;
				}

				$('.single-header .wrap').flexImages({
					rowHeight: rowH,
					container: '.item',
					object: 'div',
					truncate:lastRowOption
				});

			};


		}


		$('.single-inner.video .single-header .bg-full:not(.vTarget)').addClass('vTarget').ptMediaPlayer({
			autoplay: false,
			full:false,
			controls:true,
			ignoreMobile:false,
			removeVimeoControls: false,
			loop: false
		});


		if ( $('.single-inner.video .single-header .bg-full').data('volume') !== 0  && $('.single-inner.video').length > 0 ) {
			var $musicPlay = $('#music-player');

			$musicPlay.ptAudioPlayerCall('pause');
			$musicPlay.addClass('off');

			if ( $('.title-group .vTarget').length > 0 ) {
				$('.title-group').addClass('pause').find('.vTarget').ptMediaPlayerCall('pause');
			};

		}




		if ( $('.ajax-target .single-extend .has-video').length > 0 ) {

			$('.single-extend .item.has-video .bg-full').addClass('vTarget').ptMediaPlayer({
				autoplay: false,
				full:false,
				controls:true,
				ignoreMobile:false,
				removeVimeoControls: false,
				loop: false
			});

			allVideoCtrl();

		};


		if ( !$('.single-header .img .bg-full').data('bg')) {
			$('.single-header .pic-loader').remove()
		};



		// sc

		$('.single-inner .icon').each(function(){
			if ( $(this).data('color') ) {
				var colorCode = $(this).data('color');
				$(this).css('color',colorCode) 
			}
		});

		$('.single-inner .sc-text-carousel .wrap').addClass('owl-carousel').owlCarousel({
			loop:true,
			responsiveRefreshRate:10,
			smartSpeed:600,
			navElement:'div',
			nav:true,
			dots:true,
			items:1,
			rtl:isRTL
		});


		$('.single-inner .sc-banner .bg-full').PtBgTrans();
		$('.single-inner .sc-banner').each(function(){
			if ( $('.bg-full',this).data('src')){
				$(this).addClass('has-video')
			}
			if ( $('.img img',this).length == 0  ) {
				$(this).addClass('no-img');
				if ( $('a.btn',this).length > 0  ) {
					$('a.btn',this).wrap('<div class="btn-wrap"></div>');
					$('.intro',this).append( $('.btn-wrap',this) )
				}
			}
			if ( $(this).hasClass('style-03') && $('img',this).length > 0 ) {
				var imgUrl = $('.img img',this).attr('src');
				$('.img',this).append('<div class="bg-full" data-bg="'+imgUrl+'"></div>');
				$('.img .bg-full',this).PtBgTrans();
			}
			if ( $(this).children('.wrap').children('.bg-full,.bg-color').length == 0 ) {
				$(this).addClass('no-bg');
			}
		});

		var $ParallaxTarget;
		if ( $('body').hasClass('single') ) {
			$ParallaxTarget = window ;
		}else{
			$ParallaxTarget ='.ajax-content > .wrap'
		}

		$('.single-inner .sc-banner.parallax:not(.has-video)').attr({'data-offset':'0.5','data-direct':'0'}).ptParallax({
			target:'.wrap > .bg-full',
			addClass:'.pt-parallax',
			scrollTarget: $ParallaxTarget,
			useAnimate:false
		});

		if ( $('.single-inner .sc-banner.has-video').length > 0 ) {

			var $videoTarget = $('.sc-banner.has-video .bg-full');
			$videoTarget.addClass('vTarget').ptMediaPlayer({
				autoplay: false,
				full:true,
				controls:false,
				ignoreMobile:true,
				removeVimeoControls: true,
				loop: true
			});

			$('.sc-banner .vTarget').ptMediaPlayerCall('play');

		};


		$('.single-inner .sc-banner.parallax img').each(function(){
			$(this).imagesLoaded( function(instance) {
				var elements = instance.elements;
				$(elements).closest('.sc-banner').ptParallaxCall('resize')
			})
		});

		$('.single-inner .sc-mixbox .img').append('<abbr class="pic-loader"><i></i><i></i><i></i></abbr>');

		$('.single-inner .sc-mixbox .h').each(function(){
			if ( $(this).data('color') ) {
				var colorCode = $(this).data('color');
				$(this).css('color',colorCode)
			}
		});

		$('.single-inner .sc-mixbox').each(function(){
			if ( $('.item.has-bg-color',this).length == $('.item',this).length || $(this).hasClass('pricing-table')  ) {
				$(this).addClass('non-bg')
			}
			if ( $(this).children('.wrap').find('div').length == 0  ) {
				$(this).addClass('null')
			}
		});

		$('.single-inner .sc-mixbox .img img').PtImgLoaded();

		$('.single-inner.lightbox .bg-full').PtListBgTrans();
		$('.single-inner:not(.lightbox) .single-header ~ div .bg-full').PtListBgTrans();


	};

	//mobile-mode
	var bodyMode = function(){

		var isMobile = /Mobi|Tablet|iPad|iPhone|Android/i.test(navigator.userAgent);

		if ( isMobile ) {
			$('body').addClass('real-mobile')
		}

		if (  window.innerWidth <1024 || isMobile ) {
			$('body').addClass('m-mode').removeClass('pc-mode');
		}else{
			$('body').addClass('pc-mode').removeClass('m-mode');
		}


	};


	// main-function  ===================================================================================================================================================================

	$(function(){


		bodyMode();


		// retina logo
		if ( window.devicePixelRatio > 1 ) {
			$('html').addClass('retina');
			var $logoImg = $('.logo img');
			$logoImg.each(function(){
				if ( $(this).data('retina') !=='') {
					$(this).attr('src',$(this).data('retina'))
				}
			})
		}





		if (!!window.ActiveXObject || "ActiveXObject" in window  ){
			$('body').addClass('ie')
		}

		if ( navigator.userAgent.indexOf("Edge") > -1 ) {
			$('body').addClass('edge')
		}

		if ( navigator.userAgent.indexOf("Firefox") > -1 ) {
			$('body').addClass('ff')
		}

		if ( !navigator.userAgent.match(/window/i) ) {
			$('body').addClass('no-window')
		}


		if( $('body').hasClass('m-nav-fixed') ){
			$('.pt-header').before('<div class="m-nav-gap"></div>')
		}





		if ( $('.site-bg-code.style-01').length > 0 ){

			$('.site-bg-code').append("<i></i><i class='highlight'></i>");

		}

		if ( $('.site-bg-code.style-02').length > 0 ){

			$('.site-bg-code').append("<i></i><i></i><i></i><i></i><i></i><i></i>");

		}



		// for landing

		function landingImgShow(){
			$('.landing-img li').removeClass('show');
			var li_Index = $(this).index();
			$('.landing-img li').eq(li_Index).addClass('show');

			if ( $(this).children('.img').length > 0 && $('.landing-img li').eq(li_Index).find('.bg-full').hasClass('show') ) {
				$('.landing-img').addClass('hover');
			}else{
				$('.landing-img').removeClass('hover');
			}
		};


		function landingImgHide(){
			$('.landing-img li').removeClass('show');
			$('.landing-img').removeClass('hover');
		};



		if ( $('body').hasClass('page-landing')) {



			$('.addition-menu,.header-right').remove();
			$('.pt-header').addClass('show')
			$('.pt-header').removeClass('no-addition-menu menu-style-02').addClass('menu-style-01');

			$('.main-content .landing').append('<ul class="landing-img"></ul>');

			$('.main-menu > ul > li').each(function(){
				var imgHtml = '';
				if ( $(this).children('.img').length > 0 ) {
					imgHtml = $(this).children('.img').html();
				};
				$('.landing-img').append('<li>'+imgHtml+'</li>');
			});


			$('.main-menu > ul > li').hoverIntent({
				timeout:100,
				sensitivity:100,
				over: landingImgShow,
				out: landingImgHide
			});


			$('.landing-img .bg-full').PtBgTrans();


			var landing_color = $('.pt-header').data('text-color');
			if (landing_color =="white"){
				$('.landing-img,.site-bg').addClass('dark');
				$('.pt-header .logo,.header-right,.pt-footer .pt-social li,.call-popup,.go-top,#music-player').addClass('elem-color-white')
			};
			if (landing_color =="black"){
				$('.landing-img').addClass('light');
				$('.pt-header .logo,.header-right,.pt-footer .pt-social li,.call-popup,.go-top,#music-player').addClass('elem-color-black')
			};


		}else{
			$('.pt-header > .wrap').append('<div class="menu-bg"><span></span><span></span><span></span></div>');
		};






		


		// if user wp-bar is true
		if ( $('#wpadminbar').length==1) {
			$('html').addClass('has-wp-bar');
		};


		


		var $musicPlay = $('#music-player');
		var $ptHeader = $('.pt-header');
		var $titleGroup = $('.title-group');
		var BGMneedOFF = false;


		if($musicPlay.data('src'))
		{
			var musicSrc = $musicPlay.data('src');
			if(musicSrc.indexOf('{upload}') > -1)
			{
				musicSrc = musicSrc.replace(/\{upload\}/g, (window.__pt_upload_url || ''));
				$musicPlay.data('src', musicSrc);
			}
		}


		$musicPlay.ptAudioPlayer({
			'storeStatus':true
		});

		if( $('body.real-mobile').length > 0 ){
			$musicPlay.ptAudioPlayerCall('mute')
			$musicPlay.addClass('clickMute');
		}


		



		//all h addClass
		$("body").find("h1,h2,h3,h4,h5,h6").addClass('h');



		if ( $('.pt-header.outside-menu').length > 0 ) {

			$('.header-right').prepend( $('.menu-wrap .main-menu').clone());
			$('.header-right .main-menu').addClass('main-menu-outside').removeClass('main-menu');
			$ptHeader.append('<div class="bg-color"></div>');

			$('.pt-header.outside-menu').hover(function(){

				if ( $('.header-right.elem-color-white').length > 0 ) {
					$('header > .bg-color').addClass('show');
				}

				$('.main-menu-outside').addClass('show');

			},function(){

				$('header > .bg-color').removeClass('show');
				$('.main-menu-outside').removeClass('show');

			});


		};


		$('.text-area > a,.text-area li > a,.text-area p > a,.comment-area p a,.widget li > a').each(function(){
			if ( $(this).has('div').length == 0 && $(this).has('img').length == 0 && !$(this).hasClass('btn') ) {
				$(this).addClass('text-link')
			}
		});

		
		// detailPages video setting 
		$('.text-area iframe,.default-wrap iframe').each(function(){
			var iframe_src = $(this).attr('src');
			if ( iframe_src && (iframe_src.indexOf("youtube.com")>=0 || iframe_src.indexOf("vimeo.com")>=0 )) {
				$(this).wrap("<div class='iframe-wrap'></div>");
			};
		});

		$('.sc-banner').each(function(){
			if ( $('.img img',this).length == 0  ) {
				$(this).addClass('no-img');
				if ( $('a.btn',this).length > 0  ) {
					$('a.btn',this).wrap('<div class="btn-wrap"></div>');
					$('.intro',this).append( $('.btn-wrap',this) )
				}
			};
			if ( $(this).hasClass('style-03') && $('img',this).length > 0 ) {
				var imgUrl = $('.img img',this).attr('src');
				$('.img',this).append('<div class="bg-full" data-bg="'+imgUrl+'"></div>');
				$('.img .bg-full',this).PtBgTrans();
			}
			if ( $(this).children('.wrap').children('.bg-full,.bg-color').length == 0 ) {
				$(this).addClass('no-bg');
			}
		});


		if ( $('.grid-list').length > 0) {
			$('.pic-list .item').removeClass('p-style-01 p-style-02 p-style-03 p-style-04 p-style-05');
		};

		$('.list-category').each(function(){
			if ( $('a',this).length > 3 ) {
				$(this).addClass('overflow')
			}
		});

		

		// blog
		$('.blog-list .item').wrapInner('<div class="inner-wrap"></div>');
		$('.blog-list .item').each(function(){
			if( $(this).data('bg-color') && $(this).hasClass('text-post') ){
				var bgColor = $(this).data('bg-color');
				$(this).children('.inner-wrap').css('background',bgColor);
			}
		});


		$('.blog-list.style-02 .item').eq(0).after('<div class="item null-item"><div>');

		$('.blog-list.style-02 .item:not(.text-post) .intro').wrapInner('<div class="intro-inner"></div>');
		$('.blog-list.style-02 .item:not(.text-post)').each(function(){
			$('.intro',this).append( $('.btn-wrap',this) );
		});
		$('.blog-list.style-01 .item').each(function(){
			$('h4',this).before( $('.category',this) );
			$('.list-meta',this).append( $('.btn-wrap',this) );
		});



		if ( $('.wpcf7-form > p input:not([type="submit"])').length % 2 == 0 ) {
			$('.wpcf7-form > p input:not([type="submit"])').closest('p').addClass('form-col-2')
		}
		if ( $('.wpcf7-form > p input:not([type="submit"])').length == 3 ) {
			$('.wpcf7-form > p').eq(0).find('input:not([type="submit"])').closest('p').addClass('form-col-2');
			$('.wpcf7-form > p').eq(1).find('input:not([type="submit"])').closest('p').addClass('form-col-2');
		}







		if ( $('.site-bg .bg-full').data('bg')) {
			$('.site-bg').addClass('has-bg');
		};

		if ( $('.site-bg .bg-full').data('src')) {

			$('.site-bg .bg-full').addClass('vTarget').ptMediaPlayer({
				autoplay: true,
				full:true,
				controls:false,
				ignoreMobile:true,
				removeVimeoControls: true,
				loop: true
			});

		}




		$('.ptsc > .wrap').each(function(){
			if ( $(this).find('div').length == 0 && $(this).siblings('.section-title').length == 0 ) {
				$(this).parent().remove()
			}
		});



		// title-group



		if ( $titleGroup.length == 0 ) {
			$('body').addClass('no-title');
			$('.main-content > div').eq(0).find('.ptsc').eq(0).addClass('first-ptsc');
		}else{
			$('.title-group + .ptsc').addClass('first-ptsc');
		}


		if ( $('body.p-slider-mode .title-group').length > 0 ) {
			$titleGroup.addClass('fullscreen roll-active');
		}


		if( !$('.title-group .bg-full').data('bg') || $('.title-group .bg-full,.title-group .bg-color').length == 0 ){
			$titleGroup.addClass('no-bg')
		}


		if( $('.title-group .intro').length == 0 && $('.title-group').length > 0 ){
			$titleGroup.addClass('no-intro');
		}


		if( $('.pic-list.wide').length > 0 || $('.ptsc.first-ptsc.wide').length > 0 ){
			$titleGroup.addClass('wide');
		}










		if( $('.has-widget .blog-list').length > 0 ){

			if ( $('.category-nav li').length > 0 ) {
				$('.pt-widget-list').addClass('gap');
			}else{
				$('.category-nav').remove()
			}
			
		}

		if( !$('body').hasClass('has-widget') && $('.blog-list.large-image.style-01').length > 0 ){
			$('body').addClass('page-min-blog')
		}

		if( $('.blog-list.large-image').length > 0 ){
			$('body').addClass('blog-large-image')
		}



		$('.blog-list:not(.ajax) .v-post .img,.pic-list:not(.ajax) .v-post .img-main').append(itemVideoBtn);

		if ( $('.single-nav div').length == 0 ) {
			$('.single-nav').remove()
		}

		if ( $('.pt-social li').length == 0 ) {
			$('.pt-social').remove()
		}

		if ( $('.footer-left .pt-social li,.footer-left .call-popup,#music-player').length ==0  && $('.footer-widgets .copyright').length > 0  ){
			$('body').addClass('no-fixed-footer')
		}

		

		if ( $('.single-related .item').length == 0 ) {
			$('.single-related').remove()
		}

		if ( $('.project .single-meta .item').length == 0 ) {
			$('.single-main-intro').addClass('no-bottom-gap');
		}


		$('a.btn').each(function(){
			if ( $(this).children('span').length == 0 ) {
				$(this).wrapInner('<span></span>')
			}
		});






		$('.p-style-04 .img').each(function(){
			if ( $('.img-addition .bg-full',this).length == 0 ||  $('.img-main .bg-full',this).length == 0 ) {
				$(this).closest('.item').addClass('single')
			}
		});


		$('.p-style-03 .img-addition').each(function(){
			var imgW = $(this).data('w');
			var imgH = $(this).data('h');
			if ( imgW / imgH  < 1.33 ){
				$(this).addClass('type-02');
			}
		});



		$('.blog-list .img,.pic-list .img > div,.single-header .img,.title-group:not(.no-bg),.sc-slider .img,.single-extend .img,.sc-mixbox .img,.single-related .img').append('<abbr class="pic-loader"><i></i><i></i><i></i></abbr>');

		

		$('.title-group,.sc-slider .item,.single-extend .item,.sc-banner').each(function(){

			if ( $('.bg-full',this).data('src')) {

				$(this).addClass('has-video');

				if ( $('.bg-full',this).data('volume') == 0  ) {
					$(this).addClass('video-mute')
				}
			}
		});


		







		$('body').append('<div class="click-layer close-popup"></div>');
		$('.header-search form .wrap').append('<div class="click-layer close-search"></div>');
		



		// main menu edit ==================================================================================================================





		if ( $('header .default-menu-list').length > 0 ) {
			$('body > header ul.children,.m-header ul.children').addClass('sub-menu');
		}

		if ( $('.logo').hasClass('align-v-center')) {
			var logoH = $('.logo img:not(.addition)').attr('height');
			$('.header-right').css('height',logoH)
		}

		

		
		$('header nav .current_page_parent').addClass('current-menu-parent');
		$('header nav .current-menu-item,header nav .current_page_item').addClass('current_page_item');
		$('header nav .current_page_ancestor').addClass('current-menu-ancestor');
		
		$('.main-menu > ul > li,.main-menu-outside > ul > li').addClass('depth-1');
		$('.main-menu li.depth-1 > .sub-menu > li,.main-menu-outside li.depth-1 > .sub-menu > li').addClass('depth-2');

		// $('.main-menu li.depth-1,.main-menu-outside li.depth-1').each(function(){

		if( $('body').hasClass('woocommerce-page') ){
			$('.main-menu-outside > ul > li li.current-menu-item').closest('li.depth-1').addClass('current-menu-ancestor');
		}

		$('.main-menu li,.main-menu-outside li').each(function(){

			if ( $(this).children('.sub-menu').length > 0 ) {

				$(this).addClass('has-sub');

				if ( $(this).children('a').attr('href') =="#" || typeof($(this).children('a').attr('href')) == "undefined" ) {
					$(this).children('a').addClass('noHref');
				}else{
					$(this).children('a').after('<i class="btn call-sub-btn"></i>')
				}
			}

		});


		$('.main-menu-outside > ul > li > .sub-menu').each(function(){
			var $this = $(this);
			var li_Index = $(this).parent().index();
			if ( $this.find('.sub-menu').length == 0 && $this.children('li').length == $this.find('.bg-full').length ) {
				$this.closest('li').addClass('popup-parent');
				$this.wrap('<div class="popup-sub-menu" data-index="'+li_Index+'"><div class="wrap"></div></div>');
				$('body').append( $this.parent().parent() );
			}
		});



		// popup-sub-menu

		$('.popup-sub-menu .wrap').append('<div class="click-layer close-popup-menu"></div>');

		$('.popup-sub-menu .bg-full').PtBgTrans()

		$('.popup-parent').on('click',function(){
			var li_Index = $(this).index();
			var $target = $('.popup-sub-menu[data-index="'+li_Index+'"]');
			$target.fadeIn(10,function(){
				$target.find('.click-layer').fadeIn(500);
				$target.addClass('show');
			})
		});

		$('.close-popup-menu').on('click',function(){
			var $this = $(this);
			$this.closest('.popup-sub-menu').removeClass('show').fadeOut(300,function(){
				$this.fadeOut(0)
			});
		});

		
		$('.popup-sub-menu').each(function(){

			var sub_i = 0;

			$('li',this).each(function(){

				$(this).css({
					'transition':'all 0.6s ' + sub_i*0.1 + 's'
				});

				sub_i ++ ;

			})

		});








		$('.bg-color').each(function(){
			if ( $(this).data('color') ) {
				var colorCode = $(this).data('color');
				$(this).css('background',colorCode)
			};
		});


		$('.sc-mixbox .h').each(function(){
			if ( $(this).data('color') ) {
				var colorCode = $(this).data('color');
				$(this).css('color',colorCode)
			};
		});

		$('.sc-mixbox').each(function(){
			if ( $('.item.has-bg-color',this).length == $('.item',this).length || $(this).hasClass('pricing-table') ) {
				$(this).addClass('non-bg')
			};
			if ( $(this).children('.wrap').find('div').length == 0  ) {
				$(this).addClass('null')
			};
		});



		$('.pricing-table .item').each(function(){
			var color;
			if( $(this).data('color') ){
				color = $(this).data('color');
			}else{
				color = $('body').data('color');
			};
			$('.price-amount,li i',this).css('color',color);
			$('a.btn',this).hover(function(){
				$(this).css('background-color',color)
			},function(){
				$(this).removeAttr('style')
			});
		});


		$('.sc-text-carousel .wrap').addClass('owl-carousel').owlCarousel({
			loop:true,
			responsiveRefreshRate:10,
			smartSpeed:600,
			navElement:'div',
			nav:true,
			dots:true,
			items:1,
			rtl:isRTL
		});



		$('.p-style-02').each(function(){
			if ( !$('.img-addition .bg-full',this).data('bg')) {
				$(this).addClass('no-addition')
			}
		});


		if ( $('.addition-menu li').length <= 2) {
			$('.addition-menu').addClass('w50')
		};




		
		if ( $ptHeader.data('bg-color') ) {
			var menuBgColor = $ptHeader.data('bg-color');
			$('.pt-header .menu-bg span').css('background',menuBgColor)
		};

		if ( $('.pt-popup').data('bg-color') ) {
			var popupBgColor = $('.pt-popup').data('bg-color');
			$('.pt-popup').css('background-color',popupBgColor)
		};



		$('.site-bg .bg-full,.pt-popup .bg-full,.title-group .bg-full,.addition-menu .bg-full,.sc-banner .bg-full').PtBgTrans();







		$('.pt-popup').append('<i class="btn close-popup"></i>');

		$('.call-popup').on('click',function(){
			$('.pt-popup').addClass('show');
			$('.close-popup').fadeIn(500);
			$('body').addClass('over-hidden');
		});

		$('.close-popup').on('click',function(){
			$('.pt-popup').removeClass('show');
			$('body > .close-popup').fadeOut(300);
			$('body').removeClass('over-hidden');
		});


		$('.call-search').on('click',function(){

			$('.close-search,.header-search form').fadeIn(400);
			setTimeout(function (){ $('.header-search input.search').focus()},200);

		});

		$('.close-search').on('click',function(){
			$('.close-search,.header-search form').fadeOut(300);
			$('.header-search input.search').blur();
		});



		$('.call-menu').on('click',function(){

			if ( $('.pt-header').hasClass('no-addition-menu outside-menu') && $('body').hasClass('pc-mode') ) {
				return false;
			}

			$('.menu-open-mode-01 .menu-bg').addClass('on');

			$('.menu-wrap').css('opacity','1').addClass('on');
			$('body').addClass('over-hidden');


			setTimeout(function (){
				$ptHeader.addClass('show');
				$('i.close-menu').addClass('ddd').stop().delay(800).animate({opacity:1},400);
			},20);
			setTimeout(function (){ 
				$('.addition-menu').addClass('show');
			},400);

		});


		$('.close-menu').on('click',function(){

			$('.menu-bg').addClass('temp-translate');

			$ptHeader.removeClass('show');
			$('i.close-menu').stop().animate({opacity:0},200);
			$('.addition-menu').removeClass('show');


			$('.menu-wrap').animate({opacity:0},400,function(){
				$('.menu-wrap').removeClass('on');
			});


			$('body').removeClass('over-hidden');
			$('.pt-header:not(.outside-menu) .sub-menu').slideUp();
			$('.main-menu li').removeClass('open target');


			setTimeout(function(){
				$('.menu-bg').removeClass('temp-translate on');
			},700)
			
		});



		
	

		$musicPlay.on('click',function(){

			var $bgVideo = $('.site-bg .bg-full');

			if ( $musicPlay.hasClass('clickMute') ) {
				$musicPlay.ptAudioPlayerCall('unMute')
				$musicPlay.removeClass('clickMute off');
				$('body').removeClass('pt-audio-player-store-muted');
				BGMneedOFF = false;
			}else{
				$musicPlay.ptAudioPlayerCall('mute');
				$musicPlay.addClass('clickMute');
			}


		});



		if ( $('.title-group .bg-full').data('volume') !== 0 && $('.title-group .bg-full').data('src') ) {
			$musicPlay.ptAudioPlayerCall('pause');
			BGMneedOFF = true;
		};

		if( $('body.pt-audio-player-store-muted').length > 0 ){
		 	BGMneedOFF = true;
		 	$musicPlay.addClass('clickMute');
		};


		// normal video auto
		if( window.ptMediaPlayer ){

			window.ptMediaPlayer.onStatusChanged = function(hasVideoPlaying) {

				if( !$musicPlay.hasClass('clickMute') && $('body.single').length > 0 ){
					if( hasVideoPlaying ){
						$musicPlay.ptAudioPlayerCall('pause');
						$musicPlay.addClass('clickMute');
					}else if ( $('body.single.pt-audio-player-store-muted').length == 0){
						$musicPlay.ptAudioPlayerCall('resume');
						$musicPlay.removeClass('clickMute');
					}
				};
				
			}
		};










		var mainMenu_i = 0;
		var addiMenu_i;
		if ( $('.outside-menu').length > 0 ){
			addiMenu_i = 0;
		}else{
			addiMenu_i = 2;
		}

		$('.main-menu li.depth-1').each(function(){
			$(this).css({
				'transition':'opacity 0.5s ' + mainMenu_i*0.1 + 's'
			});
			mainMenu_i++;
		});

		$('.addition-menu li').each(function(){
			$(this).css({
				'transition':'opacity 0.5s ' + addiMenu_i*0.1 + 's'
			});
			addiMenu_i++;
		});



		$('.main-menu li.depth-1.has-sub > a.noHref,.main-menu li.depth-1 > .call-sub-btn').on('click',function(event){
			$(this).parent('li').addClass('target').toggleClass('open');
			$(this).siblings('.sub-menu').slideToggle();
			$('.main-menu li.open:not(.target) .sub-menu').slideUp();
			$('.main-menu li.open:not(.target)').removeClass('open');
			$('.main-menu li.target').removeClass('target');

			if ( $(this).attr('href')=="#") {
				event.preventDefault();
			};
		});

		$('.main-menu li.depth-2.has-sub > a.noHref,.main-menu li.depth-2 > .call-sub-btn').on('click',function(event){
			$(this).siblings('.sub-menu').slideToggle();
			if ( $(this).attr('href')=="#") {
				event.preventDefault();
			};
		});


		$('.main-menu-outside li.depth-2 .sub-menu').each(function(){
			var win_w = $(window).width();
			var offsetRight = $(this).offset().left + $(this).outerWidth();
			if ( offsetRight + 50 > win_w ) {
				$(this).addClass('left')
			}
		});


		$('.main-menu-outside a').on('click',function(event){
			if ( $(this).attr('href')=="#") {
				event.preventDefault();
			};
		});


		// site responsive ==================================================================================================================


		$('.sc-banner.style-03 .intro').wrap('<div class="ptbar-text-wrap"><div class="content"></div></div>');
		$('.ajax-fullscreen > .wrap').prepend('<div class="full-top-bar"><span class="bar-title"></span><i class="btn close-bar-single"></i></div>');




		responsive();
		sc_responsive();
		pt_shortcode();



		$('.sc-banner.style-03 .ptbar-text-wrap').ptBar({
			contentTarget: '.content'
		});

		$('.sc-mixbox .icon').each(function(){
			if ( $(this).data('color') ) {
				var colorCode = $(this).data('color');
				$(this).css('color',colorCode) 
			}
		});



		function ptListLazy(){

			$('.pic-list:not(.grid-list) .item:not(.show)').each(function(){
				var thisOffsetTop = $(this).offset().top;
				var $this = $(this);
				var thisH = $this.outerHeight() + 400;

				// for header
				if ( $(document).scrollTop() > thisOffsetTop - thisH  &&  $(document).scrollTop() < thisOffsetTop + thisH ){
					$this.find('.bg-full').PtPicListBgTrans();
					$this.addClass('show');
				};
			})

		};

		if ( $('.pic-list .item:not(.show)').length > 0 ) {
			ptListLazy();
		};

		$(window).resize(function(){
			if ( $('.pic-list .item:not(.show)').length > 0 ) {
				ptListLazy()
			}
		});
		
		$(document).on("scroll",function(){
			if ( $('.pic-list .item:not(.show)').length > 0 ) {
				ptListLazy()
			}
		});

		$('.grid-list .bg-full,.blog-list .bg-full').PtListBgTrans();



		$('body').append('<div class="go-top"></div>');

		if ( $('body > .pt-footer #music-player').length == 0 ) {
			$('.go-top').addClass('no-music')
		}



		function footerToBottom(){

			if ( $('.footer-widgets').length == 0 && $('m-mode').length == 0 ) {
				return;
			}
			
			var $footerWidget = $('.footer-widgets');
			$footerWidget.removeClass('fixed');

			var footerTop = parseInt( $footerWidget.offset().top + $footerWidget.outerHeight());

			if ( win_h > footerTop && $('.page-other').length == 0 ) {
				$footerWidget.addClass('fixed')
			}

			if( $('.page-other').length > 0 ){
				$footerWidget.css('margin-top',win_h)
			}

		};

		$('.blog-main > .wrap,.pic-list > .wrap').after( $('.pages') );



		if( $('.footer-widgets .widget').length > 0 ){

			var $footerWidget = $('.footer-widgets .widget');
			var $footerWidgetWrap = $('.footer-widgets');
			var widget_len =  $footerWidget.length;

			$footerWidgetWrap.attr('data-len',widget_len);
			$footerWidget.wrapInner('<div class="inner-wrap"></div>');

			if ( widget_len > 5 ) {
				$footerWidgetWrap.addClass('lot')
			}else{
				$footerWidget.addClass('pt-col-'+widget_len)
			}

			$('.footer-widgets .bg-full').PtBgTrans();

			if ( $footerWidgetWrap.find('.copyright').length > 0 && $footerWidgetWrap.find('.copyright').text()!==''  ) {
				$footerWidgetWrap.addClass('has-copyright');
			}


		}else if ( $('.pt-popup').length == 0 ) {
			$('.footer-right .copyright').css('display','inline-block');
		}



		


		if ( ( $titleGroup.data('header-color') == 'white' && $('.site-light').length > 0) || ($titleGroup.data('header-color') == 'black' && $('.site-dark').length > 0)  ) {
			 $titleGroup.addClass('need-to-change');
		};


		function checkElemsColor(){

			$('body:not(.page-landing) .pt-header .logo,.header-right,.pt-footer .pt-social li,.call-popup,.go-top,#music-player,.p-slider-ctrl').each(function(){
				var self = this;
				var items = $(self).ptCheckIntersects( $('.check-element-color,.footer-widgets,.title-group.need-to-change:not(.style-04),.sc-slider.fullscreen,.p-style-02,.main-content > .ptsc-list .sc-banner:not(.no-bg),.main-content > .sc-banner:not(.no-bg)') );

				if( items.length > 0 ){

					items.forEach(function(item){
						var thisHeaderColor = $(item).data('header-color');

						if( thisHeaderColor === 'white' ){
							if ( !$(self).hasClass('elem-color-white') ) {
								$(self).addClass('elem-color-white').removeClass('elem-color-black')
							}
						}else if( thisHeaderColor === 'black'){
							if ( !$(self).hasClass('elem-color-black') ) {
								$(self).addClass('elem-color-black').removeClass('elem-color-white')
							}
						}
					})



				}else if ( !$('body').hasClass('page-landing') ){

					$(this).removeClass('elem-color-white elem-color-black');

				}
			})
		}




		if ( $('.footer-widgets').length > 0 && $('m-mode').length == 0 ) {
			footerToBottom();
		};





		$(window).resize(function(){

			footerToBottom();
			checkElemsColor();

		});




		if ( $('body.real-mobile').length == 0 ) {


			$(document).on("scroll",function(){

				checkElemsColor();

				// for gototop
				if ( $(document).scrollTop() > win_h*0.6 ){
					$('.go-top:not(.show)').addClass('show');
				}else{
					$('.go-top.show').removeClass('show');
				};


				// for category is lots
				$('body > .body-category-wrap').fadeOut(200,function(){
					$('.body-category-wrap').remove();
					$('.show-all-category .close').removeAttr('style');
				});


				if ( $('.pt-header.outside-menu').length > 0 ) {
					
					if ( $(document).scrollTop() > 120 ){
						$('.pt-header:not(.hide-outsideMenu)').addClass('hide-outsideMenu');
					}else{
						$('.pt-header.hide-outsideMenu').removeClass('hide-outsideMenu');
					};

				};


			})

		};




		$('.go-top').on('click',function(){
			$('html,body').animate({scrollTop: '0px'},800)
		});



		//portfolio ==================================================================================================================

		var itemVideoBtn = '<div class="call-item-video"></div>';		


		$('.pic-list:not(.grid-list) .item').each(function(){
			if ( $(this).data('title-color')) {
				var titleColor = $(this).data('title-color');
				$('.h',this).css('color',titleColor);
				$('.list-category a',this).css({
					'color':titleColor,
					'border-color':titleColor
				})
			}
		});

		$('.grid-list .item').eq(0).after('<div class="item null-item"></div>');




		$('.grid-list.type-auto .wrap').isotope({
			itemSelector: '.item',
			transitionDuration: '0.3s',
			stamp: '.null-item',
			percentPosition: true
		}).isotope();


		$('.grid-list:not(.type-auto) .wrap').isotope({
			itemSelector: '.item',
			layoutMode: 'fitRows',
			transitionDuration: '0.3s'
		}).isotope();



		


		$('.filter-ctrl').on('click','li',function() {
			var $root = $(this).closest('.pic-list');
			$root.find('.filter-ctrl li').removeClass('active');
			$(this).addClass('active');

			var filterValue = $(this).attr('data-filter');

			$root.children('.wrap').isotope({ filter: filterValue });

			if ( $root.hasClass('type-auto')) {

				$root.find('.item').each(function(){
					var box_w = $('.img',this).width();
					var pic_w = $(this).attr('data-w');
					var pic_h = $(this).attr('data-h');
					var box_h = Math.ceil( (box_w*pic_h)/pic_w );
					$('.img',this).css({
						'height':box_h
					});
				});

				$root.children('.wrap').isotope();
			}

		});


		$('.call-filter').on('click',function() {
			if ( $('body').hasClass('.pc-mode')) {
				return false
			}
			$('.filter-ctrl').toggleClass('show')
		});



		//Parallax
		if ( $('.title-group .bg-full').data('bg') ) {

			$('.title-group.parallax').attr({'data-offset':'0.5','data-direct':'0'}).ptParallax({
				target:'.bg-full',
				addClass:'.pt-parallax',
				useAnimate:false
			})

		};

		$('body:not(.single) .sc-banner.parallax:not(.has-video)').attr({'data-offset':'0.5','data-direct':'0'}).ptParallax({
			target:'.wrap > .bg-full',
			addClass:'.pt-parallax',
			useAnimate:false
		});

		$('.item.p-style-02 .img-main').attr({'data-offset':'0.5','data-direct':'0'}).ptParallax({
			target:'.bg-full',
			addClass:'.pt-parallax',
			useAnimate:false
		});



		$('.p-style-01 .img,.item.p-style-02 .img-addition,.item.p-style-04 .img-main,.item.p-style-04 .img-addition,.p-style-03 .img > div,.p-style-05 .img > div').addClass('hoverElems').ptElasticHover({
			offset:10,
			scale: 1.02
		});

		$('ul.products li.product a img').addClass('hoverElems').ptElasticHover({
			offset:10,
			scale: 1
		});

		$('.call-menu.btn,.addition-menu .inner-wrap').addClass('hoverElems').ptElasticHover();

		$('.blog-list .img').ptElasticHover({
			target:'.bg-full',
			offset:20,
			scale: 1.1
		})

		$('.grid-list .inner-wrap').ptElasticHover({
			target:'.img > div',
			offset:10,
			scale: 1.1
		})



		$('.sc-banner.parallax img').each(function(){
			$(this).imagesLoaded( function(instance) {
				var elements = instance.elements;
				$(elements).closest('.sc-banner').ptParallaxCall('resize')
			})
		});



		$('.sc-banner.style-03 .inner-wrap>.img').attr({'data-offset':'0.1', 'data-direct':'0'}).ptElementParallax();
		$('.sc-banner.style-03 .inner-wrap>.text').attr({'data-offset':'0.1', 'data-direct':'1'}).ptElementParallax();




		$('.p-style-04 .img-addition .bg-full').attr({'data-offset':'0.1', 'data-direct':'1'}).ptElementParallax({
			container: '.item'
		});

		$('.p-style-03 .img-addition .bg-full,.p-style-05 .img-addition .bg-full').attr({'data-offset':'0.2', 'data-direct':'0'}).ptElementParallax({
			container: '.item'
		});




		// list-meta && single-meta if lots
		window.category_popup = function(){

			$('.blog-list .item .category,.post .single-main-intro .category').each(function(){
				if ( $('a',this).length > 2 ) {
					$(this).wrapInner('<div class="over-content"></div>');
					$(this).append('<em class="btn show-all-category"><i class="call"></i><i class="close"></i></em>');
				}
			});


			$('.show-all-category .call').on('click',function(){

				if ( $('body > .body-category-wrap').length == 0) {
					var $parent = $(this).closest('.category');
					var cate_a = $parent.find('.over-content').html();
					$parent.find('.close').css('display','block');
					$('body').append('<div class="body-category-wrap"><div class="inner-wrap">'+cate_a+'<em class="btn close-category"></em></div></div>');
					var thisX = $(this).offset().left;
					var thisY = $(this).offset().top - $(document).scrollTop() + 30;
					$('.body-category-wrap').css({
						'top':thisY,
						'left':thisX
					}).fadeIn(200);
				}

				$('.close-category,.show-all-category .close').on('click',function(){
					$('.body-category-wrap').fadeOut(200,function(){
						$('.body-category-wrap').remove();
					});
					$parent.find('.show-all-category .close').removeAttr('style');
				});
				

			});

			

		};

		if( $('.blog-list').length > 0 ){

			category_popup();

			$('.blog-main > .wrap').isotope({
				itemSelector: '.item',
				transitionDuration: '0.3s'
			});


		};


		$('.footer-widgets .textwidget').each(function(){
			if ( $(this).find('img.alignleft').length == 1 && $(this).find('img').length == 1 ){
				$(this).addClass('one-img').children('p:first-child').before( $('img.alignleft',this) )
			}
		});

		$('.widget_media_gallery').each(function(){
			var link = $('.gallery-icon > a',this).attr('href');
			if ( link && (link.indexOf(".jpg") > -1 || link.indexOf(".gif") > -1 || link.indexOf(".png") > -1 || link.indexOf(".jpeg") > -1 )) {
				$(this).addClass('iv');
				$('.gallery-item',this).each(function(){
					$(this).attr('data-src',$('a',this).attr('href'))
					$('a',this).removeAttr('href')
				})
			}
		});


		$('.widget_media_gallery.iv').ptImageViewer({
			itemSelector: ".gallery-item",
			fadeTime: 600,
			loop:true,
			skinStyle: 'dark'
		});

		


		// Added on 2018/06/20 for version 1.0.1
		$('.lightbox-item').ptImageViewer({
			itemSelector: ".img",
			fadeTime: 600,
			loop:true,
			skinStyle: 'dark'
		});



		//sc gallery 



		// scSlider

	

		$('.sc-slider').each(function(){

			if( $(this).data('video-autoplay')=="1"){
				$('.item.has-video',this).addClass('autoplay');
			}


			if ( $('.item',this).length !== $('.text > .h',this).length ) {
				$(this).addClass('small-dots');
			}


			if ( $('.item',this).length == 1 ) {
				$(this).addClass('single-item small-dots');
			}


			var i = 1;
			var before_num;

			$('.item',this).each(function(){

				if ( $(this).index()>=9) {
					before_num = ''
				}else{
					before_num = '0'
				};

				$('.text',this).prepend('<i class="slider-count">' + before_num + i + '</i>');
				i ++;

				if ( $('.intro',this).length == 0 ) {
					$('.text',this).addClass('no-intro');
				}

			});
			

			if ( $('.item',this).length > 1 ) {
				$('.item',this).append('<div class="s-btn s-btn-prev"></div><div class="s-btn s-btn-next"></div>');
			}


		});




		$('.sc-slider:not(.single-item)').addClass('pt-slider-root').ptSwipe({
			wrapTarget: '.wrap',
			itemTarget: '.item',
			autoplay: false,
			speed: 900,
			tweenOutOffset: 0.9,
			ease: 'cubicInOut',
			onInit: function(){

				// style
				this.$element.find('.item.current .text').addClass('show');


				var $this = this;
				setTimeout(function(){
					$this.$element.find('.item.current.has-video.autoplay .bg-full').ptMediaPlayerCall('play');
				},500)

				// if video hadn't been 'autoplay' 
				this.$element.find('.item.has-video:not(.autoplay) .bg-full').ptMediaPlayerCall('pause');
				this.$element.find('.item.has-video:not(.autoplay)').addClass('pause');

				// bg music
				if ( this.$element.find('.item.current .bg-full').data('volume') !== 0 && this.$element.find('.item.current.has-video').length > 0  ) {
					$musicPlay.ptAudioPlayerCall('pause');
				}

			},
			onSwipeBefore: function(){
				this.$element.find('.item.leaving').stop().animate({opacity:0.3},400,'easeInQuad');

				// all video to pause
				this.$element.find('.item.has-video .bg-full').ptMediaPlayerCall('pause');
				this.$element.find('.item.has-video:not(.autoplay)').addClass('pause');
			},
			onSwipeAfter: function(){

				// style
				this.$element.find('.item').stop().css('opacity','1');
				this.$element.find('.item.current .text').addClass('show')
				this.$element.find('.item:not(.current) .text').removeClass('show');


				// slider keep autoplay
				if( this.$element.find('.item.current.has-video:not(.autoplay):not(.pause),.item.current.autoplay:not(.pause)').length > 0 ){
					this.$element.ptSwipeCall('pause');
				}else if( this.$element.data('autoplay') == 1 ){
					this.$element.ptSwipeCall('resume');
				}


				$('.sc-slider .item:not(.current) .mute-for-click').removeClass('mute-for-click');
				if ( $('.sc-slider .item.current .mute-for-click').length == 0){
					$('.sc-slider .bg-full.has-data-video').ptMediaPlayerCall('unMute');
				}

				// autoplay video
				this.$element.find('.item.current.autoplay.has-video:not(.pause) .bg-full').ptMediaPlayerCall('play');


				// bg music
				if ( this.$element.find('.item.current .bg-full').data('volume') !== 0 && this.$element.find('.item.current.has-video').length > 0 ) {
					$musicPlay.ptAudioPlayerCall('pause');
					$musicPlay.addClass('off');
				}else if( !$musicPlay.hasClass('clickMute') && BGMneedOFF == false && $('.sc-slider .item.current.has-video:not(.video-mute):not(.pause)').length == 0 ){
					$musicPlay.ptAudioPlayerCall('resume');
					$musicPlay.removeClass('off');
				}

			}
		});




		// edit dots text
		$('.sc-slider').each(function(){

			var $this = $(this);
			var imagesQueueNum = 0;

			if ( $('.item',this).length == $('.h',this).length && !$(this).hasClass('small-dots')){
				$('.item',this).each(function(){
					var imagesQueueElem = $('.h',this).text();
					$this.find('.pt-swipe-dots li').eq(imagesQueueNum).append( '<span>'+imagesQueueElem+'</span>');
					imagesQueueNum ++;
				});
			}

			if ( $(this).hasClass('small-dots')) {


				$('.item',this).each(function(){

					var $intro = $('.intro',this);
					var $title = $('.text > .h',this);
					
					if ( $intro.length > 0 && $title.length > 0 ){
						$title.append( $intro ).append('<i class="btn call-slider-intro"></i>')
					}

				});

			}

		});




		// text dots for lots setting
		$('.sc-slider:not(.small-dots) .pt-swipe-dots').wrapInner('<div class="roll-target"></div>');


		var dotsCD = 900,
			dotsWinPercent = 0.3;

		function textDotsRoll(){

			$('.sc-slider').each(function(){

				var $this = $(this);

				if ( !$this.hasClass('small-dots') ) {

					var dotsItemHeight = $this.find('.pt-swipe-dots li').eq(0).height();
					var dotsMaxItemCount = Math.ceil( $this.height() *dotsWinPercent / dotsItemHeight);
					var dotsMaxHeight = dotsItemHeight * dotsMaxItemCount;
					
					$('.sc-slider .roll-target').css('max-height', dotsMaxHeight );
					$('.sc-slider .roll-target li.selected').trigger('click');

				}

				if ( $('body.m-mode').length > 0 ) {
					$('.sc-slider:not(.small-dots)').addClass('small-dots edited');
				}else{
					$('.sc-slider.small-dots.edited').removeClass('small-dots edited');
				};

				$('.sc-slider .item.current .text').addClass('show');

			})

		};


		$('.sc-slider .roll-target li').on('click',function(){

			var $root = $(this).closest('.sc-slider');

			var $dotsList = $(this).parent();
			var dotsItemHeight = $(this).height();
			var dotsMaxItemCount = Math.ceil( $root.height() * dotsWinPercent / dotsItemHeight );
			var dotsMaxHeight = dotsItemHeight * dotsMaxItemCount;

			var dotsItemCount = $dotsList.children('li').length;
			if( dotsItemCount <= dotsMaxItemCount)
			{
				return;
			}

			var parentNode = this.parentNode || {};
			if(parentNode.dotsInCD)
			{
				return;
			}

			parentNode.dotsInCD = true;
			setTimeout(function(){
				parentNode.dotsInCD = false;
			}, dotsCD);

			var $this = $(this);
			var index = $this.index();
			var cHeight = (index + 1) * dotsItemHeight;
			var y = (dotsMaxHeight / 2) - cHeight;
			y = Math.ceil(y / dotsItemHeight) * dotsItemHeight;
			if(y >= 0)
			{
				y = 0;

				$(this).closest('.pt-swipe-dots').removeClass('top-has-distance').addClass('bottom-has-distance');
			}
			else if(y <= dotsMaxHeight - dotsItemCount * dotsItemHeight)
			{
				y = dotsMaxHeight - dotsItemCount * dotsItemHeight;

				$(this).closest('.pt-swipe-dots').addClass('top-has-distance').removeClass('bottom-has-distance');
			}
			else
			{
				$(this).closest('.pt-swipe-dots').addClass('top-has-distance bottom-has-distance');
			}

			dotsOnRenderListY(parentNode, y, 600);
		});

		var dptsOnUpdateY = function(tween) {
			var target = tween.target.target;
			$(target).css('transform', 'translate3d(0,' + target._tweenValue + 'px,0)');
		};

		var dotsOnRenderListY = function(target, y, time) {
			createjs.Tween.get(
				target,
				{
					override:true,
					onChange:dptsOnUpdateY
				}
			).to(
				{_tweenValue:y},
				time === undefined ? 800 : time,
				createjs.Ease.cubicOut
			);
		};

		textDotsRoll();

		

		$(window).resize(textDotsRoll);





		function sliderArrow(){
			// slider arrow 
			$('.s-btn-prev').on('click',function(){
				$(this).closest('.pt-slider-root').ptSwipeCall('prev');
				$('body').addClass('forPrev');
				$(this).closest('.pt-slider-root').find('.roll-target li.selected').trigger('click');
				setTimeout(function(){
					$('body').removeClass('forPrev forNext');
				},800)
			});
			$('.s-btn-next').on('click',function(){
				$(this).closest('.pt-slider-root').ptSwipeCall('next');
				$('body').addClass('forNext');
				$(this).closest('.pt-slider-root').find('.roll-target li.selected').trigger('click');
				setTimeout(function(){
					$('body').removeClass('forPrev forNext');
				},800)
			});
		};

		sliderArrow();






		


		$('.sc-slider,body.single .single-inner:not(.lightbox) .single-header').each(function(){
			imageQueueLoad(this);
		});


		$('.menu-style-02 .main-menu > ul > li > .img .bg-full,.menu-style-01.edited .bg-full').PtBgTrans();



		$('.sc-mixbox .img img').PtImgLoaded();
		





		if ( $('body.p-slider-mode').length > 0 ) {

			if ('scrollRestoration' in history) {
				history.scrollRestoration = 'manual';
			}



			var sliderTop = 0;
			var isToBottom = false;
			var sliderActiveNum = 1;
			var sliderItemLen = $('.pic-list .item').length;
			var sliderInCd = false;
			var sliderIsInited = false;
			var wp_bar_h =0;
			if ( $('#wpadminbar').length == 1 ) {
				wp_bar_h = 32
			};


			if ( $titleGroup.length > 0 ) {
				sliderItemLen += 1;
			};






			var sliderCtrlHtml = '<div class="p-slider-ctrl"><div class="ctrl prev"></div><div class="slider-num"><i class="current">1</i><hr/><i class="all">'+sliderItemLen+'</i></div><div class="ctrl next"></div></div>';
			$('body').append(sliderCtrlHtml);






			var ptSlideInit = function(){

				var newTop;
				var wp_bar_h = 0;
				if ( $('#wpadminbar').length == 1 ) {
					wp_bar_h = 32
				};

				if(isToBottom)
				{
					newTop = document.body.scrollHeight - win_h;
				}
				else if ( $('.title-group').length > 0 )
				{
					if ( sliderActiveNum == 1 ) {
						newTop = $('.title-group').offset().top - wp_bar_h;
					}else{
						newTop = $('.pic-list .item').eq(sliderActiveNum - 2).offset().top - wp_bar_h;
					}
				}
				else
				{
					newTop = $('.pic-list .item').eq(sliderActiveNum - 1).offset().top - wp_bar_h;
				}

				sliderTop = newTop;
				
				sliderRender(sliderTop, 900);


			};




			if( window.ptSliderObj === undefined ){

				window.ptSliderObj = {
					value: $('html,body').scrollTop()
				}

				var recentTop = $('html,body').scrollTop();
				sliderTop = Math.round(recentTop);
				ptSliderObj.value = recentTop;
				sliderIsInited = true;

			}



			var sliderOnUpdate = function(tween) {
				if( $('.m-mode').length == 0 ){
					var scrollTop = Math.round(ptSliderObj.value);
					$('html,body').scrollTop(scrollTop);
				};
			};



			var sliderRender = function(y, time) {

				if( $('.m-mode').length > 0 ){
					return;
				};

				window.pt__sliderModeInited = true;

				createjs.Tween.get(
					ptSliderObj,
					{
						override:true,
						onChange:sliderOnUpdate
					}
				).to(
					{value:y},
					time === undefined ? 900 : time,
					createjs.Ease.cubicOut
				);

			};

			var sliderUpdateButtons = function() {

				if(sliderItemLen < 2)
				{
					$('.p-slider-ctrl .ctrl').addClass('disabled');
				}
				else if( sliderActiveNum === 1 )
				{
					$('.p-slider-ctrl .ctrl.prev').addClass('disabled');
					$('.p-slider-ctrl .ctrl.next').removeClass('disabled');
				}
				else if(sliderActiveNum === sliderItemLen)
				{
					$('.p-slider-ctrl .ctrl.prev').removeClass('disabled');
					$('.p-slider-ctrl .ctrl.next').addClass('disabled');
				}
				else
				{
					$('.p-slider-ctrl .ctrl').removeClass('disabled');
				}
			};




			$('.p-slider-ctrl .ctrl').on('click',function(){

				if(!sliderIsInited)
				{
					return;
				}

				var $this = $(this);
				var sliderDir;
				var pageH = 0;

				if ( $this.hasClass('prev') ) {

					if (isToBottom)
					{
						var newTop = 0;
						if ($('.title-group').length > 0) {
							if (sliderActiveNum == 1) {
								newTop = $('.title-group').offset().top - wp_bar_h;
							} else {
								newTop = $('.pic-list .item').eq(sliderActiveNum - 2).offset().top - wp_bar_h;
							}
						}
						else {
							newTop = $('.pic-list .item').eq(sliderActiveNum - 1).offset().top - wp_bar_h;
						}
						sliderTop = newTop;
						sliderRender(sliderTop, 600);

						isToBottom = false;
					}
					else if ( sliderActiveNum > 1 )
					{
						sliderActiveNum--;
						sliderTop -= win_h;

						if(sliderTop < 0)
						{
							sliderTop = 0;
						}

						sliderUpdateButtons();

						sliderRender(sliderTop, 900);
					}

				};

				if ( $this.hasClass('next') ) {

					if ( sliderActiveNum < sliderItemLen ) {
						sliderActiveNum++;
						sliderTop += win_h;

						sliderUpdateButtons();
						
						sliderRender(sliderTop, 900);
					}
					else if (sliderTop !== document.body.scrollHeight - win_h )
					{
						sliderRender(document.body.scrollHeight - win_h, 600);
						isToBottom = true;
					}

				};

				

				$('.slider-num .current').html(sliderActiveNum);


			});


			ptSlideInit();


			$(window).resize(ptSlideInit);


			$('.main-content,.footer-widgets').on('wheel', function(evt){


				if(!sliderIsInited || sliderInCd || $('.m-mode').length > 0 || window.ptSliderObj === undefined )
				{
					return;
				};



				sliderInCd = true;

				
				setTimeout(function(){
					sliderInCd = false;
				}, 500);

				if(evt.originalEvent.deltaY > 0)
				{
					$('.p-slider-ctrl .ctrl.next').trigger('click');
				}
				else
				{
					$('.p-slider-ctrl .ctrl.prev').trigger('click');
				}
			})

		};


		// portfolio as silder end





		

		

		


		




		// video play ==================================================================================================================

		var videoBottonHtml = '<div class="v-play v-ctrl"></div><div class="v-pause v-ctrl"></div><div class="v-mobile-play v-ctrl"></div>';
		$('.title-group.has-video,.sc-slider .item.has-video').append(videoBottonHtml);
		$('.single-extend .img').append('<div class="v-mobile-play v-ctrl"></div>');



		if ( $('.title-group.has-video').length > 0 && $('.sc-slider .item.has-video').length > 0 ) {
			$('.title-group.has-video .bg-full').attr('volume','0');
		};


		if ( $('.title-group.has-video,.sc-slider .item.has-video,.sc-banner.has-video').length > 0 ) {

			var $videoTarget = $('.title-group.has-video .bg-full,.sc-slider .item.has-video .bg-full,.sc-banner.has-video .bg-full');
			$videoTarget.addClass('vTarget').ptMediaPlayer({
				autoplay: false,
				full:true,
				controls:false,
				ignoreMobile:true,
				removeVimeoControls: true,
				loop: true
			});

			$titleGroup.find('.vTarget').ptMediaPlayerCall('play');
			$('.sc-banner .vTarget').ptMediaPlayerCall('play');

		};


		if ( $('.single-extend .item.has-video').length > 0 ) {

			$('.single-extend .item.has-video .bg-full').addClass('vTarget').ptMediaPlayer({
				autoplay: false,
				full:false,
				controls:true,
				ignoreMobile:true,
				removeVimeoControls: false,
				loop: false
			});

		};


		window.allVideoCtrl = function(){




			$('.v-pause').on('click',function(){

				$('.sc-slider .bg-full.has-data-video').ptMediaPlayerCall('unMute').removeClass('mute-for-click');

				if ( $(this).parent('.title-group.has-video').length > 0 ) {

					$titleGroup.addClass('pause');
					$('.title-group .bg-full').ptMediaPlayerCall('pause');

				}else{

					$('.item.has-video').addClass('pause');
					$('.item.has-video .bg-full').ptMediaPlayerCall('pause');

					var $video_root = $(this).closest('.sc-slider');
					if( $video_root.length > 0  && $video_root.data('autoplay')==1){
						$video_root.ptSwipeCall('resume');
					}
				};

				if ( $(this).closest('.has-video').find('.bg-full').data('volume') !== 0 && !$musicPlay.hasClass('clickMute') && BGMneedOFF == false ) {
					$musicPlay.ptAudioPlayerCall('resume');
				}

			});


			$('.v-play').on('click',function(){

				var $videoParent;
				var $video_root;

				if ( $(this).closest('.title-group.has-video').length > 0 ) {
					$videoParent = $('.title-group');
					$video_root = $('.title-group');
				}else{
					$videoParent = $(this).closest('.item');
					$video_root = $videoParent.parent().parent();
				}

				$videoParent.removeClass('pause');
				$videoParent.find('.bg-full').ptMediaPlayerCall('play');


				if ( !$videoParent.hasClass('video-mute') && $video_root.hasClass('sc-slider') ) {
					$('.sc-slider .bg-full.has-data-video').ptMediaPlayerCall('mute');
					$videoParent.find('.bg-full.has-data-video').ptMediaPlayerCall('unMute').addClass('mute-for-click');
				}


				if ( $(this).closest('.has-video').find('.bg-full').data('volume') !== 0 ) {
					$musicPlay.ptAudioPlayerCall('pause');
				}

				if ( $video_root.hasClass('sc-slider')){
					if( !$videoParent.hasClass('autoplay') ){
						$video_root.ptSwipeCall('pause');
					}
				}


			});


			// item popup player 

			$('.call-item-video,.v-mobile-play').on('click',function(){


				$musicPlay.ptAudioPlayerCall('pause');
				
				if ( $titleGroup.find('.vTarget').length > 0 && $titleGroup.find('.vTarget').data('volume') !== 0 ) {
					$titleGroup.addClass('pause').find('.vTarget').ptMediaPlayerCall('pause');
				};


				var $vTarget;

				if ( $(this).hasClass('v-mobile-play')) {

					if ( $(this).parent().hasClass('title-group') ) {
						$vTarget = $(this).siblings('.vTarget');
					}else{
						$vTarget = $(this).closest('.item').find('.vTarget');
					};

				}else{
					$vTarget = $(this).siblings('.bg-full');
				};



				var $videoHtml = $('<div class="bg-full"></div>');
				var dataParams = $vTarget.data();
				for(var pKey in dataParams)
				{
					if(dataParams.hasOwnProperty(pKey))
					{
						$videoHtml.data(pKey, dataParams[pKey]);
					}
				}

				var $videoWrap = $('<div class="popup-player"><div class="player-wrap"></div><span class="close"></span></div>');
				$videoWrap.children('.player-wrap').append($videoHtml);
				$('body').append($videoWrap);




				var $v_player = $('.popup-player');

				$v_player.fadeIn(0,function(){

					$('.popup-player .close').addClass('show');
					$v_player.animate({opacity:1},400,function(){
						
						$v_player.find('.bg-full').ptMediaPlayer({
							autoplay: true,
							full:false,
							controls:true,
							ignoreMobile:false,
							loop: false
						})
					})
				});


				$('.popup-player .close').on('click',function(){

					if ( BGMneedOFF == false && !$musicPlay.hasClass('clickMute') ) {
						$musicPlay.ptAudioPlayerCall('resume');
					}else{
						$('.title-group.pause').removeClass('pause').find('.vTarget').ptMediaPlayerCall('play')
					};


					$(this).removeClass('show');
					$v_player.animate({opacity:0},400,function(){
						$v_player.find('.bg-full').ptMediaPlayerCall('dispose',true);
						$v_player.remove();
					})

				})

			});


		};//v play end

		allVideoCtrl();






		// comment-form 
		$(".comment-form input,.comment-form textarea").focus(function(){
			$(this).closest('p').addClass('focus');
			if ( $(this).attr('name') == 'comment' ) {
				$(this).addClass('active')
			}
		});
		$(".comment-form input,.comment-form textarea").blur(function(){
			$(this).closest('p').removeClass('focus');
			var _val = $(this).val();
			if ( $(this).attr('name') == 'comment' && _val=="" ) {
				$(this).removeClass('active')
			}
		});






		$('.pic-list.ajax .item:not(.has-custom-link) a.full,.blog-list.ajax .h a,.blog-list.ajax a.full,.blog-list.ajax a.btn').addClass('ajax-link');


		// a data-href
		var ptHandleLink = function(evt)
		{
            evt = evt || {};
            var href = this.href || $(this).data('href');

            if($(this).hasClass('ajax-link') || $(this).hasClass('force-ajax-link'))
            {
                if(evt.ctrlKey && href)
                {
                    evt.preventDefault();
                    evt.stopPropagation();
                    window.open(href);
                }
                return;
            }

			if ( $(this).hasClass('ajax_add_to_cart') || $(this).hasClass('remove_from_cart_button') || $(this).hasClass('iv-link') )
			{
				return;
			}

			if(window.ptPreventLink)
			{
				window.ptPreventLink = false;
				return;
			}

			if(!href || href=='#') return;

			var cleanHref = decodeURIComponent(href).replace(' ', '');
		    if(cleanHref.indexOf('t=_blank') > -1 || cleanHref.indexOf('target=_blank') > -1)
		    {
		    	evt.preventDefault();
		        window.open(href);
		        return;
		    }

			var browserActions = ['mailto:', 'tel:', 'sms:', 'intent:'];
			var len = browserActions.length;
			for(var i = 0; i < len; i ++)
			{
				if( href.indexOf(browserActions[i]) === 0 )
				{
					return;
				}
			}

            if( evt.ctrlKey )
            {
                window.open(href);
                return;
            }

			if( this.target == '_blank' || $(this).hasClass('zoom') || $(this).hasClass('play') ){
				if(!this.href)
				{
					if(this.target === '_blank')
					{
						window.open(href);
					}
					else
					{
						window.location.href = href;
					}
				}
				return;
			};

			var pos = href.lastIndexOf('/');
			if(pos > -1)
			{
				var str = href.substring(pos + 1);
				pos = str.indexOf('#');
				if(pos > -1)
				{
					return;
				}
			};

			evt.preventDefault();

			$('.site-loader').removeClass('hide-animate').fadeIn(500,'easeOutQuad',function(){
				window.location.href = href;
			})

		};

	

		$('a:not(.comment-reply-link):not(.not-fade-link)').on('click', ptHandleLink);





		if (!!window.ActiveXObject || "ActiveXObject" in window || navigator.userAgent.indexOf("Edge") > -1 ){
			var ptRootUrl = window.__pt_theme_root_url;
			$('.s-btn.s-btn-prev').css('cursor','url("'+ptRootUrl+'/data/images/left.ico"),pointer');
			$('.s-btn.s-btn-next').css('cursor','url("'+ptRootUrl+'/data/images/right.ico"),pointer');
			$('.single-inner.lightbox .single-header .item').css('cursor','url("'+ptRootUrl+'/data/images/zoom.ico"),pointer');
			$('.click-layer,.close-single,.close-cate-popup').css('cursor','url("'+ptRootUrl+'/data/images/close.ico"),pointer');
		};



		
		ptCateNav();
		$('.m-cate-ctrl').on('click',function(){
			$('.category-nav').toggleClass('show-all')
		});



		// ajax  and  single =============================================================== 


		
		


		if ( $('body.single').length > 0) {
			ptSingleSize();
			ptSingleOnce();
			if( $('.single-header.raw-proportion').length == 0 ){
				sliderArrow();
			}
			$('.single-inner .sc-banner.style-03 .ptbar-text-wrap').ptBar({
				contentTarget: '.content'
			});
		};




		var listenAjaxLoadSingle = function(url) {

			function onSuccess(data){

				if(!window.__ptSingleAJAXCache[url])
				{
					window.__ptSingleAJAXCache[url] = data;
				}

				var $ajaxHtml = $(data);

				$('.ajax-content .ajax-target').append($ajaxHtml);

				if ( $('.ajax-content').hasClass('ajax-fullscreen') ) {


					$('.full-top-bar .bar-title').empty();
					var singleTitle = $('.single-main-intro .title').text();
					$('.full-top-bar .bar-title').append(singleTitle);


					$('.single-main-intro h2.title').remove();

					if ( $('.project .single-meta .item').length == 0 ) {
						$('.single-main-intro').remove()
					}


				}



				if ( $('.single-meta .item').length == 0 ) {
					$('.single-main-intro').addClass('no-bottom-gap')
				}

				$('.single-header .img').append('<abbr class="pic-loader"><i></i><i></i><i></i></abbr>');
				$('.ajax-target:not(.show)').addClass('show');

				NProgress.done();
				ptSingleSize();
				ptSingleOnce();
				$('.single-inner .sc-banner.style-03 .ptbar-text-wrap').ptBar({
					contentTarget: '.content'
				});

				$('.single-inner.lightbox .bg-full').PtListBgTrans();
				$('.single-inner:not(.lightbox) .single-header ~ div .bg-full').PtListBgTrans();
				$('.single-header').animate({opacity:1},600);
				$('.single-main-intro').delay(500).animate({opacity:1},600);
				$('.ajax-content:not(.ajax-fullscreen) .single-main-intro ~ div').delay(1000).animate({opacity:1},600);
				$('.ajax-content.ajax-fullscreen .single-header ~ div ').delay(500).animate({opacity:1},600);
				$('.full-top-bar').addClass('show');


				$('.single-inner:not(.lightbox) .single-header').each(function(){
					imageQueueLoad(this);
				});
				if( $('.single-header.raw-proportion').length == 0 ){
					sliderArrow();
				}


				// for single-related
				$('.ajax-content .single-related a.full').addClass('ajax-footer-a for-related');
				$('.ajax-content .single-nav a.full').addClass('ajax-footer-a for-nav');


				if ( $('.ajax-target .sc-gallery').length > 0 ) {
					sc_responsive();
					pt_shortcode();

					$('.ajax-target .sc-gallery').find('a:not(.comment-reply-link):not(.not-fade-link)').on('click', ptHandleLink);
				}


				$('a.ajax-footer-a').on('click', function(evt){

					evt.preventDefault();
					evt.stopPropagation();

					var $this;

					if ( $(this).hasClass('for-related') ) {
						$this = $(this).closest('.item');
					}else if( $(this).hasClass('for-nav') ){
						$this = $(this).closest('.ctrl');
					}


					$('.ajax-content').removeClass('white black').addClass($this.data('text-color'));
					if ( $this.data('bg-color')) {
						$('.ajax-content').find('.ajax-target').css('background',$this.data('bg-color'));
					}else{
						var $ajaxTarget = $('.ajax-target');
						var minH = $ajaxTarget.css('min-height'),
							marginTop = $ajaxTarget.css('margin-top'),
							Top = $ajaxTarget.css('top');

						$ajaxTarget.removeAttr('style').css({
							'min-height':minH,
							'margin-top':marginTop,
							'top':Top
						});
					};

					$('.ajax-content > .wrap').animate({scrollTop: '0px'},600);

					var ajaxHref = $(this).data('href') || this.href;
					$('.single-inner').fadeOut(600,function(){

						NProgress.start();
						NProgress.configure({ easing: 'ease', speed: 500 });
						

						$('.single-inner.video .single-header .bg-full').ptMediaPlayerCall('dispose',true);
						$('.single-header,.single-main-intro,.single-main-intro ~ div').css('opacity','0');
						$('.single-inner').remove();

						listenAjaxLoadSingle(ajaxHref);

					});
					
				});

				$('body').trigger('ajax-loaded', [$ajaxHtml, url]);

			}

			if(!window.__ptSingleAJAXCache)
			{
				window.__ptSingleAJAXCache = {};
			}

			if(window.__ptSingleAJAXCache[url])
			{
				onSuccess(window.__ptSingleAJAXCache[url]);
			}
			else
			{
				$.ajax({
					url: url,
					type: 'post',
					dataType: 'text',
					data: {
						single_ajax: true
					},
					success: onSuccess,
					error: function(evt){
						console.log('AJAX load single error: ' + evt);
					}
				});
			}
		};

			
		$('.pic-list.ajax .item:not(.has-custom-link) a.full,' + 
			'.pic-list.ajax .item:not(.has-custom-link) .h a,' +
			'.blog-list.ajax a.full,' + 
			'.blog-list.ajax .h a,' + 
			'.blog-list.ajax a.btn').on('click',function(evt){

            if(evt && evt.ctrlKey)
            {
                return;
            }

			evt.preventDefault();
			evt.stopPropagation();

			var $this = $(this);

			var $item = $this.closest('.item');
			var $ajaxContent = $('.ajax-content');

			$('.ajax-target').removeClass('show tempMotion').find('.single-inner').remove();

			$ajaxContent.addClass($item.data('text-color'));

			if ( $item.data('bg-color')) {
				$ajaxContent.find('.ajax-target').css('background',$item.data('bg-color'));
			};
			
			var ajaxHref = $this.data('href') || this.href;
			$ajaxContent.fadeIn(20,function(){
				$('body').addClass('over-hidden');
				NProgress.start();
				NProgress.configure({ easing: 'ease', speed: 500 });
				$('.close-single').fadeIn(800);
				$('.ajax-content > .wrap').scrollTop(0);

				setTimeout(function(){
					$ajaxContent.find('.ajax-target:not(.show)').addClass('show');
				},800)


				listenAjaxLoadSingle(ajaxHref);
			});

		});

		$('.ptsc .ajax-link, .force-ajax-link').on('click',function(evt){

            if(evt && evt.ctrlKey)
            {
                return;
            }

			evt.preventDefault();
			evt.stopPropagation();

			var $this = $(this);

			var $ajaxContent = $('.ajax-content');

			$('.ajax-target').removeClass('show tempMotion').find('.single-inner').remove();

			$ajaxContent.addClass($this.data('text-color') || 'text-black');
			$ajaxContent.find('.ajax-target').css('background', $this.data('bg-color') || '#ffffff');
			
			var ajaxHref = $this.data('href') || this.href;
			$ajaxContent.fadeIn(20,function(){
				$('body').addClass('over-hidden');
				NProgress.start();
				NProgress.configure({ easing: 'ease', speed: 500 });
				$('.close-single').fadeIn(800);
				$('.ajax-content > .wrap').scrollTop(0);

				setTimeout(function(){
					$ajaxContent.find('.ajax-target:not(.show)').addClass('show');
				},800)


				listenAjaxLoadSingle(ajaxHref);
			});

		});


		$('.close-single,.m-close-single,.close-bar-single').on('click',function(){

			NProgress.done();
			$('.full-top-bar').removeClass('show');

			$('.ajax-content').fadeOut(400,function(){

				var $ajaxTarget = $('.ajax-target');

				$('.single-inner.video .single-header .bg-full').ptMediaPlayerCall('dispose',true);
				$('body').removeClass('over-hidden');
				$('.single-header,.single-main-intro,.single-main-intro ~ div').css('opacity','0');
				$('.close-single').fadeOut(0);
				$ajaxTarget.removeClass('show tempMotion').find('.single-inner').remove();

				if ( $titleGroup.hasClass('pause') ) {
					$titleGroup.removeClass('pause').find('.vTarget').ptMediaPlayerCall('play');
				};

				if( BGMneedOFF == false && !$musicPlay.hasClass('clickMute') ){
					$musicPlay.ptAudioPlayerCall('resume');
					$musicPlay.removeClass('off');
				};

				$('.ajax-content .sc-gallery.lightbox').ptImageViewerCall('dispose');

				var minH = $ajaxTarget.css('min-height'),
					marginTop = $ajaxTarget.css('margin-top'),
					Top = $ajaxTarget.css('top');

				$ajaxTarget.removeAttr('style').css({
					'min-height':minH,
					'margin-top':marginTop,
					'top':Top
				});

			});

			$('.ajax-content').removeClass('white black');



			setTimeout(function(){
				$('.ajax-target').addClass('tempMotion')
			},50);



		});





		if ( document.addEventListener  ) {
			document.addEventListener('visibilitychange', function() { 

				var isHidden = document.hidden;
				var $videoTarget = $('.title-group.has-video:not(.pause):not(.video-mute),.sc-slider .item.has-video.current:not(.pause):not(.video-mute),.single-inner.video');

				if (isHidden) {

					if( !$musicPlay.hasClass('clickMute') ){
						$musicPlay.ptAudioPlayerCall('pause');
					}

				}else{
					
					if( !$musicPlay.hasClass('clickMute') && $videoTarget.length == 0 ){
						$musicPlay.ptAudioPlayerCall('resume');
					}
				} 
			})

		};


		$('.main-menu-outside .sub-menu').find('a[title="hr"]').after('<hr style="width:40px;margin:3px 0;border:0.5px solid rgba(166,166,166,.2)">')
		$('.sub-menu').find('a[title="hr"]').removeAttr('title');







		// loader-done
		if ( $('body.real-mobile').length == 0 ) {
			checkElemsColor();
		}

		if ( $('.site-loader .loader-img').length > 0 ) {
			$('.site-loader .loader-img').fadeOut(200,function(){
				$('.site-loader').fadeOut(600,'easeOutQuad');
			})
		}else{
			$('.site-loader').fadeOut(600,'easeOutQuad');
		};

		$('.site-loader').addClass('hide-animate');


		




	});//main-function-end



	$(window).resize(function(){


		bodyMode();
		responsive();
		sc_responsive();
		if ( $('body.single,.ajax-target').length > 0) {
			ptSingleSize();
		};

		if ( $('.pt-header.show').length == 0 ) {
			$('.menu-bg').css('display','none');
			setTimeout(function(){
				$('.menu-bg').removeAttr('style')
			},500)
		};


		setTimeout(function(){
			$('.grid-list.caption-02 .wrap').isotope();
			$('.blog-main > .wrap').isotope();
			$('.sc-gallery > .wrap').isotope();
		},400);


	})//resize end


	$(window).load(function(){

		setTimeout(function(){
			$('.grid-list.caption-02 .wrap').isotope();
			$('.blog-main > .wrap').isotope();
			$('.sc-gallery > .wrap').isotope();
		},400)


	})




}(jQuery));




