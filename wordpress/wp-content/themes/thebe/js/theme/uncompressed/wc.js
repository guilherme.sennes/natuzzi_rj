!(function($)
{
	"use strict";

	var isRTL = ($('body').css('direction') || '').toLowerCase() === 'rtl';
	
	$(function()
	{
		var $mobile_miniCart_parent = $('.m-header');

		$('.m-call-side').before('<i class="m-call-cart"><span class="cart-count"></span></i>');

		function miniCartCount(){

			var miniCartNum =  $('.widget_shopping_cart_content .woocommerce-mini-cart').first().find('li').length;
			$('.cart-count').text(miniCartNum);

			if ( miniCartNum == 0 ) {
				$('.cart-count').addClass('hide')
			}else{
				$('.cart-count').removeClass('hide')
			}

		};

		miniCartCount();



		$('.m-call-cart').on('click',function(){
			$mobile_miniCart_parent.find('.widget_shopping_cart_content').toggleClass('show');
		});



		function miniCartMove(){

			if($mobile_miniCart_parent.length == 0 ){
				return
			};
			if ( $('body.m-mode').length > 0 ) {
				$('.pt-mini-cart.edited').removeClass('edited');
				$('.m-header-right:not(.edited)').append( $('.pt-mini-cart .widget_shopping_cart_content') ).addClass('edited')
			}else{
				$('.m-header-right.edited').removeClass('edited');
				$('.pt-mini-cart:not(.edited)').append( $mobile_miniCart_parent.find('.widget_shopping_cart_content') ).addClass('edited')
			};
		};
		miniCartMove();
		$(window).resize(miniCartMove);



		function onWCFragmentsLoaded(){

			$('.flex-control-thumbs:not(.edited)').addClass('edited').wrap('<div class="wc-flex-thumbs-wrap"></div>');
			$('.woocommerce-message,main#main > .woocommerce-error,.woocommerce-store-notice').on('click',function(){$(this).fadeOut(200)});

			miniCartCount();
			miniCartMove();

		};
		onWCFragmentsLoaded();
		$( document.body ).on( 'wc_fragments_loaded wc_fragments_refreshed', onWCFragmentsLoaded );



		$('.flex-control-nav > li').on('touchend mouseup', function(evt){
			var $this = $(this);
			var parentW = this.parentNode.scrollWidth;
			var viewW = $this.parent().width();
			var halfW = viewW >> 1;
			var itemW = $this.width();
			var offsetX;
			if(this.offsetLeft !== undefined)
			{
				offsetX = this.offsetLeft;
			}
			else
			{
				offsetX = $this.offset().left - $this.parent().offset().left;
			}

			var x;

			if(isRTL)
			{
				offsetX = viewW - itemW - offsetX;

				x = halfW - (offsetX + itemW / 2);
				if(x > 0) x = 0;
				if(x < viewW - parentW) x = viewW - parentW;
				x = Math.round(x);
				x = -x;
			}
			else
			{
				x = halfW - (offsetX + itemW / 2);
				if(x > 0) x = 0;
				if(x < viewW - parentW) x = viewW - parentW;
				x = Math.round(x);
			}
			
			if(createjs && createjs.Tween)
			{

				if(this.parentNode.__tweenX === undefined)
					this.parentNode.__tweenX = 0;

				createjs.Tween.get(
					this.parentNode,
					{
						override:true,
						onChange: function(evt){
							var tween = evt.target;
							var target = tween.target;
							
							$(target).css({
								'transform': 'translate3d(' + target.__tweenX + 'px, 0, 0)'
							});
						}
					}
				).to(
					{__tweenX:x},
					600,
					createjs.Ease.quartOut
				);
			}
			else
			{
				$this.parent().css({
					'transform': 'translate3d(' + x + 'px, 0, 0)'
				})
			}
		});


		$(window).resize(function(){
			setTimeout(function(){
				$('#primary .flex-viewport').height( $('.flex-active-slide a img').height() )
			},400);
		})

	});
})(jQuery);