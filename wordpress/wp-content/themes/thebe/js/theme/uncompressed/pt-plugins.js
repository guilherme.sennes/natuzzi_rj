/**
 * PT Image Viewer
 * Created by foreverpinetree@gmail.com on 2017/3/17.
 */
!(function($, win)
{
    "use strict";

    var ImageViewer = function(element, data)
    {
        this._element = element;
        element.__ivInstance = this;
        this.$e = $(element);

        this._disposed = false;

        this._itemSelector = data['itemSelector'] || '.item';
        this._loop = data['loop'] == undefined ? true : data['loop'];
        this._showThumb = data['showThumb'] || false;
        this._animateTime = data['animateTime'] == undefined ? 500 : data['animateTime'];
        this._animateInEase = data['animateInEase'] || 'sineOut';
        this._animateOutEase = data['animateOutEase'] || 'sineOut';
        this._cd = data['cd'] == undefined ? 300 : data['cd'];
        this._fadeTime = data['fadeTime'] == undefined ? 500 : data['fadeTime'];
        this._showTitle = data['showTitle'] == undefined ? true : data['showTitle'];
        this._skinStyle = data['skinStyle'] == undefined ? 'dark' : data['skinStyle'];
        this._dragDistance = data['dragDistance'] == undefined ? 30 : data['dragDistance'];
        this._closeHandler = data['closeHandler'] || null;
        this._openClass = data['openClass'] || '';

        if(this._openClass && this._openClass.indexOf('.') === 0)
        {
            this._openClass = this._openClass.substring(1);
        }

        if(win['PT_IV_LOOP'] != undefined)
        {
            this._loop = win['PT_IV_LOOP'];
        }

        if(win['PT_IV_SHOW_THUMB'] != undefined)
        {
            this._showThumb = win['PT_IV_SHOW_THUMB'];
        }

        createjs.Ticker.timingMode = createjs.Ticker.RAF;

        this._isIE9 = navigator.userAgent.indexOf("MSIE 9.0") > -1;

        this._stageWidth = 0;

        this._isDevice = /Mobi|Tablet|iPad|iPhone|Android/i.test(navigator.userAgent);
        this._clickEvt = this._isDevice ? 'touchend mouseup' : 'click';

        this._downEvt = 'touchstart mousedown';
        this._moveEvt = 'touchmove mousemove';
        this._upEvt = 'touchend mouseup';

        this._allowCalls = ['open', 'dispose'];

        this._isRTL = ($('body').css('direction') || '').toLowerCase() == 'rtl';

        this._inCD = false;

        this._oldMouseX = 0;
        this._startDragX = 0;
        this._currentX = 0;

        this._dragTarget = null;
        this._canDrag = true;

        this._stageItems = [];

        this._currentImage = null;

        this._isJsonDataMode = false;

        this.$triggerItems = null;
        this.$items = null;
        this._items = [];
        this._total = 0;

        this._currentIndex = 0;

        this._emptyData = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';

        var ivRootClass = this._showThumb ? 'pt-iv-has-thumb pt-iv-show-thumbs ' : '';
        ivRootClass += this._skinStyle;

        if(this._isDevice)
        {
            ivRootClass += ' is-device';
        }

        this.$html = $('<div class="pt-iv-root ' + ivRootClass + '">' +
            '<div class="pt-iv-container"></div>' +
            '<div class="pt-iv-prev pt-iv-btn"><i></i></div>' +
            '<div class="pt-iv-next pt-iv-btn"><i></i></div>' +
            '<div class="pt-iv-close pt-iv-btn"><i></i></div>' +
            '<div class="pt-iv-title"><i></i><p></p></div>' +
            '<div class="pt-iv-number"><i>1</i><i>/</i><i>' + this._total + '</i></div>' +
            '<ul class="pt-iv-thumb-list"></ul>' +
            (this._showThumb ? '<div class="pt-iv-thumbs-toggle"><i></i></div>' : '') +
            '</div>');
        this.$html.fadeOut(0);

        this._useZoom = this._isDevice && !data['ignoreMobileZoom'];

        if(this._useZoom)
        {
            this._dblCD = 0;
            this._isZoomIn = false;
            this.$zoomContainer = $('<div class="pt-iv-zoom-container"></div>');
            this.$zoomContainer.on('click', function(evt, fromCloseBtn){
                if(this._isZoomIn)
                {
                    this._isZoomIn = false;

                    this.$zoomContainer.fadeOut(300, function(){
                        this.$zoomContainer.empty();
                        this.$e.removeClass('pt-zoom-modal');
                    }.bind(this));
                }
            }.bind(this));

            this.$zoomBtn = $('<div class="pt-iv-zoom-btn pt-iv-btn"><i></i></div>');
            this.$html.append(this.$zoomBtn);
            this.$zoomBtn.on(this._downEvt, function (evt){
                evt.preventDefault();
                evt.stopPropagation();

                if(this._isZoomIn)
                {
                    this.$zoomContainer.trigger('click', [true]);
                }
                else if(this._currentImage)
                {
                    var $imgItem = $(this._currentImage);
                    $imgItem.trigger('click', [$imgItem.find('img').get(0)]);
                }
            }.bind(this));
        }

        this.$thumbSource = $('<li class="pt-iv-thumb-item"><img src="' + this._emptyData + '" alt=""></li>');
        this.$thumbList = this.$html.find('.pt-iv-thumb-list');

        this.$numContainer = this.$html.find('.pt-iv-number');
        this.$currentNum = this.$numContainer.children('i:nth-child(1)');
        this.$totalNum = this.$numContainer.children('i:nth-child(3)');
        this.$numContainer.hide();
        
        this.$title = this.$html.find('.pt-iv-title>p');

        this.$imgContent = this.$html.find('.pt-iv-container');

        this._imgItems = [];
        this._thumbItems = [];

        var clickEvent = this._clickEvt;

        if(!this._isDevice)
        {
            this.$html.on('click', function (evt) {
                var $target = $(evt.target);
                if($target.hasClass('pt-iv-item') || $target.hasClass('pt-iv-loading'))
                {
                    evt.preventDefault();
                    evt.stopPropagation();
                    this._onClose();
                }
            }.bind(this));
        }

        if(this._isRTL)
        {
            this.$prevBtn = this.$html.find('.pt-iv-next');
            this.$nextBtn = this.$html.find('.pt-iv-prev');
        }
        else
        {
            this.$prevBtn = this.$html.find('.pt-iv-prev');
            this.$nextBtn = this.$html.find('.pt-iv-next');
        }

        this.$prevBtn.on(clickEvent, function (evt) {
            this._onPrev();
        }.bind(this));
        this.$nextBtn.on(clickEvent, function (evt) {
            this._onNext();
        }.bind(this));

        this.$html.find('.pt-iv-close').on(clickEvent, function (evt) {
            evt.preventDefault();
            evt.stopPropagation();
            this._onClose();
        }.bind(this));

        this.$html.on('wheel', function(event) {
            var evt = event.originalEvent;
            evt.deltaY < 0 ? this._onPrev() : evt.deltaY > 0 ? this._onNext() : '';
        }.bind(this));

        if(this._showThumb)
        {
        	this.$html.find('.pt-iv-thumbs-toggle').on(clickEvent, function (evt) {
	            evt.preventDefault();
	            evt.stopPropagation();
	            this.$html.toggleClass('pt-iv-show-thumbs');
	        }.bind(this));
        }

        this._onOpenHandler = this._onOpen.bind(this);
        this._onKeyUpHandler = this._onKeyUp.bind(this);
        this._onResizeHandler = this._onResize.bind(this);
        this._onDraggingHandler = this._onDragging.bind(this);
        this._onDragEndHandler = this._onDragEnd.bind(this);

        this.updateData();
    }

    var p = ImageViewer.prototype;

    p.updateData = function ()
    {
        var $selectEles = this.$e.find(this._itemSelector);
        if($selectEles.length === 1 && $selectEles.data('src-type') === 'json')
        {
            this._isJsonDataMode = true;

            var self = this;
            (function(){
                var data = $selectEles.data('src');
                if(data && data.src && data.src.length > 0)
                {
                    var len = data.src.length, $item, src, title, thumb;
                    var $container = $('<div></div>');
                    for(var i = 0; i < len; i ++)
                    {
                        src = data.src[i] || '';
                        if(!src) continue;

                        title = data.title ? (data.title[i] || '') : '';
                        thumb = data.thumb ? (data.thumb[i] || '') : '';
                        $item = $('<div data-src="' + src + '" data-title="' + title + '" data-thumb="' + thumb + '"></div>');

                        $container.append($item);
                    }

                    self.$items = $container.children();
                }
            })();
        }
        else
        {
            this._isJsonDataMode = false;
            this.$items = $selectEles;
        }

        this.$triggerItems = $selectEles;
        
        this._items.length = 0;

        if(this.$items)
        {
            this.$items.each(function (index, item) {
                this._items.push(item);
            }.bind(this));
        }

        this._total = this._items.length;

        if(this._total > 1)
        {
            this.$numContainer.show();
        }
        else
        {
            this.$numContainer.hide();
        }

        if(this.$triggerItems)
        {
            this.$triggerItems.off('click', this._onOpenHandler).on('click', this._onOpenHandler);
        }

        if(this._total < 2)
        {
            this.$prevBtn.addClass('pt-iv-btn-disabled');
            this.$nextBtn.addClass('pt-iv-btn-disabled');
        }

        this.$totalNum.html(this._total);

        if(this._showThumb)
        {
            this.$thumbList.empty();
            this._thumbItems.length = 0;

            var src, item, $thumb, event = this._downEvt;
            for(var i = 0; i < this._total; i ++)
            {
                item = this._items[i];
                src = $(item).data('thumb');
                $thumb = this.$thumbSource.clone();
                $thumb.children('img').attr('src', src);
                $thumb.on(event, this._onClickThumb.bind(this));
                this.$thumbList.append($thumb);

                this._thumbItems.push($thumb);
            }
        }
    }

    p.open = function (index)
    {
        index = index || 0;

        if(index < 0)
        {
            index = 0;
        }
        else if(index > this._total - 1)
        {
            index = this._total - 1;
        }

        this._currentIndex = index;
        this._onOpen(null);
    }

    p._onClickThumb = function (evt)
    {
        evt.preventDefault();
        evt.stopPropagation();

        var $thumb = $(evt.currentTarget);

        var index = $thumb.index();
        if(index === this._currentIndex)
        {
            return;
        }

        var direct;
        if(index < this._currentIndex)
        {
            direct = -1;
        }
        else
        {
            direct = 1;
        }

        var item;
        if(this._isJsonDataMode)
        {
            item = this._items[index];
            if(!item)
            {
                index = 0;
                item = this._items[index];
            }
        }
        else
        {
            item = this._items[index];
        }

        if(item)
        {
            this._currentIndex = index;

            this.$currentNum.html(index + 1);
            this._showImage($(item).attr('href') || $(item).data('src'), $(item).data('title'), direct, true);
        }
    }

    p._onResize = function (evt)
    {
        this._stageWidth = this.$html.width();

        var $thumb = this._thumbItems[this._currentIndex];
        this._updateThumbListPos($thumb);

        if(this._isZoomIn)
        {
            var $img = this.$zoomContainer.find('img');
            var imgW = $img.get(0).naturalWidth;
            var imgH = $img.get(0).naturalHeight;
            var sw = this.$zoomContainer.width();
            var sh = this.$zoomContainer.height();
            var scale = Math.max(sw / imgW, sh / imgH);
            $img.css('width', imgW * scale);
            $img.css('height', imgH * scale);
        }
    }

    p._onKeyUp = function (evt)
    {
        switch (evt.keyCode)
        {
            case 37: //left
            case 38: //up
                this._onPrev();
                break;
            case 39: //right
            case 40: //down
                this._onNext();
                break;
            case 27:
                this._onClose();
                break;
        }
    }

    p._onPrev = function ()
    {
        if(this._checkInCD()) return;

        this.$html.removeClass('pt-iv-first-open');

        var index = this._currentIndex;
        index --;
        if(index < 0)
        {
            index = this._loop ? this._total - 1 : 0;
        }

        if(this._currentIndex != index)
        {
            this._currentIndex = index;

            this.$currentNum.html(index + 1);

            var item = this._items[index];
            if(item)
            {
                this._showImage($(item).attr('href') || $(item).data('src'), $(item).data('title'), 1, true);
            }
        }
    }

    p._onNext = function ()
    {
        if(this._checkInCD()) return;

        this.$html.removeClass('pt-iv-first-open');

        var index = this._currentIndex;
        index ++;
        if(index > this._total - 1)
        {
            index = this._loop ? 0 : this._total - 1;
        }

        if(this._currentIndex != index)
        {
            this._currentIndex = index;

            this.$currentNum.html(index + 1);

            var item = this._items[index];
            if(item)
            {
                this._showImage($(item).attr('href') || $(item).data('src'), $(item).data('title'), -1, true);
            }
        }
    }

    /**
     *
     * @param src
     * @param title
     * @param direct, -1: right to left, 1: left to right
     * @param tween
     * @private
     */
    p._showImage = function (src, title, direct, tween)
    {
        this._canDrag = false;

        this._stageItems.forEach(function (item) {
            var x = direct == -1 ? - this._stageWidth : this._stageWidth;
            if(this._isRTL) x = -x;

            createjs.Tween.get(
                item,
                {
                    override:true,
                    onChange:this._onUpdate.bind(this)
                }
            ).to(
                {__tweenX:x},
                tween ? this._animateTime : 0,
                createjs.Ease[this._animateOutEase] || createjs.Ease.sineOut
            ).call(this._onTweenOutEnd.bind(this, item));

        }, this);
        this._stageItems.length = 0;

        var item = this._getImageItem(src);
        this.$imgContent.append(item);

        this._currentImage = item;

        var x = direct == -1 ? this._stageWidth : - this._stageWidth;
        if(this._isRTL) x = -x;

        this._renderX($(item), x);
        item.__tweenX = x;

        createjs.Tween.get(
            item,
            {
                override:true,
                onChange:this._onUpdate.bind(this)
            }
        ).to(
            {__tweenX:0},
            tween ? this._animateTime : 0,
            createjs.Ease[this._animateInEase] || createjs.Ease.sineOut
        ).call(this._onTweenInEnd.bind(this));

        if(this._showTitle)
        {
            this.$title.fadeOut(tween ? (this._animateTime >> 1) : 0, function (title) {
                this.$title.html(title).fadeIn(this._animateTime >> 1);
            }.bind(this, title));
        }

        this._stageItems.push(item);

        this.$thumbList.children('.pt-iv-thumb-item').removeClass('selected');
        var $thumb = this._thumbItems[this._currentIndex];
        $thumb && $thumb.addClass('selected');

        this._updateThumbListPos($thumb);
        this._updateArrow();
    }

    p._updateThumbListPos = function ($thumb)
    {
        var listW = this.$thumbList.width();
        if(listW < this._stageWidth)
        {
            this.$thumbList.css('left', ((this._stageWidth - listW) >> 1) + 'px');
            return;
        }

        var ox = 0;
        if($thumb)
        {
            ox = this._stageWidth / 2 - $thumb.offset().left - $thumb.width() / 2;
        }

        var x = this.$thumbList.position().left + ox;
        if(x > 0)
        {
            x = 0;
        }
        else if(x < this._stageWidth - listW)
        {
            x = this._stageWidth - listW;
        }

        this.$thumbList.css('left', x + 'px');
    }

    p._dragBack = function ()
    {
        createjs.Tween.get(
            this._dragTarget,
            {
                override:true,
                onChange:this._onUpdate.bind(this)
            }
        ).to(
            {__tweenX:0},
            this._animateTime,
            createjs.Ease[this._animateOutEase] || createjs.Ease.sineOut
        ).call(this._onDragBack.bind(this));
    }

    p._onDragBack = function ()
    {
        this._canDrag = true;
    }

    p._onTweenInEnd = function ()
    {
        this._canDrag = true;
    }

    p._onTweenOutEnd = function (item)
    {
        this._imgItems.push(item);
        $(item).find('img').attr('src', this._emptyData);
        $(item).remove();
    }

    p._renderX = function ($target, x)
    {
        if(this._isIE9)
        {
            $target.css('transform', 'translateX(' + x + 'px)');
        }
        else
        {
            $target.css('transform', 'translate3d(' + x + 'px,0,0)');
        }
    }
    
    p._onUpdate = function (evt)
    {
        var tween = evt.target;
        var target = tween.target;
        
        this._renderX($(target), target.__tweenX);
    }

    p._onOpen = function (evt)
    {
        if(evt)
        {
            evt.preventDefault();
            evt.stopPropagation();
        }

        if(this._checkInCD()) return;

        var item;
        if(this._isJsonDataMode)
        {
            item = this._items[this._currentIndex];
            if(!item)
            {
                this._currentIndex = 0;
                item = this._items[this._currentIndex];
            }
        }
        else if(evt)
        {
            item = evt.currentTarget;
            this._currentIndex = this._items.indexOf(item);
        }
        else
        {
            item = this._items[this._currentIndex];
        }

        if(!item)
        {
            return;
        }

        this.$html.addClass('pt-iv-first-open');

        if(this._openClass)
        {
            $('body').addClass(this._openClass);
        }

        $('body').addClass('pt-iv-modal').append(this.$html);
        this.$html.addClass('pt-iv-fading pt-iv-fading-in').fadeIn(this._fadeTime, function () {
            this.$html.removeClass('pt-iv-fading pt-iv-fading-in');
        }.bind(this));

        this.$currentNum.html(this._currentIndex + 1);

        setTimeout(function () {
            this._showImage($(item).attr('href') || $(item).data('src'), $(item).data('title'));
        }.bind(this), 150);

        $(win).on('keyup', this._onKeyUpHandler);
        $(win).on('resize', this._onResizeHandler);

        this._onResize();
    }

    p._onClose = function (evt)
    {
        this.$html.addClass('pt-iv-fading pt-iv-fading-out').fadeOut(this._fadeTime, function () {
            this.$html.removeClass('pt-iv-fading pt-iv-fading-out').detach();
            this._stageItems.forEach(function (item) {
                $(item).remove();
            }, this);
            this._stageItems.length = 0;

            if(this._openClass)
            {
                $('body').removeClass(this._openClass);
            }

            if(this._closeHandler)
            {
                this._closeHandler && this._closeHandler.call(this, this);
            }
        }.bind(this));

        $('body').removeClass('pt-iv-modal');

        this.$html.removeClass('pt-iv-first-open');

        $(win).off('keyup', this._onKeyUpHandler);
        $(win).off('resize', this._onResizeHandler);
    }

    p._getImageItem = function (src)
    {
        var item, img;
        if(this._imgItems.length > 0)
        {
            item = this._imgItems.pop();
            img = $(item).find('img').get(0);
        }
        else
        {
            item = document.createElement('div');
            item.className = 'pt-iv-item';
            item.innerHTML = '<div class="pt-iv-loading"><abbr class="pic-loader"><i></i><i></i><i></i></abbr></div>';
        }

        if(!img)
        {
            img = new Image();
            img.onload = function (evt) {
                $(this).parent().addClass('pt-iv-img-loaded');
                this.style.opacity = 1;
            };
            img.alt = '';
            item.appendChild(img);
        }

        item.__tweenX = 0;

        var $item = $(item);
        $item.removeClass('pt-iv-img-loaded');
        img.style.opacity = 0;
        img.src = src || this._emptyData;

        if(this._total > 1)
        {
            $item.on(this._downEvt, this._onStartDrag.bind(this));
        }

        if(this._useZoom)
        {
            $item.on('click', this._onClickImage.bind(this));
        }

        return item;
    }

    p._onClickImage = function (evt, imgELemnt)
    {
        var img = imgELemnt || evt.originalEvent.target;
        if(img instanceof Image)
        {
            var $zoom = this.$zoomContainer;

            $zoom.empty().append('<img class="zoom-img" alt="" src="' + img.src + '">');
            $('body').append($zoom);
            $zoom.hide().fadeIn(300);

            this.$e.addClass('pt-zoom-modal');

            this._isZoomIn = true;
            this._onResize();
        }
    }

    p._onStartDrag = function (event)
    {
        var evt = event.originalEvent;
        if(this._isDevice && evt.touches && evt.touches.length > 1)
        {
            return;
        }

        if($(evt.target).hasClass('pt-iv-item'))
        {
            return;
        }

        if(!this._isDevice)
        {
            evt.preventDefault();
        }

        if(!this._canDrag) return;
        this._canDrag = false;

        var target = evt.currentTarget;
        this._dragTarget = target;

        $(win).off(this._moveEvt, this._onDraggingHandler).on(this._moveEvt, this._onDraggingHandler);
        $(win).off(this._upEvt, this._onDragEndHandler).on(this._upEvt, this._onDragEndHandler);

        this.$imgContent.addClass('pt-iv-press');

        this._oldMouseX = this._getMouseX(evt);
        this._currentX = 0;
        this._startDragX = this._currentX;
    }

    p._onDragging = function (event)
    {
        var evt = event.originalEvent;
        if(this._isDevice && evt.touches && evt.touches.length > 1)
        {
            return;
        }

        if(!this._isDevice)
        {
            evt.preventDefault();
        }

        var x = this._getMouseX(evt);
        this._currentX += x - this._oldMouseX;
        this._oldMouseX = x;

        this._renderX($(this._dragTarget), this._currentX);
    }

    p._onDragEnd = function (event)
    {
        $(win).off(this._moveEvt, this._onDraggingHandler);
        $(win).off(this._upEvt, this._onDragEndHandler);

        if(this._dragTarget)
        {
            this._dragTarget.__tweenX = this._currentX;
        }

        var distance = this._currentX - this._startDragX;
        if(this._isRTL)
        {
            distance = - distance;
        }

        var isAvailable = false;
        if(distance < - this._dragDistance)
        {
            if(this._isRTL)
            {
                isAvailable = this._currentIndex >= 0 && this._currentIndex < this._total - 1;
            }
            else
            {
                isAvailable = this._currentIndex < this._total - 1;
            }

            this._loop || isAvailable ? this._onNext() : this._dragBack();
        }
        else if(distance > this._dragDistance)
        {
            if(this._isRTL)
            {
                isAvailable = this._currentIndex > 0 && this._currentIndex <= this._total - 1;
            }
            else
            {
                isAvailable = this._currentIndex > 0;
            }

            this._loop || isAvailable ? this._onPrev() : this._dragBack();
        }
        else
        {
            this._dragBack();
        }

        this.$imgContent.removeClass('pt-iv-press');

        this._dragTarget = null;
    }

    p._updateArrow = function ()
    {
        if(this._total < 2 || this._loop) return;

        var $prev = this.$prevBtn, $next = this.$nextBtn, className = 'pt-iv-btn-disabled';

        if(this._currentIndex <= 0)
        {
            $prev.addClass(className);
            $next.removeClass(className);
        }
        else if(this._currentIndex >= this._total - 1)
        {
            $prev.removeClass(className);
            $next.addClass(className);
        }
        else
        {
            $prev.removeClass(className);
            $next.removeClass(className);
        }
    }

    p._getMouseX = function(evt)
    {
        var x;
        if(this._isDevice)
        {
            var touches = evt.changedTouches, touch;
            touch = touches[0];
            if(touch)
            {
                x = touch.pageX;
            }
            else
            {
                x = evt.pageX !== undefined ? evt.pageX : evt.clientX;
            }
        }
        else
        {
            x = evt.pageX !== undefined ? evt.pageX : evt.clientX;
        }
        return x;
    }

    p._checkInCD = function()
    {
        if(this._inCD) return true;

        setTimeout(function () {
            this._inCD = false;
        }.bind(this), this._cd);

        this._inCD = true;
        return false;
    }

    p.execute = function(funcName, funcParams)
    {
        if(!funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return;
        }

        if(funcParams && !Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) == 'function')
        {
            f.apply(this, funcParams || []);
        }
    };

    p.dispose = function()
    {
        if(this._disposed)
        {
            return;
        }

        this._disposed = true;

        this.$triggerItems.off('click', this._onOpenHandler);

        this.$html.remove();

        var $win = $(win);

        $win.off('keyup', this._onKeyUpHandler);
        $win.off('resize', this._onResizeHandler);
        $win.off(this._moveEvt, this._onDraggingHandler);
        $win.off(this._upEvt, this._onDragEndHandler);

        var ele = this._element;
        if(ele)
        {
            ele.__ivInstance = null;
            this._element = null;
        }

        this._closeHandler = null;
    }

    $.fn.extend({
        ptImageViewer:function (obj) {
            var iv;
            this.each(function(){
                iv = this.__ivInstance;
                if(iv)
                {
                    iv.updateData();
                }
                else
                {
                    new ImageViewer(this, obj || {});
                }
            });
            return this;
        },
        ptImageViewerCall:function (funcName, funcParams) {
            this.each(function(){
                var iv = this.__ivInstance;
                iv && iv.execute(funcName, funcParams);
            });
            return this;
        }
    });
}(jQuery, window));


/**
 * Created by pinetree on 2016/11/16.
 */
!(function($, win)
{
    "use strict";

    var ptMediaPlayer = {
        playingPlayers : [],
        onStatusChanged : null
    };

    var onPTVimeoAPIReady = function ()
    {
        ptMediaPlayer.vimeoApiReady = true;

        if(ptMediaPlayer.vimeoWaitingPlayers)
        {
            var i = ptMediaPlayer.vimeoWaitingPlayers.length, player;
            while(i --)
            {
                player = ptMediaPlayer.vimeoWaitingPlayers[i];
                player && player.init();
            };
            ptMediaPlayer.vimeoWaitingPlayers.length = 0;
        }
    }

    // called by YouTube plugin
    win.onYouTubeIframeAPIReady = function ()
    {
        ptMediaPlayer.youtubeApiReady = true;

        if(ptMediaPlayer.youtubeWaitingPlayers)
        {
            var i = ptMediaPlayer.youtubeWaitingPlayers.length, player;
            while(i --)
            {
                player = ptMediaPlayer.youtubeWaitingPlayers[i];
                player && player.init();
            };
            ptMediaPlayer.youtubeWaitingPlayers.length = 0;
        }
    }

    var parseUrlParams = function(url)
    {
        var index = url.indexOf("?");
        if(index == -1) return null;

        var source = url.substring(index + 1);

        index = source.indexOf('#');
        if(index > -1)
        {
            source = source.substring(0, index);
        }

        source = source.replace(/&amp;|&amp%3b/ig, '&');

        var arr = source.split("&"), temp;
        var len = arr.length, obj = {};
        for(var i = 0; i < len; i ++)
        {
            temp = arr[i].split("=");
            if(temp.length > 1)
            {
                obj[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
            }
        }
        return obj;
    }

    var VimeoPlayer = function (container, src, options, callback, playerMgr)
    {
        this.type = 'vimeo';

        this._container = container;
        this._options = options;

        this._callback = callback;
        this._playerMgr = playerMgr;

        this._isInited = false;

        this._needPlay = false;
        this._needPause = false;
        this._needMute = false;

        var xProp = playerMgr._isRTL ? 'right' : 'left';
        var wh = this._options.full ? 'width:' + this._options.width + 'px;height:' + this._options.height + 'px;' : 'width:100%;height:100%;';
        var playerElement = '<div class="pt-vimeo-player" style="' + wh + 'position:absolute;' + xProp + ':0;top:0;"></div>';
        $(this._container).append(playerElement);

        this._src = src;

        Object.defineProperty(this, 'src',{
            get: this.getSrc
        });
    }

    var vp = VimeoPlayer.prototype;
    
    vp.init = function ()
    {
        var options = this._options;
        var volume = options.volume === undefined ? 1 : options.volume;

        if(options.autoplay && volume > 0)
        {
            volume = 0;
            this._needMute = true;
        }

        var videoOptions = {
            url: this._src,
            autopause: false,
            width: options.width,
            height: options.height,
            autoplay: options.autoplay ? true : false,
            background: options.controls ? false : true,
            loop: options.loop ? true : false,
            muted: volume === 0 ? true : false
        };

        this.player = new Vimeo.Player($(this._container).children('.pt-vimeo-player').get(0), videoOptions);
        this.player.setVolume(volume);
        this.player.on('loaded', this.onPlayerReady.bind(this));
        this.player.on('play', this._onPlay.bind(this));
        this.player.on('pause', this._onPause.bind(this));
        this.player.on('ended', this._onPlayEnd.bind(this));

        if(options.full)
        {
            Promise.all([this.player.getVideoWidth(), this.player.getVideoHeight()]).then(function(dimensions) {
                var width = dimensions[0];
                var height = dimensions[1];

                if(options.widthFromDefault)
                {
                    options.width = width;
                    $(this._playerMgr._container).data('videoWidth', options.width);
                }
                
                if(options.heightFromDefault)
                {
                    options.height = height;
                    $(this._playerMgr._container).data('videoHeight', options.height);
                }
                
                this._playerMgr.resize();

                if(options.autoplay)
                {
                    this._playerMgr.handlePlayer('play', this);
                }
            }.bind(this)).catch(function(){
                if(options.autoplay)
                {
                    this._playerMgr.handlePlayer('play', this);
                }
            }.bind(this));
        }
        else
        {
            if(options.autoplay)
            {
                this._playerMgr.handlePlayer('play', this);
            }
        }
        
    }

    vp.onPlayerReady = function()
    {
        $(this._container).animate({opacity:1}, 400);

        if(this._callback)
        {
            this._callback.call(this._playerMgr);
        }

        this._isInited = true;

        this._playerMgr.onStart();

        if(this._needPlay)
        {
            this._needPlay = false;
            this.play();
        }

        if(this._needPause)
        {
            this._needPause = false;
            this.pause()
        }

        if(this._needMute)
        {
            this._needMute = false;
            this.mute()
        }

        var self = this;
        this.player.getPaused().then(function(paused) {
            if(paused)
            {
                self._playerMgr.handlePlayer('pause', self);
            }
        }).catch(function(error) {
            // an error occurred
        });
    }

    vp.play = function ()
    {
        if(!this._isInited)
        {
            this._needPlay = true;
        }
        else
        {
            this.player.play();

            this._playerMgr.handlePlayer('play', this);
        }
    }

    vp.pause = function ()
    {
        if(!this._isInited)
        {
            this._needPause = true;
        }
        else
        {
            this.player.pause();

            this._playerMgr.handlePlayer('pause', this);
        }
    }

    vp.mute = function()
    {
        if(!this._options.volume)
        {
            return;
        }

        if(!this._isInited)
        {
            this._needMute = true;
        }
        else
        {
            this.player.setVolume(0);

            this._playerMgr.handlePlayer('mute', this);
        }
    }

    vp.unMute = function()
    {
        if(!this._options.volume)
        {
            return;
        }

        if(!this._isInited)
        {
            this._needMute = false;
        }
        else
        {
            this.player.setVolume(this._options.volume === undefined ? 1 : this._options.volume);

            this._playerMgr.handlePlayer('unMute', this);
        }
    }

    vp._onPlay = function (evt)
    {
        this._playerMgr.handlePlayer('play', this);
    }

    vp._onPause = function (evt)
    {
        this._playerMgr.handlePlayer('pause', this);
    }

    vp._onPlayEnd = function (evt)
    {
        this._playerMgr.onEnd();

        this._playerMgr.handlePlayer('end', this);
    }

    vp.getSrc = function()
    {
        return this._src;
    }

    vp.dispose = function ()
    {
        this.pause();

        this._callback = null;
        this._playerMgr = null;

        if(this.player)
        {
            this.player.unload();
            this.player = null;
        }
    }

    var YoutubePlayer = function (container, src, options, callback, playerMgr)
    {
        this.type = 'youtube';

        this._container = container;
        this._options = options;
        this.player = null;

        this._callback = callback;
        this._playerMgr = playerMgr;

        this._isInited = false;

        this._needPlay = false;
        this._needPause = false;
        this._needMute = false;

        var wh = this._options.full ? 'width:' + this._options.width + 'px;height:' + this._options.height + 'px;' : 'width:100%;height:100%;';
        var playerElement = '<div class="pt-youtube-player" style="' + wh + 'position:absolute;left:0;top:0;"></div>';
        $(this._container).append(playerElement);

        var obj = parseUrlParams(src);
        if(obj)
        {
            if(obj['hd'])
            {
                this._options['hd'] = true;
            }
        }

        var split_c = "v=";
        var split_n = 1;
        if( src.match(/(youtube.com)/) )
        {
            split_c = "v=";
            split_n = 1;
        }
        if( src.match(/(youtu.be)/) )
        {
            split_c = "/";
            split_n = 3;
        }

        var id = src.split(split_c)[split_n];
        id = id.replace(/(&)+(.*)/, "");

        this._src = id;

        Object.defineProperty(this, 'src',{
            get: this.getSrc
        });
    }
    
    var yp = YoutubePlayer.prototype;

    yp.init = function ()
    {
        this.player = new YT.Player($(this._container).children('.pt-youtube-player').get(0), {
            videoId: this._src,
            width: this._options.width,
            height: this._options.height,
            playerVars: {
                'autohide': 1,
                'autoplay': 0,
                'disablekb': 0,
                'controls': this._options.controls ? 1 : 0,
                'enablejsapi': 1,
                'modestbranding': 1,
                'showinfo': 0,
                'rel': 0,
                'wmode': 'opaque',
                'hd': this._options.hd ? 1 : 0
            },
            events: {
                'onReady': this.onPlayerReady.bind(this),
                'onStateChange': this.onPlayerStateChange.bind(this)
            }
        });

        if(this._options.autoplay)
        {
            this._needPlay = true;

            this._playerMgr.handlePlayer('play', this);
        }
    }

    yp.onPlayerStateChange = function (evt)
    {
        switch (evt.data)
        {
            case -1:
                this._playerMgr.handlePlayer('pause', this);
                break;
            case 0:
                if(this._options.loop)
                {
                    this.player.playVideo();
                }
                else
                {
                    this._playerMgr.onEnd();

                    this._playerMgr.handlePlayer('end', this);
                }
                break;
            case 1:
                this._playerMgr.handlePlayer('play', this);
                break;
            case 2:
                this._playerMgr.handlePlayer('pause', this);
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
        }
    }

    yp.onPlayerReady = function (evt)
    {
        var volume = this._options.volume === undefined ? 1 : this._options.volume;

        if(this._options.autoplay)
        {
            this._needMute = true;
        }

        this.player.setVolume(Math.floor(volume * 100));

        $(this._container).animate({opacity:1}, 400);

        if(this._callback)
        {
            this._callback.call(this._playerMgr);
        }

        this._isInited = true;

        this._playerMgr.onStart();

        if(this._options.volume === 0)
        {
            this.player.mute();
        }

        if(this._needPlay)
        {
            this._needPlay = false;
            this.play();
        }

        if(this._needPause)
        {
            this._needPause = false;
            this.pause()
        }

        if(this._needMute)
        {
            this._needMute = false;
            this.mute();
        }
    }

    yp.play = function ()
    {
        if(!this._isInited)
        {
            this._needPlay = true;
        }
        else
        {
            this.player.playVideo();

            this._playerMgr.handlePlayer('play', this);
        }
    }

    yp.pause = function ()
    {
        if(!this._isInited)
        {
            this._needPause = true;
        }
        else
        {
            this.player.pauseVideo();

            this._playerMgr.handlePlayer('pause', this);
        }
    }

    yp.mute = function()
    {
        if(!this._options.volume)
        {
            return;
        }

        if(!this._isInited)
        {
            this._needMute = true;
        }
        else
        {
            this.player.mute();

            this._playerMgr.handlePlayer('mute', this);
        }
    }

    yp.unMute = function()
    {
        if(!this._options.volume)
        {
            return;
        }

        if(!this._isInited)
        {
            this._needMute = false;
        }
        else
        {
            this.player.unMute();

            this._playerMgr.handlePlayer('unMute', this);
        }
    }

    yp.getSrc = function()
    {
        return this._src;
    }

    yp.dispose = function ()
    {
        this.pause();

        this._callback = null;
        this._playerMgr = null;

        if(this.player)
        {
            this.player.destroy();
            this.player = null;
        }
    }

    var MP4Player = function (container, src, options, callback, playerMgr)
    {
        this.type = 'mp4';

        this._streamableInited = false;
        this._streamable = false;

        if(src.indexOf('.mp4') < 0 && src.indexOf('streamable.com') > -1)
        {
            this._streamable = true;
        }

        this._container = container;
        this._src = src;
        this._options = options;
        this._callback = callback;
        this._playerMgr = playerMgr;
        this._loop = options.loop;

        this.player = null;

        this._realPlay = false;
        this._needPause = false;

        Object.defineProperty(this, 'src',{
            get: this.getSrc
        });
    }
    
    var mp = MP4Player.prototype;

    mp.init = function ()
    {
        if(this._streamable && !this._streamableInited)
        {
            var videoId = this._src.substring(this._src.indexOf('.com/') + 5);
            var self = this;
            $.ajax({
                url: 'https://api.streamable.com/videos/' + videoId,
                dataType: 'json',
                success: function(data) {
                    var mp4File = data.files ? data.files.mp4 : null;
                    if(mp4File && mp4File.url)
                    {
                        var url = mp4File.url;
                        if(url.indexOf('http:') !== 0 && url.indexOf('https:') !== 0)
                        {
                            url = 'https:' + url;
                        }
                        
                        self._streamableInited = true;

                        self._src = url;
                        self.init();
                    }
                }, 
                error: function(){
                    //
                }
            });

            return;
        }

        var options = this._options;
        var $container = $(this._container);

        var volume = options.volume != undefined ? options.volume : 1;
        var autoplay = options.autoplay ? 'autoplay' : '';
        var controls = options.controls ? 'controls' : '';
        var w = options.width != undefined ? 'width="' + options.width + '"' : '';
        var h = options.height != undefined ? 'height="' + options.height + '"' : '';
        var loop = options.loop ? 'loop' : '';
        var muted = volume === 0 || autoplay ? 'muted' : '';
        var params = [autoplay, controls, w, h, loop, muted];
        var ele = '<video class="pt-mp4-video" src="' + this._src + '" ' + params.join(' ') + ' preload="auto"></video>';
        $container.append(ele);

        this.player = $container.children('video').get(0);

        if(autoplay)
        {
            this.player.volume = 0;
            this._playerMgr.handlePlayer('mute', this);
        }
        else
        {
            this.player.volume = volume;
        }

        this.player.onplay = this._onPlay.bind(this);
        this.player.onpause = this._onPause.bind(this);
        this.player.onended = this._onPlayEnd.bind(this);

        if( options.full && (options.widthFromDefault || options.heightFromDefault) )
        {
            this.player.onloadeddata = function() {
                if(options.widthFromDefault)
                {
                    this.player.width = options.width = this.player.videoWidth;
                    $(this._playerMgr._container).data('videoWidth', options.width);
                }
                
                if(options.heightFromDefault)
                {
                    this.player.height = options.height = this.player.videoHeight;
                    $(this._playerMgr._container).data('videoHeight', options.height);
                }
                
                this._onStart();
            }.bind(this);
        }
        else
        {
            this._onStart();
        }
    }

    mp._onStart = function ()
    {
        $(this._container).animate({opacity:1}, 400);

        if(this._callback)
        {
            this._callback.call(this._playerMgr);
        }

        this._playerMgr.onStart();

        if(this._options.autoplay && !this.player.paused)
        {
            this._playerMgr.handlePlayer('play', this);
        }
    }

    mp.play = function ()
    {
        if(this.player)
        {
            this._realPlay = false;
            this._needPause = false;

            var promise = this.player.play();
            if(promise && typeof(promise.then) === 'function' )
            {
                promise.then(function(){
                    this._realPlay = true;

                    if(this._needPause)
                    {
                        this.pause();
                    }
                }.bind(this));
            }
            else
            {
                this._realPlay = true;
            }
        }

        this._playerMgr.handlePlayer('play', this);
    }

    mp.pause = function ()
    {
        if(this.player)
        {
            if(this._realPlay)
            {
                this.player.pause();
            }
            else
            {
                this._needPause = true;
            }
        }

        this._realPlay = false;

        this._playerMgr.handlePlayer('pause', this);
    }

    mp.mute = function()
    {
        if(!this._options.volume)
        {
            return;
        }

        if(this.player)
        {
            this.player.muted = true;
        }

        this._playerMgr.handlePlayer('mute', this);
    }

    mp.unMute = function()
    {
        if(!this._options.volume)
        {
            return;
        }

        if(this.player)
        {
            this.player.muted = false;

            if(this.player.volume === 0)
            {
                this.player.volume = this._options.volume;
            }
        }

        this._playerMgr.handlePlayer('unMute', this);
    }

    mp._onPlay = function (evt)
    {
        this._playerMgr.handlePlayer('play', this);
    }

    mp._onPause = function (evt)
    {
        this._playerMgr.handlePlayer('pause', this);
    }

    mp._onPlayEnd = function (evt)
    {
        if(!this._loop)
        {
            this._playerMgr.onEnd();
        }

        this._playerMgr.handlePlayer('end', this);
    }

    mp.getSrc = function()
    {
        return this._src;
    }

    mp.dispose = function ()
    {
        this.pause();

        if(this.player)
        {
            this.player.onplay = null;
            this.player.onpause = null;
            this.player.onended = null;
            this.player = null;
        }

        this._callback = null;
        this._playerMgr = null;
    }

    /**
     *
     * @param element
     * @param data, {type=>[1:mp4,2:youtube,3:vimeo], src=>string, volume=>0-1, loop=>[0,1], autoplay=>[0,1], controls=>[0,1], mask=>[0,1], width=>px, height=>px}.
     * @constructor
     */
    var MediaPlayer = function(element, data)
    {
        this._element = element;
        element.style.overflow = 'hidden';
        element.__ptMediaPlayer = this;

        var options = {};
        this._options = options;

        for(var key in data)
        {
            if(data.hasOwnProperty(key))
            {
                options[key] = data[key];
            }
        }

        if(options.full === undefined)
        {
            options.full = true;
        }

        if(options.mask === undefined)
        {
            options.mask = true;
        }

        this._src = options.src;
        this._type = options.type;
        this._fullshow = options.full;
        this._onStartHandler = options.onStart || null;
        this._onEndHandler = options.onEnd || null;
        this._onPlayStatusHandler = options.onPlayStatus || null;

        this._isRTL = ($('body').css('direction') || '').toLowerCase() == 'rtl';

        this.$volumeBtn = options.volumeBtn ? $(element).find(options.volumeBtn) : null;
        if(this.$volumeBtn && this.$volumeBtn.length === 0)
        {
            this.$volumeBtn = $(element).siblings(options.volumeBtn);
        }

        this._hasVolumeBtn = this.$volumeBtn && this.$volumeBtn.length > 0;

        this._playingClass = options.playingClass || 'pt-media-playing';

        var container = '<div class="pt-video-container" style="width:100%;height:100%;position:relative;z-index:999;"></div>';
        this._container = $(container).get(0);
        this.$container = $(this._container);
        element.appendChild(this._container);

        var wh = this._fullshow ? '' : 'width:100%;height:100%;';
        var holder = '<div class="pt-video-holder ' + (this._fullshow ? 'full' : 'not-full') + '" style="position:relative;opacity:0;' + wh + '"></div>';
        this._holder = $(holder).get(0);
        this.$holder = $(this._holder);
        this._container.appendChild(this._holder);

        if(options.mask)
        {
            var mask = '<div class="pt-video-mask" style="width:100%;height:100%;z-index:1;overflow:hidden;position:absolute;top:0;left:0;"></div>';
            this.$container.append(mask);
        }

        this._resizeHandler = this.resize.bind(this);

        var $ele = $(element);
        
        if($ele.data('type') !== undefined)
        {
            this._type = $ele.data('type');
        }

        if($ele.data('video-src') !== undefined)
        {
            this._src = $ele.data('video-src');
        }
        else if($ele.data('src') !== undefined)
        {
            this._src = $ele.data('src');
        }

        if($ele.data('volume') !== undefined)
        {
            options.volume = $ele.data('volume');
        }

        if($ele.data('loop') !== undefined)
        {
            options.loop = $ele.data('loop');
        }

        if($ele.data('autoplay') !== undefined)
        {
            options.autoplay = $ele.data('autoplay');
        }

        if($ele.data('controls') !== undefined)
        {
            options.controls = $ele.data('controls');
        }

        if(options.controls)
        {
            this.$holder.css('z-index', 2);
        }
        else
        {
            this.$holder.css('z-index', 0);
        }

        if(options.volume === 0)
        {
            this._hasVolumeBtn = false;

            if(this.$volumeBtn && this.$volumeBtn.length > 0)
            {
                this.$volumeBtn.remove();
            }
        }

        if(this._hasVolumeBtn)
        {
            this.$volumeBtn.on('click', this._onToggleVolume.bind(this));
        }

        var obj = parseUrlParams(this._src);
        if(obj)
        {
            obj.width = obj.width || obj.w || obj.mw;
            obj.height = obj.height || obj.h || obj.mh;

            if(obj.width !== undefined)
            {
                options.width = obj.width;
            }
            if(obj.height !== undefined)
            {
                options.height = obj.height;
            }
        }

        if(options.width === undefined)
        {
            options.width = 1280;
            options.widthFromDefault = true;
        }
        if(options.height === undefined)
        {
            options.height = 720;
            options.heightFromDefault = true;
        }
        
        options.width = parseInt(options.width);
        options.height = parseInt(options.height);

        this.$container.data('videoWidth', options.width);
        this.$container.data('videoHeight', options.height);

        this.player = null;

        this._vimeoJSId = '__pt_vimeo_api';
        this._youtubeJSId = '__pt_youtube_api';

        this._allowCalls = ['dispose', 'play', 'pause', 'mute', 'unMute'];

        this._isMobile =  /Mobi|Tablet|iPad|iPhone|Android/i.test(navigator.userAgent);
        if(this._isMobile)
        {
            options.mask = false;
        }

        if((options.ignoreMobile || window.mpIgnoreMobile) && this._isMobile)
        {
            //ignore mobile
        }
        else if(this._src)
        {
            this.init();
        }
    }

    var p = MediaPlayer.prototype;

    /**
     * 1:mp4, 2:youtube, 3:vimeo
     */
    p.init = function ()
    {
        var options = this._options;

        switch (this._type)
        {
            case 'mp4':
            case '1':
            case 1:
                this._type = 'mp4';
                this.player = new MP4Player(this._holder, this._src, options, this.resize, this);
                this.player.init();
                break;
            case 'youtube':
            case '2':
            case 2:
                this._type = 'youtube';
                this.player = new YoutubePlayer(this._holder, this._src, options, this.resize, this);
                if(ptMediaPlayer.youtubeApiReady)
                {
                    this.player.init();
                }
                else
                {
                    if(!ptMediaPlayer.youtubeWaitingPlayers)
                    {
                        ptMediaPlayer.youtubeWaitingPlayers = [];
                    }
                    ptMediaPlayer.youtubeWaitingPlayers.push(this.player);

                    if(!document.getElementById(this._youtubeJSId))
                    {
                        this._insertJS('https://www.youtube.com/iframe_api', null, this._youtubeJSId);
                    }
                }
                break;
            case 'vimeo':
            case '3':
            case 3:
                this._type = 'vimeo';
                this.player = new VimeoPlayer(this._holder, this._src, options, this.resize, this);
                if(ptMediaPlayer.vimeoApiReady)
                {
                    this.player.init();
                }
                else
                {
                    if(!ptMediaPlayer.vimeoWaitingPlayers)
                    {
                        ptMediaPlayer.vimeoWaitingPlayers = [];
                    }
                    ptMediaPlayer.vimeoWaitingPlayers.push(this.player);

                    if(!document.getElementById(this._vimeoJSId))
                    {
                        this._insertJS('https://player.vimeo.com/api/player.js', onPTVimeoAPIReady, this._vimeoJSId);
                    }
                }
                break;
            default:
                this._type = 'mp4';
                this.player = new MP4Player(this._holder, this._src, options, this.resize, this);
                break;
        }

        if(this._fullshow)
        {
            $(win).on('resize', this._resizeHandler);
        }
    }

    p.onStart = function ()
    {
        if(this._onStartHandler)
        {
            this._onStartHandler(this._element, this.player);
        }
    }

    p.onEnd = function ()
    {
        if(this._onEndHandler)
        {
            this._onEndHandler(this._element, this.player);
        }
    }

    p.handlePlayer = function (type, player)
    {
        var players = ptMediaPlayer.playingPlayers;

        switch (type)
        {
            case 'play':
                if(players.indexOf(player) === -1)
                {
                    players.push(player);

                    if(ptMediaPlayer.onStatusChanged)
			        {
			        	ptMediaPlayer.onStatusChanged(true, type);
			        }
                }
                $('body').addClass(this._playingClass);

                if(this._onPlayStatusHandler)
                {
                    this._onPlayStatusHandler(type);
                }
                break;

            case 'pause':
            case 'end':
                var index = players.indexOf(player);
                if(index > -1)
                {
                    players.splice(index, 1);

                    if(ptMediaPlayer.onStatusChanged)
			        {
			        	ptMediaPlayer.onStatusChanged(players.length > 0, type);
			        }
                }

                if(players.length <= 0)
                {
                    $('body').removeClass(this._playingClass);
                }

                if(this._onPlayStatusHandler)
                {
                    this._onPlayStatusHandler(type);
                }

                break;

            case 'mute':
                $(this._element).addClass('pt-mediaplayer-muted');
                break;

            case 'unMute':
                $(this._element).removeClass('pt-mediaplayer-muted');
                break;
        }
    }

    p._onToggleVolume = function(evt)
    {
        if($(this._element).hasClass('pt-mediaplayer-muted'))
        {
            this.unMute();
        }
        else
        {
            this.mute();
        }
    }

    p.resize = function (evt)
    {
        if(!this._fullshow)
        {
            return;
        }

        var $container = this.$container;
        var $holder = this.$holder;
        var vw = $container.data('videoWidth');
        var vh = $container.data('videoHeight');
        var sw = this._container.clientWidth;
        var sh = this._container.clientHeight;
        var scale = Math.max(sw / (vw - 5), sh / (vh - 5));
        var x, y, w, h;

        if(this._type == 'mp4')
        {
            w = vw * scale;
            h = vh * scale;
            x = ((sw - w) >> 1) - 1;
            y = ((sh - h) >> 1) - 1;

            if(this._isRTL)
            {
                $holder.css({
                    right: x + 'px',
                    top: y + 'px',
                    width: (w + 2) + 'px',
                    height: (h + 2) + 'px'
                });
            }
            else
            {
                $holder.css({
                    left: x + 'px',
                    top: y + 'px',
                    width: (w + 2) + 'px',
                    height: (h + 2) + 'px'
                });
            }
        }
        else
        {
            var $iframe = $holder.find('iframe');

            w = vw * scale + 2;
            h = vh * scale + 2;

            x = ((sw - w) >> 1) - 1;
            y = ((sh - h) >> 1) - 1;

            $iframe.css({
                width: w + 'px',
                height: h + 'px'
            });

            if(this._isRTL)
            {
                $holder.css({
                    right: x + 'px',
                    top: y + 'px'
                });
            }
            else
            {
                $holder.css({
                    left: x + 'px',
                    top: y + 'px'
                });
            }
        }
    }

    p._insertJS = function(src, callback, id)
    {
        var tag = document.createElement('script');

        if (callback)
        {
            if (tag.readyState)
            {
                tag.onreadystatechange = function(){
                    if (tag.readyState === "loaded" ||
                        tag.readyState === "complete"){
                        tag.onreadystatechange = null;
                        callback();
                    }
                };
            }
            else
            {
                tag.onload = function() {
                    callback();
                };
            }
        }

        tag.src = src;
        if(id)
        {
            tag.id = id;
        }

        var firstScriptTag = document.getElementsByTagName('script')[0];
        if(firstScriptTag)
        {
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        }
        else
        {
            var headTag = document.getElementsByTagName('head')[0];
            headTag.appendChild(tag);
        }
    };

    p.play = function()
    {
        this.player.play();
    };

    p.pause = function()
    {
        this.player.pause();
    };

    p.mute = function()
    {
        this.player.mute();
    }

    p.unMute = function()
    {
        this.player.unMute();
    }

    p.execute = function(funcName, funcParams)
    {
        if(!this.player || !funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return;
        }

        if(funcParams && !Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) == 'function')
        {
            f.apply(this, funcParams || []);
        }
    };

    p.dispose = function(needRemoveContainer)
    {
        if(this.player)
        {
            var index;
            if(ptMediaPlayer.vimeoWaitingPlayers)
            {
                index = ptMediaPlayer.vimeoWaitingPlayers.indexOf(this.player);
                if(index > -1)
                {
                    ptMediaPlayer.vimeoWaitingPlayers.splice(index, 1);
                }
            }

            if(ptMediaPlayer.vimeoWaitingPlayers)
            {
                index = ptMediaPlayer.vimeoWaitingPlayers.indexOf(this.player);
                if(index > -1)
                {
                    ptMediaPlayer.vimeoWaitingPlayers.splice(index, 1);
                }
            }

            this.player.dispose();
            this.player = null;
        }

        if(this.$volumeBtn && this.$volumeBtn.length > 0)
        {
            this.$volumeBtn.off('click');
        }

        if(needRemoveContainer)
        {
            this.$container.remove();
        }

        if(this._resizeHandler)
        {
            $(win).off('resize', this._resizeHandler);
            this._resizeHandler = null;
        }

        if(this._element)
        {
            this._element.__ptMediaPlayer = null;
            this._element = null;
        }

        this._onPlayStatusHandler = null;
    };

    $.fn.extend({
        ptMediaPlayer:function (options) {
            this.each(function(){
                var mp = this.__ptMediaPlayer;
                mp && mp.dispose();
                new MediaPlayer(this, options);
            });
            return this;
        },
        ptMediaPlayerCall:function (funcName, funcParams) {
            this.each(function(){
                var mp = this.__ptMediaPlayer;
                mp && mp.execute(funcName, funcParams);
            });
            return this;
        }
    });
}(jQuery, window));


/**
 * Created by foreverpinetree@gmail.com on 2017/06/25.
 */
!(function($, win)
{
    "use strict";

    var __ars = [];

    var AreaRoll = function(ele, obj)
    {
        __ars.push(this);

        this._element = ele;
        this.$element = $(ele);

        ele.__ptAreaRoll = this;

        createjs.Ticker.timingMode = createjs.Ticker.RAF;

        this._moveRatio = obj.moveRatio == undefined ? 0.8 : obj.moveRatio;
        this._animateTime = obj.animateTime == undefined ? 500 : obj.animateTime;
        this._easing = obj.easing == undefined ? 'quartOut' : obj.easing;
        this._useWheel = obj.useWheel == undefined ? true : obj.useWheel;

        this._winWidth = 0;

        this._targetHeight = 0;
        this._eleHeight = 0;
        this._moveDistance = 0;
        this._y = 0;
        this._tweenValue = 0;

        this._allowCalls = ['dispose', 'resize'];

        this._isDevice = /Mobi|Tablet|iPad|iPhone|Android/i.test(navigator.userAgent);

        if(this._isDevice)
        {
            this._useWheel = false;

            this.$element.addClass('pt-area-roll-device');
        }

        this._clickEvt = this._isDevice ? 'touchend mouseup' : 'click';

        this._onResizeHandler = this._onResize.bind(this);
        this._onWheelHandler = this._onWheel.bind(this);
        this._onTweenUpdateHandler = this._onTweenUpdate.bind(this);

        this.$target = null;
        this.$wrap = null;
        this.$prevBtn = null;
        this.$nextBtn = null;

        this._init();
    }

    var p = AreaRoll.prototype;

    p._init = function()
    {
        var $ele = this.$element;
        $ele.wrapInner('<div class="pt-area-roll-inner"></div>');
        $ele.wrap('<div class="pt-area-roll-wrap"></div>');
        $ele.on('scroll', function(evt){
            var scrollTop = $ele.scrollTop();
            this._y = -scrollTop;
            this._updateButtonState(this._y);
        }.bind(this));

        this.$target = $ele.children('.pt-area-roll-inner');

        var $wrap = $ele.parent();
        this.$wrap = $wrap;

        var html = '<div class="pt-area-roll-up pt-area-roll-btn"></div>';
        html += '<div class="pt-area-roll-down pt-area-roll-btn"></div>';
        $wrap.append(html);

        this.$prevBtn = $wrap.children('.pt-area-roll-up');
        this.$nextBtn = $wrap.children('.pt-area-roll-down');

        this.$prevBtn.on(this._clickEvt, this._onPrev.bind(this));
        this.$nextBtn.on(this._clickEvt, this._onNext.bind(this));

        $(win).on('resize', this._onResizeHandler);
        this._onResize();
    }

    p._onResize = function(evt)
    {
        var sw = $(win).width();
        if(evt)
        {
            if(this._winWidth === sw)
            {
                return;
            }
        }

        this._winWidth = sw;

        this._targetHeight = this.$target.height();
        this._eleHeight = $(this._element).height();
        this._moveDistance = Math.ceil(this._eleHeight * this._moveRatio);

        if(this._useWheel)
        {
            if(this._targetHeight > this._eleHeight)
            {
                this._element.addEventListener('wheel', this._onWheelHandler, true);
            }
            else
            {
                this._element.removeEventListener('wheel', this._onWheelHandler, true);
            }
        }

        if(this._y >= 0)
        {
            this._y = 0;
        }

        if(this._y <= this._eleHeight - this._targetHeight)
        {
            this._y = this._eleHeight - this._targetHeight;
        }

        this._render(false);
    }

    p.resize = function()
    {
        this._onResize();
    }

    p._onWheel = function(evt)
    {
        if(this._eleHeight >= this._targetHeight)
        {
            return;
        }

        if(evt.deltaY < 0)
        {
            if(this._y >= 0)
            {
                return;
            }

            evt.preventDefault();
            this._onPrev();
        }
        else if(evt.deltaY > 0)
        {
            if(this._y <= this._eleHeight - this._targetHeight)
            {
                return;
            }

            evt.preventDefault();
            this._onNext();
        }
    }

    p._onPrev = function(evt)
    {
        if(evt)
        {
            evt.preventDefault();
            evt.stopPropagation();
        }

        if(this._eleHeight >= this._targetHeight)
        {
            return;
        }

        if(this._y >= 0)
        {
            return;
        }

        this._y += this._moveDistance;

        if(this._y >= 0)
        {
            this._y = 0;
        }

        this._render(true);
    }

    p._onNext = function(evt)
    {
        if(evt)
        {
            evt.preventDefault();
            evt.stopPropagation();
        }

        if(this._eleHeight >= this._targetHeight)
        {
            return;
        }

        if(this._y <= this._eleHeight - this._targetHeight)
        {
            return;
        }

        this._y -= this._moveDistance;

        if(this._y <= this._eleHeight - this._targetHeight)
        {
            this._y = this._eleHeight - this._targetHeight;
        }

        this._render(true);
    }

    p._render = function(tween)
    {
        if(this._isDevice)
        {
            if(tween)
            {
                this._tweenValue = this.$element.scrollTop();
                createjs.Tween.get(
                    this,
                    {
                        override:true,
                        onChange:this._onTweenUpdateHandler
                    }
                ).to(
                    {_tweenValue: -this._y },
                    this._animateTime,
                    createjs.Ease[this._easing] || createjs.Ease.cubicOut
                );
            }
            else
            {
                this.$element.scrollTop(-this._y);
            }
        }
        else
        {
            if(tween)
            {
                this._tweenValue = parseInt(this.$target.css('top'));
                createjs.Tween.get(
                    this,
                    {
                        override:true,
                        onChange:this._onTweenUpdateHandler
                    }
                ).to(
                    {_tweenValue: this._y },
                    this._animateTime,
                    createjs.Ease[this._easing] || createjs.Ease.cubicOut
                );
            }
            else
            {
                this.$target.css('top', this._y + 'px');
            }
        }

        this._updateButtonState(this._y);
    }

    p._onTweenUpdate = function(evt)
    {
        if(this._isDevice)
        {
            this.$element.scrollTop(this._tweenValue);
        }
        else
        {
            this.$target.css('top', this._tweenValue + 'px');
        }
    }

    p._updateButtonState = function(y)
    {
        if(this._eleHeight >= this._targetHeight)
        {
            this.$prevBtn.addClass('disabled');
            this.$nextBtn.addClass('disabled');

            this.$wrap.addClass('no-roll');
        }
        else if(y >= 0)
        {
            this.$prevBtn.addClass('disabled');
            this.$nextBtn.removeClass('disabled');

            this.$wrap.removeClass('no-roll');
        }
        else if(y <= this._eleHeight - this._targetHeight)
        {
            this.$prevBtn.removeClass('disabled');
            this.$nextBtn.addClass('disabled');

            this.$wrap.removeClass('no-roll');
        }
        else
        {
            this.$prevBtn.removeClass('disabled');
            this.$nextBtn.removeClass('disabled');
            
            this.$wrap.removeClass('no-roll');
        }
    }

    p.execute = function(funcName, funcParams)
    {
        if(!funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return;
        }

        if(funcParams && !Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) == 'function')
        {
            f.apply(this, funcParams || []);
        }
    };

    p.dispose = function()
    {
        this.$prevBtn && this.$prevBtn.off(this._clickEvt);
        this.$nextBtn && this.$nextBtn.off(this._clickEvt);
        this.$target && this.$target.stop();

        if(this._element)
        {
            if(this._useWheel)
            {
                this._element.removeEventListener('wheel', this._onWheelHandler, true);
            }

            this._element.__ptAreaRoll = null;
            this._element = null;
        }

        createjs.Tween.removeTweens(this);

        this._onWheelHandler = null;
        this._onTweenUpdateHandler = null;

        var index = __ars.indexOf(this);
        if(index > -1)
        {
            __ars.splice(index, 1);
        }
    }

    $.fn.extend({
        ptAreaRoll:function (options) {
            this.each(function(){
                var ar = this.__ptAreaRoll;
                ar && ar.dispose();
                new AreaRoll(this, options || {});
            });
            return this;
        },
        ptAreaRollCall:function (funcName, funcParams) {
            this.each(function(){
                var ar = this.__ptAreaRoll;
                ar && ar.execute(funcName, funcParams);
            });
            return this;
        }
    });

    win.addEventListener('load', function(evt){
        __ars.forEach(function(ar){
            ar && ar.resize();
        });
    });
}(jQuery, window));


/**
 * Created by foreverpinetree@gmail.com on 2017/08/12.
 * Site: http://www.3theme.com
 */
!(function($, win)
{
    "use strict";

    var Swipe = function(element, options)
    {
        this._element = element;
        element.__ptSwipe = this;

        this.$element = $(this._element);

        options = options || {};
        this._options = options;

        this._ease = options.ease || 'cubicOut';
        this._dragEase = options.dragEase || 'quartOut';
        this._speed = options.speed === undefined ? 350 : options.speed;
        this._tweenOutOffset = options.tweenOutOffset || 0;
        this._useDrag = options.useDrag === undefined ? true : options.useDrag;

        var autoplay = this.$element.data('autoplay');
        if(autoplay === undefined)
        {
            this._autoplay = options.autoplay === undefined ? true : options.autoplay;
        }
        else
        {
            this._autoplay = autoplay ? true : false;
        }

        var duration = this.$element.data('duration');
        if(duration === undefined)
        {
            this._duration = options.duration || 5000;
        }
        else
        {
            this._duration = duration || 5000;
        }

        this._isPaused = false;

        this._prevMovingClass = 'prev-moving';
        this._nextMovingClass = 'next-moving';

        this._onSwipeBefore = options.onSwipeBefore || null;
        this._onSwipeAfter = options.onSwipeAfter || null;

        createjs.Ticker.timingMode = createjs.Ticker.RAF;

        this._isIE9 = navigator.userAgent.indexOf("MSIE 9.0") > -1;

        var $content = null;
        if(options.wrapTarget && typeof(options.wrapTarget) === 'object')
        {
            $content = $(options.wrapTarget);
        }
        else
        {
            $content = this.$element.find(options.wrapTarget || '.wrap');
        }

        this.$content = $content;

        var $items = null;
        if(options.itemTarget && typeof(options.itemTarget) === 'object')
        {
            $items = $(options.itemTarget);
        }
        else if(options.itemTarget)
        {
            $items = this.$content.find(options.itemTarget);
        }

        this._itemClass = typeof(options.itemTarget) === 'string' ? options.itemTarget : '';
        if(this._itemClass.indexOf('.') === 0)
        {
            this._itemClass = this._itemClass.substring(1);
        }

        this.$items = $items;
        $items.css({
            'position': 'absolute',
            'top': '0',
        });

        $items.each(function(i, item){
            $(item).css('z-index', i).attr('data-index', i);
        });

        var prevLabel = this.$element.data('prevLabel') || '';
        var nextLabel = this.$element.data('nextLabel') || '';
        var len = $items.length;
        
        var html = '<div class="pt-swipe-arrow">';
        html += '<span class="pt-swipe-prev">' + prevLabel + '</span><span class="pt-swipe-next">' + nextLabel + '</span></div>';
        html += '<div class="pt-swipe-dots"><ul>';
        for(var i = 0; i < len; i ++)
        {
            html += '<li>' + (i + 1) + '</li>';
        }
        html += '</ul>';

        this.$element.append(html);

        this.$element.find('.pt-swipe-prev').on('click', this._onPrev.bind(this));
        this.$element.find('.pt-swipe-next').on('click', this._onNext.bind(this));

        this.$dotList = this.$element.find('.pt-swipe-dots>ul>li');
        this.$dotList.on('click', this._onClickDot.bind(this));

        this._disposed = false;

        this._isTweening = false;
        this._isDragging = false;

        this._dargScale = 0.3;
        this._dragDistance = 50;

        this._timerId = -1;

        this._allowCalls = ['setAutoplay', 'prev', 'next', 'pause', 'resume', 'dispose'];

        this._currentIndex = 0;
        this._totalCount = len;

        this._onResizeHandler = this._onResize.bind(this);

        $(win).on('resize', this._onResizeHandler);

        this._isDevice = /Mobi|Tablet|iPad|iPhone|Android/i.test(navigator.userAgent);

        this._clickEvt = this._isDevice ? 'touchend mouseup' : 'click';
        this._downEvt = 'touchstart mousedown';
        this._moveEvt = 'touchmove mousemove';
        this._upEvt = 'touchend mouseup';

        this._onDraggingHandler = this._onDragging.bind(this);
        this._onDragEndHandler = this._onDragEnd.bind(this);

        if(this._useDrag)
        {
            $items.on(this._downEvt, this._onStartDrag.bind(this));
        }
        
        this._onResize();

        if(this._totalCount > 0)
        {
            this._open(0);
        }

        if(options.onInit && typeof(options.onInit) === 'function')
        {
            options.onInit.apply(this, null);
        }
    }

    var p = Swipe.prototype;

    p._onResize = function(evt)
    {
        this._width = this.$content.width();
    }

    p._onStartDrag = function (event)
    {
        if(!this._isDevice && this._itemClass && !$(event.target).hasClass(this._itemClass))
        {
            return;
        }

    	if(this._isTweening || this._isDragging)
    	{
    		return;
    	}

        this._isDragging = true;

        if(this._timerId > -1)
        {
            clearTimeout(this._timerId);
        }

        var evt = event.originalEvent;

        var target = evt.currentTarget;
        this._dragTarget = target;

        $(win).off(this._moveEvt, this._onDraggingHandler).on(this._moveEvt, this._onDraggingHandler);
        $(win).off(this._upEvt, this._onDragEndHandler).on(this._upEvt, this._onDragEndHandler);

        this._oldMouseX = this._getMouseX(evt);
        this._currentX = 0;
        this._startDragX = this._currentX;
    }

    p._onDragging = function (event)
    {
        var evt = event.originalEvent;

        var x = this._getMouseX(evt);
        this._currentX += x - this._oldMouseX;
        this._oldMouseX = x;

        this._renderX($(this._dragTarget), this._currentX * this._dargScale);

        var comingIndex, leavingX;
        if(this._currentX > 0) //prev
        {
            comingIndex = this._currentIndex - 1;
            if(comingIndex < 0)
            {
                comingIndex = this._totalCount - 1;
            }
            leavingX = - this._width + this._currentX * this._dargScale;
        }
        else if(this._currentX < 0) //next
        {
            comingIndex = this._currentIndex + 1;
            if(comingIndex > this._totalCount - 1)
            {
                comingIndex = 0;
            }
            leavingX = this._width + this._currentX * this._dargScale;
        }

        this.$items.removeClass('leaving');
        var comingItem = this.$items.get(comingIndex);
        $(comingItem).addClass('leaving');
        this._renderX($(comingItem), leavingX);
    }

    p._onDragEnd = function (event)
    {
        $(win).off(this._moveEvt, this._onDraggingHandler);
        $(win).off(this._upEvt, this._onDragEndHandler);

        if(this._dragTarget)
        {
            this._dragTarget.__tweenX = this._currentX * this._dargScale;
        }

        var distance = this._currentX - this._startDragX;

        if(distance < - this._dragDistance)
        {
            this._onNext();
            this._isDragging = false;
        }
        else if(distance > this._dragDistance)
        {
            
            this._onPrev();
            this._isDragging = false;
        }
        else
        {
            this._dragBack();
        }

        this._dragTarget = null;
    }

    p._getMouseX = function(evt)
    {
        var x;
        if(this._isDevice)
        {
            var touches = evt.changedTouches, touch;
            touch = touches[0];
            if(touch)
            {
                x = touch.pageX;
            }
            else
            {
                x = evt.pageX !== undefined ? evt.pageX : evt.clientX;
            }
        }
        else
        {
            x = evt.pageX !== undefined ? evt.pageX : evt.clientX;
        }
        return x;
    }

    p._dragBack = function ()
    {
        createjs.Tween.get(
            this._dragTarget,
            {
                override:true,
                onChange:this._onUpdate.bind(this)
            }
        ).to(
            {__tweenX:0},
            300,
            createjs.Ease.sineOut
        ).call(this._onDragBack.bind(this));
    }

    p._onUpdate = function (evt)
    {
        var tween = evt.target;
        var target = tween.target;
        
        this._renderX($(target), target.__tweenX);
    }

    p._onDragBack = function ()
    {
        this._isDragging = false;
        this._checkAutoplay();
    }

    p._open = function(index, direction)
    {
        if(this._isTweening)
        {
            return;
        }

        this.$items.removeClass('current leaving');

        this.$dotList.removeClass('selected');
        $(this.$dotList.get(index)).addClass('selected');

        var currentItem = this.$items.get(index);
        $(currentItem).addClass('current');

        if(index === this._currentIndex)
        {
            var $item = $(currentItem);
            this._bringToTop(currentItem);
            this._renderX($item, 0);

            this._checkAutoplay();

            return;
        }
        var prevIndex = this._currentIndex;
        this._currentIndex = index;

        var prevItem = this.$items.get(prevIndex);
        $(prevItem).addClass('leaving');

        if(direction === undefined)
        {
            direction = index > prevIndex ? 1 : -1;
        }

        this._isTweening = true;

        var movingClass = direction < 0 ? this._prevMovingClass : this._nextMovingClass;
        this.$element.removeClass(this._prevMovingClass).removeClass(this._nextMovingClass).addClass(movingClass);

        if(this._onSwipeBefore)
        {
            this._onSwipeBefore.call(this, null);
        }

        this._tweenOut(prevItem, direction);
        this._tweenIn(currentItem, direction);
    }

    p._tweenIn = function(item, direction)
    {
        var x = direction > 0 ? this._width : - this._width;

        if(this._isDragging)
        {
            x = item.__renderingX || 0;
        }

        this._renderX($(item), x);

        item.__tweenValue = x;

        this._bringToTop(item);

        this._tweenX(item, 0, true);
    }

    p._tweenOut = function(item, direction)
    {
        var x = direction > 0 ? - this._width : this._width;
        var startX = item.__renderingX || 0;

        item.__tweenValue = this._isDragging ? startX : 0;
        
        this._bringToTop(item);

        if(direction > 0)
        {
            x += this._tweenOutOffset * this._width;
        }
        else
        {
            x -= this._tweenOutOffset * this._width;
        }

        this._tweenX(item, this._isDragging ? x + startX : x, false);
    }

    p._tweenX = function (item, x, listenComplete)
    {
        var tween = createjs.Tween.get(
            item,
            {
                override:true,
                onChange:this._onAniUpdate.bind(this)
            }
        );

        tween.to(
            {__tweenValue:x},
            this._speed,
            this._isDragging ? (createjs.Ease[this._dragEase] || createjs.Ease.quartOut) : (createjs.Ease[this._ease] || createjs.Ease.cubicOut)
        );

        if(listenComplete)
        {
            tween.call(this._onAniComplete.bind(this));
        }
    }

    p._onAniUpdate = function (evt)
    {
        var tween = evt.target;
        var target = tween.target;
        this._renderX($(target), target.__tweenValue);
    }

    p._onAniComplete = function()
    {
        this._isTweening = false;
        this.$element.removeClass(this._prevMovingClass).removeClass(this._nextMovingClass);
        this.$items.removeClass('leaving');

        if(this._onSwipeAfter)
        {
            this._onSwipeAfter.call(this, null);
        }

        this._checkAutoplay();
    }

    p._renderX = function($target, xValue)
    {
        if(this._isDragging && $target.get(0))
        {
            $target.get(0).__renderingX = xValue;
        }

        if(this._isIE9)
        {
            $target.css('-ms-transform', 'translateX(' + xValue + 'px)');
        }
        else
        {
            $target.css('transform', 'translate3d(' + xValue + 'px,0,0)');
        }
    }

    p._bringToTop= function(target)
    {
        var arr = [];
        this.$items.each(function(){
            if(this !== target)
            {
                arr.push(this);
            }
        });

        arr.forEach(function(item, i){
            $(item).css('z-index', i);
        });
        $(target).css('z-index', arr.length);
    }

    p._checkAutoplay = function()
    {
        if(!this._autoplay)
        {
            return;
        }

        if(this._isPaused)
        {
            return;
        }

        if(this._timerId > -1)
        {
            clearTimeout(this._timerId);
        }

        this._timerId = setTimeout(function(){
            this._timerId = -1;
            this._onNext();
        }.bind(this), this._duration);
    }

    p._onPrev = function(evt)
    {
        if(this._isTweening)
        {
            return;
        }

        var index = this._currentIndex - 1;
        if(index < 0)
        {
            index = this._totalCount - 1;
        }

        this._open(index, -1);
    }

    p._onNext = function(evt)
    {
        if(this._isTweening)
        {
            return;
        }

        var index = this._currentIndex + 1;
        if(index > this._totalCount - 1)
        {
            index = 0;
        }

        this._open(index, 1);
    }

    p._onClickDot = function(evt)
    {
        if($(evt.currentTarget).hasClass('selected'))
        {
            return;
        }

        var index = $(evt.currentTarget).index();
        this._open(index);
    }

    p.prev = function()
    {
        this._onPrev();
    }

    p.next = function()
    {
        this._onNext();
    }

    p.setAutoplay = function(autoplay)
    {
        this._autoplay = autoplay;

        if(autoplay)
        {
            if(this._timerId < 0)
            {
                this._checkAutoplay();
            }
        }
        else
        {
            if(this._timerId > 0)
            {
                clearTimeout(this._timerId);
                this._timerId = -1;
            }
        }
    }

    p.pause = function()
    {
    	this._isPaused = true;

    	if(this._timerId > 0)
        {
            clearTimeout(this._timerId);
            this._timerId = -1;
        }
    }

    p.resume = function()
    {
    	this._isPaused = false;
    	this._checkAutoplay();
    }

    p.execute = function(funcName, funcParams)
    {
        if(!funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return;
        }

        if(!Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) === 'function')
        {
            f.apply(this, funcParams);
        }
    }

    p.dispose = function()
    {
    	if(this._disposed) return;
        this._disposed = true;

        this._onSwipeBefore = null;
        this._onSwipeAfter = null;

        $(win).off('resize', this._onResizeHandler);
        this._onResizeHandler = null;

        this.$content.off(this._upEvt).off(this._moveEvt).off(this._downEvt);

        if(this._element)
        {
            this.$element.find('.pt-swipe-prev').off('click');
            this.$element.find('.pt-swipe-next').off('click');
            this.$dotList.off('click');

            this._element.__ptSwipe = null;
            this._element = null;
        }
    }

    $.fn.extend({
        ptSwipe:function (options) {
            this.each(function(){
                var fr = this.__ptSwipe;
                fr && fr.dispose();
                new Swipe(this, options);
            });
            return this;
        },
        ptSwipeCall:function (funcName, funcParams) {
            this.each(function(){
                var fr = this.__ptSwipe;
                fr && fr.execute(funcName, funcParams);
            });
            return this;
        }
    });
}(jQuery, window));


/**
 * Created by foreverpinetree@gmail.com on 2017/05/23.
 */
!(function($, win)
{
    "use strict";

    var Parallax = function(element, obj)
    {
    	this._element = element;
    	this._element.__ptParallax = this;

    	this.$element = $(element);

        createjs.Ticker.timingMode = createjs.Ticker.RAF;

    	this._targetQuery = obj.target || '.item';
    	this._addClass = obj.addClass || 'pt-parallax';
    	this._addTargetClass = obj.addTargetClass || 'pt-parallax-target';
        this._useAnimate = obj.useAnimate == undefined ? true : obj.useAnimate;
        this._animateTime = obj.animateTime == undefined ? 500 : obj.animateTime;
        this._animateEasing = obj.animateEasing || createjs.Ease.cubicOut;
        this._resizeCD = obj.resizeCD === undefined ? 200 : obj.resizeCD;
        this._scrollTarget = obj.scrollTarget || window;

        this._offset = parseFloat(this.$element.data('offset') || 0) || 0;
        this._direct = this.$element.data('direct') == '1' ? 1 : 0;

        if(this._offset < 0)
        {
            this._offset = 0;
        }

        this._eleWidth = 0;
        this._eleHeight = 0;
        this._targetHeight = 0;
        this._offsetHeight = 0;

        this._initResizeId = -1;

        this._forceAnimate = false;

        this._disposed = false;

        if(this._addClass.indexOf('.') == 0)
        {
            this._addClass = this._addClass.substring(1);
        }

        if(this._addTargetClass.indexOf('.') == 0)
        {
            this._addTargetClass = this._addTargetClass.substring(1);
        }

        this.$element.addClass(this._addClass);

        this._onResizeHandler = null;
        this._onScrollHandler = null;
        this._onAniUpdateHandler = null;

        this._aniValue = 0;

        this.$target = this.$element.find(this._targetQuery);

        if(this.$target.length > 0)
        {
            this._init();
        }
    }

    var p = Parallax.prototype;

    p._init = function()
    {
    	if(this._addTargetClass)
    	{
    		this.$target.addClass(this._addTargetClass);
    	}

    	var $win = $(win);

        this._onResizeHandler = this._onResize.bind(this);
        $win.on('resize', this._onResizeHandler);

        this._onScrollHandler = this._onScroll.bind(this);
        $(this._scrollTarget).on('scroll', this._onScrollHandler);

        this._onAniUpdateHandler = this._onAniUpdate.bind(this);

        this._initResizeId = setTimeout(function () {
            this._initResizeId = -1;
            this._onResize();
        }.bind(this), this._resizeCD);
    }

    p._onResize = function(evt)
    {
        if(this._disposed)
        {
            return;
        }

    	var winHeight = $(win).height();

        this._eleWidth = this._element.clientWidth;
        this._eleHeight = this._element.clientHeight;
        this._targetHeight = Math.ceil(this._eleHeight + (this._eleHeight * this._offset) * ((winHeight - this._eleHeight) / winHeight));
        this._offsetHeight = Math.ceil(this._eleHeight * this._offset);

        this.$target.css('height', this._targetHeight + 'px');

        this._onScroll();
    }

    p._onScroll = function (evt) 
    {
        var $ele = this.$element, scrollTop = $(win).scrollTop(), offsetTop = $ele.offset().top, winHeight = $(win).height();
        if(offsetTop + this._eleHeight > scrollTop && offsetTop < scrollTop + winHeight)
        {
            var $target = this.$target, y = 0, top, p;

            if(this._direct == 0)
            {
                top = scrollTop - offsetTop;
                p = top / winHeight;
                y = p * this._offsetHeight;
            }
            else
            {
                top = scrollTop - offsetTop - this._eleHeight;
                p = top / (winHeight + this._eleHeight);
                y = -1 * (p + 1) * this._offsetHeight;
            }

            if(this._useAnimate || this._forceAnimate)
            {
                createjs.Tween.get(
                    this,
                    {
                        override:true,
                        onChange:this._onAniUpdateHandler
                    }
                ).to(
                    {_aniValue:y},
                    this._animateTime,
                    createjs.Ease[this._animateEasing] || createjs.Ease.cubicOut
                );
            }
            else
            {
                this._renderY($target, y);
            }
        }
    }

    p._onAniUpdate = function()
    {
        this._renderY(this.$target, this._aniValue);
    }

    p._renderY = function ($target, y)
    {
    	y = Math.round(y);
        $target.css('transform', 'translate3d(0,' + y + 'px,0)');
    }

    p.resize = function()
    {
    	this._forceAnimate = true;
        this._onResize();
        this._forceAnimate = false;
    }

    p.execute = function(funcName, funcParams)
    {
        if(funcName.indexOf('_') === 0)
        {
            return;
        }

        var f = this[funcName];
        if(f && typeof(f) == 'function')
        {
            f.apply(this, funcParams || []);
        }
    }

    p.dispose = function()
    {
        if(this._disposed)
        {
            return;
        }

        this._disposed = true;

        if (this._initResizeId > -1)
        {
            clearTimeout(this._initResizeId);
            this._initResizeId = -1;
        }

    	var $win = $(win);

    	if(this._onScrollHandler)
    	{
    		$(this._scrollTarget).off('scroll', this._onScrollHandler);
    		this._onScrollHandler = null;
    	}

        if(this._onResizeHandler)
        {
            $win.off('resize', this._onResizeHandler);
            this._onResizeHandler = null;
        }

    	if(this._element)
    	{
    		this._element.__ptParallax = null;
    		this._element = null;
    	}

        this._onAniUpdateHandler = null;
    }

	$.fn.extend({
        ptParallax:function (obj) {
        	var ua = navigator.userAgent;
            var isDevice = /Mobi|Tablet|iPad|iPhone|Android/i.test(ua);
            var isIE = (!!window.ActiveXObject || "ActiveXObject" in window);
            var isIE11 = (ua.toLowerCase().indexOf("trident") > -1 && ua.indexOf("rv") > -1);

            if(isDevice || (isIE && !isIE11))
            {
                return this;
            }

            this.each(function(){
            	var parallax = this.__ptParallax;
                parallax && parallax.dispose();

                new Parallax(this, obj || {});
            });
            return this;
        },
        ptParallaxCall: function(funcName, funcParams){
            this.each(function(){
                var parallax = this.__ptParallax;
                parallax && parallax.execute(funcName, funcParams);
            });
            return this;
        }
    });
})(jQuery, window);


/**
 * Elastic hover
 * @author foreverpinetree@gmail.com
 **/

!(function($, win)
{
    "use strict";
    
    var elaticHovers = [];

    var ElasticHover = function(element, data)
    {
        element.__ptElasticHover = this;
        elaticHovers.push(this);

        this._element = element;

        createjs.Ticker.timingMode = createjs.Ticker.RAF;

        this._direct = data.direct || 'xy';

        this._tweenSpeed = data.tweenSpeed === undefined ? 500 : data.tweenSpeed;
        this._backSpeed = data.backSpeed === undefined ? 800 : data.backSpeed;

        this._tweenEase = data.tweenEase || createjs.Ease.quintOut;
        this._backEase = data.backEase || createjs.Ease.quintOut;

        this._scale = data.scale === undefined ? 1 : data.scale;
        this._scaleSpeed = data.scaleSpeed === undefined ? 500 : data.scaleSpeed;

        this.$element = $(element);

        if(data.target)
        {
            this.$target = this.$element.find(data.target);
        }
        else
        {
            this.$target = this.$element;
        }

        this._disposed = false;
        this._isMoving = false;

        this._allowCalls = ['dispose'];

        this._offset = data.offset || 10;
        this._rect = {x:0, y:0, w:0, h:0};

        this._aniValueX = 0;
        this._aniValueY = 0;
        this._scaleObj = {scale:1};

        this.$element.hover(this._onOver.bind(this), this._onOut.bind(this));

        this._onMovingHandler = this._onMoving.bind(this);
    }

    var p = ElasticHover.prototype;

    p._onOver = function()
    {
        setTimeout(function(){

            $(win).on('mousemove', this._onMovingHandler);
            var w = this.$element.width(), h = this.$element.outerHeight();
            var jOffset = this.$element.offset();
            var offsetX = jOffset.left, offsetY = jOffset.top;
            this._rect.x = offsetX - this._offset;
            this._rect.y = offsetY - this._offset;
            this._rect.w = w + this._offset * 2;
            this._rect.h = h + this._offset * 2;

            this._zoom(this._scale);

            this._isMoving = true;

        }.bind(this), 100);

        var i, eh, len = elaticHovers.length;
        for(i = 0; i < len; i ++)
        {
            eh = elaticHovers[i];
            if(eh && eh !== this)
            {
                eh.stop();
            }
        }
    }

    p._onOut = function()
    {
        //
    }

    p._onMoving = function(evt)
    {
        var mx = evt.pageX, my = evt.pageY;
        var percentX = (mx - this._rect.x) / this._rect.w;
        var percentY = (my - this._rect.y) / this._rect.h;

        var x = percentX * 2 * this._offset - this._offset;
        var y = percentY * 2 * this._offset - this._offset;

        if(x < -this._offset || x > this._offset || y < -this._offset || y > this._offset)
        {
            this.stop();
        }
        else
        {
            if(this._direct === 'x')
            {
                y = 0;
            }
            else if(this._direct === 'y')
            {
                x = 0;
            }

            this._render(x, y, this._tweenSpeed, this._tweenEase);
        }
    }

    p._zoom = function(value)
    {
        if(this._scale !== 1)
        {
            createjs.Tween.get(
                this._scaleObj,
                {
                    override:true,
                    onChange:this._onAniUpdate.bind(this)
                }
            ).to(
                {
                    scale: value,
                },
                this._scaleSpeed,
                createjs.Ease.cubicOut
            );
        }
    }

    p.stop = function()
    {
        if(!this._isMoving)
        {
            return;
        }

        this._isMoving = false;

        this._render(0, 0, this._backSpeed, this._backEase);

        this._zoom(1);

        $(win).off('mousemove', this._onMovingHandler);
    }

    p._render = function(x, y, speed, ease)
    {
        createjs.Tween.get(
            this,
            {
                override:true,
                onChange:this._onAniUpdate.bind(this)
            }
        ).to(
            {
                _aniValueX: x,
                _aniValueY: y
            },
            speed,
            createjs.Ease[ease] || createjs.Ease.cubicOut
        );
    }

    p._onAniUpdate = function(evt)
    {
        this.$target.css('transform', 'translate3d(' + this._aniValueX + 'px,' + this._aniValueY + 'px, 0) scale(' + this._scaleObj.scale + ')');
    }

    p.execute = function(funcName, funcParams)
    {
        if(!funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return;
        }

        if(!Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) === 'function')
        {
            return f.apply(this, funcParams);
        }
    }

    p.dispose = function()
    {
        if(this._disposed)
        {
            return;
        }

        var index = elaticHovers.indexOf(this);
        if(index > -1)
        {
            elaticHovers.splice(index, 1);
        }

        this._disposed = true;

        createjs.Tween.removeTweens(this);
        $(win).off('mousemove', this._onMovingHandler);
        this.$element.off('mouseenter mouseleave');

        this._onMovingHandler = null;

        this._element.__ptElasticHover = null;
    }

    $.fn.extend({
        ptElasticHover:function (obj) {
            var isDevice = /Mobi|Tablet|iPad|iPhone|Android/i.test(navigator.userAgent);
            if(isDevice || win.PT_REMOVE_ELASTIC_HOVER)
            {
                return this;
            }

            this.each(function(){
                var eh = this.__ptElasticHover;
                eh && eh.dispose();
                new ElasticHover(this, obj || {});
            });
            return this;
        },
        ptElasticHoverCall: function(funcName, funcParams){
            var arr = [];
            this.each(function(){
                var eh = this.__ptElasticHover;
                var returnValue = eh && eh.execute(funcName, funcParams);
                arr.push({target:this, value:returnValue});
            });
            return arr;
        }
    });
})(jQuery, window);


/**
 * Created by qiu on 2018/01/05.
 */
!(function($, win)
{
    "use strict";

    var AudioPlayer = function(element, data)
    {
        this._element = element;
        this._element.__ptAudioPlayer = this;

        var $element = $(this._element);
        this.$element = $element;

        this._srcs = (this.$element.data('src') || '').split(',');
        this._autoplay = this.$element.data('autoplay') || false;

        this._index = 0;
        this._total = this._srcs.length;

        this._seek = 0;

        this._sound = null;
        this._soundId = '';

        this._isPaused = false;

        // for chrome autoplay policy changes.
        this._needGesture = true;

        this._volume = parseFloat(this.$element.data('volume') || data.volume || win.PT_AUDIO_PLAYER_VOLUME || 1);

        this._allowCalls = ['play', 'stop', 'pause', 'resume', 'next', 'prev', 'mute', 'unMute', 'dispose'];

        this._fadeDuration = data.fadeDuration === undefined ? 500 : data.fadeDuration;
        this._storeStatus = data.storeStatus === undefined ? true : data.storeStatus;

        this._isMuted = false;

        this._muteClass = data.muteClass || 'pt-audio-player-muted';
        this._storeKey = 'pt_audio_player_muted';

        this._onUnloadHandler = this._onUnload.bind(this);

        this.init();
    }

    var p = AudioPlayer.prototype;

    p.init = function()
    {
        if(this._storeStatus)
        {
            var ls = window.localStorage;
            if(ls)
            {
                var isMuted = ls.getItem(this._storeKey);

                if(isMuted === null || isMuted === undefined)
                {
                    if(this._autoplay)
                    {
                        this._isMuted = false;
                    }
                    else
                    {
                        this._isMuted = true;
                    }
                }
                else
                {
                    this._isMuted = isMuted === '1';
                }

                var playerParamsStr = ls.getItem('pt_audio_player_params');
                if(playerParamsStr)
                {
                    var playerParams = null;

                    try
                    {
                        playerParams = JSON.parse(playerParamsStr);
                    }
                    catch(e)
                    {
                        //
                    }
                    
                    if(playerParams)
                    {
                        this._index = playerParams.index || 0;
                        this._seek = playerParams.seek || 0;
                    }
                }
            }
        }
        else
        {
            if(this._autoplay)
            {
                this._isMuted = false;
            }
            else
            {
                this._isMuted = true;
            }
        }

        
        if(!this._isMuted)
        {
            this.play(this._index, this._seek);
        }
        else
        {
            $('body').addClass(this._muteClass + ' ' + 'pt-audio-player-store-muted');
        }

        $(win).on('unload', this._onUnloadHandler);
    }

    p._onUnload = function(evt)
    {
        if(!this._storeStatus || !this._sound)
        {
            return;
        }

        var ls = window.localStorage;
        if(ls)
        {
            var params = {
                seek: this._sound.seek(),
                index: this._index
            };

            ls.setItem('pt_audio_player_params', JSON.stringify(params));
        }
    }

    p.mute = function()
    {
        if(this._sound)
        {
            this.pause();
        }

        this._isMuted = true;

        if(this._storeStatus)
        {
            var ls = window.localStorage;
            if(ls)
            {
                ls.setItem(this._storeKey, '1');
            }
        }

        $('body').addClass(this._muteClass);
    }

    p.unMute = function()
    {
        if(this._sound)
        {
            this.resume();
        }
        else
        {
            this.play(this._index);
        }

        this._isMuted = true;

        if(this._storeStatus)
        {
            var ls = window.localStorage;
            if(ls)
            {
                ls.setItem(this._storeKey, '0');
            }
        }
        
        $('body').removeClass(this._muteClass);
    }

    p.prev = function()
    {
        if(this._total < 2)
        {
            return;
        }

        this._index --;
        if(this._index < 0)
        {
            this._index = this._total - 1;
        }

        this.play(this._index);
    }

    p.next = function()
    {
        if(this._total < 2)
        {
            return;
        }

        this._index ++;
        if(this._index >= this._total)
        {
            this._index = 0;
        }

        this.play(this._index);
    }

    p.play = function(index, seek)
    {
        var self = this;
        var url = this._srcs[index || 0];

        if(!url)
        {
            return;
        }

        this._index = index;

        this.stop(true);

        var sound = new Howl({
            src: [url],
            volume: this._volume,
            html5: true
        });

        this._sound = sound;

        if(this._needGesture)
        {
            sound.once('playerror', function(err){
                var func = function(evt){
                    self._isPaused = true;
                    self.resume();

                    self._needGesture = false;

                    win.removeEventListener('click', func, false);
                };

                win.addEventListener('click', func, false);
            });
        }

        sound.once('load', function(){
            self._soundId = sound.play();
            
            if(seek !== undefined && seek > 0)
            {
                setTimeout(function(){
                    sound.seek(seek);
                }, 0);
            }
        });

        sound.once('end', function(){
            self._onPlayEnd();
        });
    }

    p._onPlayEnd = function()
    {
        if(this._total > 1)
        {
            this.next();
        }
        else if(this._total === 1)
        {
            var self = this;
            setTimeout(function(){
                self.play(self._index);
            }, 200);
        }
    }

    p.stop = function(noFade)
    {
        var self = this;

        if(!noFade)
        {
            if(this._sound)
            {
                this._sound.fade(this._volume, 0, this._fadeDuration, this._soundId);
                setTimeout(function(){
                    self._sound.stop();
                    self._soundId = '';
                    self._isPaused = false;
                    self._sound = null;
                }, this._fadeDuration);
            }
            else
            {
                this._soundId = '';
                this._isPaused = false;
            }
        }
        else
        {
            if(this._sound)
            {
                this._sound.stop();
                this._sound = null;
            }

            this._soundId = '';
            this._isPaused = false;
        }
        
    }

    p.pause = function()
    {
        var self = this;
        if(this._sound)
        {
            this._sound.fade(this._volume, 0, this._fadeDuration, this._soundId);
            setTimeout(function(){
                self._sound.pause();
                self._isPaused = true;
            }, this._fadeDuration);
        }
    }

    p.resume = function()
    {
        if(this._sound && this._isPaused)
        {
            this._isPaused = false;
            this._sound.play();
            this._sound.fade(0, this._volume, this._fadeDuration, this._soundId);
        }
    }

    p.execute = function(funcName, funcParams)
    {
        if(!funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return;
        }

        if(!Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) === 'function')
        {
            return f.apply(this, funcParams);
        }
    }

    p.dispose = function()
    {
        if(this._disposed) return;
        this._disposed = true;

        this.stop(true);

        if(this._element)
        {
            this.$element.off('mouseenter mouseleave mousemove wheel');
            this._element.__ptAudioPlayer = null;
            this._element = null;
        }

        $(win).off('unload', this._onUnloadHandler);
    }

    $.fn.extend({
        ptAudioPlayer:function (obj) {
            this.each(function(){
                var scroller = this.__ptAudioPlayer;
                scroller && scroller.dispose();
                new AudioPlayer(this, obj || {});
            });
            return this;
        },
        ptAudioPlayerCall: function(funcName, funcParams){
            var arr = [];
            this.each(function(){
                var scroller = this.__ptAudioPlayer;
                var returnValue = scroller && scroller.execute(funcName, funcParams);
                arr.push({target:this, value:returnValue});
            });
            return arr;
        }
    });
}(jQuery, window));


/**
 * Created by foreverpinetree@gmail.com on 2018/03/01.
 */
!(function($, win)
{
    "use strict";

    var ElementParallax = function(element, obj)
    {
        this._element = element;
        this._element.__ptElementParallax = this;

        this.$element = $(element);

        if(obj.container)
        {
            this.$container = this.$element.closest(obj.container);
            if(!this.$container.get(0))
            {
                this.$container = this.$element.parent();
            }
        }
        else
        {
            this.$container = this.$element.parent();
        }

        this._offset = parseFloat(this.$element.data('offset') || 0) || 0;
        this._direct = this.$element.data('direct') == '1' ? 1 : 0;

        if(this._offset < 0)
        {
            this._offset = 0;
        }

        this._eleHeight = 0;
        this._offsetHeight = 0;

        this._onResizeHandler = null;
        this._onScrollHandler = null;

        this._init();
    }

    var p = ElementParallax.prototype;

    p._init = function()
    {
        var $win = $(win);

        this._onResizeHandler = this._onResize.bind(this);
        $win.on('resize', this._onResizeHandler);

        this._onScrollHandler = this._onScroll.bind(this);
        $win.on('scroll', this._onScrollHandler);

        this._onResize();
    }

    p._onResize = function(evt)
    {
        this._eleHeight = this._element.clientHeight;
        this._offsetHeight = Math.ceil(this._eleHeight * this._offset);

        this._onScroll();
    }

    p._onScroll = function (evt) 
    {
        var scrollTop = $(win).scrollTop(), offsetTop = this.$container.offset().top, winHeight = $(win).height();
        if(offsetTop + this._eleHeight > scrollTop && offsetTop < scrollTop + winHeight)
        {
            var y = 0, top, p;

            if(this._direct === 0)
            {
                top = scrollTop - offsetTop;
                p = top / winHeight;
                y = p * this._offsetHeight;
            }
            else
            {
                top = scrollTop - offsetTop;
                p = top / winHeight;
                y = -p * this._offsetHeight;
            }

            this._renderY(y);
        }
    }

    p._renderY = function (y)
    {
        y = Math.round(y);
        this.$element.css('transform', 'translate3d(0,' + y + 'px,0)');
    }

    p.resize = function()
    {
        this._onResize();
    }

    p.execute = function(funcName, funcParams)
    {
        if(funcName.indexOf('_') === 0)
        {
            return;
        }

        var f = this[funcName];
        if(f && typeof(f) == 'function')
        {
            f.apply(this, funcParams || []);
        }
    }

    p.dispose = function()
    {
        var $win = $(win);

        if(this._onScrollHandler)
        {
            $win.off('scroll', this._onScrollHandler);
            this._onScrollHandler = null;
        }

        if(this._onResizeHandler)
        {
            $win.off('resize', this._onResizeHandler);
            this._onResizeHandler = null;
        }

        if(this._element)
        {
            this._element.__ptElementParallax = null;
            this._element = null;
        }
    }

    $.fn.extend({
        ptElementParallax:function (obj) {
            var ua = navigator.userAgent;
            var isDevice = /Mobi|Tablet|iPad|iPhone|Android/i.test(ua);
            var isIE = (!!window.ActiveXObject || "ActiveXObject" in window);
            var isIE11 = (ua.toLowerCase().indexOf("trident") > -1 && ua.indexOf("rv") > -1);

            if(isDevice || (isIE && !isIE11))
            {
                return this;
            }

            this.each(function(){
                var parallax = this.__ptElementParallax;
                parallax && parallax.dispose();

                new ElementParallax(this, obj || {});
            });
            return this;
        },
        ptElementParallaxCall: function(funcName, funcParams){
            this.each(function(){
                var parallax = this.__ptElementParallax;
                parallax && parallax.execute(funcName, funcParams);
            });
            return this;
        }
    });
})(jQuery, window);


/**
 * Created by foreverpinetree.com on 2018/03/29.
 */
!(function($)
{
    "use strict";

    var check = function($src, $targets, resultHolder, middleOffset)
    {
    	var rect1 = [
    		$src.offset().left,
    		$src.offset().top,
    		$src.outerWidth(),
	    	$src.outerHeight()
    	];

    	if(middleOffset)
    	{
    		rect1[3] /= 2
    	}

    	var isIntersected = false;

    	$targets.each(function(i, item){
    		var $item = $(item);
    		var rect2 = [
	    		$item.offset().left,
	    		$item.offset().top,
	    		$item.outerWidth(),
	    		$item.outerHeight()
	    	];

	    	var result = intersects(rect1, rect2);
	    	if(result)
	    	{
	    		isIntersected = true;

	    		if(Array.isArray(resultHolder))
	    		{
	    			resultHolder.push($item);
	    		}
	    	}
    	});

    	return isIntersected;
    }

    var intersects = function(rect1, rect2)
    {
        var dx = Math.abs(rect1[0] + rect1[2] / 2 - (rect2[0] + rect2[2] / 2));
        var dy = Math.abs(rect1[1] + rect1[3] / 2 - (rect2[1] + rect2[3] / 2));
        return dx + dx < rect1[2] + rect2[2] && dy + dy < rect1[3] + rect2[3];
    }

    $.fn.extend({
        ptCheckIntersects:function(target, middleOffset) {
        	var arr = [];
        	this.each(function(){
                check($(this), $(target), arr, middleOffset || middleOffset === undefined);
            });
            return arr;
        }
    });
}(jQuery));


/**
 * Created by qiu on 2017/10/08.
 */
!(function($, win)
{
    "use strict";

    var __bars = [];

    var Bar = function(element, data)
    {
    	__bars.push(this);

        this._element = element;
        this._element.__ptBar = this;

        var $element = $(this._element);
        this.$element = $element;

        this.$content = $element.find(data.contentTarget || '.content');
        this._content = this.$content.get(0);

        var $html = $('<div class="pt-bar-group">' + 
            '<div class="pt-bar"></div>' + 
            '</div>');
        $element.append($html);

        this.$barTarget = $html.find('.pt-bar');

        this.$barTarget.on('mousedown', this._onStartDrag.bind(this));

        this._oldMouseY = 0;
        this._currentY = 0;
        this._containerHeight = 0;
        this._scrollRectHeight = 0;
        this._barHeight = 0;
        this._contentHeight = 0;
        this._offsetHeight = 0;

        this._isSetMaxHeight = this.$element.css('max-height') !== 'none';

        this._disposed = false;

        this._isDragging = false;

        this._allowCalls = ['resize'];

        this._onResizeHandler = this._onResize.bind(this);
        this._onDraggingHandler = this._onDragging.bind(this);
        this._onStopDragHandler = this._onStopDrag.bind(this);

        $(win).on('resize', this._onResizeHandler);

        this.$content.on('scroll', this._onScroll.bind(this));

        this._onResize();
    }

    var p = Bar.prototype;

    p._onResize = function (evt)
    {
        if(this._isSetMaxHeight)
        {
            this.$element.css('height', 'auto');
            this.$element.height(this.$element.outerHeight());
        }

        this._barHeight = this.$barTarget.height();
        this._containerHeight = this.$element.height();
        this._scrollRectHeight = this._containerHeight - this._barHeight;
        this._contentHeight = this._content.scrollHeight;
        this._offsetHeight = this._contentHeight - this._containerHeight;

        if(this._offsetHeight <= 1)
        {
            this.$element.addClass('no-roll');
        }
        else
        {
            this.$element.removeClass('no-roll');
        }
    }

    p._onStartDrag = function (evt)
    {
        if(this._contentHeight <= this._containerHeight)
        {
            return;
        }

        this._isDragging = true;

        evt.preventDefault();
        evt.stopPropagation();

        var $win = $(win);
        $win.off('mousemove', this._onDraggingHandler).on('mousemove', this._onDraggingHandler);
        $win.off('mouseup', this._onStopDragHandler).on('mouseup', this._onStopDragHandler);

        this._oldMouseY = evt.pageY === undefined ? evt.clientY : evt.pageY;

        var scrollTop = this._content.scrollTop;
        var percent = scrollTop / this._offsetHeight;
        this._currentY = this._scrollRectHeight * percent;
    }

    p._onDragging = function (evt)
    {
        if(this._contentHeight <= this._containerHeight)
        {
            return;
        }

        evt.preventDefault();
        evt.stopPropagation();

        var mY = evt.pageY === undefined ? evt.clientY : evt.pageY;
        this._currentY += mY - this._oldMouseY;
        this._oldMouseY = mY;

        if(this._currentY < 0)
        {
            this._currentY = 0;
        }
        else if(this._currentY > this._scrollRectHeight)
        {
            this._currentY = this._scrollRectHeight;
        }

        var percent = this._currentY / this._scrollRectHeight;

        this.$barTarget.css('transform', 'translate3d(0, ' + this._currentY + 'px, 0)');

        this._content.scrollTop = percent * this._offsetHeight;
    }

    p._onStopDrag = function (evt)
    {
        evt.preventDefault();
        evt.stopPropagation();

        this._isDragging = false;

        var $win = $(win);
        $win.off('mousemove', this._onDraggingHandler);
        $win.off('mouseup', this._onStopDragHandler);
    }

    p._onScroll = function(evt)
    {
        if(this._isDragging) return;

        if(this._contentHeight <= this._containerHeight)
        {
            return;
        }

        var scrollTop = this._content.scrollTop;
        var percent = scrollTop / this._offsetHeight;
        var y = this._scrollRectHeight * percent;
        this.$barTarget.css('transform', 'translate3d(0, ' + y + 'px, 0)');
    }

    p.resize = function()
    {
    	if(this._disposed) return;
    	
        this._onResize();
    }

    p.execute = function(funcName, funcParams)
    {
        if(!funcName || this._allowCalls.indexOf(funcName) < 0)
        {
            return null;
        }

        if(!Array.isArray(funcParams))
        {
            funcParams = [funcParams];
        }

        var f = this[funcName];
        if(f && typeof(f) === 'function')
        {
            return f.apply(this, funcParams);
        }
        return null;
    }

    p.dispose = function()
    {
        if(this._disposed) return;
        this._disposed = true;

        var $win = $(win);

        if(this._onResizeHandler)
        {
            $win.off('resize', this._onResizeHandler);
            this._onResizeHandler = null;
        }

        if(this._element)
        {
            this.$element.off('mouseenter mouseleave mousemove wheel');
            this._element.__ptBar = null;
            this._element = null;
        }

        this.$barTarget.off('mousedown', this._onStartDragHandler);
        $win.off('mousemove', this._onDraggingHandler);
        $win.off('mouseup', this._onStopDragHandler);

        if(this.$content)
        {
            this.$content.off('scroll');
        }

        this._onStartDragHandler = null;
        this._onDraggingHandler = null;
        this._onStopDragHandler = null;

        var index = __bars.indexOf(this);
        if(index > -1)
        {
        	__bars.splice(index, 1);
        }
    }

    $.fn.extend({
        ptBar:function (obj) {
            this.each(function(){
                var bar = this.__ptBar;
                bar && bar.dispose();
                new Bar(this, obj);
            });
            return this;
        },
        ptBarCall: function(funcName, funcParams){
            var arr = [];
            this.each(function(){
                var bar = this.__ptBar;
                var returnValue = bar && bar.execute(funcName, funcParams);
                arr.push({target:this, value:returnValue});
            });
            return arr;
        }
    });

    win.addEventListener('load', function(evt){
    	__bars.forEach(function(bar){
    		bar && bar.resize();
    	});
    });
}(jQuery, window));