/**
 * @author   foreverpinetree@gmail.com
 * @profile  www.3theme.com
 * @version v1.0.2
 */

!(function($, win)
{
	"use strict";
	
	var ImageGroup = function(element, options)
	{
		this._element = element;
		this.$element = $(element);

        this.$parent = this.$element.parent();
        this.childIndex = this.$element.index();

		this._options = options;
		
		this.$images = this.get('.image-group-images');
		this.$categories = this.get('.image-group-categories');
		this.$extends = this.get('.image-group-item-edit-extend');

		this._imgListMedia = null;
		this._imgSingleMedia = null;
		
		this._categories = {};

		this._extends = [];
		this._extendEles = {};
		this._extendDefaults = {};

		this._enableKeyBoard = false;
		
		this._itemIndex = 0;
		
		this._currentEditId = '';
		
		this._element.__ptImageGroup = this;

		this._uncategorizedName = 'Uncategorized';
		
		this._onKeyUpHandler = this._onKeyUp.bind(this);
		
		this.init();
	};
	
	var p = ImageGroup.prototype;
	
	p.init = function()
	{
		this.$extends.children('.ig-extend-item').each(function(i, item){
			var $item = $(item);
			var key = $item.data('key');
			if(this._extends.indexOf(key) == -1)
			{
				this._extends.push(key);
			}
			var $target = $item.find('.ig-extend-target:not(div)');
			this._extendDefaults[key] = $target.data('default');
			this._extendEles[key] = item;

			var type = $item.data('type'), self = this;
			if(type === 'checkbox')
			{
				$item.on('change', function(evt){
					var value = self._getProperty($(this));
					var isChecked = parseInt(value) === 1;

					if(isChecked)
					{
						self.get('.image-group-item-edit').addClass('ig-vars-checked-' + key);
					}
					else
					{
						self.get('.image-group-item-edit').removeClass('ig-vars-checked-' + key);
					}
				});
			}
			else if(type === 'select')
			{
				$item.on('change', function(evt){
					var value = self._getProperty($(this));

					$(this).find('option').each(function(){
						var val = $(this).attr('value');
						self.get('.image-group-item-edit').removeClass('ig-vars-selected-' + key + '-' + val);
					});
					self.get('.image-group-item-edit').addClass('ig-vars-selected-'+ key + '-' + value);
				});
			}
		}.bind(this));

		this.get('.image-group-open-btn').on('click', function(evt){
			this.open();
		}.bind(this));
		
		this.get('.image-group-cancel-btn').on('click', function(evt){
			this.close(false);
		}.bind(this));

		this.get('.image-group-close-btn').on('click', function(evt){
			this.close(true);
		}.bind(this));
		
		this.get('.image-group-add-images').on('click', function(evt){
			this._openListMediaSelect();
		}.bind(this));
		
		this.get('.image-group-item-edit').on('click', function(evt){
			if($(evt.target).hasClass('image-group-item-edit'))
			{
				this.closeEdit();
			}
		}.bind(this));

		this.get('.image-group-item-edit-image-close').on('click', function(evt){
			this.closeEdit();
		}.bind(this));

		this.get('.image-group-item-edit .image-group-item-edit-image').on('dblclick', this._changeImage.bind(this));
		this.get('.image-group-item-edit-image-btn').on('click', this._changeImage.bind(this));

		this.get('.image-group-item-edit-image-prev').on('click', this._onPrevEdit.bind(this));
		this.get('.image-group-item-edit-image-next').on('click', this._onNextEdit.bind(this));

		//$(win).on('keyup', this._onKeyUpHandler);
		
		this.$images.sortable({
			items: '>.image-group-item',
			cursor: 'move',
            distance: 5
        });
		
		this.get('.image-group-items').sortable({
			items: '>.image-group-item',
			cursor: 'move',
			connectWith: '.image-group-items'
        });
		
		this.$categories.find('.image-group-details').sortable({
			items: '>.image-group-category',
			cursor: 'move',
            distance: 5
        });
		
		this.get('.image-group-add-category').on('click', function(evt){
			this._createCategory('', true);
		}.bind(this));
		
		this.get('.image-group-images-btn').on('click', function(evt){
			this.$images.show();
			this.$categories.hide();

			this.get('.image-group-header').addClass('image-group-image-mode').removeClass('image-group-category-mode');
			
			this.get('.image-group-add-category').hide();
			this.get('.image-group-add-images').show();

			this.get('.image-group-images-btn').addClass('current');
			this.get('.image-group-categories-btn').removeClass('current');
		}.bind(this));
		
		this.get('.image-group-categories-btn').on('click', function(evt){
			this.$images.hide();
			this.$categories.show();

			this.get('.image-group-header').removeClass('image-group-image-mode').addClass('image-group-category-mode');
			
			this.get('.image-group-add-images').hide();
			this.get('.image-group-add-category').show();

			this.get('.image-group-categories-btn').addClass('current');
			this.get('.image-group-images-btn').removeClass('current');
		}.bind(this));
	}

	p._setUI = function()
	{
		this._categories = {};
		this._itemIndex = 0;
		var catData = JSON.parse(this.get('.image-group-data').val() || '{}');
		var data = this._parseData(catData);

		this._clear();

		var cats = [];
		data.forEach(function(obj){
			var $item = this._createItem(obj, 'pt' + this._itemIndex, true);
			$item.addClass('big-item');
			this.$images.append($item);
			
			var $category = this._categories[obj.category];
			if(!$category)
			{
				if(obj.category == '*')
				{
					$category = this.$categories.find('.image-group-category-all');
					this._categories[obj.category] = $category;
				}
				else
				{
					$category = this._createCategory(obj.category, false);
					this._categories[obj.category] = $category;
					
					cats.push( {item:$category, index:obj.cat_index} );
				}
			}
			$item = this._createItem(obj, 'pt' + this._itemIndex, false);
			$category.find('.image-group-items').append($item);
			
			this._itemIndex ++;
		}, this);
		
		cats.sort(this._onSort);
		
		cats.forEach(function(obj){
			var $html = obj.item;
			this.get('.image-group-details').append($html);
		}, this);

		this.$categories.find('.image-group-category-all .image-group-input').val(this._uncategorizedName);

		if(data.length < 1)
		{
			this.$element.addClass('no-items');
		}
	}
	
	p._onKeyUp = function(evt)
	{
		if(this._enableKeyBoard)
		{
			switch(evt.keyCode)
			{
				case 37://left
					this._onPrevEdit();
					break;
				case 39://right
					this._onNextEdit();
					break;
			}
		}
	}

	p._createItem = function(obj, id, addControl)
	{
		var $html;
		if(addControl)
		{
			$html = $('<div data-rid="' + obj.rid + '" data-id="' + id + '" class="image-group-item">' +
			'<div class="image-group-item-buttons"><div class="image-group-item-edit-btn image-group-item-btn">' +
			'</div><div class="image-group-item-delete-btn image-group-item-btn"></div></div></div>');

			this._extends.forEach(function(key){
				var val = obj.variables[key];
				$html.data(key,val === undefined || val === null ? '' : val);
			});

			this._setImageByAttachmentId($html, obj.rid, 'thumbnail');
			
			$html.find('.image-group-item-edit-btn').on('click', function(evt){
				var $target = $(evt.currentTarget).parent().parent();
				var id = $target.data('id');
				var $item = this.$categories.find('.image-group-item[data-id="' + id + '"]');
				this.openEdit($item);
			}.bind(this));
			
			$html.find('.image-group-item-delete-btn').on('click', function(evt){
				var $target = $(evt.currentTarget).parent().parent();
				this._removeItem($target);
			}.bind(this));
			
			$html.on('dblclick', function(evt){
				if(evt.target != evt.currentTarget)
				{
					return;
				}
				
				var $target = $(evt.currentTarget);
				var id = $target.data('id');
				var $item = this.$categories.find('.image-group-item[data-id="' + id + '"]');
				this.openEdit($item);
			}.bind(this));
		}
		else
		{
			$html = $('<div data-rid="' + obj.rid + '" data-id="' + id + '" class="image-group-item"></div>');

			this._extends.forEach(function(key){
				var val = obj.variables[key];
				$html.data(key,val === undefined || val === null ? '' : val);
			});

			this._setImageByAttachmentId($html, obj.rid, 'thumbnail');
		}
		return $html;
	}
	
	p._createCategory = function(category, addToStage)
	{
		var $html = $( 
			'<div class="image-group-category">' +
				'<div class="image-group-category-header">' +
					'<input class="image-group-input" value="' + (category || '') + '" placeholder="Enter category"/>' +
					'<div class="image-group-category-delete"></div>' +
				'</div>' +
				'<div class="image-group-items"></div>' +
			'</div>'
		);
		
		if(addToStage)
		{
			this.get('.image-group-details').append($html);
		}
		
		$html.find('.image-group-items').sortable({
			items: '>.image-group-item',
			cursor: 'move',
			connectWith: '.image-group-items'
		});
		
		$html.find('.image-group-category-delete').on('click', function(evt){
			var $cat = $(evt.currentTarget).closest('.image-group-category');
			var $items = $cat.find('.image-group-items>.image-group-item');
			this.$categories.find('.image-group-category-all>.image-group-items').append($items);
			$cat.remove();
		}.bind(this));
		
		return $html;
	}

	p._clear = function()
	{
		this.$images.empty();

		this.$categories.find('.image-group-category-all>.image-group-items').empty();
		this.get('.image-group-details').empty();
	}
	
	p._parseData = function(data)
	{
		var cat, rids = [], indexes = [], i, len, objs = [], obj;
		for(var key in data)
		{
			if(key == 'uncated_name')
			{
				this._uncategorizedName = data[key];
				continue;
			}

			cat = data[key];
			if(!cat.rid) cat.rid = [];
			if(!cat.index) cat.index = [];
			if(!cat.variables) cat.variables = {};

			this._extends.forEach(function(extendKey){
				if(!cat.variables[extendKey]) cat.variables[extendKey] = [];
			});
			
			len = cat.rid.length;
			for(i = 0; i < len; i ++)
			{
				obj = {};
				
				obj.category = key;
				obj.cat_index = cat.cat_index || 0;
				obj.rid = cat.rid[i] || '';
				obj.index = cat.index[i] || 0;
				obj.variables = {};

				this._extends.forEach(function(extendKey){
					var value = cat.variables[extendKey][i];
					obj.variables[extendKey] = value !== undefined && value !== null ? value : this._extendDefaults[extendKey];
				}, this);
				
				objs.push(obj);
			}
		}
		
		objs.sort(this._onSort);
		
		return objs;
	}
	
	p._onSort = function(obj1, obj2)
	{
		return obj1.index - obj2.index;
	}
	
	p._getData = function()
	{
		var allImages = {}, index = 0;
		this.$images.find('.image-group-item').each(function(i, item){
			var id = $(item).data('id');
			allImages[id] = index;
			index ++;
		});
		
		var data = {}, cat, catIndex = 0;
		this.get('.image-group-category').each(function(i, catItem){
			var $catItem = $(catItem);
			var $catInput = $catItem.find('.image-group-input');
			if($catInput.hasClass('image-group-uncategorized'))
			{
				cat = '*';
			}
			else
			{
				cat = $catInput.val() || '*';
			}

			if(cat == '*')
			{
				data['uncated_name'] = $catInput.val();
			}
			
			if(!data[cat])
			{
				data[cat] = {rid:[], index:[], cat_index: catIndex, variables: {}};
				this._extends.forEach(function(key){
					data[cat].variables[key] = [];
				});

				catIndex ++;
			}
			
			var obj = data[cat];
			$catItem.find('.image-group-items>.image-group-item').each(function(j, item){
				var $item = $(item);

				var rid = $item.data('rid');
				var id = $item.data('id');
				
				obj.rid.push(rid);

				this._extends.forEach(function(key){
					var value = $item.data(key);
					obj.variables[key].push( value !== undefined && value !== null ? value : this._extendDefaults[key]);
				}, this);
				
				var index = allImages[id];
				if(index < 0)
				{
					index = 0;
				}
				obj.index.push(index);
			}.bind(this));
		}.bind(this));
		
		return JSON.stringify(data);
	}

	p._setImageByAttachmentId = function($target, rid, sizeType)
	{
		var sizes = ['thumbnail', 'medium', 'large', 'full'];
		if(sizes.indexOf(sizeType) == -1)
		{
			sizeType = 'full';
		}

		var attachment = wp.media.attachment(rid);
		var index = sizes.indexOf(sizeType);

		$target.addClass('is-loading');
		
		if(attachment.get('url'))
		{
			$target.removeClass('is-loading');

			var sizeObj = attachment.get('sizes');
			if(sizeObj)
			{
				var obj;
				while( !(obj = sizeObj[sizeType]) )
				{
					index ++;
					if(index >= sizes.length)
					{
						break;
					}
					sizeType = sizes[index];
				}
				
				if(obj && obj.url)
				{
					$target.css('background-image', 'url(' + obj.url + ')');
					return;
				}
			}

			$target.css('background-image', 'url(' + attachment.get('url') + ')');
			return;
		}

		attachment.fetch({
			success:function(attachment){
				$target.removeClass('is-loading');

				var sizeObj = attachment.get('sizes');
				if(sizeObj)
				{
					var obj;
					while( !(obj = sizeObj[sizeType]) )
					{
						index ++;
						if(index >= sizes.length)
						{
							break;
						}
						sizeType = sizes[index];
					}
					
					if(obj && obj.url)
					{
						$target.css('background-image', 'url(' + obj.url + ')');
						return;
					}
				}

				$target.css('background-image', 'url(' + attachment.get('url') + ')');
			}
		});
	}

	p._openListMediaSelect = function()
	{
		if(!this._imgListMedia)
		{
			this._imgListMedia = win.wp.media( {multiple:true} );
			this._imgListMedia.on('select', function(){
				var state = this._imgListMedia.state();
			    var selection = state.get('selection');
			    if ( ! selection ) return;

			    var rids = '', arr = [];
			    selection.each(function(attachment) {
			    	var rid = attachment.attributes.id;
			        arr.push(rid);
			    });

			    rids = arr.join(',');
				
				var winTop = $(win).scrollTop();
				setTimeout(function(){
					$(win).scrollTop(winTop);
				}, 0); 

				this._addItems(rids);

				this.$element.removeClass('no-items');
				   
			}.bind(this));
		}
		
		this._imgListMedia.open();

		return false;
	}

	p._openSingleMediaSelect = function()
	{
		if(!this._imgSingleMedia)
		{
			this._imgSingleMedia = win.wp.media( {multiple:false} );
			this._imgSingleMedia.on('select', function(){
				var state = this._imgSingleMedia.state();
			    var selection = state.get('selection');
			    if ( ! selection || !selection.models || selection.models.length < 1 ) return;

			    var rid = selection.models[0].id;
				
				var winTop = $(win).scrollTop();
				setTimeout(function(){
					$(win).scrollTop(winTop);
				}, 0);

				var $item = this.$images.find('.image-group-item[data-id="' + this._currentEditId + '"]');
				var $catItem = this.$categories.find('.image-group-item[data-id="' + this._currentEditId + '"]');

				$item.data('rid', rid);
				$catItem.data('rid', rid);

				this._setImageByAttachmentId($item, rid, 'thumbnail');
				this._setImageByAttachmentId($catItem, rid, 'thumbnail');

				var $detailImage = this.get('.image-group-item-edit .image-group-item-edit-image');
				$detailImage.css('background-image', '');
				this._setImageByAttachmentId($detailImage, rid, 'large');

				this._enableKeyBoard = true;
			}.bind(this)).on('escape', function(evt){
				this._enableKeyBoard = true;
			}.bind(this));
		}
		
		this._imgSingleMedia.open();

		this._enableKeyBoard = false;

		return false;
	}
	
	p._addItems = function(rids)
	{
		var arr = rids.split(',');
		arr.forEach(function(rid){
			var obj = {
				rid: rid,
				variables: {}
			};

			this._extends.forEach(function(key){
				obj.variables[key] = this._extendDefaults[key];
			}, this);

			var $item = this._createItem(obj, 'pt' + this._itemIndex, true);
			$item.addClass('big-item');
			this.$images.append($item);
			
			var $category = this.$categories.find('.image-group-category-all');
			$item = this._createItem(obj, 'pt' + this._itemIndex, false);
			$category.find('.image-group-items').append($item);
			
			this._itemIndex ++;
		}, this);
	}
	
	p._removeItem = function($item)
	{
		var id = $item.data('id');
		var $catItem = this.$categories.find('.image-group-item[data-id="' + id + '"]');

		$item.removeClass('is-loading');
		
		$item.remove();
		$catItem.remove();

		var count = this.$images.find('.image-group-item').length;
		if(count < 1)
		{
			this.$element.addClass('no-items');
		}
	}
	
	p._save = function()
	{
		var $item = this.$categories.find('.image-group-item[data-id="' + this._currentEditId + '"]'), $groupEditItem;
		this._extends.forEach(function(key){
			$groupEditItem = $(this._extendEles[key]);
			$item.data(key, this._getProperty($groupEditItem));
		}, this);
	}

	p._getProperty = function($groupItem)
	{
		var type = $groupItem.data('type');
		var $target = $groupItem.find('.ig-extend-target:not(div)');
		var value = '';

		switch(type)
		{
			case 'text':
				value = $target.val();
				break;
			case 'textarea':
				value = $target.val();
				break;
			case 'checkbox':
				value = $target.prop('checked') ? 1 : 0;
				break;
			case 'color':
				value = $target.val();
				break;
			case 'select':
				value = $target.val();
				break;
		}

		return value;
	}

	p._setProperty = function($groupItem, value, key)
	{
		var type = $groupItem.data('type');
		var $target = $groupItem.find('.ig-extend-target:not(div)');

		switch(type)
		{
			case 'text':
				$target.val(value);
				break;
			case 'textarea':
				$target.val(value);
				break;
			case 'checkbox':
				var isChecked = parseInt(value) === 1;
				$target.prop('checked', isChecked);

				if(isChecked)
				{
					this.get('.image-group-item-edit').addClass('ig-vars-checked-' + key);
				}
				else
				{
					this.get('.image-group-item-edit').removeClass('ig-vars-checked-' + key);
				}
				break;
			case 'color':
				$target.wpColorPicker('color', value); 
				break;
			case 'select':
				if($target.hasClass('is-pt-select'))
				{
					$target.ptSelectCall('setValue', value);
				}
				else
				{
					$target.val(value);
				}

				$target.find('option').each(function(i, item){
					var val = $(item).attr('value');
					this.get('.image-group-item-edit').removeClass('ig-vars-selected-' + key + '-' + val);
				}.bind(this));
				this.get('.image-group-item-edit').addClass('ig-vars-selected-' + key + '-' + value);
				break;
		}
	}
	
	p.openEdit = function($item)
	{
		var id = $item.data('id');
		var rid = $item.data('rid');

		this._currentEditId = id;

		this._updatePNState();

		var $target = this.get('.image-group-item-edit .image-group-item-edit-image');
		$target.css('background-image', '');
		this._setImageByAttachmentId($target, rid, 'large');

		this._extends.forEach(function(key){
			var $groupEditItem = $(this._extendEles[key]);
			var value = $item.data(key);
			this._setProperty($groupEditItem,  value !== undefined && value !== null ? value : this._extendDefaults[key], key);
		}, this);
		
		this.get('.image-group-item-edit').addClass('open');

		this._enableKeyBoard = true;
	}
	
	p.closeEdit = function()
	{
		this._save();
		
		this.get('.image-group-item-edit').removeClass('open');

		this._enableKeyBoard = false;
	}

	p._onPrevEdit = function(evt)
	{
		if(this.get('.image-group-item-edit-image-prev').hasClass('pn-btn-hide'))
		{
			return;
		}

		var obj = this._getIndexDataById(this._currentEditId);
		var index = obj.index - 1;
		if(index >= 0)
		{
			this._save();

			var id = $(this.$images.find('.image-group-item').get(index)).data('id');
			var $item = this.$categories.find('.image-group-item[data-id="' + id + '"]');
			this.openEdit($item);
		}
	}

	p._onNextEdit = function(evt)
	{
		if(this.get('.image-group-item-edit-image-next').hasClass('pn-btn-hide'))
		{
			return;
		}

		var obj = this._getIndexDataById(this._currentEditId);
		var index = obj.index + 1;
		if(index < obj.total)
		{
			this._save();

			var id = $(this.$images.find('.image-group-item').get(index)).data('id');
			var $item = this.$categories.find('.image-group-item[data-id="' + id + '"]');
			this.openEdit($item);
		}
	}

	p._updatePNState = function()
	{
		var obj = this._getIndexDataById(this._currentEditId)
		var index = obj.index;
		var total = obj.total;
		var $prevBtn = this.get('.image-group-item-edit-image-prev');
		var $nextBtn = this.get('.image-group-item-edit-image-next');

		if(total <= 1)
		{
			$prevBtn.addClass('pn-btn-hide');
			$nextBtn.addClass('pn-btn-hide');
			return;
		}

		if(index - 1 < 0)
		{
			$prevBtn.addClass('pn-btn-hide');
		}
		else
		{
			$prevBtn.removeClass('pn-btn-hide');
		}

		if(index + 1 > total - 1)
		{
			$nextBtn.addClass('pn-btn-hide');
		}
		else
		{
			$nextBtn.removeClass('pn-btn-hide');
		}
	}

	p._getIndexDataById = function(id)
	{
		var images = this.$images.find('.image-group-item');
		var len = images.length, index = -1;
		for(var i = 0; i < len; i ++)
		{
			if($(images[i]).data('id') == this._currentEditId)
			{
				index = i;
				break;
			}
		}

		return {index: index, total: len};
	}

	p.open = function()
	{
        $('body').append(this.$element);

		$('body').addClass('image-group-open');
		this.get('.image-group-content').css('display', 'block');

		this.get('.image-group-images-btn').trigger('click');

		this._setUI();
	}
	
	p.close = function(needSave)
	{
		if(needSave === undefined)
		{
			needSave = true;
		}

		if(needSave)
		{
			var dataString = this._getData();
			this.get('.image-group-data').val(dataString);
		}

        if(this.childIndex !== undefined)
        {
            var prevChildIndex = this.childIndex - 1;
            if(prevChildIndex < 0)
            {
                this.$parent.prepend(this.$element);
            }
            else
            {
                var $preChild = this.$parent.children().eq(prevChildIndex);
                this.$element.insertAfter($preChild);
            }
        }
        else
        {
            this.$parent.append(this.$element);
        }

		$('body').removeClass('image-group-open');
		this.get('.image-group-content').hide();
	}

	p._changeImage = function(evt)
	{
		this._openSingleMediaSelect();
	}
	
	p.get = function(vars)
	{
		return this.$element.find(vars);
	}
	
	p.dispose = function()
	{
		$(win).off('keyup', this._onKeyUpHandler);

		if(this._element)
		{
			this._element.__ptImageGroup = null;
			this._element = null;
		}
	}
	
	$.fn.extend({
        ptImageGroup:function (obj) {
            this.each(function(){
            	var ig = this.__ptImageGroup;
            	ig && ig.dispose();

                new ImageGroup(this, obj || {});
            });
            return this;
        },
        ptImageGroupDispose:function () {
            this.each(function(){
				var ig = this.__ptImageGroup;
            	ig && ig.dispose();
            });
            return this;
        }
    });
})(jQuery, window);