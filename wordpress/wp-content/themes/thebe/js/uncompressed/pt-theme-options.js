/**
 * @author   foreverpinetree@gmail.com
 * @profile  www.3theme.com
 */
 
!(function($) {

    "use strict";

    var currentTabID = '';

    window.__isThemeOpion = true;

    $.organicTabs = function(el, options) {
        var base = this;
        base.$el = $(el);
        base.init = function() {
            base.options = $.extend({},$.organicTabs.defaultOptions, options);
            base.$nav = base.$el.find(base.options.headingsSelector);
            base.$nav.delegate("li a", "click", function() {
                var listID = $(this).attr("href").substring(1);
                var dataID = base.$el.data('id') || '';
				dataID = window.__pt_theme_name + '-' + dataID;

                base.changeTo(listID, dataID);
                return false;
            });
        };
        base.init();
        el.organicTabs = base;
		
		var ls = window.localStorage;
		if(ls)
		{
			var dataID = base.$el.data('id') || '';
			dataID = window.__pt_theme_name + '-' + dataID;
			var tabID = ls.getItem(dataID + '_tabID') || '';
            if(tabID)
			{
                currentTabID = tabID;
                
                base.changeTo(tabID, dataID);
            }
		}
    };
    $.organicTabs.prototype.changeTo = function(tabID, dataID) {
		var ls = window.localStorage;
		if(ls)
		{
			dataID = dataID || '';
			ls.setItem(dataID + '_tabID', tabID);
		}

        if(tabID == '13')
        {
            $('.pt-theme-options .pt-btn>input[type="submit"]').hide();
        }
        else
        {
            $('.pt-theme-options .pt-btn>input[type="submit"]').show();
        }
		
        var self = this;
        var curList = self.currentTab();
        var $newList = self.$el.find(self.options.headingsSelector + " a[href='#" + tabID + "']");
        var $allListWrap = self.$el.find(self.options.contentsSelector);
        var curListHeight = $allListWrap.outerHeight(true);
        $allListWrap.height(curListHeight);

        if ((curList.length > 0) && (tabID.length > 0) && (tabID != curList) && ( self.$el.find(":animated").length === 0))
        {
            self.$el.find(self.options.contentsSelector + " #" + curList).fadeOut(self.options.fadingSpeed, self.options.fadingEasing, function() {
                var $listEl = self.$el.find(self.options.contentsSelector + " #" + tabID);
                $listEl.fadeIn(self.options.fadingSpeed, self.options.fadingEasing);
                var newHeight = $listEl.outerHeight(true);
                if(self.options.updateAlong !== null) {
                    $(self.options.updateAlong).each(function(index, el) {
                        $(el).animate({
                            height: $(el).height() - curListHeight + newHeight
                        }, self.options.sizingSpeed,self.options.sizingEasing);
                    });
                }
                $allListWrap.animate({
                    height: newHeight
                }, self.options.sizingSpeed, self.options.sizingEasing, function() {
                    self.$el.triggerHandler("organicTabs.changed", [tabID, $listEl]);
                });

                self.$el.find(self.options.headingsSelector + " li a").removeClass("current");
                $newList.addClass("current");
            });
        }
    };

    $.organicTabs.prototype.currentTab = function() {
        return this.$el.find("a.current").attr("href").substring(1);
    }


    $.organicTabs.defaultOptions = {
        headingsSelector: ".pt-tabs-nav",      
        contentsSelector: ".list-wrap", 

        updateAlong: null, 
        fadingSpeed: 0, 
        fadingEasing: "swing",

        sizingSpeed: 0,
        sizingEasing: "swing" 
    };

    $.fn.organicTabs = function(args) {
        var organicArgs = Array.prototype.slice.call(arguments);
        if (typeof args !== "string")
        {
            return this.each(function() {
                new $.organicTabs(this, args);
            });
        }
        else
        {
            methodName = organicArgs.shift();
            return this[0].organicTabs[methodName].apply(this[0].organicTabs, organicArgs);
        }
    };
    
    $(function () {
        $('[data-organic-tabs="organic"]').each(function () {
            var $tabs = $(this);
            $tabs.organicTabs();
        });

        $('.pt-theme-options-loading').hide();
        $('.pt-theme-options').show();
        $('body').find(".pt-theme-options").organicTabs();
        $('body').find('.pt-wrap input.pt-checkbox').checkboxpicker();


        $('.nice-select').one('click',function(){
            var $this = $(this);
            setTimeout(function(){
                $this.find('ul.list').animate({scrollTop: $this.find('li.option.selected').get(0).offsetTop },400);
            },400)
        });

        $('.nice-select.fonts-list ul.list').before('<div class="list-index"></div>');

        var pt_A_Z = "";
        for(var i = 65; i < 91; i ++)
        {
            pt_A_Z = '<i>' + String.fromCharCode(i) + '</i>';
            $('.nice-select.fonts-list .list-index').append(pt_A_Z)
        }


        $('.nice-select.fonts-list .list-index i').on('click',function(evt){
            evt.preventDefault();
            evt.stopPropagation();

            var key = $(this).text(), $list = $(this).closest('.nice-select.fonts-list').find('ul.list');
            var $foundItem = null, isReady = false;
            $list.children('li').each(function(){
                var text = $(this).text().toUpperCase();
                if(text.indexOf('ABEEZEE') > -1)
                {
                    isReady = true;
                }

                if(isReady && !$foundItem && text.indexOf(key) === 0)
                {
                    $foundItem = $(this);
                }
            });

            if($foundItem)
            {
                $list.animate({scrollTop: $foundItem.get(0).offsetTop },400);
            }
        });


        $('.pt-fonts-preview').append('<div class="g-fonts-ctrl"><span class="prev"></span><span class="next"></span></div>');

        $('.g-fonts-ctrl span').on('click',function(){

            var $fontList = $(this).closest('.pt-fonts-preview').siblings('.nice-select.fonts-list');
            var fontsLen = $fontList.find('li').length - 1;
            var $LiActive = $fontList.find('li.selected');

            if ( $(this).hasClass('next')) {
                if ( $LiActive.index()< fontsLen) {
                    $LiActive.next('li').trigger('click')
                }
            }
            if ( $(this).hasClass('prev')) {
                if ( $LiActive.index() > 0 ) {
                    $LiActive.prev('li').trigger('click')
                };
            }
        });


        $('.nice-select.fonts-list li,.image-select-list li').on('click',function(){
             $(this).closest('.list-wrap').css('height','auto');
        });



        $('form#pt-theme-options').on('submit', function (e) {
            var data = {}, key, val;
            $('.pt-theme-option-item .theme-option-value').each(function(){
                key = $(this).attr('name');
                if( key )
                {
                    val = $(this).val();
                    data[key] = val;
                }
            });

            $(this).find('#store-target').val(JSON.stringify(data));
        });

        $('.pt-option-tips').each(function(){
            var $tips = $(this).find('.pt-tips');
            
            $(this).find('i.pt-tips-btn').on('click', function(evt){
                if($tips.children('img').length < 1)
                {
                    var id = $tips.data('img');
                    var url = window.__pt_root_url + '/data/images/other/'+ id + '.png';
                    $tips.append('<img src="' + url + '" alt=""/>');
                }

                $tips.addClass('is-show');
                $('body').addClass('pt-tips-opened');
            });

            $tips.on('click', function(evt){
                if($(evt.target).hasClass('pt-tips'))
                {
                    $(this).removeClass('is-show');
                    $('body').removeClass('pt-tips-opened');
                }
            });

            $tips.children('.pt-tips-close').on('click', function(evt){
                evt.stopPropagation();

                $(this).parent().removeClass('is-show');
                $('body').removeClass('pt-tips-opened');
            });
        });

        function pt_tab_title(){
            setTimeout(function(){
                var tab_name = $('.pt-tabs-nav li a.current').text();
                $('.pt-theme-options .pt-header p').text(tab_name + " Setting");
            },0);
        };
        pt_tab_title();
        $('.pt-tabs-nav li a').on('click', function() { pt_tab_title() });

        //import options
        $('.importUpdateOptions input').on('click', function(evt){
            $('#options-import-input').click();
        });

        $('#options-import-input').on('change', function(evt){
            $('#options-import-form').ajaxSubmit({
                type:"post",
                url:ajaxurl,
                data:{
                    action: 'import_options'
                },
                success:function(data){
                    var obj = JSON.parse(data);
                    var status = obj.status;

                    if(status == '1')
                    {
                        alert("Congratulations! Options updated!");
                        window.location.reload();
                    }
                    else if(status == '0')
                    {
                        alert("Error, please try again!");
                    }
                    else if(status == '-1')
                    {
                        alert("Error, no file uploaded!");
                    }
                    else if(status == '-2')
                    {
                        alert("Error, you don't have access to this page!");
                    }
                    else if(status == '-3')
                    {
                        alert("Error, verification failed!");
                    }

                    $('#options-import-input').val('');
                }
            });
        });

        //export options
        var optionsExportLimit = false;

        $('.exportOptions input').on('click', function(evt){
            if(optionsExportLimit)
            {
                alert('Don\'t export so offen! Just wait a few seconds!');
                return;
            }

            optionsExportLimit = true;
            setTimeout(function(){
                optionsExportLimit = false;
            }, 5000);

            $('#options-import-form').ajaxSubmit({
                type:"post",
                url:ajaxurl,
                data:{
                    action: 'export_options'
                },
                success:function(data){
                    var obj = JSON.parse(data);
                    var status = obj.status;

                    if(status == '1')
                    {
                        var url = obj.url;
                        if(url)
                        {
                            var a = document.createElement('a');
                            a.download = 'theme-options.json';
                            a.setAttribute('href', url);
                            a.click();
                        }
                    }
                    else if(status == '-2')
                    {
                        alert("Error, you don't have access to this page!");
                    }
                    else if(status == '-3')
                    {
                        alert("Error, fail to create ZIP file!");
                    }
                    else if(status == '-4')
                    {
                        alert('Don\'t export so offen! Just wait a few seconds!');
                    }
                }
            });
        });
    });


    // sub-tab-setting
    $(function(){
        var initArr = [];

        $('.pt-tabs-right .tabs-item').each(function(){

             if( typeof( $("dl",this).attr("data-tab-name") )!=="undefined") {
                $(this).addClass('has-sub-tab');

                var tabname1 = $('.sub-tab-1',this).attr('data-tab-name');
                var tabname2 = $('.sub-tab-2',this).attr('data-tab-name');
                var tabname3 = $('.sub-tab-3',this).attr('data-tab-name');
                var tabname4 = $('.sub-tab-4',this).attr('data-tab-name');

                if ( $('.sub-tab-3',this).length==0 && $('.sub-tab-2',this).length>0 ) {
                    $(this).prepend('<div class="sub-tab"><i class="on" data-id="tab-1">'+ tabname1 +'</i><i data-id="tab-2">'+tabname2+'</i></div>');
                };
                if ( $('.sub-tab-4',this).length==0 && $('.sub-tab-3',this).length>0 ) {
                    $(this).prepend('<div class="sub-tab"><i class="on" data-id="tab-1">'+ tabname1 +'</i><i data-id="tab-2">'+tabname2+'</i><i data-id="tab-3">'+tabname3+'</i></div>');
                };
                if ( $('.sub-tab-4',this).length > 0) {
                    $(this).prepend('<div class="sub-tab"><i class="on" data-id="tab-1">'+ tabname1 +'</i><i data-id="tab-2">'+tabname2+'</i><i data-id="tab-3">'+tabname3+'</i><i data-id="tab-4">'+tabname4+'</i></div>');
                }
                
             }

            var ls = window.localStorage;
            if(ls)
            {
                var dataID = $(this).closest('.pt-theme-options').data('id') || '';
                dataID = window.__pt_theme_name + '-' + dataID;

                var tabID = $(this).attr('id');
                if(tabID === currentTabID)
                {
                    var storeIndex = ls.getItem(dataID + '_' + tabID + '_subTabID');
                    if(storeIndex !== undefined)
                    {
                        var index = parseInt(storeIndex) || 0;
                        var btn = $(this).find('.sub-tab i').get(index);
                        if(btn)
                        {
                            initArr.push(btn);
                        }
                    }
                }
            }
        });

        $('.sub-tab i').on('click',function(){

            var index = $(this).index();
            var ls = window.localStorage;
            if(ls)
            {
                var dataID = $(this).closest('.pt-theme-options').data('id') || '';
                dataID = window.__pt_theme_name + '-' + dataID;
                var tabID = $(this).closest('.tabs-item').attr('id');
                ls.setItem(dataID + '_' + tabID + '_subTabID', index);
            }

            $(this).parent().find('i').removeClass('on');
            $(this).addClass('on');
            var sub_tab_active = $(this).data('id');
            $(this).closest('.has-sub-tab').find('dl').css('display','none');
            $(this).closest('.has-sub-tab').find('dl.sub-'+sub_tab_active).each(function(){
                var display = 'block', $this = $(this);
                if($this.hasClass('col-2') || $this.hasClass('col-3') || $this.hasClass('col-4'))
                {
                    display = 'inline-block';
                }
                $this.css('display', display);
            });

            var outerwrap_h = $(this).closest('.tabs-item').outerHeight(true);
            $(this).closest('.list-wrap').height(outerwrap_h);
        });

        initArr.forEach(function(btn){
            $(btn).trigger('click');
        });
    })

})(jQuery);

