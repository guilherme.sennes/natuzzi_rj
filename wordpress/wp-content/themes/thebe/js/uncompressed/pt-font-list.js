/**
 * @author   foreverpinetree@gmail.com
 * @profile  www.3theme.com
 */

!(function($)
{	
	"use strict";

	$(document).ready(function(){
		$('.fonts-setting-wrapper select.fonts-list').niceSelect();
		
		$('select.fonts-list').on('change', function(evt){
			var fontName = this.value;
			if(!fontName) return;
			
			var $wrapper = $(this.parentNode);
			var $checkList = $(this.parentNode).find('.fonts-checkbox-list');
			
			$wrapper.find('.fonts-checkbox').css('display', 'none').addClass('is-invisible');
			$wrapper.find('.fonts-checkbox>input').removeAttr('checked');
			
			if(fontName == 'None' || fontName == 'Default')
			{
				$wrapper.find('.pt-fonts-hidden-value').val('');
				$wrapper.find('.pt-fonts-preview').css('font-family', '').css('color', '#000000');
				return;
			}
			
			var fontsEnum = JSON.parse(window.__fonts_enum);
			var obj = fontsEnum[fontName];
			
			if(obj['specified'] != undefined)
			{
				fontName = obj['specified'];
			}
			
			var selectid = $(this).data('selectid');

			var storedFontName = '';
			var value = $wrapper.find('.pt-fonts-hidden-value').val();
			if(value)
			{
				var index = value.indexOf(':');
				if(index > -1)
				{
					storedFontName = value.substring(0, index);
					value = value.substring(index + 1);
				}
				index = value.indexOf('|');
				if(index > -1)
				{
					value = value.substring(0, index);
				}
				var arr = value.split(',');
				arr.map(function(str, i, arr){
					$checkList.find('.font-weight-' + str + '>input').prop('checked', true);
				});
			}

			var variants = obj['variants'];
			for(var k in variants)
			{
				$checkList.find('.fonts-checkbox.font-weight-' + variants[k]).css('display', 'inline-block').removeClass('is-invisible');

				if(storedFontName != fontName)
				{
					$checkList.find('.font-weight-100>input,.font-weight-400>input,.font-weight-700>input,.font-weight-900>input').prop('checked', true);
				}
			}
			
			$wrapper.find('.pt-fonts-preview').css('color', '#cccccc');
			
			var isLocalFont = this.value.indexOf(' - ') > -1;
			if(isLocalFont)
			{
				$wrapper.find('.pt-fonts-preview').css('font-family', fontName).css('color', '#000000');
			}
			else
			{
				var fontLink = fontName.replace(' ', '+');
				var head = document.getElementsByTagName("HEAD").item(0);
				var cssLink = document.createElement("link");
				cssLink.href = 'https://fonts.googleapis.com/css?family=' + fontLink;
				cssLink.type = "text/css";
				cssLink.className = 'font-family-link ' + selectid;
				cssLink.rel = "stylesheet";
				var className = '.font-family-link.' + selectid;
				cssLink.onload = function(evt){
					$(className).not(this).remove();
					var $wrapper = $('select[data-selectid="' + selectid + '"]').parent();
					$wrapper.find('.pt-fonts-preview').css('font-family', fontName).css('color', '#000000');
				}
				head.appendChild(cssLink);
			}
			
			var fallback = obj.fallback || '';
			
			var vars = [];
			$checkList.find('.fonts-checkbox:not(.is-invisible)>input').each(function(){
				if(this.checked)
				{
					vars.push(this.value);
				}
			});
			
			var data = $wrapper.find('select.fonts-list').val();
			if(vars.length > 0)
			{
				data += ':' + vars.join(',');
			}
			else
			{
				data += ':';
			}
			if(fallback)
			{
				data += '|' + fallback;
				$wrapper.data('fallback', fallback);
			}
			$wrapper.find('.pt-fonts-hidden-value').val(data);
		});
		
		$('.fonts-checkbox>input').on('change', function(evt){
			var $wrapper = $(this.parentNode.parentNode.parentNode);
			var $checkList = $(this.parentNode.parentNode);
			var vars = [];
			$checkList.find('.fonts-checkbox:not(.is-invisible)>input').each(function(){
				if(this.checked)
				{
					vars.push(this.value);
				}
			});
			
			var data = $wrapper.find('select.fonts-list').val();
			if(vars.length > 0)
			{
				data += ':' + vars.join(',');
			}
			else
			{
				data += ':';
			}
			var fallback = $wrapper.data('fallback');
			if(fallback)
			{
				data += '|' + fallback;
			}
			$wrapper.find('.pt-fonts-hidden-value').val(data);
		});
		
		$('select.fonts-list').trigger('change');
	});
}(jQuery));