/**
 * @author   foreverpinetree@gmail.com
 * @profile  www.3theme.com
 */

!(function($, win)
{
	"use strict";
	
	win.DEFAULT_TEMPLATE_TYPE = 'default';
	win.SHORTCODE_TEMPLATE_TYPE = 'shortcode';
	win.PORTFOLIO_TEMPLATE_TYPE = 'portfolio';
	win.GALLERY_TEMPLATE_TYPE = 'gallery';
	win.BLOG_TEMPLATE_TYPE = 'blog';
	win.CONTACT_TEMPLATE_TYPE = 'contact';
	win.LANDING_TEMPLATE_TYPE = 'landing';

	win.SHOP_TYPE = 'shop';

	win.pageSelectedType = '';

	var interests = [
		win.DEFAULT_TEMPLATE_TYPE,
		win.SHORTCODE_TEMPLATE_TYPE,
		//win.CONTACT_TEMPLATE_TYPE,
		win.PORTFOLIO_TEMPLATE_TYPE,
		//win.GALLERY_TEMPLATE_TYPE,
		win.BLOG_TEMPLATE_TYPE,
		win.LANDING_TEMPLATE_TYPE
		//win.SHOP_TYPE
    ];
    
    if(Array.isArray(win.__ptPostOptionInterests))
    {
        interests = interests.concat(win.__ptPostOptionInterests);
    }
		
	var checkPageTemplateSelect = function(name)
	{
		var type = '';
		var i = interests.length;
		while(i --)
		{
			if(name.indexOf(interests[i]) > -1)
			{
				type = interests[i];
				break;
			}
		}

		win.__admin_refresh_before && win.__admin_refresh_before(type);
		
		if(type)
		{
			win.pageSelectedType = type;

			$('.' + type + '_custom_page_box').each(function(){
				if($(this).hasClass('display-inline-block'))
				{
					$(this).css('display', 'inline-block');
				}
				else
				{
					$(this).show();
				}
			});
			$('.' + type + '_custom_page_input').removeAttr('disabled');
		}

		unselectPageTemplate(type, true);
		
		win.__admin_refresh_after && win.__admin_refresh_after(type);
		
		setTimeout(function(){
			$('.wp-picker-input-wrap>.color-field,.wp-picker-input-wrap>.wp-picker-clear').css('display', 'none');
		}, 100);
	};
	
	var unselectPageTemplate = function(except, isTemplate)
	{
		except = except.toLowerCase();

		var name = '', len = interests.length;
		for(var i = 0; i < len; i ++)
		{
			name = interests[i].toLowerCase();
			if(name != except)
			{
				$('.' + name + '_custom_page_box').hide();
				$('.' + name + '_custom_page_input').attr('disabled', 'disabled');

				$('body').removeClass('page-template-' + name);
			}
		}

		if(except && isTemplate)
		{
			$('body').addClass('page-template-' + except);
		}

		$(win).trigger('scroll');
	};

	$(win).on('DOMContentLoaded', function(){
		if(win.__ptWCObject && win.__ptWCObject.isAdminPage)
		{
			$('body').addClass('page-wc page-wc-' + win.__ptWCObject.pageType);

			unselectPageTemplate(win.__ptWCObject.pageType, false);
		}
		else
		{
			if(win.pageSelectedType)
			{
				unselectPageTemplate(win.pageSelectedType, true);
			}

			var $selects = $('#page_template');

			$selects.children('option').each(function(){
				var str = $(this).val() || 'default';
				var name = str.replace(/(page-)|(\.php)/ig, '').replace(/-/g, '');
				$(this).data('name', name);
			});
			
			$selects.on('change', function(){
				checkPageTemplateSelect($(this).children('option:selected').data('name'));
			});
			
			$selects.children('option:selected').each(function(){
				checkPageTemplateSelect($(this).data('name'));
			});
		}

		$(document).on('block-editor.page-template-changed', function(evt, pageTemplate){
			checkPageTemplateSelect(pageTemplate);
		});

		var initClassPrefix = 'init-page-template-';
		var initClasses = interests.map(function(className){
			return initClassPrefix + className;
		});

		var classIndex = initClasses.length, $body = $('body'), initClass;
		while(classIndex --)
		{
			initClass = initClasses[classIndex];
			if($body.hasClass(initClass))
			{
				$body.removeClass(initClass);
				checkPageTemplateSelect(initClass.substring(initClassPrefix.length));

				break;
			}
		}
		
		win.__pt_admin_ready && win.__pt_admin_ready();
    });
    
})(jQuery, window);