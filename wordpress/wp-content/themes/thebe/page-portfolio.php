<?php
/*
 * Template Name: Portfolio
 */
	thebe_prepare_setting(
		array(
			'type' => 'page',
			'template' => 'portfolio',
		)
	);

	global $thebe_global_site_vars;

	$thebe_use_ajax = thebe_get_page_setting_property( 'portfolio_use_ajax', '0' ) === '1';

	if( $thebe_use_ajax )
	{
		$thebe_global_site_vars['use_ajax'] = true;
	}

	$thebe_global_site_vars['ajax_fullscreen'] = thebe_get_page_setting_property( 'portfolio_fullscreen', '0' ) === '1';

	$thebe_portfolio_type = thebe_get_page_setting_property( 'portfolio_type', '1' );
	$thebe_portfolio_slider = false;

	if( $thebe_portfolio_type !== '7' )
	{
		$thebe_portfolio_slider = thebe_get_page_setting_property( 'portfolio_as_slider', '0' ) === '1';
		if( $thebe_portfolio_slider )
		{
			$thebe_global_site_vars['portfolio_slider'] = 'p-slider-mode';
		}
	}

	get_header();

	if ( post_password_required() )
	{
		?>
		<div class="page-password-required password-required-msg">
		<?php
			the_content();
		?>
		</div>
		<?php
	}
	else
	{

		thebe_output_page_title();

		require thebe_parse_part_path( '/template-parts/portfolio/content' );

		if( !$thebe_portfolio_slider )
		{
			thebe_output_page_shortcode();
		}
	}
	
	get_footer(); 
?>