<?php
	thebe_prepare_setting(
		array(
			'type' => 'page',
			'template' => 'default',
		)
	);

	get_header();

	if ( post_password_required() )
	{
		?>
		<div class="page-password-required password-required-msg">
		<?php
			the_content();
		?>
		</div>
		<?php
	}
	else
	{
		?>
		<div class="default-template-page">
			<div class="wrap">
			<?php
				while (have_posts())
				{
					the_post();

					?><h1 class="title"><?php the_title(); ?></h1><?php
					?><div class="text-area cf"><?php the_content(); ?></div><?php

					wp_link_pages('before=<div class="page-links">&after=</div>&next_or_number=next&previouspagelink='.esc_html__('Prev Page', 'thebe').'&nextpagelink='.esc_html__('Next Page', 'thebe'));
					
					$thebe_show_comment = thebe_get_theme_option('thebe_show_comment', '1');
					if ($thebe_show_comment != '0' && (comments_open() || get_comments_number())) {
						comments_template();
					}
				}
			?>
			</div>
		</div>
		<?php
	}
	
	get_footer(); 
?>