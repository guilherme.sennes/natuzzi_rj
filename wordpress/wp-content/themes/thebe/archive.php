<?php
	global $thebe_global_site_vars;
	if( !isset($thebe_global_site_vars) )
	{
		$thebe_global_site_vars = array();
	}

	$thebe_global_site_vars['use_widget'] = is_active_sidebar('sidebar-right');
	
	get_header(); 

	if( have_posts() )
	{
		get_template_part('template-parts/content', 'archive'); 
	}
	else
	{
		get_template_part('template-parts/content', 'none'); 
	}

	get_footer();
?>