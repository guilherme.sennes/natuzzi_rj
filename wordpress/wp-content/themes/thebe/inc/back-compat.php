<?php
function thebe_switch_theme() {
	switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );

	unset( $_GET['activated'] );

	add_action( 'admin_notices', 'thebe_upgrade_notice' );
}
add_action( 'after_switch_theme', 'thebe_switch_theme' );

function thebe_upgrade_notice() {
	$message = sprintf( esc_html__( 'thebe Theme requires at least WordPress version 4.8. You are running version %s. Please upgrade and try again.', 'thebe' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

function thebe_customize() {
	wp_die( sprintf( esc_html__( 'thebe Theme requires at least WordPress version 4.8. You are running version %s. Please upgrade and try again.', 'thebe' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'thebe_customize' );

function thebe_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( esc_html__( 'thebe Theme requires at least WordPress version 4.8. You are running version %s. Please upgrade and try again.', 'thebe' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'thebe_preview' );
