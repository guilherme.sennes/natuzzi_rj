<?php 
	if (!current_user_can('manage_options'))
	{
		wp_die(esc_html__("You don't have access to this page", 'thebe'));
	}
	
	require_once THEBE_THEME_DIR . '/inc/theme/options/theme.options.php';

	global $thebe_global_fonts_data;
	if(!isset($thebe_global_fonts_data) || $thebe_global_fonts_data == '')
	{
		global $wp_filesystem;
		WP_Filesystem();
		$thebe_fonts_path = THEBE_THEME_DIR . '/data/fonts_enum.json';
		$thebe_fonts_string = $wp_filesystem->get_contents($thebe_fonts_path);
		$thebe_global_fonts_data = apply_filters( 'thebe_custom_font_parse_data', json_decode($thebe_fonts_string) );
	}

	function thebe_theme_options_parse_fonts()
	{
		global $thebe_global_fonts_data;

		$fonts_enum = $thebe_global_fonts_data->fonts;

		$fonts_arr = array();
		foreach( $fonts_enum as $key=>$obj )
		{
			if( stripos( $key, 'custom font' ) === 0 )
			{
				array_splice( $fonts_arr, 1, 0, $key );
			}
			else
			{
				$fonts_arr[] = $key;
			}
		}

		return $fonts_arr;
	}

	global $thebe_global_fonts_names;
	$thebe_global_fonts_names = thebe_theme_options_parse_fonts();

    ?>

	<form method="post" id="options-import-form">
		<?php wp_nonce_field( 'thebe_options_import_action', 'thebe_options_import' );?>
		<input id="options-import-input" type="file" name="importFile" accept="application/json" />
	</form>
    <form method="post" action="options.php" id="pt-theme-options">
        <?php
			settings_fields('theme_options');
		?>
		<input type="hidden" id="store-target" name="<?php echo esc_attr( Thebe_Theme_Options::$options_key ); ?>" value="<?php echo esc_attr( get_option(Thebe_Theme_Options::$options_key, '') ); ?>"/>
		<h3 class="pt-theme-options-page-title"><?php esc_html_e("Theme Options", 'thebe'); ?></h3>
		<img class="pt-theme-options-loading" src="<?php echo esc_url(THEBE_THEME_URL.'/data/images/loading.gif'); ?>" alt="<?php esc_attr_e('Loading', 'thebe'); ?>"/>
        <div class="pt-theme-options pt-wrap cf" data-id="theme_option_container">
			<ul class="pt-tabs-nav">
			<?php
				$thebe_data_length = count( thebe_Theme_Options::$data );
				for($thebe_data_index = 0; $thebe_data_index < $thebe_data_length; $thebe_data_index ++)
				{
					$thebe_data_obj = thebe_Theme_Options::$data[$thebe_data_index];
					if($thebe_data_index == 0)
					{
						echo '<li class="nav-'.$thebe_data_index.'"><a href="#ts-tab-'.$thebe_data_index.'" class="current">'.esc_html($thebe_data_obj['title']).'</a></li>';
					}
					else
					{
						echo '<li class="nav-'.$thebe_data_index.'"><a href="#ts-tab-'.$thebe_data_index.'">'.esc_html($thebe_data_obj['title']).'</a></li>';
					}
				}
			?>
			</ul>
			<div class="pt-tabs-right">
				<div class="pt-header">
					<p class="pt-options-title"></p>
					<div class="pt-btn"><input type="submit" value="<?php esc_attr_e('Update', 'thebe'); ?>"></div>
				</div>
				<div class="list-wrap">
				<?php
					$thebe_data_length = count( thebe_Theme_Options::$data );
					for($thebe_data_index = 0; $thebe_data_index < $thebe_data_length; $thebe_data_index ++)
					{
						if($thebe_data_index == 0)
						{
							echo '<div id="ts-tab-'.$thebe_data_index.'" class="tabs-item">';
						}
						else
						{
							echo '<div id="ts-tab-'.$thebe_data_index.'" class="tabs-item hidden">';
						}
						
						$thebe_data_obj = thebe_Theme_Options::$data[$thebe_data_index];
						$thebe_data_array = $thebe_data_obj['data'];
						foreach($thebe_data_array as $thebe_data_value)
						{
							$thebe_data_key = thebe_Theme_Options::$prefix.$thebe_data_value['var'];
							thebe_generate_html($thebe_data_value, thebe_get_theme_option($thebe_data_key, NULL));
						}
						echo '</div>';
						
					}
				?>
				</div>
			</div>
		</div>
    </form>

<?php
	//function thebe_generate_html start
	function thebe_generate_html($data, $value)
	{
		$var = isset($data['var']) ? $data['var'] : '';
		$module_label = isset($data['label']) ? $data['label'] : $var;
		$placeholder = isset($data['placeholder']) ? $data['placeholder'] : '';
		$default = isset($data['default']) ? $data['default'] : '';
		$type = isset($data['type']) ? $data['type'] : '';
		$tab_name = isset($data['tab_name']) ? $data['tab_name'] : '';
		$des = isset($data['des']) ? $data['des'] : "";
		$des_span = $des != '' ? '<span>'.$des.'</span>' : '';
		$name = thebe_Theme_Options::$prefix.$var;
		$help = isset($data['help']) ? $data['help'] : '';
		
		$extend_class = ' '.$name;
		$extend_class .= isset($data['class']) ? ' '.$data['class'] : '';
		
		if($value === null)
		{
			$value = $default;
		}
		
		if($type == 'title')
		{
			?>
			<dl class="pt-theme-option-item pt-title<?php echo esc_attr($extend_class); ?>" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt><?php echo esc_html( $module_label ); ?></dt>
				<dd><?php thebe_kses_content( $des ); ?></dd>
			</dl>
			<?php
		}
		elseif($type == 'info')
		{
			?>
			<dl class="pt-theme-option-item pt-info<?php echo esc_attr($extend_class); ?>" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt><?php echo esc_html( $module_label ); ?></dt>
				<dd><?php thebe_kses_content( $des ); ?></dd>
			</dl>
			<?php
		}
		elseif($type == 'tips')
		{
			?>
			<dl class="pt-theme-option-item pt-option-tips<?php echo esc_attr($extend_class); ?>" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt><i class="pt-tips-btn"><?php echo esc_html($module_label); ?></i></dt>
				<dd>
					<div class="pt-tips" data-img="<?php echo esc_attr($name); ?>">
						<div class="pt-tips-close">
							<i class="fa fa-times" aria-hidden="true"></i>
						</div>
					</div>
				</dd>
			</dl>
			<?php
		}
		elseif($type == "file")
		{
			?>
			<dl class="pt-theme-option-item pt-theme-option-file<?php echo esc_attr($extend_class); ?>" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt></dt>
				<dd>
					<input class="theme-setting-file" type="file" />
					<?php if($help != '') :?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php endif;?>
				</dd>
			</dl>
			<?php
		}
		elseif($type == "button")
		{
			?>
			<dl class="pt-theme-option-item pt-theme-option-button<?php echo esc_attr($extend_class); ?>" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt></dt>
				<dd>
					<input class="theme-setting-button button-primary" type="button" value="<?php echo esc_attr($module_label); ?>" />
					<?php if($help != '') :?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php endif;?>
				</dd>
			</dl>
			<?php
		}
		elseif($type == "text")
		{
			?>
			<dl class="pt-theme-option-item<?php echo esc_attr($extend_class); ?>" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt><?php thebe_kses_content( $module_label.$des_span ); ?></dt>
				<dd>
					<input class="theme-setting-text theme-option-value" type="text" name="<?php echo esc_attr($name); ?>" value="<?php echo esc_attr($value); ?>" placeholder="<?php echo esc_attr($placeholder); ?>"/>
					<?php if($help != '') :?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php endif;?>
				</dd>
			</dl>
			<?php
		}
		elseif($type == "textarea")
		{
			$extend_var = '';
			if(isset($data['rows']))
			{
				$extend_var .= 'rows="'.esc_attr($data['rows']).'"';
			}
			if(isset($data['cols']))
			{
				$extend_var .= ' cols="'.esc_attr($data['cols']).'"';
			}

			?>
			<dl class="pt-theme-option-item<?php echo esc_attr($extend_class); ?>" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt><?php thebe_kses_content( $module_label.$des_span ); ?></dt>
				<dd>
					<textarea class="theme-setting-textarea theme-option-value" <?php thebe_kses_content( $extend_var ); ?> name="<?php echo esc_attr($name); ?>" placeholder="<?php echo esc_attr($placeholder); ?>"><?php echo esc_html($value); ?></textarea>
					<?php if($help != '') :?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php endif;?>
				</dd>
			</dl>
			<?php
		}
		elseif($type == "popup_textarea")
		{
			$extend_var = '';
			if(isset($data['rows']))
			{
				$extend_var .= 'rows="'.esc_attr($data['rows']).'"';
			}
			if(isset($data['cols']))
			{
				$extend_var .= ' cols="'.esc_attr($data['cols']).'"';
			}

			$btn_name = isset($data['btn_name']) ? $data['btn_name'] : __('Edit', 'thebe');
			$textarea_w = isset($data['w']) ? $data['w'] : 800;
			$textarea_h = isset($data['h']) ? $data['h'] : 500;

			?>
			<dl class="pt-theme-option-item<?php echo esc_attr($extend_class); ?>" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt><?php thebe_kses_content( $module_label.$des_span ); ?></dt>
				<dd>
					<div class="popup-textarea-open-btn button button-primary button-large"><?php echo esc_html($btn_name); ?></div>
					<div class="popup-textarea-container <?php echo esc_attr( 'popup_tc_'.$var ); ?>">
						<div class="popup-textarea-content" style="width:<?php echo esc_attr($textarea_w); ?>px;height:<?php echo esc_attr($textarea_h); ?>px;">
							<textarea class="theme-setting-textarea theme-option-value" <?php thebe_kses_content( $extend_var ); ?> name="<?php echo esc_attr($name); ?>" placeholder="<?php echo esc_attr($placeholder); ?>"><?php echo esc_html($value); ?></textarea>
							<div class="popup-textarea-close-btn"><span><?php esc_html_e('Finish', 'thebe'); ?></span></div>
						</div>
					</div>
					<?php if($help != '') :?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php endif;?>
				</dd>
			</dl>
			<?php
		}
		elseif($type == "code")
		{
			$extend_var = '';
			
			if(isset($data['editor']))
			{
				$extend_var .= 'data-editor="'.esc_attr($data['editor']).'"';
			}
			else
			{
				$extend_var .= 'data-editor="javascript"';
			}

			if(isset($data['height']))
			{
				$extend_var .= ' data-height="'.esc_attr($data['height']).'"';
			}
			
			?>
			<dl class="pt-theme-option-item<?php echo esc_attr($extend_class); ?>" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt><?php thebe_kses_content( $module_label.$des_span ); ?></dt>
				<dd>
					<input class="theme-option-code theme-option-value" type="hidden" <?php thebe_kses_content( $extend_var ); ?> name="<?php echo esc_attr($name); ?>" value="<?php echo esc_attr($value); ?>" />
					<?php if($help != '') :?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php endif;?>
				</dd>
			</dl>
			<?php
		}
		elseif($type == "select")
		{
			$choices = $data['options'];
			if($choices == NULL || !is_array($choices) || count($choices) < 1) return;
			
			$use_preview = isset($data['use_preview']) && $data['use_preview'] == true;
			$dropdown_class = $use_preview ? 'pt-select select-previewable ' : '';

			$only_available = isset($data['only_available']) ? $data['only_available'] : null;
			$available_value = $only_available != null ? json_encode($only_available) : '';
			?>
			<dl class="pt-theme-option-item<?php echo esc_attr($extend_class); ?>" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt><?php thebe_kses_content( $module_label.$des_span ); ?></dt>
				<dd>
					<select class="<?php echo esc_attr($dropdown_class); ?>theme-setting-select theme-option-value" name="<?php echo esc_attr($name); ?>" data-available="<?php echo esc_attr($available_value); ?>">
					<?php
						foreach ($choices as $choice_key=>$choice_value)
						{
							$preview_id = $var.'_'.$choice_key;
							$preview_value = $use_preview ? ' data-previewid="'.thebe_Theme_Options::$prefix.esc_attr($preview_id).'"' : '';
							if($choice_key == (string)$value)
							{
								echo '<option'.$preview_value.' class="theme-setting-option" selected="selected" value="'.esc_attr($choice_key).'">'.esc_html($choice_value).'</option>';
							}
							else
							{
								echo '<option'.$preview_value.' class="theme-setting-option" value="'.esc_attr($choice_key).'">'.esc_html($choice_value).'</option>';
							}
						}
					?>
					</select>
					<?php if($help != '') :?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php endif;?>
				</dd>
			</dl>
			<?php
		}
		elseif($type == "image_select")
		{
			$choices = $data['options'];
			if($choices == NULL || !is_array($choices) || count($choices) < 1) return;

			$only_available = isset($data['only_available']) ? $data['only_available'] : null;
			$available_value = $only_available != null ? json_encode($only_available) : '';

			?>
			<dl class="pt-theme-option-item<?php echo esc_attr($extend_class); ?> pt-image-select" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt><?php thebe_kses_content( $module_label.$des_span ); ?></dt>
				<dd>
					<input type="hidden" class="theme-option-value" name="<?php echo esc_attr($name); ?>" data-available="<?php echo esc_attr($available_value); ?>" value="<?php echo esc_attr($value); ?>"/>
					<ul class="image-select-list">
					<?php
						foreach ($choices as $choice_value)
						{
							$preview_id = $var.'_'.$choice_value;
							if($choice_value == (string)$value)
							{
								?><li class="selected" data-value="<?php echo esc_attr($choice_value); ?>" data-img-name="<?php echo esc_attr($preview_id); ?>"></li><?php
							}
							else
							{
								?><li data-value="<?php echo esc_attr($choice_value); ?>" data-img-name="<?php echo esc_attr($preview_id); ?>"></li><?php
							}
						}
					?>
					</ul>
					<?php if($help != '') :?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php endif;?>
				</dd>
			</dl>
			<?php
		}
		elseif($type == "checkbox")
		{
			$only_available = isset($data['only_available']) ? $data['only_available'] : null;
			$available_value = $only_available != null ? json_encode($only_available) : '';
			?>
			<dl class="pt-theme-option-item<?php echo esc_attr($extend_class); ?> checkbox-item" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt><?php thebe_kses_content( $module_label.$des_span ); ?></dt>
				<dd>
					<input type="hidden" class="theme-option-value" name="<?php echo esc_attr($name); ?>" value="<?php echo esc_attr($value); ?>" data-available="<?php echo esc_attr($available_value); ?>"/>
					<input class="theme-setting-check pt-checkbox" type="checkbox" <?php echo ($value == '1' ? 'checked' : ''); ?>/>
					<?php if($help != '') :?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php endif;?>
				</dd>
			</dl>
			<?php
		}
		elseif($type == "color")
		{
			?>
			<dl class="pt-theme-option-item<?php echo esc_attr($extend_class); ?>" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt><?php thebe_kses_content( $module_label.$des_span ); ?></dt>
				<dd>
					<input class="theme-setting-color color-field theme-option-value" data-alpha="true" data-custom-width="80px" type="text" name="<?php echo esc_attr($name); ?>" value="<?php echo esc_attr($value); ?>" placeholder="<?php echo esc_attr($placeholder); ?>"/>
					<?php if($help != '') :?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php endif;?>
				</dd>
			</dl>
			<?php
		}
		elseif($type == "image")
		{
			?>
			<dl class="pt-theme-option-item<?php echo esc_attr($extend_class); ?>" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt><?php echo esc_html( $module_label )?></dt>
				<dd>
					<input class="theme-setting-image image_box_uploader_input theme-option-value" type="hidden" name="<?php echo esc_attr($name); ?>" value="<?php echo esc_attr($value); ?>" />
					<div class="option-item-upload-button button-primary single-image-upload"><?php esc_html_e('Upload', 'thebe'); ?></div>
					<em><?php thebe_kses_content( $des ); ?></em>
					<?php if($help != '') :?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php endif;?>
					<?php
						$empty = "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";
						$img_url = $empty;
						if($value != '')
						{
							$img_obj = wp_get_attachment_image_src( $value, 'thumbnail' );
							if($img_obj)
							{
								$img_url = $img_obj[0];
							}
						}

						$img_extend_class = '';
						if( $img_url == $empty )
						{
							$img_extend_class = 'empty';
						}
					?>
					<div class="uploader-image-container single-image-input single-image-upload-delete <?php echo esc_attr($img_extend_class); ?>" style="display:<?php echo ($value == '' ? 'none' : 'block'); ?>">
						<span class="single-image-upload-delete-button upload-delete-button"></span>
						<img class="uploader-image-preview" alt="<?php esc_attr_e('Image preview', 'thebe'); ?>" src="<?php echo esc_attr($img_url); ?>" data-empty="<?php echo esc_attr($empty); ?>"/>
					</div>
				</dd>
			</dl>
			<?php
		}
		elseif($type == "font")
		{
			global $thebe_global_fonts_data;
			$font_weight_options_data = $thebe_global_fonts_data->fontWeightOptionData;

			global $thebe_global_fonts_names;
			$fonts_arr = $thebe_global_fonts_names;
			
			?>
			<dl class="pt-theme-option-item<?php echo esc_attr($extend_class); ?>" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt><?php echo esc_html( $module_label );?></dt>
				<dd>
					<div class="fonts-setting-wrapper">
						<div class="pt-fonts-preview" style="font-family:'Sansation';"><?php esc_html_e('Fonts-Preview FONTS-PREVIEW', 'thebe'); ?></div>
						<input type="hidden" class="pt-fonts-hidden-value theme-option-value" name="<?php echo esc_attr($name); ?>" value="<?php echo esc_attr($value); ?>" />
						<?php
							$pos = strpos($value, ':');
							$font_name = $value;
							if($pos !== false)
							{
								$font_name = substr($value, 0, $pos);
							}
						?>
						<select class="fonts-list" data-selectid="<?php echo esc_attr($name); ?>">

							<optgroup label="local Fonts">
							</optgroup>

							<optgroup label="loaded from Google Fonts" class="google-fonts-group">
								<?php
									foreach( $fonts_arr as $key )
									{
										if($key == $font_name)
										{
											echo '<option value="'.esc_attr( $key ).'" selected>'.esc_html( $key ).'</option>';
										}
										else
										{
											echo '<option value="'.esc_attr( $key ).'">'.esc_html( $key ).'</option>';
										}
									}
								?>
							</optgroup>
						</select>
						<div class="pt-cf"></div>
						<div class="fonts-checkbox-list">
							<?php
								foreach($font_weight_options_data as $option_value)
								{
									$item_id = $var.'_weight_'.$option_value;
									?>
									<div class="fonts-checkbox <?php echo esc_attr($item_id); ?> font-weight-<?php echo esc_attr($option_value); ?>">
										<input id="<?php echo esc_attr($item_id); ?>" type="checkbox" value="<?php echo esc_attr($option_value); ?>"/>
										<label for="<?php echo esc_attr($item_id); ?>"></label>
										<span><?php echo esc_html( $option_value ); ?></span>
									</div>
									<?php
								}
							?>
						</div>
					</div>
			</dl>
			<?php
		}
		elseif($type == "group")
		{
            $group_class = isset($data['group_class']) ? $data['group_class'] : '';
            $add_item_label = isset($data['add_item_label']) ? $data['add_item_label'] : __('Add Item', 'thebe');
			?>
			<dl class="pt-theme-option-item<?php echo esc_attr($extend_class); ?> pt-group-root" data-tab-name="<?php echo esc_html($tab_name); ?>">
				<dt><?php echo esc_html( $module_label );?></dt>
				<dd>
					<div class="pt-group">
					
						<input type="hidden" class="pt-group-value theme-option-value" name="<?php echo esc_attr($name); ?>" value="<?php echo esc_attr($value) ; ?>"/>
						<span class="uploader-image-des"><?php thebe_kses_content($des); ?></span>
						<div class="pt-option-add-button button-primary"><?php echo esc_html( $add_item_label ); ?></div>
						
						<?php
						if( $help != '' )
						{
							?><span class="pt-admin-help-<?php echo esc_attr($type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($help); ?>"></span><?php
						}
						?>

						<div class="pt-group-container">
							<div class="pt-group-item <?php echo esc_attr($group_class); ?> hidden" style="display:none">
								<div class="intro"></div>
								<span class="pt-group-item-label"><?php esc_html_e('Item', 'thebe'); ?></span>
								<button class="pt-group-item-delete" type="button"><?php esc_html_e('Delete', 'thebe'); ?></button>
								
								<?php
									$items = isset($data['items']) ? $data['items'] : null;
									if($items != null)
									{
										foreach($items as $item)
										{
											$item_type = $item['type'];
											$item_var = $item['var'];
											$item_default = isset($item['default']) ? $item['default'] : '';
											$item_extend_class = isset($item['class']) ? ' '.$item['class'] : '';
											$item_class = 'pt-option-item-'.$item_var.$item_extend_class;
											$item_label = isset($item['label']) ? $item['label'] : $item_var;
											$item_des = isset($item['des']) ? $item['des'] : '';
											$item_help = isset($item['help']) ? $item['help'] : '';
                                            $item_params = isset($item['params']) ? $item['params'] : '';
                                            $item_placeholder = isset($item['placeholder']) ? $item['placeholder'] : '';
											
											switch($item_type)
											{
												case 'text':
													?>
													<div class="pt-group-option-item <?php echo esc_attr($item_class); ?>">
														<?php
															if($item_label != '') 
															{
																?><strong><?php echo esc_html($item_label); ?>:</strong><?php
															}
														?>
														<input class="pt-group-option-data" data-name="<?php echo esc_attr($item_var); ?>" type="text" value="<?php echo esc_attr($item_default); ?>" placeholder="<?php echo esc_attr($item_placeholder); ?>"/>
														<span class="pt-option-item-des"><?php thebe_kses_content($item_des); ?></span>
														<?php
															if( $item_help != '' )
															{
																?><span class="pt-admin-help-<?php echo esc_attr($item_type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($item_help); ?>"></span><?php
															}
														?>
													</div>
													<?php
													break;
												case 'color':
													?>
													<div class="pt-group-option-item <?php echo esc_attr($item_class); ?>">
														<?php
															if($item_label != '') 
															{
																?><strong><?php echo esc_html($item_label); ?>:</strong><?php
															}
														?>
														<input class="pt-group-option-data group-color-field" data-alpha="true" data-custom-width="50px" data-name="<?php echo esc_attr($item_var); ?>" type="text" value="<?php echo esc_attr($item_default); ?>"/>
														<span class="pt-option-item-des"><?php thebe_kses_content($item_des); ?></span>
														<?php
															if( $item_help != '' )
															{
																?><span class="pt-admin-help-<?php echo esc_attr($item_type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($item_help); ?>"></span><?php
															}
														?>
													</div>
													<?php
													break;
												case 'icon':
													?>
													<div class="pt-group-option-item <?php echo esc_attr($item_class); ?>">
														<?php
															if($item_label != '') 
															{
																?><strong><?php echo esc_html($item_label); ?>:</strong><?php
															}
														?>
														<input class="pt-group-option-data icon-input" data-name="<?php echo esc_attr($item_var); ?>" type="text" value="<?php echo esc_attr($item_default); ?>" placeholder="<?php echo esc_attr($item_placeholder); ?>"/>
														<div class="icons-insert-button icons-btn-pe" data-id="pe" title="<?php echo esc_attr__('Pe icon 7', 'thebe'); ?>"></div>
														<div class="icons-insert-button icons-btn-fontawesome" data-id="fontawesome" title="<?php echo esc_attr__('FontAwesome', 'thebe'); ?>"></div>
														<span class="pt-option-item-des"><?php thebe_kses_content($item_des); ?></span>
														<?php
															if( $item_help != '' )
															{
																?><span class="pt-admin-help-<?php echo esc_attr($item_type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($item_help); ?>"></span><?php
															}
														?>
													</div>
													<?php
													break;
												case 'textarea':
													?>
													<div class="pt-group-option-item <?php echo esc_attr($item_class); ?>">
														<?php
															if($item_label != '') 
															{
																?><strong><?php echo esc_html($item_label); ?>:</strong><?php
															}
														?>
														<textarea class="pt-group-option-data" data-name="<?php echo esc_attr($item_var); ?>" placeholder="<?php echo esc_attr($item_placeholder); ?>"><?php echo esc_html($item_default); ?></textarea>
														<span class="pt-option-item-des"><?php thebe_kses_content($item_des); ?></span>
														<?php
															if( $item_help != '' )
															{
																?><span class="pt-admin-help-<?php echo esc_attr($item_type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($item_help); ?>"></span><?php
															}
														?>
													</div>
													<?php
													break;
												case 'select':

													$use_preview = isset($item['use_preview']) && $item['use_preview'] == true;
													$preview_class = $use_preview ? 'select-previewable ' : '';
													$outer_preview_class = $use_preview ? ' has-pt-select' : '';
													$dropdown_class = $use_preview ? 'pt-select ' : '';
													$only_available = isset($item['only_available']) ? $item['only_available'] : null;
													$available_value = $only_available != null ? json_encode($only_available) : '';

													?>
													<div class="pt-group-option-item <?php echo esc_attr($outer_preview_class.$item_class); ?>">
														<?php
															if($item_label != '') 
															{
																?><strong><?php echo esc_html($item_label); ?>:</strong><?php
															}
														?>
														<select class="<?php echo esc_attr($dropdown_class.$preview_class); ?>pt-group-option-data" data-name="<?php echo esc_attr($item_var); ?>" data-available="<?php echo esc_attr($available_value); ?>" data-type="group">
														<?php
															$item_options = $item['options'];
															foreach ($item_options as $choice_key=>$choice_value)
															{
																$preview_id = $item_var.'_'.$choice_key;

																if($choice_key == (string)$item_default)
																{
																	?>
																	<option <?php echo ($use_preview ? ' data-previewid="pt_group_item_'.esc_attr($preview_id).'"' : ''); ?> value="<?php echo esc_attr($choice_key); ?>" selected="selected"><?php echo esc_html($choice_value); ?></option>
																	<?php
																}
																else
																{
																	?>
																	<option <?php echo ($use_preview ? ' data-previewid="pt_group_item_'.esc_attr($preview_id).'"' : ''); ?> value="<?php echo esc_attr($choice_key); ?>"><?php echo esc_html($choice_value); ?></option>
																	<?php
																}
																
															}
														?>
														</select>
														<span class="pt-option-item-des"><?php thebe_kses_content($item_des); ?></span>
														<?php
															if( $item_help != '' )
															{
																?><span class="pt-admin-help-<?php echo esc_attr($item_type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($item_help); ?>"></span><?php
															}
														?>
													</div>
													<?php
													break;
												case 'checkbox':
													$only_available = isset($item['only_available']) ? $item['only_available'] : null;
													$available_value = $only_available != null ? json_encode($only_available) : '';

													?>
													<div class="pt-group-option-item checkbox-item <?php echo esc_attr($item_class); ?>">
														<?php
															if($item_label != '')
															{
																?><strong><?php echo esc_html($item_label); ?>:</strong><?php
															}
														?>

														<input class="pt-group-option-data" data-name="<?php echo esc_attr($item_var); ?>" type="hidden" value="<?php echo esc_attr($item_default); ?>" data-available="<?php echo esc_attr($available_value); ?>" data-type="group"/>

														<label class="pt-option-item-des">
															<input type="checkbox"/>
															<?php thebe_kses_content($item_des); ?>
														</label>

														<?php
															if( $item_help != '' )
															{
																?><span class="pt-admin-help-<?php echo esc_attr($item_type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($item_help); ?>"></span><?php
															}
														?>
													</div>
													<?php
													break;
												case 'image':
													?>
													<div class="pt-group-option-item <?php echo esc_attr($item_class); ?>">
														<?php
															if($item_label != '') 
															{
																?><strong><?php echo esc_html($item_label); ?>:</strong><?php
															}

															$empty = "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";
														?>
														<input class="pt-group-option-data" type="hidden" data-name="<?php echo esc_attr($item_var); ?>" data-bindto="image&uploader-image-preview&src" value="<?php echo esc_attr($item_default); ?>"/>
														<div class="option-item-upload-button button-primary single-image-upload"><?php esc_html_e('Upload', 'thebe'); ?></div>
														<span class="uploader-image-des"><?php thebe_kses_content($item_des); ?></span>
														<?php 
															if( $item_help != '' )
															{
																?><span class="pt-admin-help-<?php echo esc_attr($item_type); ?> pt-admin-help-icon" data-id="<?php echo esc_attr($item_help); ?>"></span><?php
															}
														?>
														<div class="uploader-image-container single-image-input single-image-upload-delete empty" style="display:none;">
															<span class="single-image-upload-delete-button upload-delete-button"></span>
															<img class="uploader-image-preview" alt="<?php esc_attr_e('Image preview', 'thebe'); ?>" src="<?php echo esc_attr($empty); ?>" data-empty="<?php echo esc_attr($empty); ?>"/>
														</div>
													</div>
													<?php
												break;
											}
										}
									}
								?>
							</div>
						</div>						
					</div>
				</dd>
			</dl>
			<?php
		}
	}
	//function thebe_generate_html end
?>