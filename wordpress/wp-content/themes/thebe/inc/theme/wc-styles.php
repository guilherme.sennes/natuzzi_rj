<?php
/**
 * foreverpinetree@gmail.com
 */

//Dynamic styles
function thebe_wc_styles($custom)
{	
	//Main color start-------------------------------------------------
	$main_color = thebe_get_theme_option('thebe_main_color', '#d49be8');

	$custom .= '.woocommerce span.onsale,
	.woocommerce #respond input#submit:hover,
	.woocommerce a.button:hover,
	.woocommerce button.button:hover,
	.woocommerce input.button:hover,
	.woocommerce nav.woocommerce-pagination ul li a:focus,
	.woocommerce nav.woocommerce-pagination ul li a:hover,
	.woocommerce nav.woocommerce-pagination ul li span.current,
	.woocommerce #respond input#submit.alt,
	.woocommerce a.button.alt,
	.woocommerce button.button.alt,
	.woocommerce input.button.alt,
	.woocommerce button.button.alt.disabled,
	.woocommerce input.button.alt.disabled,
	.woocommerce button.button.alt.disabled:hover,
	.woocommerce input.button.alt.disabled:hover,
	.woocommerce #respond input#submit.alt:hover,
	.woocommerce a.button.alt:hover,
	.woocommerce button.button.alt:hover,
	.woocommerce input.button.alt:hover,
	.woocommerce .woocommerce-tabs li:after,
	.select2-container--default .select2-results__option--highlighted[aria-selected],
	.select2-container--default .select2-results__option--highlighted[data-selected],
	.site-dark .select2-container--default .select2-results__option--highlighted[aria-selected],
	.site-dark .select2-container--default .select2-results__option--highlighted[data-selected],
	.woocommerce-MyAccount-navigation li.is-active,
	.woocommerce-Address a.edit,
	.pt-mini-cart a.button.wc-forward,
	.woocommerce #review_form #respond .form-submit input#submit:hover
	{
		background-color: '.$main_color.';
	}'."\n";

	$custom .= '.woocommerce nav.woocommerce-pagination ul li a:focus,
	.woocommerce nav.woocommerce-pagination ul li a:hover,
	.woocommerce nav.woocommerce-pagination ul li span.current,
	.woocommerce-message,
	.woocommerce-info,
	#add_payment_method table.cart td.actions .coupon .input-text:focus,
	.woocommerce-cart table.cart td.actions .coupon .input-text:focus,
	.woocommerce-checkout table.cart td.actions .coupon .input-text:focus,
	.cart-collaterals input.input-text:focus,
	.cart-collaterals select:focus,
	body .woocommerce .input-text:focus,
	a.shipping-calculator-button
	{
		border-color: '.$main_color.';
	}'."\n";

	$custom .= '.woocommerce .woocommerce-result-count:before,
	.product_meta a:hover,
	.woocommerce-message::before,
	.woocommerce-info::before,
	.woocommerce-LostPassword a:hover,
	.pt-header-miniCart li > a:not(.remove):hover,
	.woocommerce div.product .stock
	{
		color: '.$main_color.';
	}'."\n";

	//Output all the styles
	wp_add_inline_style('thebe-style', $custom);	
}
add_action( 'wp_enqueue_scripts', 'thebe_wc_styles', 11 );