<?php

add_filter( 'pt-ocdi/import_files', 'thebe_demo_import' );
if( !function_exists('thebe_demo_import') )
{
	function thebe_demo_import() 
	{
	    return array(
	        array(
	        	'type'                         => 'light',
	            'import_file_name'             => esc_html__('Light', 'thebe'),
	            'local_import_file'            => THEBE_THEME_DIR . '/demo-files/light/content.xml',
	            'local_import_widget_file'     => THEBE_THEME_DIR . '/demo-files/light/widgets.wie',
	            'import_preview_image_url'     => THEBE_THEME_URL . '/demo-files/light/preview.jpg',
	            'preview_url'                  => 'https://3theme.com/tf005/demo1/',
	        ),
	        array(
	        	'type'                         => 'dark',
	            'import_file_name'             => esc_html__('Dark', 'thebe'),
	            'local_import_file'            => THEBE_THEME_DIR . '/demo-files/dark/content.xml',
	            'local_import_widget_file'     => THEBE_THEME_DIR . '/demo-files/dark/widgets.wie',
	            'import_preview_image_url'     => THEBE_THEME_URL . '/demo-files/dark/preview.jpg',
	            'preview_url'                  => 'https://3theme.com/tf005/demo2/',
	        ),
	    );
	}
}


add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );


add_filter( 'pt-ocdi/plugin_page_setup', 'thebe_demo_plugin_page_setup' );
if( !function_exists('thebe_demo_plugin_page_setup') )
{
	function thebe_demo_plugin_page_setup( $default_settings )
	{
		$default_settings['page_title']  = esc_html__( 'One Click Demo Import' , 'thebe' );
		$default_settings['menu_title']  = esc_html__( 'Import Demo Data' , 'thebe' );
		$default_settings['menu_slug']   = 'one-click-demo-import';

		return $default_settings;
	}
}


add_filter( 'pt-ocdi/plugin_intro_text', 'thebe_demo_plugin_intro_text' );
if( !function_exists('thebe_demo_plugin_intro_text') )
{
	function thebe_demo_plugin_intro_text( $default_text )
	{
		ob_start();

		?>
		<div class="demo-install-intro">
			<hr>
			<p class="intro-title"><?php esc_html_e( 'When you import the data, the following things might happen:', 'thebe' ); ?></p>
			<ul>
				<li><?php esc_html_e( 'No existing posts, pages, categories, images, custom post types or any other data will be deleted or modified.', 'thebe' ); ?></li>
				<li><?php esc_html_e( 'Posts, pages, images, widgets, menus and other theme settings will get imported.', 'thebe' ); ?></li>
				<li><?php esc_html_e( 'Please click on the Import button only once and wait, it can take a couple of minutes.', 'thebe' ); ?></li>
			</ul>
			<hr>
		</div>
		<?php

		return ob_get_clean();
	}
}


add_action( 'pt-ocdi/after_import', 'thebe_demo_after_import' );
if( !function_exists('thebe_demo_after_import') )
{
	function thebe_demo_after_import( $selected_import )
	{
		// set menu ------------------------------------------
		$possible_names = array( 
			'Main', 'Primary', 'Main Menu', 'Main Nav', 'Primary Menu', 'Primary Nav', 
			'main', 'primary', 'main menu', 'main nav', 'primary menu', 'primary nav',
		);

		foreach ($possible_names as $menu_name)
		{
			$main_menu = get_term_by( 'name', $menu_name, 'nav_menu' );
			if( $main_menu !== false )
			{
				set_theme_mod( 'nav_menu_locations', array(
					'primary' => $main_menu->term_id,
				));

				break;
			}
		}

		// set home page ------------------------------------
		if( $selected_import['type'] === 'light' )
		{
			$home_page = 'Main Layouts';
		}
		elseif( $selected_import['type'] === 'dark' )
		{
			$home_page = 'Landing';
		}
		
		$front_page = get_page_by_title( $home_page );
		if( $front_page )
		{
			update_option( 'show_on_front', 'page' );
	    	update_option( 'page_on_front', $front_page->ID );
		}
	    
		// import theme options -----------------------------
		$file_path = THEBE_THEME_DIR . '/demo-files/' . $selected_import['type'] . '/options.json';

		global $wp_filesystem;
		WP_Filesystem();
		
		if( $wp_filesystem->exists( $file_path ) )
		{
			$json_str = $wp_filesystem->get_contents( $file_path );
			$json = json_decode( $json_str );
			if( $json !== NULL ) //check if the json is valid
			{
				if(get_option( Thebe_Theme_Options::$options_key ) === false)
				{
					add_option( Thebe_Theme_Options::$options_key, $json_str, '', 'yes' );
				}
				else
				{
					update_option( Thebe_Theme_Options::$options_key, $json_str );
				}
			}
		}
	}
}