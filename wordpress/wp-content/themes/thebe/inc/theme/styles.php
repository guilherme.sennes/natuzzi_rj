<?php
/**
 * foreverpinetree@gmail.com
 */

//Dynamic styles
function thebe_custom_styles($custom)
{
	$custom = apply_filters( 'thebe_custom_font_parse_fonts', $custom );
	
	//Body text start -------------------------------------------------
	$body_font_css = 'body,textarea,input,button,select,.single-meta a,.h .intro';
	$font_var = '';
	$body_font_family = thebe_get_theme_option('thebe_body_font_family', '');
	if($body_font_family != '')
	{
		$pos = strpos($body_font_family, ' - ');
		if($pos !== false)
		{
			$body_font_family = substr($body_font_family, $pos + 3);
		}
		
		$pos = strrpos($body_font_family, '|');
		$fallback = '';
		if($pos !== false)
		{
			$fallback = ',' . substr($body_font_family, $pos + 1);
		}
		
		$pos = strpos($body_font_family, ':');
		$font_name = $body_font_family;
		if($pos !== false)
		{
			$font_name = substr($body_font_family, 0, $pos);
		}

		$font_var .= "font-family:" . $font_name . $fallback . ";";
	}
	
	$body_font_weight = thebe_get_theme_option('thebe_body_font_weight', '0');
	if( $body_font_weight != '0' )
	{
		$font_var .= "font-weight:" . $body_font_weight . ";";
	}

	if( $font_var )
	{
		$custom .= $body_font_css;
		$custom .= "{" . $font_var . "}"."\n";
	}
	//Body text end ---------------------------------------------------
	
	//Title text start-------------------------------------------------
	$title_font_css = 'h1,h2,h3,h4,h5,h6,strong,b,mark,legend,blockquote,.price-amount,.title,.h a,.widget-title,.category-nav li,.filter';
	$font_var = '';
	$title_font_family = thebe_get_theme_option('thebe_title_font_family', '');
	if($title_font_family != '')
	{
		$pos = strpos($title_font_family, ' - ');
		if($pos !== false)
		{
			$title_font_family = substr($title_font_family, $pos + 3);
		}
		
		$pos = strrpos($title_font_family, '|');
		$fallback = '';
		if($pos !== false)
		{
			$fallback = ',' . substr($title_font_family, $pos + 1);
		}
		
		$pos = strpos($title_font_family, ':');
		$font_name = $title_font_family;
		if($pos !== false)
		{
			$font_name = substr($title_font_family, 0, $pos);
		}
		
		$font_var .= "font-family:" . $font_name . $fallback . ";";
	}
	
	$title_font_weight = thebe_get_theme_option('thebe_title_font_weight', '0');
	if( $title_font_weight != '0' )
	{
		$font_var .= "font-weight:" . $title_font_weight . ";";
	}

	if( $font_var )
	{
		$custom .= $title_font_css;
		$custom .= "{" . $font_var . "}"."\n";
	}
	//Title text end---------------------------------------------------
	
	//Menu text start--------------------------------------------------
	$menu_font_css = '.main-menu > ul > li > a,.addition-menu p,.popup-sub-menu a';
	$font_var = '';
	$menu_font_family = thebe_get_theme_option('thebe_menu_font_family', '');
	if($menu_font_family != '')
	{
		$pos = strpos($menu_font_family, ' - ');
		if($pos !== false)
		{
			$menu_font_family = substr($menu_font_family, $pos + 3);
		}
		
		$pos = strrpos($menu_font_family, '|');
		$fallback = '';
		if($pos !== false)
		{
			$fallback = ',' . substr($menu_font_family, $pos + 1);
		}
		
		$pos = strpos($menu_font_family, ':');
		$font_name = $menu_font_family;
		if($pos !== false)
		{
			$font_name = substr($menu_font_family, 0, $pos);
		}
		
		$font_var .= "font-family:" . $font_name . $fallback . ";";
	}
	
	$menu_font_weight = thebe_get_theme_option('thebe_menu_font_weight', '0');
	if( $menu_font_weight != '0' )
	{
		$font_var .= "font-weight:" . $menu_font_weight . ";";
	}

	if( $font_var )
	{
		$custom .= $menu_font_css;
		$custom .= "{" . $font_var . "}"."\n";
	}
	//Menu text end----------------------------------------------------

	//Custom text 1 start--------------------------------------------------
	$custom_font_css = thebe_get_theme_option('thebe_custom_font_selector1', '');
	if( $custom_font_css != '' )
	{
		$font_var = '';
		$custom_font_family = thebe_get_theme_option('thebe_custom_font_family1', '');
		if($custom_font_family != '')
		{
			$pos = strpos($custom_font_family, ' - ');
			if($pos !== false)
			{
				$custom_font_family = substr($custom_font_family, $pos + 3);
			}
			
			$pos = strrpos($custom_font_family, '|');
			$fallback = '';
			if($pos !== false)
			{
				$fallback = ',' . substr($custom_font_family, $pos + 1);
			}
			
			$pos = strpos($custom_font_family, ':');
			$font_name = $custom_font_family;
			if($pos !== false)
			{
				$font_name = substr($custom_font_family, 0, $pos);
			}
			
			$font_var .= "font-family:" . $font_name . $fallback . ";";
		}
		
		$custom_font_weight = thebe_get_theme_option('thebe_custom_font_weight1', '');
		if($custom_font_weight == '')
		{
			$custom_font_weight = '700';
		}
		$font_var .= "font-weight:" . $custom_font_weight . ";";

		if( $font_var )
		{
			$custom .= $custom_font_css;
			$custom .= "{" . $font_var . "}"."\n";
		}
	}
	//Custom text 1 end----------------------------------------------------

	//Custom text 2 start--------------------------------------------------
	$custom_font_css = thebe_get_theme_option('thebe_custom_font_selector2', '');
	if( $custom_font_css != '' )
	{
		$font_var = '';
		$custom_font_family = thebe_get_theme_option('thebe_custom_font_family2', '');
		if($custom_font_family != '')
		{
			$pos = strpos($custom_font_family, ' - ');
			if($pos !== false)
			{
				$custom_font_family = substr($custom_font_family, $pos + 3);
			}
			
			$pos = strrpos($custom_font_family, '|');
			$fallback = '';
			if($pos !== false)
			{
				$fallback = ',' . substr($custom_font_family, $pos + 1);
			}
			
			$pos = strpos($custom_font_family, ':');
			$font_name = $custom_font_family;
			if($pos !== false)
			{
				$font_name = substr($custom_font_family, 0, $pos);
			}
			
			$font_var .= "font-family:" . $font_name . $fallback . ";";
		}
		
		$custom_font_weight = thebe_get_theme_option('thebe_custom_font_weight2', '');
		if($custom_font_weight == '')
		{
			$custom_font_weight = '700';
		}
		$font_var .= "font-weight:" . $custom_font_weight . ";";

		if( $font_var )
		{
			$custom .= $custom_font_css;
			$custom .= "{" . $font_var . "}"."\n";
		}
	}
	//Custom text 2 end----------------------------------------------------
	
	//Main color start-------------------------------------------------
	$main_color = thebe_get_theme_option('thebe_main_color', '#05b14a');

	$custom .= 'input.search-field:focus,.wpcf7 input:not(.wpcf7-submit):focus,.wpcf7 textarea:focus,.single-meta a:not(.btn):hover,a.text-link:hover,.list-meta a:hover,.fn a:hover,.pic-loader:after,.single-tags a:hover,.single-related .category a:hover,.comment-root .reply a:hover,.comment-root textarea:focus,.comment-root p:not(.form-submit) input:focus,.body-category-wrap a:hover,.blog-list a.btn:hover,.pages li .current.page-numbers:after,.intro .h a:not(.btn),.grid-list .list-category a:hover,.blog-list.style-01 a.btn:hover,.blog-list.style-02 .item .h a:hover,.site-bg-code.style-01 i.highlight,.tagcloud a:hover{
		border-color: '.$main_color.';
	}'."\n";

	$custom .= '.comment-awaiting-moderation:before,.blog-list.style-01 a.btn,.comment-notes:before,.call-popup:hover:before,.sub-menu li:hover a,.pt-social a:hover,.recentcomments span,.go-top:hover:before,#cancel-comment-reply-link:hover:before,.widget a:hover,.widget_archive li,.widget_rss cite,.sub-menu li.current_page_item a:before,.blog-list:not(.style-02) .category a:hover,.blog-list:not(.style-02) .item .h a:hover,.cate-item a:hover,#music-player:hover:before,.comment-root .title span,.single-extend .text h2 i,i.close-bar-single:hover:before,.sub-menu .current-menu-ancestor>a:before,.item:not(.text-post) .category:before,.item.text-post:not([data-bg-color]) .category:before,.title-group .h i,.sc-mixbox .title:after,.single-extend h2:before,.comment-respond .h.title:before{
		color:'.$main_color.';
	}'."\n";

	$custom .= '.main-menu a:after,.main-menu-outside a:after,.title-group:after,.v-ctrl.v-pause:hover,.pic-list .h span:after,.category-nav .active:after,.single-main-intro .category a,.filter li:after,em.close-category:hover,.category-nav li.active:after,i.call-filter,.pages li .current,a.btn:hover:after,.item.sticky .img:before,.item.sticky.text-post:before,.cate-nav-ctrl:hover:before,#nprogress .bar,.reply a:hover,.comment-root input[type="submit"]:hover,.text-area input[type="submit"]:hover,.default-template-page input[type="submit"]:hover.blog-list.style-02 .item:not(.text-post) .category a:hover,.blog-list:not(.style-02) em.show-all-category:hover,.single-related .category a:hover,.tagcloud a:hover,.widget-title:before,.wpcf7-submit,.single-related h5:after,.title-group h1:after,.title-group .intro:after,.title-group .intro:before,.small-btn a:hover,.owl-nav>div:hover,.grid-list.caption-02 .list-category a:hover,.loader-icon i,.sc-text-carousel .owl-dot.active span,.pc-mode .call-popup:hover:before,.call-search.btn:hover,.single-tags a:hover,.pt-iv-btn:after{
		background-color: '.$main_color.';
	}'."\n";
	//Main color end------------------------------------------------

	$custom .= '::selection{
		color:#fff;
		background:'.$main_color.';
		text-shadow:none;
	}'."\n";


	$normal_font_size = thebe_get_theme_option('thebe_normal_font_size', '14');
	$custom .= 'html{font-size:'.$normal_font_size.'px;}'."\n";

	$custom_style = thebe_get_theme_option('thebe_custom_style', '');
	if($custom_style != '')
	{
		$custom .= $custom_style."\n";
	}

	//Output all the styles
	wp_add_inline_style('thebe-style', $custom);	
}
add_action( 'wp_enqueue_scripts', 'thebe_custom_styles' );