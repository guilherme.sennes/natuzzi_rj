<?php
	/**
	 * Use the TGM Plugin Activator class to notify the user to install the required/recommended Plugin
	 */
	add_action( 'tgmpa_register', 'thebe_register_required_plugins' );
	function thebe_register_required_plugins() {
	    $plugins = array(
	        array(
	            'name'         => esc_html__('Thebe Options Manager', 'thebe'),
	            'slug'         => 'thebe-options-manager',
	            'source'       => THEBE_THEME_DIR . '/inc/plugins/thebe-options-manager.zip',
	            'external_url' => THEBE_THEME_DIR . '/inc/plugins/thebe-options-manager.zip',
	            'required'     => true,
	            'version'      => '1.0.3'
			),
			
			array(
	            'name'         => esc_html__('Thebe Projects', 'thebe'),
	            'slug'         => 'thebe-projects',
	            'source'       => THEBE_THEME_DIR . '/inc/plugins/thebe-projects.zip',
	            'external_url' => THEBE_THEME_DIR . '/inc/plugins/thebe-projects.zip',
	            'required'     => true,
	            'version'      => '1.0.1'
	        ),

	        array(
	            'name'         => esc_html__('Thebe Shortcodes', 'thebe'),
	            'slug'         => 'thebe-shortcodes',
	            'source'       => THEBE_THEME_DIR . '/inc/plugins/thebe-shortcodes.zip',
	            'external_url' => THEBE_THEME_DIR . '/inc/plugins/thebe-shortcodes.zip',
	            'required'     => true,
	            'version'      => '1.0.7'
	        ),

	        array(
	            'name'         => esc_html__('Thebe Menu Image', 'thebe'),
	            'slug'         => 'thebe-menu-image',
	            'source'       => THEBE_THEME_DIR . '/inc/plugins/thebe-menu-image.zip',
	            'external_url' => THEBE_THEME_DIR . '/inc/plugins/thebe-menu-image.zip',
	            'required'     => true,
	            'version'      => '1.0.0'
	        ),

	        array(
	            'name'         => esc_html__('Thebe Items Order', 'thebe'),
	            'slug'         => 'thebe-items-order',
	            'source'       => THEBE_THEME_DIR . '/inc/plugins/thebe-items-order.zip',
	            'external_url' => THEBE_THEME_DIR . '/inc/plugins/thebe-items-order.zip',
	            'required'     => false,
	            'version'      => '1.0.0'
	        ),

	        array(
	            'name'         => esc_html__('Thebe Custom Font', 'thebe'),
	            'slug'         => 'thebe-custom-font',
	            'source'       => THEBE_THEME_DIR . '/inc/plugins/thebe-custom-font.zip',
	            'external_url' => THEBE_THEME_DIR . '/inc/plugins/thebe-custom-font.zip',
	            'required'     => false,
	            'version'      => '1.0.0'
	        ),

	        array(
	            'name'         => esc_html__('Contact Form 7', 'thebe'),
	            'slug'         => 'contact-form-7',
	            'required'     => true,
	            'version'      => '4.7'
	        ),

	        array(
	            'name'         => esc_html__('Thebe Custom Script', 'thebe'),
	            'slug'         => 'thebe-custom-script',
	            'source'       => THEBE_THEME_DIR . '/inc/plugins/thebe-custom-script.zip',
	            'external_url' => THEBE_THEME_DIR . '/inc/plugins/thebe-custom-script.zip',
	            'required'     => false,
	            'version'      => '1.0.1'
	        ),

	        array(
	            'name'         => esc_html__('One Click Demo Import', 'thebe'),
	            'slug'         => 'one-click-demo-import',
	            'required'     => false,
	            'version'      => '2.4.0'
	        ),
	    );

		$config = array(
	        'id'           => 'thebe-tgmpa',
	        'default_path' => '',
	        'menu'         => 'tgmpa-install-plugins',
	        'parent_slug'  => 'themes.php',
	        'capability'   => 'edit_theme_options',
	        'has_notices'  => true,
	        'dismissable'  => true,
	        'dismiss_msg'  => '',
	        'is_automatic' => false,
	        'message'      => ''
	    );

		tgmpa( $plugins, $config );
	}
?>