<?php

/**
 * Register custom menu page
 */
add_action('admin_menu', 'thebe_register_custom_menu_page');
if(!function_exists('thebe_register_custom_menu_page'))
{
	function thebe_register_custom_menu_page()
	{
		add_action('admin_init', 'thebe_register_options');

		add_theme_page(esc_html__('Theme Options', 'thebe'), esc_html__('Theme Options', 'thebe'), 'administrator', 'theme_options', 'thebe_output_options_page');
	}
}


/**
 * Output theme-options page
 */
if(!function_exists('thebe_output_options_page'))
{
	function thebe_output_options_page()
	{
		require_once THEBE_THEME_DIR . '/inc/theme/theme-options-page.php';
	}
}


/**
 * Register options
 */
function thebe_register_options()
{
	register_setting('theme_options', Thebe_Theme_Options::$options_key);
}

?>