<?php


/**
 * Parse js path
 */
if( !function_exists('thebe_parse_js_path') )
{
	function thebe_parse_js_path( $file_name, $extend_holder = '' )
	{
		$new_path = THEBE_THEME_URL.'/js/'.$extend_holder;
		if( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG )
		{
			$new_path .= 'uncompressed/'.$file_name.'.js';
		}
		else
		{
			$new_path .= $file_name.'.min.js';
		}
		return $new_path;
	}
}


/**
 * Enqueue theme scripts/styles
 */
add_action( 'wp_enqueue_scripts', 'thebe_theme_scripts' );
if( !function_exists('thebe_theme_scripts') )
{
	function thebe_theme_scripts()
	{
		wp_enqueue_style( 'thebe-fonts', esc_url(THEBE_THEME_URL . '/css/fonts.css'), array(), THEBE_THEME_VERSION );
		wp_enqueue_style( 'thebe-font-awesome', esc_url(THEBE_THEME_URL . '/css/font-awesome/font-awesome.css'), array(), THEBE_THEME_VERSION );
		wp_enqueue_style( 'thebe-pe-icon-7', esc_url(THEBE_THEME_URL . '/css/pe-icon-7-stroke.css'), array(), THEBE_THEME_VERSION );

		wp_enqueue_style( 'thebe-other', esc_url(THEBE_THEME_URL . '/css/other.css'), array(), THEBE_THEME_VERSION );
		wp_enqueue_style( 'thebe-style', get_stylesheet_uri(), array(), THEBE_THEME_VERSION );

		//third plugins ------------------
		wp_enqueue_script('easing', esc_url(THEBE_THEME_URL . '/js/theme/plugins/jquery.easing.min.js'), array('jquery', 'imagesloaded', 'hoverIntent'), THEBE_THEME_VERSION, true);
		wp_enqueue_script('isotope', esc_url(THEBE_THEME_URL . '/js/theme/plugins/isotope.min.js'), array(), THEBE_THEME_VERSION, true);
		wp_enqueue_script('fleximages', esc_url(THEBE_THEME_URL . '/js/theme/plugins/jquery.fleximages.min.js'), array(), THEBE_THEME_VERSION, true);
		wp_enqueue_script('tween-js', esc_url(THEBE_THEME_URL . '/js/theme/plugins/tweenjs-0.6.2.min.js'), array(), THEBE_THEME_VERSION, true);
		wp_enqueue_script('nprogress', esc_url(THEBE_THEME_URL . '/js/theme/plugins/nprogress.min.js'), array(), THEBE_THEME_VERSION, true);
		wp_enqueue_script('owl', esc_url(THEBE_THEME_URL . '/js/theme/plugins/owl.carousel.min.js'), array(), THEBE_THEME_VERSION, true);
		wp_enqueue_script('howler', esc_url(THEBE_THEME_URL . '/js/theme/plugins/howler.min.js'), array(), THEBE_THEME_VERSION, true);
		//third plugins ------------------

		wp_enqueue_script('thebe-pt-plugins', thebe_parse_js_path('pt-plugins', 'theme/'), array(), THEBE_THEME_VERSION, true);

		wp_enqueue_style( 'thebe-widget', esc_url(THEBE_THEME_URL . '/css/widget.css'), array(), THEBE_THEME_VERSION );

		$custom_script = get_option('thebe_custom_script', '');
		if($custom_script != '')
	    {
	    	$custom_script = '!(function($){' . PHP_EOL . $custom_script . PHP_EOL . '})(jQuery);';
			wp_add_inline_script( 'thebe-pt-plugins', $custom_script, 'before' );
		}

		$custom_js = '!(function(win){
			"use strict";
			win.__pt_theme_root_url = "'.esc_url(THEBE_THEME_URL).'";
			win.__pt_upload_url = "'.esc_url(THEBE_UPLOAD_URL).'";
		})(window);';
		wp_add_inline_script( 'thebe-pt-plugins', $custom_js, 'before' );

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		wp_localize_script( 'thebe-script', 'screenReaderText', array(
			'expand'   => '<span class="screen-reader-text">' . esc_html__( 'expand child menu', 'thebe' ) . '</span>',
			'collapse' => '<span class="screen-reader-text">' . esc_html__( 'collapse child menu', 'thebe' ) . '</span>',
		) );

		thebe_add_font_family();

		$main_js_path = thebe_parse_js_path('main', 'theme/');
		wp_enqueue_script('thebe-main', esc_url($main_js_path), array(), THEBE_THEME_VERSION, true);
	}
}


/**
 * Add font family
 */
if( !function_exists('thebe_add_font_family') )
{
	function thebe_add_font_family()
	{
		$fonts = array('body'=>'body_font_family', 'title'=>'title_font_family', 'menu'=>'menu_font_family', 'custom1'=>'custom_font_family1', 'custom2'=>'custom_font_family2');
		$font_temp = array();
		foreach($fonts as $key => $value)
		{
			$font_name = thebe_get_theme_option('thebe_'.$value, '');
			if($font_name == '') continue;

			$pos = strpos($font_name, ' - ');
			if($pos === false)
			{
				$pos = strrpos($font_name, '|');
				if($pos !== false)
				{
					$font_name = substr($font_name, 0, $pos);
				}

				if( $font_name != '' && !in_array($font_name, $font_temp) )
				{
					$font_temp[] = $font_name;
					wp_enqueue_style( 'thebe-'.$key.'-fonts', '//fonts.googleapis.com/css?family='.$font_name );
				}
			}
		}
	}
}


/**
 * Enqueue google map
 */
if( !function_exists('thebe_enqueue_map') )
{
	function thebe_enqueue_map()
	{
		$map_API_key = thebe_get_theme_option('thebe_map_api_key', '');
		$map_API_key_string = '';
		if($map_API_key != '')
		{
			$map_API_key_string = '&key='.$map_API_key;
		}
		
		wp_enqueue_script('thebe-map', thebe_parse_js_path('map', 'theme/'), array(), false, true);
		wp_enqueue_script('thebe-google-map', 'https://maps.googleapis.com/maps/api/js?callback=pt_gmap.initMap'.$map_API_key_string, array(), false, true);
	}
}


/**
 * Enqueue admin custom scripts/styles
 */
add_action( 'admin_enqueue_scripts', 'thebe_admin_custom', 10 );
if(!function_exists('thebe_admin_custom'))
{
	function thebe_admin_custom()
	{
		wp_enqueue_style( 'wp-color-picker' ); 
		
		wp_enqueue_script( 'thebe-wp-color-picker-alpha', esc_url(THEBE_THEME_URL . '/js/wp-color-picker-alpha.min.js'), array('wp-color-picker'), THEBE_THEME_VERSION, true );

		wp_enqueue_script( 'thebe-custom-post-page-js', thebe_parse_js_path('custom-post-page'), array(), THEBE_THEME_VERSION, true );

		wp_enqueue_script( 'thickbox' );
		wp_enqueue_style( 'thickbox' );

		wp_enqueue_script( 'thebe-imagegroup-js', thebe_parse_js_path('pt-imagegroup'), array(), THEBE_THEME_VERSION );
		wp_enqueue_style( 'thebe-imagegroup-style', esc_url(THEBE_THEME_URL . '/css/pt-imagegroup.css'), array(), THEBE_THEME_VERSION );

		$custom_js = '!(function(win){
			"use strict";
			win.__pt_root_url = "'.esc_url(THEBE_THEME_URL).'";
			win.__pt_upload_url = "'.esc_url(THEBE_UPLOAD_URL).'";
			win.__pt_theme_name = "'.esc_js(THEBE_THEME_NAME).'";
			win.__pt_wp_version = "'.esc_js(get_bloginfo( 'version' )).'";
		})(window);';
		wp_add_inline_script( 'thebe-imagegroup-js', $custom_js, 'before' );
		
		wp_enqueue_script( 'thebe-admin-custom-js', thebe_parse_js_path('admin-custom'), array(), THEBE_THEME_VERSION );
		wp_enqueue_style( 'thebe-admin-custom-style', esc_url(THEBE_THEME_URL . '/css/admin-custom.css'), array(), THEBE_THEME_VERSION );

		wp_enqueue_style( 'thebe-admin-addition-style', esc_url(THEBE_THEME_URL . '/css/admin-addition.css'), array(), THEBE_THEME_VERSION );
				
		wp_enqueue_style( 'thebe-admin-font-awesome', esc_url(THEBE_THEME_URL . '/css/font-awesome/font-awesome.css'), array(), THEBE_THEME_VERSION );
		wp_enqueue_style( 'thebe-admin-pe-icon-7', esc_url(THEBE_THEME_URL . '/css/pe-icon-7-stroke.css'), array(), THEBE_THEME_VERSION );

		wp_enqueue_script( 'jquery-ui-sortable' );

		wp_enqueue_script( 'thebe-icons-insert-js', thebe_parse_js_path('pt-icons-insert'), array(), THEBE_THEME_VERSION, true );
	}
}


/**
 * Enqueue scripts/styles for theme-options page
 */
add_action( 'admin_enqueue_scripts', 'thebe_theme_options_scripts' );
if(!function_exists('thebe_theme_options_scripts'))
{
	function thebe_theme_options_scripts($hook)
	{
		if($hook == 'appearance_page_theme_options')
		{
			if(function_exists( 'wp_enqueue_media' ))
			{
				wp_enqueue_media();
			}
			else
			{
				wp_enqueue_style('thickbox');
				wp_enqueue_script('media-upload');
				wp_enqueue_script('thickbox');
			}

			wp_enqueue_script('jquery-form');
			
			wp_enqueue_script( 'jquery-nice-select', esc_url(THEBE_THEME_URL . '/js/jquery.nice-select.min.js'), array(), THEBE_THEME_VERSION );
			
			wp_enqueue_script( 'thebe-theme-ace-js', esc_url(THEBE_THEME_URL . '/js/ace/ace.js'), array(), THEBE_THEME_VERSION );

			wp_enqueue_style( 'thebe-fonts', esc_url(THEBE_THEME_URL . '/css/fonts.css'), array(), THEBE_THEME_VERSION );
			
			wp_enqueue_style( 'thebe-font-list-style', esc_url(THEBE_THEME_URL . '/css/font-list-style.css'), array(), THEBE_THEME_VERSION );
			wp_enqueue_script( 'thebe-font-list-js', thebe_parse_js_path('pt-font-list'), array(), THEBE_THEME_VERSION );
			
			wp_enqueue_style( 'thebe-theme-option-style', esc_url(THEBE_THEME_URL . '/css/pt-theme-options.css'), array(), THEBE_THEME_VERSION );
			wp_enqueue_script( 'thebe-theme-option-js', thebe_parse_js_path('pt-theme-options'), array(), THEBE_THEME_VERSION );

			global $thebe_global_fonts_data;
			if(!isset($thebe_global_fonts_data) || $thebe_global_fonts_data == '')
			{
				global $wp_filesystem;
				WP_Filesystem();
				$path = THEBE_THEME_DIR . '/data/fonts_enum.json';
				$fonts_string = $wp_filesystem->get_contents($path);
				$thebe_global_fonts_data = apply_filters( 'thebe_custom_font_parse_data', json_decode($fonts_string) );
			}
			$font_script = 'window.__fonts_enum = "'.addcslashes(json_encode($thebe_global_fonts_data->fonts), '"').'";';
			wp_add_inline_script( 'thebe-font-list-js', $font_script, 'before' );
		}
	}
}

?>