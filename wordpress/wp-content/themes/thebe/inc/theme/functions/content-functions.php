<?php

/**
 * Page title
 */
if(!function_exists('thebe_output_page_title'))
{
	function thebe_output_page_title()
	{
		$title_type = thebe_get_basic_setting_property( 'title_type', '4' );
		$title = thebe_get_basic_setting_property( 'title', '' );
		$title_description = thebe_get_basic_setting_property( 'title_description', '' );
		$img_obj = thebe_get_image(array(
			'rid' => thebe_get_basic_setting_property( 'title_bg', '' ),
			'max_height' => 1200
		));

		if( ( $title_type === '4' && ( !$title && !$title_description ) ) || ( $title_type !== '4' && ( !$title && !$title_description && !$img_obj ) ) )
		{
			return;
		}

		$title_fullscreen = thebe_get_basic_setting_property( 'title_fullscreen', '0' );
		$title_parallax = $title_type === '4' ? '0' : thebe_get_basic_setting_property( 'title_parallax', '0' );

		$title_classes = 'title-group'.( $title_fullscreen === '1' ? ' fullscreen' : '' ).( $title_parallax === '1' ? ' parallax' : '' ).( ' style-0'.$title_type );

		if( $title_type === '4' )
		{
			?>
			<div class="<?php echo esc_attr( $title_classes ); ?>">
			<div class="text">
				<?php
					if( $title )
					{
						?><h1><?php thebe_kses_content( $title, true, null, true ); ?></h1><?php
					}

					if( $title_description )
					{
						?><div class="intro"><?php thebe_kses_content( $title_description, true, null, true ); ?></div><?php
					}
				?>
			</div>
			<?php
		}
		else
		{
			$title_header_color = thebe_get_basic_setting_property( 'title_header_color', 'white' );

			?>
			<div class="<?php echo esc_attr( $title_classes ); ?>" data-header-color="<?php echo esc_attr( $title_header_color ); ?>">
			<div class="text">
				<?php
					if( $title )
					{
						?><h1><?php thebe_kses_content( $title, true, null, true ); ?></h1><?php
					}

					if( $title_description )
					{
						?><div class="intro"><?php thebe_kses_content( $title_description, true, null, true ); ?></div><?php
					}
				?>
			</div>
			<?php

			$title_use_video = thebe_get_basic_setting_property( 'use_video', '0' );
			$title_video_type = thebe_get_basic_setting_property( 'video_type', '1' );
			$title_video_link = thebe_get_basic_setting_property( 'video_link', '' );

			if( ( $title_use_video === '1' && $title_video_link ) || $title_use_video !== '1' )
			{
				$title_bg_color = thebe_get_basic_setting_property( 'title_bg_color', '' );

				if( $title_use_video === '1' && $title_video_link)
				{
					?><div class="bg-full" data-bg="<?php echo esc_url( $img_obj['thumb'] ); ?>" data-src="<?php echo esc_url( $title_video_link ); ?>" data-type="<?php echo esc_attr( $title_video_type ); ?>" data-volume="0"></div><?php
				}
				elseif( $img_obj )
				{
					?><div class="bg-full" data-bg="<?php echo esc_url( $img_obj['thumb'] ); ?>"></div><?php
				}

				if( $title_bg_color )
				{
					?><div class="bg-color" data-color="<?php echo esc_attr( $title_bg_color ); ?>"></div><?php
				}
			}
		}
		?>
		</div>
		<?php
	}
}


/**
 * Logo
 */
if( !function_exists('thebe_output_logo') )
{
	function thebe_output_logo( $site_style )
	{
		$logo_size_w = 100;
		$logo_size_h = 48;

		if( $site_style === 'light' )
		{
			$logo = THEBE_THEME_URL.'/data/images/logo-light.png';
			$logo_retina = THEBE_THEME_URL.'/data/images/logo-light-retina.png';

			$logo_invert = THEBE_THEME_URL.'/data/images/logo-dark.png';
			$logo_invert_retina = THEBE_THEME_URL.'/data/images/logo-dark-retina.png';
		}
		else
		{
			$logo = THEBE_THEME_URL.'/data/images/logo-dark.png';
			$logo_retina = THEBE_THEME_URL.'/data/images/logo-dark-retina.png';

			$logo_invert = THEBE_THEME_URL.'/data/images/logo-light.png';
			$logo_invert_retina = THEBE_THEME_URL.'/data/images/logo-light-retina.png';
		}
		
		$logo_w = $logo_size_w;
		$logo_h = $logo_size_h;

		$logo_id = thebe_get_theme_option('thebe_logo', '');
		$img_obj = thebe_get_image(array(
			'rid' => $logo_id,
			'max_width' => 360,
		));
		if( $img_obj )
		{
			$logo = $img_obj['thumb'];
			$logo_w = $img_obj['thumb_width'];
			$logo_h = $img_obj['thumb_height'];

			$logo_retina = '';//if user upload logo, then remove the default retina logo.
		}

		$logo_retina_id = thebe_get_theme_option('thebe_logo_retina', '');
		$img_obj = thebe_get_image(array(
			'rid' => $logo_retina_id
		));

		if( $img_obj )
		{
			$logo_retina = $img_obj['thumb'];
		}

		$logo_invert_w = $logo_size_w;
		$logo_invert_h = $logo_size_h;

		$logo_invert_id = thebe_get_theme_option('thebe_logo_invert', '');
		$img_obj = thebe_get_image(array(
			'rid' => $logo_invert_id,
			'max_width' => 360,
		));
		if( $img_obj )
		{
			$logo_invert = $img_obj['thumb'];
			$logo_invert_w = $img_obj['thumb_width'];
			$logo_invert_h = $img_obj['thumb_height'];
			
			$logo_invert_retina = '';//if user upload logo, then remove the default retina logo.
		}

		$logo_invert_retina_id = thebe_get_theme_option('thebe_logo_invert_retina', '');
		$img_obj = thebe_get_image(array(
			'rid' => $logo_invert_retina_id
		));

		if( $img_obj )
		{
			$logo_invert_retina = $img_obj['thumb'];
		}

		?>
		<div class="logo">
			<a href="<?php echo esc_url( home_url('/') );?>">
				<img alt="<?php esc_attr_e('Logo', 'thebe'); ?>" src="<?php echo esc_url($logo); ?>" data-retina="<?php echo esc_url($logo_retina); ?>" width="<?php echo esc_attr($logo_w); ?>" height="<?php echo esc_attr($logo_h); ?>">
				<img alt="<?php esc_attr_e('Addition logo', 'thebe'); ?>" class="addition" src="<?php echo esc_url($logo_invert); ?>" data-retina="<?php echo esc_url($logo_invert_retina); ?>" width="<?php echo esc_attr($logo_invert_w); ?>" height="<?php echo esc_attr($logo_invert_h); ?>">
			</a>
		</div>
		<?php
	}
}


/**
 * Generate - Project lightbox data
 */
if( !function_exists('thebe_generate_project_lightbox_data') )
{
	function thebe_generate_project_lightbox_data( $post_data, $featured_image )
	{
		$gallery_json = array(
			'src' => array(),
			'title' => array(),
		);

		$image_list = thebe_get_property( $post_data, 'image_list', '', 'pt_project_' );
		if( $image_list )
		{
			$image_list_data = explode(',', $image_list);
			foreach( $image_list_data as $image_rid )
			{
				$img_obj = thebe_get_image(array(
					'rid' => $image_rid,
					'max_width' => 1920,
				));

				if( $img_obj )
				{
					$gallery_json['src'][] = $img_obj['thumb'];
					$gallery_json['title'][] = '';
				}
			}
		}
		else
		{
			$gallery_json['src'][] = $featured_image;
			$gallery_json['title'][] = '';
		}

		return $gallery_json;
	}
}


/**
 * Site background
 */
if( !function_exists('thebe_output_site_bg') )
{
	function thebe_output_site_bg()
	{
		$thebe_site_bg_type = '1';
		$thebe_site_css_bg_type = '1';
		$thebe_site_bg_url = '';
		$thebe_site_bg_mask = '';
		$thebe_site_bg_use_video = false;
		$thebe_site_bg_video_type = '1';
		$thebe_site_bg_video_link = '';

		global $thebe_global_site_vars;

		if( isset( $thebe_global_site_vars ) )
		{
			$thebe_site_bg_type = isset( $thebe_global_site_vars['site_bg_type'] ) ? $thebe_global_site_vars['site_bg_type'] : '1';
			if( $thebe_site_bg_type === '2' )
			{
				if( isset( $thebe_global_site_vars['site_bg'] ) && $thebe_global_site_vars['site_bg'] )
				{
					$img_obj = thebe_get_image(array(
						'rid' => $thebe_global_site_vars['site_bg'],
						'max_width' => 2200,
					));
					
					if( $img_obj )
					{
						$thebe_site_bg_url = $img_obj['thumb'];
					}
				}

				if( isset( $thebe_global_site_vars['site_bg_mask'] ) )
				{
					$thebe_site_bg_mask = $thebe_global_site_vars['site_bg_mask'];
				}

				if( isset( $thebe_global_site_vars['site_bg_video_type'] ) )
				{
					$thebe_site_bg_video_type = $thebe_global_site_vars['site_bg_video_type'];
				}

				if( isset( $thebe_global_site_vars['site_bg_video_link'] ) )
				{
					$thebe_site_bg_video_link = $thebe_global_site_vars['site_bg_video_link'];
				}

				$thebe_site_bg_use_video = isset( $thebe_global_site_vars['site_bg_use_video'] ) && $thebe_global_site_vars['site_bg_use_video'] && $thebe_site_bg_video_link;
			}
			elseif( $thebe_site_bg_type === '3' )
			{
				if( isset( $thebe_global_site_vars['site_css_bg_type'] ) )
				{
					$thebe_site_css_bg_type = $thebe_global_site_vars['site_css_bg_type'];
				}
			}
		}

		if( $thebe_site_bg_type === '2' )
		{
			if( $thebe_site_bg_url || $thebe_site_bg_use_video )
			{
				?>
				<div class="site-bg">
				<?php
					if( $thebe_site_bg_url && $thebe_site_bg_use_video )
					{
						?>
						<div class="bg-full" data-bg="<?php echo esc_url( $thebe_site_bg_url ); ?>" data-src="<?php echo esc_attr( $thebe_site_bg_video_link ); ?>" data-type="<?php echo esc_attr( $thebe_site_bg_video_type ); ?>" data-volume="0"></div>
						<?php
					}
					else if( !$thebe_site_bg_use_video )
					{
						?>
						<div class="bg-full" data-bg="<?php echo esc_url( $thebe_site_bg_url ); ?>"></div>
						<?php
					}
					else
					{
						?>
						<div class="bg-full" data-src="<?php echo esc_attr( $thebe_site_bg_video_link ); ?>" data-type="<?php echo esc_attr( $thebe_site_bg_video_type ); ?>" data-volume="0"></div>
						<?php
					}

					if( $thebe_site_bg_mask )
					{
						?><div class="bg-color" data-color="<?php echo esc_attr( $thebe_site_bg_mask ); ?>"></div><?php
					}

					?>
				</div>
				<?php
			}
		}
		else if( $thebe_site_bg_type === '3' )
		{
			?>
			<div class="site-bg">
				<div class="site-bg-code style-0<?php echo esc_attr( $thebe_site_css_bg_type ); ?>"></div>
			</div>
			<?php
		}
	}
}


/**
 * Generate paging navigation
 */
if(!function_exists('thebe_paging_nav'))
{
	function thebe_paging_nav($query = null, $paged = null)
	{
		if( $query === null )
		{
			global $wp_query;
			$query = $wp_query;
		}
		
		if( is_numeric($query) )
		{
			$total = $query;
		}
		else
		{
			$total = $query->max_num_pages;
		}

		if( $paged === null )
		{
			if ( get_query_var('paged') ) {
			    $paged = get_query_var('paged');
			} elseif ( get_query_var('page') ) {
			    $paged = get_query_var('page');
			} else {
			    $paged = 1;
			}
		}
		
		$big = 999999999;
		$paging_nav = paginate_links(array(
			'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
			'format' => '?paged=%#%',
			'current' => max(1, intval($paged)),
			'total' => $total,
			'prev_text' => esc_html__('<', 'thebe'),
			'next_text' => esc_html__('>', 'thebe'),
			'type' => 'array'
		));
		
		$paging_link = '';
		if($paging_nav != "")
		{
			$paging_link = '<div class="pages"><ul>';
			foreach($paging_nav as $nav_str)
			{
				$paging_link .= '<li>'.$nav_str.'</li>';
			}
			$paging_link .= '</ul></div>';
		}
		return $paging_link;
	}
}


/**
 * Build menu
 */
if(!function_exists('thebe_output_menu'))
{
	function thebe_output_menu($location, $container = '', $id = '', $class = '', $container_class = null, $depth = 3, $fallback_page = false )
	{
		$container_allowed_tags = array( 'nav', 'div', 'ul' );

		if ( !in_array( $container, $container_allowed_tags ) )
		{
			$container = 'nav';
		}

		if( $container_class == null )
		{
			$container_class = $location;
		}

		if ( has_nav_menu($location) )
		{
			wp_nav_menu(array(
				'theme_location'  => $location,
				'container'       => $container,
				'container_class' => $container_class,
				'menu_id'         => $id,
				'menu_class'      => $class,
				'depth'           => $depth
			));
		}
		else if( $fallback_page )
		{
			$show_home = true;//consider to add an option in the feature
			if($show_home)
			{
				echo '<'.$container.' class="'.esc_attr( $container_class ).' default-menu-list"><ul id="'.esc_attr( $id ).'" class="'.esc_attr( $class ).'"><li><a href="'.esc_url( home_url('/') ).'">'.esc_html__('Home', 'thebe').'</a></li>';
			}
			else	
			{
				echo '<'.$container.' class="'.esc_attr( $container_class ).' default-menu-list"><ul id="'.esc_attr( $id ).'" class="'.esc_attr( $class ).'">';
			}

			wp_list_pages('depth='.$depth.'.&title_li=');

			echo '</ul></'.$container.'>';
		}
	}
}


/**
 * Generate post socials
 */
if(!function_exists('thebe_post_socials'))
{
	function thebe_post_socials($post_id = '')
	{
		$html = '';
		$permalink = get_permalink($post_id);
		if(thebe_get_theme_option('thebe_use_facebook', '') == '1')
		{
			$link = 'http://www.facebook.com/share.php?u='.esc_url_raw($permalink);
			$html .= '<a class="share-facebook" target="_blank" href="'.esc_url($link).'"><i class="fa fa-facebook"></i></a>';
		}
		
		if(thebe_get_theme_option('thebe_use_twitter', '') == '1')
		{
			$title = rawurlencode(html_entity_decode(get_the_title($post_id)));
			$link = 'http://twitter.com/home/?status='.$title.' '.esc_url_raw($permalink);
			$html .= '<a class="share-twitter" target="_blank" href="'.esc_url($link).'"><i class="fa fa-twitter"></i></a>';
		}
		
		if(thebe_get_theme_option('thebe_use_pinterest', '') == '1')
		{
			if(has_post_thumbnail($post_id))
			{
				$img_id = get_post_thumbnail_id($post_id);
				$img_obj = wp_get_attachment_image_src($img_id, 'full');
				$imageurl = $img_obj[0];
				$title = rawurlencode(html_entity_decode(get_the_title($post_id)));
				$link = 'https://www.pinterest.com/pin/create/button/?url='.esc_url_raw($permalink).'&media='.esc_url_raw($imageurl).'&description='.$title;
				$html .= '<a class="share-pinterest" target="_blank" href="'.esc_url($link).'"><i class="fa fa-pinterest-p"></i></a>';
			}
		}
		
		if(thebe_get_theme_option('thebe_use_linkedin', '') == '1')
		{
			$title = rawurlencode(html_entity_decode(get_the_title($post_id)));
			$sitename = rawurlencode(html_entity_decode(get_bloginfo('name')));
			$summary = rawurlencode(html_entity_decode(get_post($post_id)->post_excerpt));
			
			$link = 'https://www.linkedin.com/shareArticle?mini=true&url='.esc_url_raw($permalink);
			$link .= '&title='.$title;
			$link .= '&summary='.$summary;
			$link .= '&source='.$sitename;
			$html .= '<a class="share-linkedin" target="_blank" href="'.esc_url($link).'"><i class="fa fa-linkedin"></i></a>';
		}
		
		if(thebe_get_theme_option('thebe_use_tumblr', '') == '1')
		{
			$title = rawurlencode(html_entity_decode(get_the_title($post_id)));
			$summary = rawurlencode(html_entity_decode(get_post($post_id)->post_excerpt));
			
			$link = 'http://www.tumblr.com/share/link?url='.esc_url_raw($permalink);
			$link .= '&name='.$title;
			$link .= '&description='.$summary;
			$html .= '<a class="share-tumblr" target="_blank" href="'.esc_url($link).'"><i class="fa fa-tumblr"></i></a>';
		}
		
		if(thebe_get_theme_option('thebe_use_googleplus', '') == '1')
		{
			$link = 'https://plus.google.com/share?url='.esc_url_raw($permalink);
			$html .= '<a class="share-googleplus" target="_blank" href="'.esc_url($link).'"><i class="fa fa-google-plus"></i></a>';
		}
		
        $socials_group_value = thebe_get_theme_option( 'thebe_share_group', '' );
        if( $socials_group_value )
        {
            $socials_group_data = thebe_parse_group_data( $socials_group_value );
            foreach( $socials_group_data as $social_group_item_obj )
            {
                $social_link = $social_group_item_obj['link'];
                if( $social_link )
                {
                    $social_icon = $social_group_item_obj['icon'];
                    $html .= '<a target="_blank" href="'.esc_url( $social_link ).'"><i class="'.esc_attr( $social_icon ).'"></i></a>';
                }
            }
        }

		return $html;
	}
}


/**
 * A function for outputing page shortcode.
 */
if(!function_exists('thebe_output_page_shortcode'))
{
	function thebe_output_page_shortcode()
	{
		$shortcode_meta_prefix = 'pt_sc_';
		$text = thebe_get_page_setting_property( $shortcode_meta_prefix. 'shortCodeText', '' );
		if($text != '')
		{
			echo do_shortcode(shortcode_unautop(wp_specialchars_decode(stripslashes($text), ENT_QUOTES)));
		}
	}
}


/**
 * Get related posts. Return a list of object contains [link, title, tags, imageurl].
 */
if(!function_exists('thebe_get_related_posts_data'))
{
	function thebe_get_related_posts_data( $post_id, $post_type = 'post', $num = 4, $post_style = 1 )
	{
		$result = array();
		$post_ids = array($post_id);

		$query_params = array(
			'post_type' => $post_type,
			'post_style' => $post_style,
		);

		$taxonomy = $post_type == 'project' ? 'project_tag' : 'post_tag';
		$terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'ids'));
		if($terms)
		{
			$args = array(
				'tax_query' => array(
			        array(
			            'taxonomy' => $taxonomy,
			            'terms'    => $terms
			        ),
			    ),
				'post__not_in' => $post_ids,
				'showposts' => $num,
				'ignore_sticky_posts' => 1,
				'meta_key' => '_thumbnail_id',
				'post_type' => array($post_type),
				'suppress_filters' => false
			);

			$query_obj = thebe_query_related_posts($args, $taxonomy, $query_params);

			$post_ids = array_merge($post_ids, $query_obj['ids']);
			$result = array_merge($result, $query_obj['result']);
		}

		$count = count($result);
		if( $count >= $num )
		{
			return $result;
		}
		
		$taxonomy = $post_type == 'project' ? 'project_cat' : 'category';
		$terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'ids'));
		if($terms)
		{
			$args = array(
				'tax_query' => array(
			        array(
			            'taxonomy' => $taxonomy,
			            'terms'    => $terms
			        ),
			    ),
				'post__not_in' => $post_ids,
				'showposts' => $num - $count,
				'ignore_sticky_posts' => 1,
				'meta_key' => '_thumbnail_id',
				'post_type' => array($post_type),
				'suppress_filters' => false
			);

			$query_obj = thebe_query_related_posts($args, $taxonomy, $query_params);

			$post_ids = array_merge($post_ids, $query_obj['ids']);
			$result = array_merge($result, $query_obj['result']);
		}

		$count = count($result);
		if( $count >= $num )
		{
			return $result;
		}

		$args = array(
			'post__not_in' => $post_ids,
			'showposts' => $num - $count,
			'ignore_sticky_posts' => 1,
			'meta_key' => '_thumbnail_id',
			'post_type' => array($post_type),
			'suppress_filters' => false
		);

		$query_obj = thebe_query_related_posts($args, $taxonomy, $query_params);
		$result = array_merge($result, $query_obj['result']);

		return $result;
	}
}


/**
 * parse related posts
 */
if(!function_exists( 'thebe_query_related_posts' ))
{
	function thebe_query_related_posts($args, $taxonomy, $params)
	{
		$post_ids = array();
		$result = array();

		$post_type = $params['post_type'];
		$post_style = $params['post_style'];

		$query = new WP_Query($args);
		if ($query->have_posts())
		{
			while ($query->have_posts())
			{
				$query->the_post();
				
				$related_post_id = apply_filters( 'wpml_object_id', get_the_ID(), $post_type );
				$post_data = thebe_get_post_setting( $related_post_id );
				$image_crop_part = thebe_get_property( $post_data, 'image_crop_part', 3, 'pt_'.$post_type.'_' );
				$ajax_bg_color = thebe_get_property( $post_data, 'ajax_bg_color', '', 'pt_'.$post_type.'_' );
				$ajax_text_color = thebe_get_property( $post_data, 'ajax_text_color', 'white', 'pt_'.$post_type.'_' );

				$post_ids[] = $related_post_id;
				
				$img_obj = $img_obj = thebe_get_image(array(
					'rid' => get_post_thumbnail_id(),
					'width' => 640,
					'height' => 420,
					'crop_part_vertical' => $image_crop_part,
				));

				if( !$img_obj )
				{
					continue;
				}
				
				$tags = get_the_terms( $related_post_id, $taxonomy );
				$tag_names = array();
				if($tags)
				{
					foreach($tags as $tag)
					{
						$tag_names[] = '<span>'.$tag->name.'</span>';
					}
				}

				$cats = array();
				$categories = $post_type === 'project' ? get_the_terms( $related_post_id, 'project_cat' ) : get_the_category();
				if( $categories && !is_wp_error( $categories ) && count( $categories ) > 0 )
				{
					if( $post_type === 'project' )
					{
						foreach( $categories as $cat )
						{
							$cats[] = '<a href="'.esc_url( get_category_link( $cat->term_id ) ).'">'.esc_html( $cat->name ).'</a>';
						}
					}
					else
					{
						foreach( $categories as $cat )
						{
							$cats[] = '<a href="'.esc_url( get_category_link( $cat->term_id ) ).'">'.esc_html( $cat->cat_name ).'</a>';
						}
					}
				}

				$related_link = get_permalink($related_post_id);
				
				$result[] = array( 
					'title' => get_the_title(), 
					'link' => $related_link, 
					'tags' => $tag_names, 
					'cats' => $cats,
					'imageurl' => $img_obj['thumb'], 
					'w' => $img_obj['thumb_width'], 
					'h' => $img_obj['thumb_height'],
					'bg_color' => $ajax_bg_color,
					'text_color' => $ajax_text_color,
				);
			}
		}
		
		wp_reset_postdata();

		return array('ids'=>$post_ids, 'result'=>$result);
	}
}


/**
 * Kses content
 */
if (!function_exists( 'thebe_kses_content' ))
{
	function thebe_kses_content($content, $echo = true, $custom_tags = null, $decode_html = false) 
	{
		$tags = $custom_tags ? $custom_tags : wp_kses_allowed_html( 'post' );

		if( $decode_html )
		{
			if( $echo )
			{
				echo wp_specialchars_decode( stripslashes(wp_kses($content, $tags)), ENT_QUOTES );
			}
			else
			{
				return wp_specialchars_decode( stripslashes(wp_kses($content, $tags)), ENT_QUOTES );
			}
		}
		else
		{
			if( $echo )
			{
				echo wp_kses( $content, $tags );
			}
			else
			{
				return wp_kses( $content, $tags );
			}
		}
		
		return '';
	}
}


add_filter( 'thebe_menu_image_url', 'thebe_get_menu_image_url' );
if(!function_exists('thebe_get_menu_image_url'))
{
	function thebe_get_menu_image_url( $rid = '' )
	{
		if( $rid == '' )
		{
			return '';
		}

		$img_obj = thebe_get_image(array(
			'rid' => $rid,
			'max_height' => 1080,
			'max_crop_width' => 1600
		));

		if( $img_obj )
		{
			return $img_obj['thumb'];
		}

		return '';
	}
}


/**
 * Modify read-more link
 */
add_filter( 'the_content_more_link', 'thebe_modify_read_more_link' );
if(!function_exists('thebe_modify_read_more_link'))
{
	function thebe_modify_read_more_link()
	{
		return '<a class="more-link" href="' . esc_url(get_permalink()) . '">'.esc_html__('READ MORE...', 'thebe').'</a>';
	}
}

?>