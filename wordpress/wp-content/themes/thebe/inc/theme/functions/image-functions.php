<?php

/**
 * get image data
 */
if (!function_exists( 'thebe_get_image' ))
{
	function thebe_get_image( $data = null )
	{
		if( !$data || !isset( $data['rid'] ) || $data['rid'] == '' )
		{
			return null;
		}

		$rid = $data['rid'];
		$width = isset( $data['width'] ) ? $data['width'] : 0;
		$height = isset( $data['height'] ) ? $data['height'] : 0;
		$max_width = isset( $data['max_width'] ) ? $data['max_width'] : 0;
		$max_height = isset( $data['max_height'] ) ? $data['max_height'] : 0;
		$max_crop_width = isset( $data['max_crop_width'] ) ? $data['max_crop_width'] : 0;
		$max_crop_height = isset( $data['max_crop_height'] ) ? $data['max_crop_height'] : 0;
		$crop_part_vertical = isset( $data['crop_part_vertical'] ) ? $data['crop_part_vertical'] : 3;
		$crop_part_horizontal = isset( $data['crop_part_horizontal'] ) ? $data['crop_part_horizontal'] : 3;

		$img_obj = wp_get_attachment_image_src( $rid, 'full' );
		if( !$img_obj )
		{
			return null;
		}

		$img_path = $img_obj[0];
		$img_ori_width = intval( $img_obj[1] );
		$img_ori_height = intval( $img_obj[2] );

		if( $width == 0 && $height == 0 && $max_width == 0 && $max_height == 0 )
		{
			return array(
				'thumb' => $img_path,
				'image' => $img_path,
				'width' => $img_ori_width,
				'height' => $img_ori_height,
				'thumb_width' => $img_ori_width,
				'thumb_height' => $img_ori_height
			);
		}
		
		$thumb_width = 0;
		$thumb_height = 0;
		
		$img_width = $img_ori_width;
		$img_height = $img_ori_height;

		if( $width > 0 )
		{
			$thumb_width = $width;
		}

		if( $height > 0 )
		{
			$thumb_height = $height;
		}

		if( $thumb_width == 0 || $thumb_height == 0 )
		{
			if( $max_width > 0 && $img_width > $max_width )
			{
				$img_height = floor( $img_height / $img_width * $max_width );
				$img_width = $max_width;

				$thumb_width = max( $img_width, $thumb_width );
			}

			if( $max_height > 0 && $img_height > $max_height )
			{
				$img_width = floor( $img_width / $img_height * $max_height );
				$img_height = $max_height;

				$thumb_height = max( $img_height, $thumb_height );

				if( $thumb_width > $img_width )
				{
					$thumb_width = $img_width;
				}
			}

			if( $thumb_width == 0 )
			{
				$thumb_width = $img_width;
			}
			
			if( $thumb_height == 0 )
			{
				$thumb_height = $img_height;
			}
		}

		if( $max_crop_width > 0 && $thumb_width > $max_crop_width )
		{
			$thumb_width = $max_crop_width;
		}

		if( $max_crop_height > 0 && $thumb_height > $max_crop_height )
		{
			$thumb_height = $max_crop_height;
		}

		$thumburl = aq_resize( $img_path, $thumb_width, $thumb_height, true, true, true, $crop_part_vertical, $crop_part_horizontal );
		if( $thumburl )
		{
			if( $thumburl === $img_path )
			{
				$img_width = $img_ori_width;
				$img_height = $img_ori_height;
				$thumb_width = $img_ori_width;
				$thumb_height = $img_ori_height;
			}

			return array(
				'thumb' => $thumburl,
				'image' => $img_path,
				'width' => $img_width,
				'height' => $img_height,
				'thumb_width' => $thumb_width,
				'thumb_height' => $thumb_height
			);
		}

		return null;
	}
}


/**
 * get thumbnail
 */
if(!function_exists( 'thebe_get_thumbnail' ))
{
	function thebe_get_thumbnail( $rid, $params = null )
	{
		$thumburl = '';

		$img_obj = wp_get_attachment_image_src( $rid, 'thumbnail' );
		if( $img_obj )
		{
			$thumburl = $img_obj[0];
		}
		else if( $params && isset($params['default']) )
		{
			$thumburl = $params['default'];
		}

		return $thumburl;
	}
}


/**
 * A function for checking if the image is stored locally.
 */
if(!function_exists('thebe_image_is_local'))
{
	function thebe_image_is_local($url)
	{
		if($url == '')
		{
			return false;
		}
		
		$upload_url = THEBE_UPLOAD_URL;

		if( is_ssl() )
		{
			$upload_url = str_ireplace( 'https://', '', $upload_url );
			$upload_url = str_ireplace( 'http://', '', $upload_url );
		}

		return stripos( $url, $upload_url ) !== false;
	}
}

?>