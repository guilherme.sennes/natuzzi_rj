<?php

if(is_admin())
{
	/**
	 * WP-AJAX import options
	 */
	add_action('wp_ajax_import_options', 'thebe_import_options');
	function thebe_import_options()
	{
		if(is_admin())
		{
			if(!isset($_FILES['importFile']))
			{
				exit('{"status":-1}');//no file
			}

			if( ! wp_verify_nonce($_POST['thebe_options_import'], 'thebe_options_import_action') )
			{
				exit('{"status":-3}');//verify error
			}

			$dir = THEBE_THEME_DIR.'/import-options/';
			wp_mkdir_p($dir);

			$path = $dir.$_FILES['importFile']['name'];
			$ok = @move_uploaded_file($_FILES['importFile']['tmp_name'], $path);
			if($ok)
			{
				global $wp_filesystem;
				WP_Filesystem();
				$json_str = $wp_filesystem->get_contents($path);

	            if(wp_delete_file($path))
	            {
	            	//
	            }

				$json = json_decode($json_str);
				if($json !== NULL) //check if the json is valid
				{
					if(get_option( Thebe_Theme_Options::$options_key ) === false)
					{
						add_option( Thebe_Theme_Options::$options_key, $json_str, '', 'yes' );
					}
					else
					{
						update_option( Thebe_Theme_Options::$options_key, $json_str );
					}
					exit('{"status":1}');//sussesss
				}
				else
				{
					exit('{"status":0}');//fail
				}
			}
		}
		exit('{"status":-2}');
	}


	/**
	 * WP-AJAX import options warnning if user no login
	 */
	add_action('wp_ajax_nopriv_import_options', 'thebe_import_options_nopriv');
	function thebe_import_options_nopriv()
	{
		exit('{"status":-2}');//not allow
	}


	/**
	 * WP-AJAX export options
	 */
	add_action('wp_ajax_export_options', 'thebe_export_options');
	function thebe_export_options()
	{
		if(is_admin())
		{
			$export_time = get_option('options_export_time', 0);
			if($export_time === FALSE)
			{
				add_option('options_export_time', time(), '', 'yes');
			}
			else
			{
				$offset_time = time() - $export_time;
				if($offset_time < 5)
				{
					exit('{"status":-4}');//in CD (export too offten)
				}

				update_option('options_export_time', time());
			}

			$json_str = get_option( Thebe_Theme_Options::$options_key, '' );
			if( $json_str === '' )
			{
				global $thebe_global_theme_options;
				if( isset($thebe_global_theme_options) )
				{
					$json_str = json_encode( $thebe_global_theme_options );
				}
			}

			$file_name = 'options-'.date('Ymd').'.json';

			$dir = THEBE_THEME_DIR.'/export-options/';
			wp_mkdir_p($dir);

			global $wp_filesystem;
			WP_Filesystem();
			$wp_filesystem->put_contents($dir.$file_name, $json_str);
			
			exit('{"status":1, "url":"'.esc_url(THEBE_THEME_URL.'/export-options/'.$file_name).'"}');
		}
		exit('{"status":-2}');
	}


	/**
	 * WP-AJAX export options warnning if user no login
	 */
	add_action('wp_ajax_nopriv_export_options', 'thebe_export_options_nopriv');
	function thebe_export_options_nopriv()
	{
		exit('{"status":-2}');
	}
}

?>