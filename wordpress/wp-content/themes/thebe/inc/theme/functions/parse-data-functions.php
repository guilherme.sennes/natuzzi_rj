<?php


/**
 * A function for preparing page setting
 */
if (!function_exists( 'thebe_prepare_setting' ))
{
	function thebe_prepare_setting( $param )
    {
		$type = isset($param['type']) ? $param['type'] : null;
		if( !$type )
		{
			return;
		}

		$template = isset($param['template']) ? $param['template'] : null;
		$parse_bg = isset($param['parse_bg']) ? $param['parse_bg'] : true;// default is true
		$parse_seo = isset($param['parse_seo']) ? $param['parse_seo'] : true;// default is true
		$queried_obj_id = isset($param['id']) ? $param['id'] : get_queried_object_id();

		global $thebe_global_site_vars;
		if( !isset($thebe_global_site_vars) )
		{
			$thebe_global_site_vars = array();
		}

		if( $type === 'page' )
		{
			global $thebe_global_basic_setting;
			global $thebe_global_page_setting;
			
			$thebe_global_basic_setting = array();
			$thebe_global_page_setting = array();

			// ------- basic setting -------
			$thebe_page_basic_setting = get_post_meta( $queried_obj_id, '_thebe_meta_page_basic', true );
			$thebe_page_basic_data_params = explode( '&', $thebe_page_basic_setting );

			foreach( $thebe_page_basic_data_params as $thebe_page_basic_param )
			{
				$thebe_page_temp_array = explode( '=', $thebe_page_basic_param );
				if( count($thebe_page_temp_array) > 1 )
				{
					$thebe_page_temp_key = urldecode( $thebe_page_temp_array[0] );
					$thebe_global_basic_setting[$thebe_page_temp_key] = urldecode( $thebe_page_temp_array[1] );
				}
			}

			// ------- page setting -------
			$thebe_page_setting = get_post_meta( $queried_obj_id, '_thebe_meta_post_page', true );
			$thebe_page_setting_params = explode( '&', $thebe_page_setting );

			foreach( $thebe_page_setting_params as $thebe_page_param )
			{
				$thebe_page_temp_array = explode( '=', $thebe_page_param );
				if( count($thebe_page_temp_array) > 1 )
				{
					$thebe_page_temp_key = urldecode( $thebe_page_temp_array[0] );
					$thebe_global_page_setting[$thebe_page_temp_key] = urldecode( $thebe_page_temp_array[1] );
				}
			}
		}

		if( $parse_bg )
		{
			global $thebe_global_bg_setting;
			$thebe_global_bg_setting = array();

			// ------- bg setting -------
			$thebe_page_bg_setting = get_post_meta( $queried_obj_id, '_thebe_meta_page_bg', true );
			$thebe_page_bg_data_params = explode( '&', $thebe_page_bg_setting );

			foreach( $thebe_page_bg_data_params as $thebe_page_bg_param )
			{
				$thebe_page_bg_temp_array = explode( '=', $thebe_page_bg_param );
				if( count($thebe_page_bg_temp_array) > 1 )
				{
					$thebe_page_bg_temp_key = urldecode( $thebe_page_bg_temp_array[0] );
					$thebe_global_bg_setting[$thebe_page_bg_temp_key] = urldecode( $thebe_page_bg_temp_array[1] );
				}
			}

			$thebe_global_site_vars['site_bg_type'] = thebe_get_bg_setting_property( 'site_bg_type', '1' );
			$thebe_global_site_vars['site_css_bg_type'] = thebe_get_bg_setting_property( 'site_css_bg_type', '1' );
			$thebe_global_site_vars['site_bg'] = thebe_get_bg_setting_property( 'site_bg', '' );
			$thebe_global_site_vars['site_bg_mask'] = thebe_get_bg_setting_property( 'site_bg_mask', '' );
			$thebe_global_site_vars['site_bg_use_video'] = thebe_get_bg_setting_property( 'site_bg_use_video', '0' ) === '1';
			$thebe_global_site_vars['site_bg_video_type'] = thebe_get_bg_setting_property( 'site_bg_video_type', '1' );
			$thebe_global_site_vars['site_bg_video_link'] = thebe_get_bg_setting_property( 'site_bg_video_link', '' );
		}

		if( $parse_seo )
		{
			global $thebe_global_seo_setting;
			$thebe_global_seo_setting = array();

			// ------- bg setting -------
			$thebe_seo_setting = get_post_meta( $queried_obj_id, '_thebe_meta_seo', true );
			$thebe_seo_data_params = explode( '&', $thebe_seo_setting );

			foreach( $thebe_seo_data_params as $thebe_seo_param )
			{
				$thebe_seo_temp_array = explode( '=', $thebe_seo_param );
				if( count($thebe_seo_temp_array) > 1 )
				{
					$thebe_seo_temp_key = urldecode( $thebe_seo_temp_array[0] );
					$thebe_global_seo_setting[$thebe_seo_temp_key] = urldecode( $thebe_seo_temp_array[1] );
				}
			}

			$thebe_global_site_vars['seo_keywords'] = thebe_get_seo_setting_property( 'seo_keywords', '' );
			$thebe_global_site_vars['seo_description'] = thebe_get_seo_setting_property( 'seo_description', '' );
		}

		if( $template )
		{
			$thebe_global_site_vars['template'] = 'page-'.$template;
		}
    }
}


/**
 * A function for setting properties (return a new Array)
 */
if (!function_exists( 'thebe_set_properties' ))
{
	function thebe_set_properties($source_obj, $variables)
    {
    	if( $source_obj == null || $variables == null )
    	{
    		return null;
    	}

    	$target_obj = array();
    	foreach ( $variables as $key => $default_value )
    	{
    		if( isset( $source_obj[ $key ] ) )
    		{
    			$target_obj[ $key ] = $source_obj[ $key ];
    		}
    		else
    		{
    			$target_obj[ $key ] = $default_value;
    		}
    	}
    	return $target_obj;
    }
}


/**
 * A function for getting custom property from post_meta.
 */
if(!function_exists('thebe_get_property'))
{
	function thebe_get_property($array, $key, $default = null, $prefix = 'pt_post_')
	{
		if( $array == null )
		{
			return $default;
		}

		$key = $prefix . $key;
		if( ! isset( $array[ $key ] ) )
		{
			return $default;
		}
		return $array[ $key ];
	}
}


/**
 * A function for getting theme option.
 */
if(!function_exists('thebe_get_theme_option'))
{
	function thebe_get_theme_option($key, $default = false, $prefix = '')
	{
		if( !$key )
		{
			return null;
		}

		if( $prefix )
		{
			$key = $prefix.$key;
		}

		global $thebe_global_theme_options;

		if( !isset( $thebe_global_theme_options ) )
		{
			return null;
		}

		if( isset( $thebe_global_theme_options[$key] ) )
		{
			$result = $thebe_global_theme_options[$key];
		}
		else
		{
			$result = $default;
		}

		$wpml_obj = Thebe_Theme_Options::check_wpml( $key );
		if( $wpml_obj )
		{
			$result = apply_filters('wpml_translate_single_string', $result, Thebe_Theme_Options::$domain, $wpml_obj['title'] );
		}

		return $result;
	}
}


/**
 * A function for getting property from page setting.
 */
if(!function_exists('thebe_get_page_setting_property'))
{
	function thebe_get_page_setting_property($key, $default = null)
	{
		global $thebe_global_page_setting;
		if( ! isset( $thebe_global_page_setting ) || $thebe_global_page_setting == null )
		{
			return $default;
		}

		if( ! isset( $thebe_global_page_setting[$key] ) )
		{
			return $default;
		}
		return $thebe_global_page_setting[$key];
	}
}


/**
 * A function for getting property from basic setting.
 */
if(!function_exists('thebe_get_basic_setting_property'))
{
	function thebe_get_basic_setting_property($key, $default = null, $prefix = 'pt_basic_')
	{
		$key = $prefix.$key;

		global $thebe_global_basic_setting;
		if( ! isset( $thebe_global_basic_setting ) || $thebe_global_basic_setting == null )
		{
			return $default;
		}

		if( ! isset( $thebe_global_basic_setting[$key] ) )
		{
			return $default;
		}
		return $thebe_global_basic_setting[$key];
	}
}


/**
 * A function for getting property from bg setting.
 */
if(!function_exists('thebe_get_bg_setting_property'))
{
	function thebe_get_bg_setting_property($key, $default = null, $prefix = 'pt_bg_')
	{
		$key = $prefix.$key;

		global $thebe_global_bg_setting;
		if( ! isset( $thebe_global_bg_setting ) || $thebe_global_bg_setting == null )
		{
			return $default;
		}

		if( ! isset( $thebe_global_bg_setting[$key] ) )
		{
			return $default;
		}
		return $thebe_global_bg_setting[$key];
	}
}


/**
 * A function for getting property from SEO setting.
 */
if(!function_exists('thebe_get_seo_setting_property'))
{
	function thebe_get_seo_setting_property($key, $default = null, $prefix = 'pt_seo_')
	{
		$key = $prefix.$key;

		global $thebe_global_seo_setting;
		if( ! isset( $thebe_global_seo_setting ) || $thebe_global_seo_setting == null )
		{
			return $default;
		}

		if( ! isset( $thebe_global_seo_setting[$key] ) )
		{
			return $default;
		}
		return $thebe_global_seo_setting[$key];
	}
}


/**
 * A function for getting post setting.
 */
if(!function_exists('thebe_get_post_setting'))
{
	function thebe_get_post_setting($post_id, $key = null)
	{
		$post_setting = get_post_meta($post_id, "_thebe_meta_post_page", true);

		$post_data = null;
		if($post_setting != null)
		{
			$post_data_params = explode("&", $post_setting);
			$post_data = array();
			foreach($post_data_params as $post_param)
			{
				$arr = explode("=", $post_param);
				if(count($arr) > 1)
				{
					$post_data[urldecode($arr[0])] = urldecode($arr[1]);
				}
			}
		}

		if($key != null)
		{
			$key = 'pt_post_'.$key;
			if($post_data == null || empty($post_data[$key]))
				return null;
			return $post_data[$key];
		}
		
		return $post_data;
	}
}


/**
 * parse group data which generated by pt-group plugin
 */
if (!function_exists( 'thebe_parse_group_data' ))
{
	function thebe_parse_group_data( $data_string = '', $defaults = null ) 
	{
		$data = array();

		if( $data_string == '' )
		{
			return $data;
		}

		$items = explode( ',', $data_string );
		foreach( $items as $item_string )
		{
			$item_array = explode( '&', wp_specialchars_decode( stripslashes($item_string), ENT_QUOTES ) );
			$item_data = array();

			foreach( $item_array as $child_string )
			{
				$child_array = explode( '=', $child_string );

				if( $child_array && count($child_array) >= 2 )
				{
					$item_data[urldecode($child_array[0])] = urldecode($child_array[1]);			
				}
			}

			if( $defaults )
			{
				foreach( $defaults as $default_key => $default_value )
				{
					if( !isset($item_data[$default_key]) )
					{
						$item_data[$default_key] = $default_value;
					}
				}
			}

			$data[] = $item_data;
		}

		return $data;
	}
}


/**
 * parse image-group data
 */
if(!function_exists('thebe_parse_image_group_data'))
{
	function thebe_parse_image_group_data( $data_str )
	{
		if( $data_str == '' )
		{
			return array( 'item' => array(), 'cat' => array() );
		}
		
		$group_data = json_decode( wp_specialchars_decode( stripslashes($data_str), ENT_QUOTES ) );

		if( $group_data == null )
		{
			return array( 'item' => array(), 'cat' => array() );
		}

		$gallery_data = array();
		$gallery_cats = array();
		$uncated_name = isset( $group_data->uncated_name ) ? $group_data->uncated_name : __('Uncategorized', 'thebe');

		foreach( $group_data as $cat_key => $cat )
		{
			if( $cat_key == 'uncated_name' )
			{
				continue;
			}

			if( !isset($cat->rid) ) $cat->rid = array();
			if( !isset($cat->index) ) $cat->index = array();
			if( !isset($cat->variables) ) $cat->variables = array();

			if( count($cat->rid) < 1 )
			{
				continue;
			}

			if( $cat_key == '*' )
			{
				$cat_key = $uncated_name;
			}

			$gallery_cats[ $cat->cat_index ] = $cat_key;

			$len = count( $cat->rid );
			for( $i = 0; $i < $len; $i ++ )
			{
				$item_data = array(
					'category' => $cat_key,
					'cat_index' => isset( $cat->cat_index ) ? $cat->cat_index : 0,
					'rid' => isset( $cat->rid[$i] ) ? $cat->rid[$i] : '',
					'index' => isset( $cat->index[$i] ) ? $cat->index[$i] : 0,
					'variables' => array()
				);

				foreach($cat->variables as $extend_key => $extend_item)
				{
					$item_data['variables'][$extend_key] = $extend_item[$i];
				}

				$gallery_data[] = $item_data;
			}
		}
		
		usort( $gallery_data, 'thebe_sort_asc_by_index' );
		ksort( $gallery_cats, SORT_NUMERIC );

		return array( 'item' => $gallery_data, 'cat' => $gallery_cats );
	}
}


/**
 * parse sc - image-info-list data
 */
if (!function_exists( 'thebe_parse_image_info_list_data' ))
{
	function thebe_parse_image_info_list_data( $data_string = '' ) 
	{
		if( !$data_string )
		{
			return array();
		}

		$result = json_decode( rawurldecode( $data_string ), true );

		if( !$result || !is_array( $result ) )
		{
			$result = array();
		}
		
		return $result;
	}
}


/**
 * get 9-grid align class
 */
if(!function_exists('thebe_get_9grid_class'))
{
	function thebe_get_9grid_class( $align = null )
	{
		if( $align === null )
		{
			return '';
		}

		$index = intval( $align ) - 1;
		if( $index < 0 || $index > 8 )
		{
			return '';
		}

		$align_classes = array(
			'align-v-top align-h-left',
			'align-v-top align-h-center',
			'align-v-top align-h-right',
			'align-v-center align-h-left',
			'align-v-center align-h-center',
			'align-v-center align-h-right',
			'align-v-bottom align-h-left',
			'align-v-bottom align-h-center',
			'align-v-bottom align-h-right',
		);

		return $align_classes[$index];
	}
}

?>