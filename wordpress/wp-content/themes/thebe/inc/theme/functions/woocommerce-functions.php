<?php

/**
 * Theme setup
 */
add_action( 'after_setup_theme', 'thebe_theme_wc_setup' );
if( !function_exists('thebe_theme_wc_setup') )
{
	function thebe_theme_wc_setup()
	{
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );

		//------------   custom styles   -------------
		require_once THEBE_THEME_DIR . '/inc/theme/wc-styles.php';
	}
}


/**
 * Enqueue theme scripts/styles
 */
add_action( 'wp_enqueue_scripts', 'thebe_wc_scripts', 11 );
if(!function_exists('thebe_wc_scripts'))
{
	function thebe_wc_scripts()
	{
		wp_enqueue_style( 'thebe-wc-style', esc_url(THEBE_THEME_URL . '/css/wc.css') );
		wp_enqueue_script('thebe-wc-script', thebe_parse_js_path('wc', 'theme/'), array(), false, true);
	}
}

/**
 * Enqueue scripts for woocommerce
 */
add_action( 'admin_enqueue_scripts', 'thebe_wc_admin_enqueue_scripts', 11 );
if( !function_exists('thebe_wc_admin_enqueue_scripts') )
{
	function thebe_wc_admin_enqueue_scripts()
	{
		$page_id = get_the_id();
		$page_types = array( 'shop', 'cart', 'checkout', 'myaccount', 'terms' );

		$shop_page_ids = array(
			wc_get_page_id( $page_types[0] ),
			wc_get_page_id( $page_types[1] ),
			wc_get_page_id( $page_types[2] ),
			wc_get_page_id( $page_types[3] ),
			wc_get_page_id( $page_types[4] ),
		);

		$index = array_search( $page_id, $shop_page_ids );
		if( $index !== false )
		{
			$admin_js = '!function(){';
			$admin_js .= 'var o = {}; window.__ptWCObject = o;';
			$admin_js .= 'o.isAdminPage = true;';
			$admin_js .= 'o.pageType = "'.esc_js( $page_types[ $index ] ).'";';
			$admin_js .= 'o.pageTypesConst = '.json_encode( $page_types ).';';
			$admin_js .= '}();';

			wp_add_inline_script(
				'thebe-custom-post-page-js', 
				$admin_js, 
				'before'
			);
		}
	}
}


/**
 * Hide template selector when page belongs to woocommerce
 */
add_filter( 'theme_page_templates', 'thebe_wc_hide_cpt_archive_templates', 10, 3 );
if( !function_exists('thebe_wc_hide_cpt_archive_templates') )
{
	function thebe_wc_hide_cpt_archive_templates( $page_templates, $class, $post )
	{
		if( !$post )
		{
			return $page_templates;
		}

		$page_types = array( 'cart', 'checkout', 'myaccount', 'terms' );

		$shop_page_ids = array( 
			wc_get_page_id( $page_types[0] ),
			wc_get_page_id( $page_types[1] ),
			wc_get_page_id( $page_types[2] ),
			wc_get_page_id( $page_types[3] ),
		);

		if ( in_array( $post->ID, $shop_page_ids ) )
		{
			$page_templates = array();
		}

		return $page_templates;
	}
}