<?php
	if( !class_exists( 'Thebe_Shortcode_Options' ) )
	{
		class Thebe_Shortcode_Options
		{
			static public $data = null;

			static private $inited = false;

			static public function init_data()
			{
				if(self::$inited) return;
				self::$inited = true;

				self::$data = array(
					'pt_sc'=>array(
						array('var'=>'shortCodeText', 'label'=>'', 'default'=>'', 'type'=>'shortcode')
					)
				);
			}
		}
	}

	Thebe_Shortcode_Options::init_data();
?>