<?php 
	if( !class_exists( 'Thebe_Theme_Options' ) )
	{
		class Thebe_Theme_Options
		{
			static public $domain = 'Theme Options';
			static public $options_key = 'thebe_theme_options';
			static public $prefix = 'thebe_';
			static public $data = null;
			static public $wpml_options = null;

			static private $inited = false;

			static public function init_data()
			{
				if(self::$inited) return;
				self::$inited = true;

				self::$data = array(
					array(
						'title'=>esc_html__('Color', 'thebe'),
						'des'=>'',
						'data'=>array(
							array('var'=>'site_style', 'label'=>esc_html__('Site style', 'thebe'), 'default'=>'1', 'type'=>'select', 
								'options'=>array(
									'1' => esc_html__('Light', 'thebe'), 
									'2' => esc_html__('Dark', 'thebe')
								), 
								'des'=>''
							),
							array('var'=>'main_color', 'label'=>esc_html__('Main color', 'thebe'), 'default'=>'#05b14a', 'type'=>'color', 'des'=>''),
						)
					),
					array(
						'title'=>esc_html__('Menu', 'thebe'),
						'des'=>'',
						'data'=>array(
							array('var'=>'menu_style', 'label'=>esc_html__('Menu style', 'thebe'), 'default'=>'3', 'type'=>'image_select', 
								'options'=>array( '1', '2', '3' ), 
								'only_available'=>array(
									'1' => array('additional_menu'),
									'3' => array('additional_menu'),
								),
								'des'=>''
							),

							array('var'=>'additional_menu', 'label'=>esc_html__('Additional menu', 'thebe'), 'default'=>'', 'wpml'=>true, 'type'=>'group', 'des'=>'', 'class'=>'pt-hidden-menu-item', 'group_class'=>'pt-hidden-menu-data hidden-menu-option-item mini',
								'items'=>array(
									array('var'=>'title', 'label'=>esc_html__('Title', 'thebe'), 'default'=>'', 'type'=>'text', 'des'=>''),
									array('var'=>'link', 'label'=>esc_html__('Link', 'thebe'), 'default'=>'', 'type'=>'text', 'des'=>''),
									array('var'=>'src', 'label'=>esc_html__('Item image', 'thebe'), 'default'=>'', 'type'=>'image', 'des'=>'')
								)
							),

							array('var'=>'menu_text_color', 'label'=>esc_html__('Menu text color', 'thebe'), 'default'=>'white', 'type'=>'select', 
								'options'=>array(
									'white' => esc_html__('White', 'thebe'), 
									'black' => esc_html__('Black', 'thebe')
								), 
								'des'=>''
							),
							array('var'=>'menu_bg_color', 'label'=>esc_html__('Menu bg color', 'thebe'), 'default'=>'', 'type'=>'color', 'des'=>''),

							array('var'=>'mobile_menu_fixed', 'label'=>esc_html__('Fixed position (for mobile)', 'thebe'), 'default'=>'1', 'type'=>'checkbox', 'des'=>''),
						)
					),
					array(
						'title'=>esc_html__('Logo', 'thebe'),
						'des'=>'',
						'data'=>array(
							array('var'=>'logo', 'label'=>esc_html__('Logo', 'thebe'), 'default'=>'', 'type'=>'image', 'des'=>''),
							array('var'=>'logo_retina', 'label'=>esc_html__('Rentina logo', 'thebe'), 'default'=>'', 'type'=>'image', 'des'=>esc_html__('A retina logo image is the same as the normal logo image, though twice the size.', 'thebe')),

							array('var'=>'logo_invert', 'label'=>esc_html__('Logo (invert color)', 'thebe'), 'default'=>'', 'type'=>'image', 'des'=>''),
							array('var'=>'logo_invert_retina', 'label'=>esc_html__('Rentina logo (invert color)', 'thebe'), 'default'=>'', 'type'=>'image', 'des'=>''),
						)
					),
					array(
						'title'=>esc_html__('Popup', 'thebe'),
						'des'=>'',
						'data'=>array(
							array('var'=>'popup_button', 'label'=>esc_html__('Button label', 'thebe'), 'type'=>'text', 'default'=>esc_html__('Profile', 'thebe'), 'des'=>''),
							array('var'=>'popup_title', 'label'=>esc_html__('Title', 'thebe'), 'wpml'=>true, 'type'=>'text', 'default'=>'', 'des'=>''),
							array('var'=>'popup_content', 'label'=>esc_html__('Content', 'thebe'), 'default'=>'', 'wpml'=>true, 'type'=>'popup_textarea', 'btn_name'=>esc_html__('Edit content', 'thebe'), 'des'=>''),
							array('var'=>'popup_img', 'label'=>esc_html__('Image', 'thebe'), 'default'=>'', 'type'=>'image', 'des'=>''),
							array('var'=>'popup_style', 'label'=>esc_html__('Text color', 'thebe'), 'default'=>'white', 'type'=>'select', 
								'options'=>array(
									'white' => esc_html__('White', 'thebe'), 
									'black' => esc_html__('Black', 'thebe')
								), 
								'des'=>''
							),
							array('var'=>'popup_bg_color', 'label'=>esc_html__('Bg color', 'thebe'), 'default'=>'', 'type'=>'color', 'des'=>''),
						)
					),
					array(
						'title'=>esc_html__('Site', 'thebe'),
						'des'=>'',
						'data'=>array(
							array('var'=>'loading_img', 'label'=>esc_html__('Loading image', 'thebe'), 'default'=>'', 'type'=>'image', 'des'=>esc_html__('Upload your loading image(usually it is GIF or PNG) here.', 'thebe')),

							array('var'=>'music_src', 'label'=>esc_html__('Music path', 'thebe'), 'type'=>'text', 'default'=>'', 'des'=>esc_html__('If mutiple files please use comma to separate them.', 'thebe')),
							array('var'=>'music_volume', 'label'=>esc_html__('Music volume', 'thebe'), 'type'=>'text', 'default'=>'1', 'class'=>'col-2', 'des'=>''),
							array('var'=>'music_autoplay', 'label'=>esc_html__('Music autoplay', 'thebe'), 'type'=>'checkbox', 'default'=>'0', 'class'=>'col-2', 'des'=>''),

							array('var'=>'archives_img_raw', 'label'=>esc_html__('Raw proportion for archives images', 'thebe'), 'type'=>'checkbox', 'default'=>'1', 'class'=>'col-2', 'des'=>''),
							
							array('var'=>'show_search_button', 'label'=>esc_html__('Show search button', 'thebe'), 'type'=>'checkbox', 'default'=>'1', 'class'=>'col-2', 'des'=>''),
							array('var'=>'image_quality', 'label'=>esc_html__('Image quality', 'thebe'), 'default'=>'90', 'type'=>'text', 'des'=>esc_html__('It should be 50 - 100. Note: it affects new added image only.', 'thebe'), 'placeholder'=>''),
						)
					),
					array(
						'title'=>esc_html__('Fonts', 'thebe'),
						'des'=>'',
						'data'=>array(
							array('var'=>'normal_font_size', 'label'=>esc_html__('Normal font size', 'thebe'), 'default'=>'14', 'class'=>'sub-tab-1', 'tab_name'=>esc_html__('Font Setting', 'thebe'), 'type'=>'select', 
								'options'=>array(
									'11' => esc_html__('11 Pixel', 'thebe'), 
									'12' => esc_html__('12 Pixel', 'thebe'), 
									'13' => esc_html__('13 Pixel', 'thebe'), 
									'14' => esc_html__('14 Pixel', 'thebe'), 
									'15' => esc_html__('15 Pixel', 'thebe'), 
									'16' => esc_html__('16 Pixel', 'thebe'), 
									'17' => esc_html__('17 Pixel', 'thebe'), 
									'18' => esc_html__('18 Pixel', 'thebe')
								), 
								'des'=>''
							),
							
							array('var'=>'', 'label'=>esc_html__('Font weight', 'thebe'), 'class'=>'sub-tab-1', 'tab_name'=>esc_html__('Font Setting', 'thebe'), 'type'=>'title', 'des'=>''),
							array('var'=>'body_font_weight', 'label'=>esc_html__('Body', 'thebe'), 'default'=>'0', 'class'=>'sub-tab-1 col-3', 'tab_name'=>esc_html__('Font Setting', 'thebe'), 'type'=>'select', 
								'options'=>array(
									'0'=>'Default', 
									'100' => esc_html__('100', 'thebe'), 
									'200' => esc_html__('200', 'thebe'), 
									'300' => esc_html__('300', 'thebe'), 
									'400' => esc_html__('400', 'thebe'), 
									'500' => esc_html__('500', 'thebe'), 
									'600' => esc_html__('600', 'thebe'), 
									'700' => esc_html__('700', 'thebe'), 
									'800' => esc_html__('800', 'thebe'), 
									'900' => esc_html__('900', 'thebe')
								), 
								'des'=>''
							),
							array('var'=>'title_font_weight', 'label'=>esc_html__('Title', 'thebe'), 'default'=>'0', 'class'=>'sub-tab-1 col-3', 'tab_name'=>esc_html__('Font Setting', 'thebe'), 'type'=>'select', 
								'options'=>array(
									'0'=>'Default', 
									'100' => esc_html__('100', 'thebe'), 
									'200' => esc_html__('200', 'thebe'), 
									'300' => esc_html__('300', 'thebe'), 
									'400' => esc_html__('400', 'thebe'), 
									'500' => esc_html__('500', 'thebe'), 
									'600' => esc_html__('600', 'thebe'), 
									'700' => esc_html__('700', 'thebe'), 
									'800' => esc_html__('800', 'thebe'), 
									'900' => esc_html__('900', 'thebe')
								), 
								'des'=>''
							),
							array('var'=>'menu_font_weight', 'label'=>esc_html__('Menu', 'thebe'), 'default'=>'0', 'class'=>'sub-tab-1 col-3', 'tab_name'=>esc_html__('Font Setting', 'thebe'), 'type'=>'select', 
								'options'=>array(
									'0'=>'Default', 
									'100' => esc_html__('100', 'thebe'), 
									'200' => esc_html__('200', 'thebe'), 
									'300' => esc_html__('300', 'thebe'), 
									'400' => esc_html__('400', 'thebe'), 
									'500' => esc_html__('500', 'thebe'), 
									'600' => esc_html__('600', 'thebe'), 
									'700' => esc_html__('700', 'thebe'), 
									'800' => esc_html__('800', 'thebe'), 
									'900' => esc_html__('900', 'thebe')
								), 
								'des'=>''
							),
							

							array('var'=>'body_font_family', 'label'=>esc_html__('Body Font-family', 'thebe'), 'default'=>'', 'class'=>'sub-tab-2', 'tab_name'=>esc_html__('Font Family', 'thebe'), 'type'=>'font', 'des'=>''),
							array('var'=>'title_font_family', 'label'=>esc_html__('Title Font-family', 'thebe'), 'default'=>'', 'class'=>'sub-tab-2', 'tab_name'=>esc_html__('Font Family', 'thebe'), 'type'=>'font', 'des'=>''),
							array('var'=>'menu_font_family', 'label'=>esc_html__('Menu Font-family', 'thebe'), 'default'=>'', 'class'=>'sub-tab-2', 'tab_name'=>esc_html__('Font Family', 'thebe'), 'type'=>'font', 'des'=>''),

							array('var'=>'custom_font_weight1', 'label'=>esc_html__('Custom Font Weight', 'thebe'), 'default'=>'700', 'class'=>'sub-tab-3', 'tab_name'=>esc_html__('Custom Font 1', 'thebe'), 'type'=>'select', 
								'options'=>array(
									'100' => esc_html__('100', 'thebe'), 
									'200' => esc_html__('200', 'thebe'), 
									'300' => esc_html__('300', 'thebe'), 
									'400' => esc_html__('400', 'thebe'), 
									'500' => esc_html__('500', 'thebe'), 
									'600' => esc_html__('600', 'thebe'), 
									'700' => esc_html__('700', 'thebe'), 
									'800' => esc_html__('800', 'thebe'), 
									'900' => esc_html__('900', 'thebe')
								), 
								'des'=>''
							),
							array('var'=>'custom_font_family1', 'label'=>esc_html__('Custom Font-family', 'thebe'), 'default'=>'', 'class'=>'sub-tab-3', 'tab_name'=>esc_html__('Custom Font 1', 'thebe'), 'type'=>'font', 'des'=>''),
							array('var'=>'custom_font_selector1', 'label'=>esc_html__('Custom Font Selector', 'thebe'), 'default'=>'', 'class'=>'sub-tab-3', 'tab_name'=>esc_html__('Custom Font 1', 'thebe'), 'type'=>'code', 'height'=>150, 'editor'=>'css', 'des'=>esc_html__('Enter custom font selector (class, id, etc...) here, for example: .font-title', 'thebe')),

							array('var'=>'custom_font_weight2', 'label'=>esc_html__('Custom Font Weight', 'thebe'), 'default'=>'700', 'class'=>'sub-tab-4', 'tab_name'=>esc_html__('Custom Font 2', 'thebe'), 'type'=>'select', 
								'options'=>array(
									'100' => esc_html__('100', 'thebe'), 
									'200' => esc_html__('200', 'thebe'), 
									'300' => esc_html__('300', 'thebe'), 
									'400' => esc_html__('400', 'thebe'), 
									'500' => esc_html__('500', 'thebe'), 
									'600' => esc_html__('600', 'thebe'), 
									'700' => esc_html__('700', 'thebe'), 
									'800' => esc_html__('800', 'thebe'), 
									'900' => esc_html__('900', 'thebe')
								), 
								'des'=>''
							),
							array('var'=>'custom_font_family2', 'label'=>esc_html__('Custom Font-family', 'thebe'), 'default'=>'', 'class'=>'sub-tab-4', 'tab_name'=>esc_html__('Custom Font 2', 'thebe'), 'type'=>'font', 'des'=>''),
							array('var'=>'custom_font_selector2', 'label'=>esc_html__('Custom Font Selector', 'thebe'), 'default'=>'', 'class'=>'sub-tab-4', 'tab_name'=>esc_html__('Custom Font 2', 'thebe'), 'type'=>'code', 'height'=>150, 'editor'=>'css', 'des'=>esc_html__('Enter custom font selector (class, id, etc...) here, for example: .font-title', 'thebe'))
						)
					),
					array(
						'title'=>esc_html__('Map', 'thebe'),
						'des'=>'',
						'data'=>array(
							array('var'=>'map_api_key', 'label'=>esc_html__('Google map API key', 'thebe'), 'default'=>'', 'type'=>'text', 'des'=>esc_html__('Go to https://developers.google.com/maps/documentation/javascript/ and click "GET A KEY" to get your key, then type it into this field.', 'thebe')),
							array('var'=>'map_style', 'label'=>esc_html__('Map style', 'thebe'), 'default'=>'1', 'type'=>'select', 'use_preview'=>true, 
								'options'=>array(
									'0' => esc_html__('Basic', 'thebe'), 
									'1' => esc_html__('Style 1', 'thebe'), 
									'2' => esc_html__('Style 2', 'thebe'), 
									'3' => esc_html__('Style 3', 'thebe'), 
									'4' => esc_html__('Style 4', 'thebe'), 
									'5' => esc_html__('Style 5', 'thebe')
								), 
								'des'=>''
							),
							array('var'=>'map_marker', 'label'=>esc_html__('Custom marker', 'thebe'), 'default'=>'', 'type'=>'image', 'des'=>esc_html__('Upload custom map marker icon here.', 'thebe'))
						)
					),
					array(
						'title'=>esc_html__('Post & Share', 'thebe'),
						'des'=>'',
						'data'=>array(
							array('var'=>'use_post_footer_info', 'label'=>esc_html__('Show Post footer info', 'thebe'), 'default'=>'1', 'type'=>'checkbox', 'class'=>'sub-tab-1', 
							'only_available'=>array(
								'1' => array('post_footer_img', 'post_footer_title', 'post_footer_subtitle', 'post_footer_intro'),
							),'tab_name'=>esc_html__('Post footer', 'thebe'), 'des'=>''),
							array('var'=>'post_footer_img', 'label'=>esc_html__('Post footer image', 'thebe'), 'default'=>'', 'type'=>'image', 'class'=>'sub-tab-1','des'=>''),
							array('var'=>'post_footer_title', 'label'=>esc_html__('Post footer title', 'thebe'), 'wpml'=>true, 'type'=>'text', 'default'=>esc_html__('You can edit this in Dashboard.', 'thebe'), 'class'=>'sub-tab-1', 'tab_name'=>esc_html__('Post footer', 'thebe'), 'des'=>''),
							array('var'=>'post_footer_subtitle', 'label'=>esc_html__('Post footer subtitle', 'thebe'), 'wpml'=>true, 'type'=>'text', 'default'=>esc_html__('Subtitle', 'thebe'), 'class'=>'sub-tab-1', 'tab_name'=>esc_html__('Post footer', 'thebe'), 'des'=>''),
							array('var'=>'post_footer_intro', 'label'=>esc_html__('Post footer intro', 'thebe'), 'wpml'=>true, 'type'=>'textarea', 'default'=>esc_html__('Mauris sit amet mi enim. Donec et leo fringilla nunc tincidunt pulvinar. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris in ultrices dui, ac laoreet sem. Aenean eu sem ante. Morbi sed posuere nunc, nec ullamcorper arcu.
							', 'thebe'), 'class'=>'sub-tab-1', 'tab_name'=>esc_html__('Post footer', 'thebe'), 'rows'=>5, 'des'=>''),
							
							array('var'=>'use_facebook', 'label'=>esc_html__('Enable Facebook', 'thebe'), 'type'=>'checkbox', 'default'=>'1', 'class'=>'sub-tab-2 col-3', 'tab_name'=>esc_html__('Share', 'thebe'), 'des'=>''),
							array('var'=>'use_twitter', 'label'=>esc_html__('Enable Twitter', 'thebe'), 'type'=>'checkbox', 'default'=>'1', 'class'=>'sub-tab-2 col-3', 'tab_name'=>esc_html__('Share', 'thebe'), 'des'=>''),
							array('var'=>'use_pinterest', 'label'=>esc_html__('Enable Pinterest', 'thebe'), 'type'=>'checkbox', 'default'=>'1', 'class'=>'sub-tab-2 col-3', 'tab_name'=>esc_html__('Share', 'thebe'), 'des'=>''),
							array('var'=>'use_tumblr', 'label'=>esc_html__('Enable Tumblr', 'thebe'), 'type'=>'checkbox', 'default'=>'0', 'class'=>'sub-tab-2 col-3', 'tab_name'=>esc_html__('Share', 'thebe'), 'des'=>''),
							array('var'=>'use_googleplus', 'label'=>esc_html__('Enable Google+', 'thebe'), 'type'=>'checkbox', 'default'=>'0', 'class'=>'sub-tab-2 col-3', 'tab_name'=>esc_html__('Share', 'thebe'), 'des'=>''),
							array('var'=>'use_linkedin', 'label'=>esc_html__('Enable Linkedin', 'thebe'), 'type'=>'checkbox', 'default'=>'0', 'class'=>'sub-tab-2 col-3', 'tab_name'=>esc_html__('Share', 'thebe'), 'des'=>''),
                            array('var'=>'share_group', 'label'=>esc_html__('Custom Share', 'thebe'), 'default'=>'', 'class'=>'share-social custom-social sub-tab-2 col-1', 'type'=>'group', 'tab_name'=>esc_html__('Share', 'thebe'),
                                'add_item_label' => esc_html__('Add Icon', 'thebe'),
                                'items' => array(
                                    array(
                                        'var' => 'icon',
                                        'type' => 'icon',
                                        'label'=>esc_html__('Icon', 'thebe'),
                                        'placeholder'=>esc_html__('Social icon class ...', 'thebe'),
                                    ),
                                    array(
                                        'var' => 'link',
                                        'type' => 'text',
                                        'label'=>esc_html__('Link', 'thebe'),
                                        'placeholder'=>esc_html__('Social link ...', 'thebe'),
                                    ),
                                )
                            ),

							array('var'=>'show_comment', 'label'=>esc_html__('Show comment', 'thebe'), 'default'=>'1', 'type'=>'checkbox', 'class'=>'sub-tab-3', 'tab_name'=>esc_html__('Comments', 'thebe'), 'des'=>esc_html__('If select No, this site will not show comment (Blog Post & Page, excluded Project).', 'thebe')),
							array('var'=>'show_project_comment', 'label'=>esc_html__('Show Project comment', 'thebe'), 'type'=>'checkbox', 'class'=>'sub-tab-3', 'tab_name'=>esc_html__('Comments', 'thebe'), 'des'=>esc_html__('If select No, this site will not show Project comment.', 'thebe')),
						)
					),
					array(
						'title'=>esc_html__('Project', 'thebe'),
						'des'=>'',
						'data'=>array(
							array('var'=>'project_slug', 'label'=>esc_html__('Project slug', 'thebe'), 'default'=>'project_list', 'type'=>'text', 'des'=>esc_html__('WordPress generates the permalink of archive of Project with this slug, so it should be an unique string. When finish changing you need to goto Dashboard &raquo; Settings &raquo; Permalinks and click \'Save Changes\', then refresh the browser. The same as below "Project taxonomy slug" and "Project tag slug".', 'thebe'), 'placeholder'=>''),
							array('var'=>'project_taxonomy_slug', 'label'=>esc_html__('Project taxonomy slug', 'thebe'), 'default'=>'project_cat', 'type'=>'text', 'des'=>'', 'placeholder'=>''),
							array('var'=>'project_tag_slug', 'label'=>esc_html__('Project tag slug', 'thebe'), 'default'=>'project_tag', 'type'=>'text', 'des'=>'', 'placeholder'=>'')
						)
					), 
					array(
						'title'=>esc_html__('Site Socials', 'thebe'),
						'des'=>'',
						'data'=>array(
							array('var'=>'social_facebook', 'label'=>esc_html__('Facebook', 'thebe'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thebe')),
							array('var'=>'social_twitter', 'label'=>esc_html__('Twitter', 'thebe'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thebe')),
							array('var'=>'social_pinterest', 'label'=>esc_html__('Pinterest', 'thebe'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thebe')),
							array('var'=>'social_flickr', 'label'=>esc_html__('Flickr', 'thebe'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thebe')),
							array('var'=>'social_instagram', 'label'=>esc_html__('Instagram', 'thebe'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thebe')),
							array('var'=>'social_linkedin', 'label'=>esc_html__('LinkedIn', 'thebe'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thebe')),
							array('var'=>'social_whatsapp', 'label'=>esc_html__('Whats app', 'thebe'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thebe')),
							array('var'=>'social_behance', 'label'=>esc_html__('Behance', 'thebe'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thebe')),
							array('var'=>'social_tumblr', 'label'=>esc_html__('Tumblr', 'thebe'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thebe')),
							array('var'=>'social_googleplus', 'label'=>esc_html__('Google+', 'thebe'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thebe')),
							array('var'=>'social_vimeo', 'label'=>esc_html__('Vimeo', 'thebe'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thebe')),
							array('var'=>'social_youtube', 'label'=>esc_html__('Youtube', 'thebe'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thebe')),
							array('var'=>'social_dribbble', 'label'=>esc_html__('Dribbble', 'thebe'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thebe')),
							array('var'=>'social_email', 'label'=>esc_html__('Email', 'thebe'), 'default'=>'', 'class'=>'col-2 small-option-gap', 'type'=>'text', 'des'=>'', 'placeholder'=>esc_html__('Social link ...', 'thebe')),
                            
                            array('var'=>'social_group', 'label'=>esc_html__('Custom Socials', 'thebe'), 'default'=>'', 'class'=>'site-social custom-social col-1', 'type'=>'group', 
                                'add_item_label' => esc_html__('Add Icon', 'thebe'),
                                'items' => array(
                                    array(
                                        'var' => 'icon',
                                        'type' => 'icon',
                                        'label'=>esc_html__('Icon', 'thebe'),
                                        'placeholder'=>esc_html__('Social icon class ...', 'thebe'),
                                    ),
                                    array(
                                        'var' => 'link',
                                        'type' => 'text',
                                        'label'=>esc_html__('Link', 'thebe'),
                                        'placeholder'=>esc_html__('Social link ...', 'thebe'),
                                    ),
                                )
                            ),
                        )
					),
					array(
						'title'=>esc_html__('Footer', 'thebe'),
						'des'=>'',
						'data'=>array(
							array('var'=>'footer_widgets_wide', 'label'=>esc_html__('Wide (widgets area)', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 'des'=>esc_html__('Wide or Boxed.', 'thebe')),

							array('var'=>'footer_copyright', 'label'=>esc_html__('Copyright', 'thebe'), 'default'=>esc_html__('Designed by ForeverPinetree', 'thebe'), 'wpml'=>true, 'type'=>'text', 'des'=>''),
							array('var'=>'copyright_position', 'label'=>esc_html__('Copyright position', 'thebe'), 'default'=>'1', 'type'=>'select',
								'options'=>array(
									'1' => esc_html__('Footer widget', 'thebe'), 
									'2' => esc_html__('Popup', 'thebe'), 
								), 
								'des'=>''
							),
							array('var'=>'footer_bg', 'label'=>esc_html__('Background image', 'thebe'), 'default'=>'', 'type'=>'image', 'des'=>''),
							array('var'=>'footer_text_color', 'label'=>esc_html__('Text color', 'thebe'), 'default'=>'white', 'type'=>'select', 
								'options'=>array(
									'white' => esc_html__('White', 'thebe'), 
									'black' => esc_html__('Black', 'thebe'),
								), 
								'des'=>''
							),
						)
					),
					array(
						'title'=>esc_html__('SEO', 'thebe'),
						'des'=>'',
						'data'=>array(
							array('var'=>'seo_keywords', 'label'=>esc_html__('SEO keywords', 'thebe'), 'default'=>'', 'wpml'=>true, 'type'=>'textarea', 'rows'=>2, 'des'=>esc_html__('It may be overridden by single posts & pages.', 'thebe')),
							array('var'=>'seo_description', 'label'=>esc_html__('SEO description', 'thebe'), 'default'=>'', 'wpml'=>true, 'type'=>'textarea', 'rows'=>5, 'des'=>esc_html__('It may be overridden by single posts & pages.', 'thebe')),
							array('var'=>'seo_enable', 'label'=>esc_html__('Use built-in fileds', 'thebe'), 'default'=>'1', 'type'=>'checkbox', 'des'=>esc_html__('Turn it to "NO" if you want to use external SEO plugin.', 'thebe')),
						)
					),
					array(
						'title'=>esc_html__('404 & Search', 'thebe'),
						'des'=>'',
						'data'=>array(
							array('var'=>'not_found_page_msg', 'label'=>esc_html__('404 warning', 'thebe'), 'default'=>'', 'wpml'=>true, 'type'=>'textarea', 'rows'=>5, 'des'=>''),
							array('var'=>'not_found_content_msg', 'label'=>esc_html__('No-Content warning', 'thebe'), 'default'=>'', 'wpml'=>true, 'type'=>'textarea', 'rows'=>5, 'des'=>'')
						)
					),
					array(
						'title'=>esc_html__('Import & Export', 'thebe'),
						'des'=>'',
						'data'=>array(
							array('var'=>'', 'label'=>esc_html__('Import options', 'thebe'), 'type'=>'title', 'des'=>esc_html__('Import options from *.json file. (Note: before importing you\'d better backup your options via Export options.)', 'thebe')),
							array('var'=>'', 'class'=>'importUpdateOptions', 'label'=>esc_html__('Import', 'thebe'), 'default'=>'', 'type'=>'button', 'des'=>''),

							array('var'=>'', 'label'=>esc_html__('Export options', 'thebe'), 'type'=>'title', 'des'=>esc_html__('Export options to *.json file. If your browser blocks the download, you should set the browser to allow it. If your browser opens the *.json file directly, you should right-click the mouse then save it as *.json file. Also, you can get the *.json file from {theme}/export-options/ folder.', 'thebe')),
							array('var'=>'', 'class'=>'exportOptions', 'label'=>esc_html__('Export', 'thebe'), 'default'=>'', 'type'=>'button')
						)
					),
				);

				// ----------------- start ---------------------
				self::_start();
			}

			static private function _start()
			{
				if( !self::$wpml_options )
				{
					self::$wpml_options = self::_get_register_wpml_string_data();
				}
				global $thebe_global_theme_options;

				if( !isset( $thebe_global_theme_options ))
				{
					$options_key = self::$options_key;
					$options = get_option( $options_key, '' );

					if( $options === '' )
					{
						$options = self::_get_data_from_db();
						if( get_option( $options_key ) === false )
						{
							add_option( $options_key, $options, '', 'yes' );
							self::register_wpml_theme_options( $options );
						}
						else
						{
							update_option( $options_key, $options );
						}
					}
					
					$thebe_global_theme_options = json_decode( $options, true );
				}
			}

			static private function _get_data_from_db()
			{
				$data = self::$data;
				$len = count($data);
				$result = array();
				$prefix = self::$prefix;
				for($i = 0; $i < $len; $i ++)
				{
					$obj = $data[$i];
					$arr = $obj['data'];
					foreach($arr as $value)
					{
						if( $value['var'] )
						{
							$key = $prefix.$value['var'];
							$result[$key] = get_option($key, isset($value['default']) ? $value['default'] : '');
						}
					}
				}

				return json_encode( $result );
			}

			static private function _get_register_wpml_string_data()
			{
				$data = self::$data;
				$len = count($data);
				$result = array();
				$prefix = self::$prefix;
				for($i = 0; $i < $len; $i ++)
				{
					$obj = $data[$i];
					$arr = $obj['data'];
					foreach($arr as $value)
					{
						if( $value['var'] && isset($value['wpml']) && $value['wpml'] )
						{
							$id = $prefix.$value['var'];
							$result[$id] = array(
								'id' => $id,
								'title' => $obj['title'].' - '.$value['label'],
							);
						}
					}
				}

				return $result;
			}

			static public function check_wpml( $key )
			{
				if( isset( self::$wpml_options[$key] ) )
				{
					return self::$wpml_options[$key];
				}
				return null;
			}

			static public function register_wpml_theme_options( $json_str )
			{
				if ( !function_exists( 'icl_register_string' ) )
				{
					return;
				}
		
				$json = json_decode( $json_str, true );
				
				$wpml_options = self::$wpml_options;
				if( is_array($wpml_options) )
				{
					foreach( $wpml_options as $option )
					{
						$id = $option['id'];
		
						if( !isset( $json[$id]) )
						{
							continue;
						}

						$value = $json[$id];
						icl_register_string( self::$domain, $option['title'], $value );
					}
				}
			}

			static public function get_keys()
			{
				$data = self::$data;
				$len = count($data);
				$json = array();
				$prefix = self::$prefix;
				$keys = array();
				for($i = 0; $i < $len; $i ++)
				{			
					$obj = $data[$i];
					$arr = $obj['data'];
					foreach($arr as $value)
					{
						if($value['var'] != '')
						{
							$keys[] = $prefix.$value['var'];
						}
					}
				}
				return $keys;
			}
		}
	}

	Thebe_Theme_Options::init_data();

	add_action( 'update_option_'.Thebe_Theme_Options::$options_key, 'thebe_update_theme_options_hook', 10, 2 );
	if( !function_exists( 'thebe_update_theme_options_hook' ) )
	{
		function thebe_update_theme_options_hook( $old_value, $value )
		{
			Thebe_Theme_Options::register_wpml_theme_options( $value );
		}
	}
?>