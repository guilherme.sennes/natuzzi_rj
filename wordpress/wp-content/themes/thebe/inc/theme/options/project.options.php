<?php
	if( !class_exists( 'Thebe_Project_Options' ) )
	{
		class Thebe_Project_Options
		{
			static public $data = null;

			static private $inited = false;

			static public function init_data()
			{
				if(self::$inited) return;
				self::$inited = true;

				self::$data = array(
					'pt_project'=>array(

						array('var'=>'project_style', 'label'=>esc_html__('Project style', 'thebe'), 'default'=>'1', 'type'=>'image_select', 
							'class'=>'project-tab-01 tab-01',
							'options'=>array('1', '2', '3', '4', '5'), 
							'only_available' => array(
								'2' => array('header_color'),
							),
							'des'=>''
						),

						array('var'=>'additional_img', 'label'=>esc_html__('Additional image', 'thebe'), 'default'=>'', 'class'=>'project-tab-01 tab-01', 'type'=>'image', 'help'=>'additional_img?w=600&h=400&ox=0&oy=-180&bc=#eeeeee', 'des'=>''),

						array('var'=>'header_color', 'label'=>esc_html__('Header color', 'thebe'), 'default'=>'white', 'type'=>'select', 'class'=>'project-tab-01 tab-01',
							'options'=>array(
								'white' => esc_html__('White', 'thebe'), 
								'black' => esc_html__('Black', 'thebe'), 
							), 
							'help'=>'header_color?w=600&h=400&ox=0&oy=-180&bc=#eeeeee', 
							'des'=>''
						),
						array('var'=>'title_color', 'label'=>esc_html__('Title color', 'thebe'), 'type'=>'color', 'class'=>'project-tab-01 tab-01', 'des'=>''),

						array('var'=>'lightbox_mode', 'label'=>esc_html__('Lightbox mode', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 'class'=>'project-tab-01 tab-01','des'=>esc_html__('If choose "Yes", click on an item will directly load all of the images of this project into a lightbox instead of opening the single page.', 'thebe')),

						array('var'=>'banner_type', 'label'=>esc_html__('Banner type', 'thebe'), 'default'=>'1', 'type'=>'image_select', 'class'=>'project-tab-02 tab-02',
							'options'=>array('1', '2', '3'),
							'only_available' => array(
								'1' => array('image_list', 'rawsize', 'banner_autoplay', 'banner_duration&banner_autoplay=1'),//slider
								'2' => array('image_list', 'shape_type', 'last_row&shape_type=type-auto'),//lightbox
								'3' => array('video_type', 'video_link', 'video_volume', 'video_bg_img')//video
							),
							'des'=>''
						),
						array('var'=>'banner_autoplay', 'label'=>esc_html__('Autoplay banner', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 'class'=>'project-tab-02 tab-02',
							'only_available' => array(
								'1' => array('banner_duration&banner_type=1'),
							),
						'des'=>''),
						array('var'=>'banner_duration', 'label'=>esc_html__('Duration', 'thebe'), 'default'=>'5000', 'type'=>'text', 'class'=>'project-tab-02 tab-02 mini-width', 'des'=>esc_html__('Banner play time(milliseconds).', 'thebe')),

						array('var'=>'video_type', 'label'=>esc_html__('Video type', 'thebe'), 'default'=>'1', 'type'=>'select', 'class'=>'project-tab-02 tab-02',
							'options'=>array(
								'1' => esc_html__('*.mp4', 'thebe'), 
								'2' => esc_html__('Youtube', 'thebe'), 
								'3' => esc_html__('Vimeo', 'thebe')
							), 
							'des'=>''
						),
						array('var'=>'video_link', 'label'=>esc_html__('Video link', 'thebe'), 'default'=>'', 'type'=>'text', 'class'=>'project-tab-02 tab-02', 'help'=>'video_link?w=600&h=400&ox=0&oy=-180&bc=#eeeeee', 'des'=>''),
						array('var'=>'video_volume', 'label'=>esc_html__('Video volume', 'thebe'), 'default'=>'1', 'type'=>'text', 'class'=>'project-tab-02 tab-02', 'des'=>esc_html__('From 0 to 1.', 'thebe')),
						array('var'=>'video_bg_img', 'label'=>esc_html__('Video bg image', 'thebe'), 'default'=>'', 'type'=>'image', 'class'=>'project-tab-02 tab-02', 'des'=>''),

						array('var'=>'image_list', 'label'=>esc_html__('Main images', 'thebe'), 'default'=>'', 'type'=>'imagelist', 'class'=>'project-tab-02 tab-02', 'des'=>esc_html__('They are shown in single page.', 'thebe')),

						array('var'=>'shape_type', 'label'=>esc_html__('Shape type', 'thebe'), 'default'=>'type-h', 'type'=>'image_select', 'class'=>'project-tab-02 tab-02', 'remove_path_prefix'=>true, 
							'options'=>array( 'type-h', 'type-v', 'type-s', 'type-auto' ), 
							'only_available' => array(
								'type-auto' => array('last_row&banner_type=2'),
							),
							'des'=>''
						),

						array('var'=>'last_row', 'label'=>esc_html__('Last row type', 'thebe'), 'default'=>'1', 'type'=>'image_select', 'class'=>'project-tab-02 tab-02', 'remove_path_prefix'=>true, 
							'options'=>array( '1', '2' ), 
							'des'=>''
						),

						array('var'=>'rawsize', 'label'=>esc_html__('Raw proportion', 'thebe'), 'default'=>'1', 'type'=>'checkbox', 'class'=>'project-tab-02 tab-02', 'des'=>''),

						array('var'=>'custom_meta', 'label'=>esc_html__('Custom meta', 'thebe'), 'default'=>'', 'type'=>'group', 'class'=>'pt-custom-meta-item project-tab-02 tab-02', 'group_class'=>'pt-custom-meta-data mini',
							'items'=>array(
								array('var'=>'title', 'label'=>esc_html__('Item title', 'thebe'), 'default'=>'', 'type'=>'text', 'des'=>''),
								array('var'=>'des', 'label'=>esc_html__('Item text', 'thebe'), 'default'=>'', 'type'=>'textarea', 'rows'=>6, 'des'=>''),
							),
							'des'=>''
						),

						array('var'=>'ajax_bg_color', 'label'=>esc_html__('AJAX bg color', 'thebe'), 'default'=>'', 'type'=>'color', 'class'=>'project-tab-02 tab-02', 'des'=>''),
						array('var'=>'ajax_text_color', 'label'=>esc_html__('AJAX text color', 'thebe'), 'default'=>'white', 'type'=>'select', 'class'=>'project-tab-02 tab-02',
							'options'=>array(
								'white' => esc_html__('White', 'thebe'), 
								'black' => esc_html__('Black', 'thebe'), 
							), 
							'des'=>''
						),

						array('var'=>'custom_hyperlink', 'label'=>esc_html__('Custom hyperlink', 'thebe'), 'default'=>'', 'type'=>'text', 'class'=>'project-tab-01 tab-01', 'des'=>esc_html__('You can set a custom hyperlink to cover the project original hyperlink.', 'thebe')),

						//----------------- Extra gallery option -----------------
						array('var'=>'setting_box_title', 'default'=>'', 'type'=>'text', 'class'=>'project-tab-02 tab-02', 'des'=>esc_html__('Extra gallery option', 'thebe')),
						
						array('var'=>'post_gallery', 'label'=>esc_html__('Extra image', 'thebe'), 'default'=>'', 'type'=>'image-group', 'class'=>'project-tab-02 tab-02',
							'extend' => array(
								'gap' => array('type'=>'checkbox', 'label'=>esc_html__('Bottom gap', 'thebe'), 'default'=>'1'),
								'display' => array('type'=>'select', 'label'=>esc_html__('Display', 'thebe'), 'options'=>array(
									'boxed' => esc_html__('Boxed', 'thebe'),
									'fullwidth' => esc_html__('Wide', 'thebe')
								), 'default'=>'fullwidth'),
								'title' => array('type'=>'text', 'label'=>esc_html__('Title', 'thebe'), 'default'=>''),
								'des' => array('type'=>'textarea', 'label'=>esc_html__('Description', 'thebe'), 'default'=>''),
								'video_type' => array('type'=>'select', 'label'=>esc_html__('Video type', 'thebe'), 'options'=>array(
									'0' => esc_html__('None', 'thebe'),
									'1' => esc_html__('*.mp4', 'thebe'), 
									'2' => esc_html__('Youtube', 'thebe'), 
									'3' => esc_html__('Vimeo', 'thebe')
								), 'default'=>'0'),
								'video_link' => array('type'=>'text', 'label'=>esc_html__('Video link', 'thebe'), 'default'=>''),
								'video_volume' => array('type'=>'text', 'label'=>esc_html__('Video volume', 'thebe'), 'default'=>''),
							),
							'hide_cat'=>true,
							'des'=>''
						),
					)
				);
			}
		}
	}

	Thebe_Project_Options::init_data();
?>