<?php
	if( !class_exists( 'Thebe_Global_Options' ) )
	{
		class Thebe_Global_Options
		{
			static public $bg_data = null;
			static public $seo_data = null;

			static private $inited = false;

			static public function init_data()
			{
				if(self::$inited) return;
				self::$inited = true;

				self::$bg_data = array(
					'pt_bg'=>array(
						array('var'=>'site_bg_type', 'label'=>esc_html__('Page bg type', 'thebe'), 'default'=>'1', 'type'=>'select', 
							'options'=>array(
								'1' => esc_html__('None', 'thebe'), 
								'2' => esc_html__('Media', 'thebe'), 
								'3' => esc_html__('CSS', 'thebe'),
							), 
							'only_available' => array(
								'2' => array( 'site_bg', 'site_bg_mask', 'site_bg_use_video', 'site_bg_video_type&site_bg_use_video=1', 'site_bg_video_link&site_bg_use_video=1', ),
								'3' => array( 'site_css_bg_type' ),
							),
							'des'=>''
						),

						array('var'=>'site_css_bg_type', 'label'=>esc_html__('CSS bg type', 'thebe'), 'default'=>'1', 'type'=>'image_select',
							'options'=>array('1', '2'), 
							'des'=>''
						),

						array('var'=>'site_bg', 'label'=>esc_html__('Page bg image', 'thebe'), 'default'=>'', 'type'=>'image', 'des'=>''),

						array('var'=>'site_bg_mask', 'label'=>esc_html__('Page bg mask', 'thebe'), 'default'=>'', 'type'=>'color', 'des'=>''),

						array('var'=>'site_bg_use_video', 'label'=>esc_html__('Page bg use video', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 
							'only_available' => array(
								'1' => array( 'site_bg_video_type&site_bg_type=2', 'site_bg_video_link&site_bg_type=2' )//video
							),
							'des'=>''
						),
						array('var'=>'site_bg_video_type', 'label'=>esc_html__('Page bg video type', 'thebe'), 'default'=>'1', 'type'=>'select',
							'options'=>array(
								'1' => esc_html__('*.mp4', 'thebe'), 
								'2' => esc_html__('Youtube', 'thebe'), 
								'3' => esc_html__('Vimeo', 'thebe')
							), 
							'des'=>''
						),
						array('var'=>'site_bg_video_link', 'label'=>esc_html__('Page bg video link', 'thebe'), 'default'=>'', 'type'=>'text', 'help'=>'video_link?w=600&h=400&ox=0&oy=-180&bc=#eeeeee', 'des'=>''),
					)
				);

				self::$seo_data = array(
					'pt_seo'=>array(
						array('var'=>'seo_keywords', 'label'=>esc_html__('SEO keywords', 'thebe'), 'default'=>'', 'type'=>'textarea', 'rows'=>2, 'des'=>esc_html__('These settings overrides Theme Options settings.', 'thebe')),
						array('var'=>'seo_description', 'label'=>esc_html__('SEO description', 'thebe'), 'default'=>'', 'type'=>'textarea', 'rows'=>5, 'des'=>esc_html__('These settings overrides Theme Options settings.', 'thebe')),
					)
				);
			}
		}
	}

	Thebe_Global_Options::init_data();
?>