<?php
	if( !class_exists( 'Thebe_Post_Options' ) )
	{
		class Thebe_Post_Options
		{
			static public $data = null;

			static private $inited = false;

			static public function init_data()
			{
				if(self::$inited) return;
				self::$inited = true;

				self::$data = array(
					'pt_post'=>array(
						
						array('var'=>'banner_type', 'label'=>esc_html__('Banner type', 'thebe'), 'default'=>'1', 'type'=>'image_select',
							'options'=>array('1', '2', '3'),
							'only_available' => array(
								'1' => array('image_list', 'rawsize', 'banner_autoplay', 'banner_duration&banner_autoplay=1'),//slider
								'2' => array('image_list', 'shape_type', 'rawsize', 'last_row&shape_type=type-auto'),//lightbox
								'3' => array('video_type', 'video_link', 'video_volume', 'video_bg_img')//video
							),
							'des'=>''
						),

						array('var'=>'banner_autoplay', 'label'=>esc_html__('Autoplay banner', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 
							'only_available' => array(
								'1' => array('banner_duration&banner_type=1'),
							),
						'des'=>''),
						array('var'=>'banner_duration', 'label'=>esc_html__('Duration', 'thebe'), 'default'=>'5000', 'type'=>'text', 'class'=>'mini-width', 'des'=>esc_html__('Banner play time(milliseconds).', 'thebe')),

						array('var'=>'video_type', 'label'=>esc_html__('Video type', 'thebe'), 'default'=>'1', 'type'=>'select',
							'options'=>array(
								'1' => esc_html__('*.mp4', 'thebe'), 
								'2' => esc_html__('Youtube', 'thebe'), 
								'3' => esc_html__('Vimeo', 'thebe')
							), 
							'des'=>''
						),
						array('var'=>'video_link', 'label'=>esc_html__('Video link', 'thebe'), 'default'=>'', 'type'=>'text', 'help'=>'video_link?w=600&h=400&ox=0&oy=-180&bc=#eeeeee', 'des'=>''),
						array('var'=>'video_volume', 'label'=>esc_html__('Video volume', 'thebe'), 'default'=>'1', 'type'=>'text', 'des'=>esc_html__('From 0 to 1.', 'thebe')),
						array('var'=>'video_bg_img', 'label'=>esc_html__('Video bg image', 'thebe'), 'default'=>'', 'type'=>'image', 'des'=>''),

						array('var'=>'image_list', 'label'=>esc_html__('Main images', 'thebe'), 'default'=>'', 'type'=>'imagelist', 'des'=>esc_html__('They are shown in single page.', 'thebe')),

						array('var'=>'shape_type', 'label'=>esc_html__('Shape type', 'thebe'), 'default'=>'type-h', 'type'=>'image_select', 'remove_path_prefix'=>true, 
							'options'=>array( 'type-h', 'type-v', 'type-s', 'type-auto' ), 
							'only_available' => array(
								'type-auto' => array('last_row&banner_type=2'),
							),
							'des'=>''
						),

						array('var'=>'last_row', 'label'=>esc_html__('Last row type', 'thebe'), 'default'=>'1', 'type'=>'image_select', 'remove_path_prefix'=>true, 
							'options'=>array( '1', '2' ), 
							'des'=>''
						),

						array('var'=>'rawsize', 'label'=>esc_html__('Raw proportion', 'thebe'), 'default'=>'1', 'type'=>'checkbox', 'des'=>''),

						array('var'=>'ajax_bg_color', 'label'=>esc_html__('AJAX bg color', 'thebe'), 'default'=>'', 'type'=>'color', 'des'=>''),
						array('var'=>'ajax_text_color', 'label'=>esc_html__('AJAX text color', 'thebe'), 'default'=>'white', 'type'=>'select',
							'options'=>array(
								'white' => esc_html__('White', 'thebe'), 
								'black' => esc_html__('Black', 'thebe'), 
							), 
							'des'=>''
						),
						
						//----------------- Extra gallery option -----------------
						array('var'=>'setting_box_title', 'default'=>'', 'type'=>'text', 'des'=>esc_html__('Extra gallery option', 'thebe')),
						
						array('var'=>'post_gallery', 'label'=>esc_html__('Extra image', 'thebe'), 'default'=>'', 'type'=>'image-group',
							'extend' => array(
								'gap' => array('type'=>'checkbox', 'label'=>esc_html__('Bottom gap', 'thebe'), 'default'=>'1'),
								'display' => array('type'=>'select', 'label'=>esc_html__('Display', 'thebe'), 'options'=>array(
									'boxed' => esc_html__('Boxed', 'thebe'),
									'fullwidth' => esc_html__('Wide', 'thebe')
								), 'default'=>'fullwidth'),
								'title' => array('type'=>'text', 'label'=>esc_html__('Title', 'thebe'), 'default'=>''),
								'des' => array('type'=>'textarea', 'label'=>esc_html__('Description', 'thebe'), 'default'=>''),
								'video_type' => array('type'=>'select', 'label'=>esc_html__('Video type', 'thebe'), 'options'=>array(
									'0' => esc_html__('None', 'thebe'),
									'1' => esc_html__('*.mp4', 'thebe'), 
									'2' => esc_html__('Youtube', 'thebe'), 
									'3' => esc_html__('Vimeo', 'thebe')
								), 'default'=>'0'),
								'video_link' => array('type'=>'text', 'label'=>esc_html__('Video link', 'thebe'), 'default'=>''),
								'video_volume' => array('type'=>'text', 'label'=>esc_html__('Video volume', 'thebe'), 'default'=>''),
							),
							'hide_cat'=>true,
							'des'=>''
						),
					)
				);
			}
		}
	}

	Thebe_Post_Options::init_data();
?>