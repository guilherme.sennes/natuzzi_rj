<?php
	if( !class_exists( 'Thebe_Product_Options' ) )
	{
		class Thebe_Product_Options
		{
			static public $data = null;

			static private $inited = false;

			static public function init_data()
			{
				if(self::$inited) return;
				self::$inited = true;

				self::$data = array(
					'pt_product'=>array(
					)
				);
			}
		}
	}

	Thebe_Product_Options::init_data();
?>