<?php
	if( !class_exists( 'Thebe_Page_Options' ) )
	{
		class Thebe_Page_Options
		{
			static public $basic_data = null;
			static public $data = null;

			static private $inited = false;

			static public function init_data()
			{
				if(self::$inited) return;
				self::$inited = true;

				self::$basic_data = array(
					'pt_basic'=>array(
						array('var'=>'title_type', 'label'=>esc_html__('Type', 'thebe'), 'default'=>'4', 'type'=>'image_select',
							'options'=>array('4', '1', '2', '3', ), 
							'only_available' => array(
								'1' => array( 'title_bg', 'title_bg_color', 'title_parallax', 'title_header_color', 'use_video', 'video_type&use_video=1', 'video_link&use_video=1', ),
								'2' => array( 'title_bg', 'title_bg_color', 'title_parallax', 'title_header_color', 'use_video', 'video_type&use_video=1', 'video_link&use_video=1', ),
								'3' => array( 'title_bg', 'title_bg_color', 'title_parallax', 'title_header_color', 'use_video', 'video_type&use_video=1', 'video_link&use_video=1', ),
							),
							'des'=>''
						),

						array('var'=>'title_header_color', 'label'=>esc_html__('Header elements color', 'thebe'), 'default'=>'white', 'type'=>'select', 
							'options'=>array(
								'white' => esc_html__('White', 'thebe'), 
								'black' => esc_html__('Black', 'thebe'), 
							), 
							'help'=>'header_color?w=600&h=400&ox=0&oy=-180&bc=#eeeeee', 
							'des'=>''
						),

						array('var'=>'title_bg', 'label'=>esc_html__('Background image', 'thebe'), 'type'=>'image', 'des'=>''),
						array('var'=>'title_bg_color', 'label'=>esc_html__('Background color', 'thebe'), 'type'=>'color', 'des'=>''),

						array('var'=>'title_fullscreen', 'label'=>esc_html__('Fullscreen', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 'des'=>''),
						array('var'=>'title_parallax', 'label'=>esc_html__('Use parallax', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 'des'=>''),
						
						array('var'=>'title', 'label'=>esc_html__('Title', 'thebe'), 'default'=>'', 'type'=>'text', 'des'=>''),
						array('var'=>'title_description', 'label'=>esc_html__('Description', 'thebe'), 'default'=>'', 'type'=>'textarea', 'rows'=>3, 'des'=>''),

						array('var'=>'use_video', 'label'=>esc_html__('Use video', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 
							'only_available' => array(
								'1' => array('video_type&title_type=1|2|3', 'video_link&title_type=1|2|3',)
							),
							'des'=>'',
						),
						array('var'=>'video_type', 'label'=>esc_html__('Video type', 'thebe'), 'default'=>'1', 'type'=>'select', 
							'options'=>array(
								'1' => esc_html__('*.mp4', 'thebe'), 
								'2' => esc_html__('Youtube', 'thebe'), 
								'3' => esc_html__('Vimeo', 'thebe'),
							), 
							'des'=>''
						),
						array('var'=>'video_link', 'label'=>esc_html__('Video link', 'thebe'), 'default'=>'', 'type'=>'text', 'help'=>'video_link?w=600&h=400&ox=0&oy=-180&bc=#eeeeee', 'des'=>''),
					)
				);

				self::$data = array(
					'default'=>array(
					),

					'shortcode'=>array(
					),

					'landing'=>array(
					),

					'blog'=>array(
						array('var'=>'style', 'label'=>esc_html__('Blog style', 'thebe'), 'default'=>'1', 'type'=>'image_select',
							'options'=>array('1', '2'),
							'des'=>''
						),

						array('var'=>'use_ajax', 'label'=>esc_html__('Use AJAX', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 'des'=>esc_html__('Whether use AJAX or not when opening item.', 'thebe')),
						array('var'=>'fullscreen', 'label'=>esc_html__('Fullscreen (for AJAX)', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 'des'=>esc_html__('If choose "Yes", some elements will be hidden.', 'thebe')),

                        array('var'=>'data_src_type', 'label'=>esc_html__('Data source type', 'thebe'), 'default'=>'1', 'type'=>'select', 
							'options'=>array(
								'1' => esc_html__('Category', 'thebe'), 
								'2' => esc_html__('Id', 'thebe'), 
								'3' => esc_html__('Query args string', 'thebe'),
                            ), 
                            'only_available' => array(
                                '1' => array( 'category' ),
                                '2' => array( 'data_ids' ),
                                '3' => array( 'data_query_str' ),
                            ),
							'des'=>''
                        ),
                        array('var'=>'category', 'label'=>esc_html__('Category', 'thebe'), 'default'=>'', 'type'=>'category', 'data'=>'post', 'des'=>''),
                        array('var'=>'data_ids', 'label'=>esc_html__('Post ids', 'thebe'), 'default'=>'', 'type'=>'text', 'des'=>esc_html__('Use comma to separate them.', 'thebe')),
                        array('var'=>'data_query_str', 'label'=>esc_html__('Query args string', 'thebe'), 'default'=>'', 'type'=>'text', 'des'=>esc_html__('This is an advanced option, see more from here: https://codex.wordpress.org/Class_Reference/WP_Query', 'thebe')),

						array('var'=>'num', 'label'=>esc_html__('Item count', 'thebe'), 'default'=>'12', 'type'=>'text', 'class'=>'mini-width', 'des'=>esc_html__('Show how many items on a page. It means all when equal -1.', 'thebe')),
						array('var'=>'rawsize', 'label'=>esc_html__('Raw proportion', 'thebe'), 'default'=>'1', 'type'=>'checkbox', 'des'=>''),
						array('var'=>'large_img', 'label'=>esc_html__('Large image', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 'des'=>''),

						array('var'=>'show_widgets', 'label'=>esc_html__('Show widgets (right bar)', 'thebe'), 'default'=>'1', 'type'=>'checkbox', 'des'=>esc_html__('If it is "Yes", you also need to add widgets to "Right Side Widget Area" in Appearance > Widgets.', 'thebe')),
					),

					'portfolio'=>array(
						array('var'=>'type', 'label'=>esc_html__('Portfolio type', 'thebe'), 'default'=>'1', 'type'=>'image_select',
							'options'=>array('1', '2', '3', '4', '5', '6', '7'),
							'only_available'=>array(
								'1' => array('as_slider', 'large_text'),
								'2' => array('as_slider', 'large_text'),
								'3' => array('as_slider', 'large_text'),
								'4' => array('as_slider', 'large_text'),
								'5' => array('as_slider', 'large_text'),
								'6' => array('as_slider', 'large_text'),
								'7' => array('wide', 'thumb_type', 'columns', 'caption_pos'),
							),
							'body_class_prefix'=>'portfolio-type-',
							'des'=>''
						),
						array('var'=>'use_ajax', 'label'=>esc_html__('Use AJAX', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 'des'=>esc_html__('Whether use AJAX or not when opening item.', 'thebe')),
						array('var'=>'fullscreen', 'label'=>esc_html__('Fullscreen (for AJAX)', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 'des'=>esc_html__('If choose "Yes", some elements will be hidden.', 'thebe')),
						
						array('var'=>'as_slider', 'label'=>esc_html__('Set as slider', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 'body_class_prefix'=>'portfolio-as-slider-', 'des'=>''),
						array('var'=>'wide', 'label'=>esc_html__('Wide', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 'des'=>''),
						array('var'=>'thumb_type', 'label'=>esc_html__('Thumb type', 'thebe'), 'default'=>'type-h', 'type'=>'image_select',
							'options'=>array('type-h', 'type-v', 'type-s', 'type-auto'),
							'des'=>''
						),
						array('var'=>'columns', 'label'=>esc_html__('Columns', 'thebe'), 'default'=>'col-auto', 'type'=>'image_select',
							'options'=>array('col-auto', 'col-two', 'col-three', 'col-four'),
							'des'=>esc_html__('The columns may change while the width of window is smaller than 1280 pixels.', 'thebe'),
						),
						array('var'=>'caption_pos', 'label'=>esc_html__('Caption position', 'thebe'), 'default'=>'1', 'type'=>'image_select',
							'options'=>array('1', '2'),
							'des'=>''
						),
                        array('var'=>'large_text', 'label'=>esc_html__('Large text', 'thebe'), 'default'=>'0', 'type'=>'checkbox', 'des'=>''),
                        
                        array('var'=>'data_src_type', 'label'=>esc_html__('Data source type', 'thebe'), 'default'=>'1', 'type'=>'select', 
							'options'=>array(
								'1' => esc_html__('Category', 'thebe'), 
								'2' => esc_html__('Id', 'thebe'), 
								'3' => esc_html__('Query args string', 'thebe'),
                            ), 
                            'only_available' => array(
                                '1' => array( 'category' ),
                                '2' => array( 'data_ids' ),
                                '3' => array( 'data_query_str' ),
                            ),
							'des'=>''
                        ),
                        array('var'=>'category', 'label'=>esc_html__('Category', 'thebe'), 'default'=>'', 'type'=>'category', 'data'=>'project', 'des'=>''),
                        array('var'=>'data_ids', 'label'=>esc_html__('Project ids', 'thebe'), 'default'=>'', 'type'=>'text', 'des'=>esc_html__('Use comma to separate them.', 'thebe')),
                        array('var'=>'data_query_str', 'label'=>esc_html__('Query args string', 'thebe'), 'default'=>'', 'type'=>'text', 'des'=>esc_html__('This is an advanced option, see more from here: https://codex.wordpress.org/Class_Reference/WP_Query', 'thebe')),
						array('var'=>'num', 'label'=>esc_html__('Item count', 'thebe'), 'default'=>'12', 'type'=>'text', 'class'=>'mini-width', 'des'=>esc_html__('Show how many items on a page. It means all when equal -1.', 'thebe')),
					),
                );
                
                self::$basic_data = apply_filters( 'thebe_page_options_basic_data', self::$basic_data );
                self::$data = apply_filters( 'thebe_page_options_data', self::$data );
			}
		}
	}

	Thebe_Page_Options::init_data();
?>