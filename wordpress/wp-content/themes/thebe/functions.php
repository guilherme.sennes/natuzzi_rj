<?php
 /**
 * Theme directory and url
 */
 
define( 'THEBE_THEME_DIR', untrailingslashit( get_template_directory() ) );
define( 'THEBE_THEME_URL', untrailingslashit( get_template_directory_uri() ) );
define( 'THEBE_STYLE_DIR', untrailingslashit( get_stylesheet_directory() ) );
define( 'THEBE_STYLE_URL', untrailingslashit( get_stylesheet_directory_uri() ) );

$thebe_upload_directory = wp_upload_dir();
define( 'THEBE_UPLOAD_DIR', untrailingslashit( $thebe_upload_directory['basedir'] ) );
define( 'THEBE_UPLOAD_URL', untrailingslashit( $thebe_upload_directory['baseurl'] ) );

$thebe_the_theme = wp_get_theme();
$thebe_the_theme_parent = $thebe_the_theme->parent();
$thebe_the_theme_data = $thebe_the_theme_parent ? $thebe_the_theme_parent : $thebe_the_theme;

define( 'THEBE_THEME_NAME', strtolower( $thebe_the_theme_data->get( 'Name' ) ) );
define( 'THEBE_THEME_VERSION', strtolower( $thebe_the_theme_data->get( 'Version' ) ) );

if( !function_exists('thebe_log') )
{
	function thebe_log( $msg )
	{
		if( !defined('WP_DEBUG') || !WP_DEBUG )
		{
			return;
		}

		global $wp_filesystem;
		WP_Filesystem();

		$file_path = THEBE_THEME_DIR . '/log.txt';
		$file_str = $wp_filesystem->get_contents( $file_path );
		$wp_filesystem->put_contents( $file_path, $file_str.PHP_EOL.$msg );
	}
}
require_once THEBE_THEME_DIR . '/inc/template-tags.php';
require_once THEBE_THEME_DIR . '/inc/aq_resizer.php';

require_once THEBE_THEME_DIR . '/inc/theme/options/theme.options.php';

if( is_admin() )
{
	//--------------    plugins    ---------------
	require_once THEBE_THEME_DIR . '/inc/class-tgm-plugin-activation.php';
	require_once THEBE_THEME_DIR . '/inc/theme/register-plugins.php';

	require_once THEBE_THEME_DIR . '/inc/theme/options/global.options.php';
	require_once THEBE_THEME_DIR . '/inc/theme/options/post.options.php';
	require_once THEBE_THEME_DIR . '/inc/theme/options/project.options.php';
	require_once THEBE_THEME_DIR . '/inc/theme/options/product.options.php';
	require_once THEBE_THEME_DIR . '/inc/theme/options/page.options.php';
	require_once THEBE_THEME_DIR . '/inc/theme/options/shortcode.options.php';
	require_once THEBE_THEME_DIR . '/inc/theme/custom-admin-page.php';
	require_once THEBE_THEME_DIR . '/inc/theme/functions/ajax-options-functions.php';
	require_once THEBE_THEME_DIR . '/inc/theme/functions/shortcode-admin-functions.php';
	require_once THEBE_THEME_DIR . '/inc/theme/demo-install.php';
}

require_once THEBE_THEME_DIR . '/inc/theme/enqueue-scripts.php';
require_once THEBE_THEME_DIR . '/inc/theme/functions/comment-functions.php';
require_once THEBE_THEME_DIR . '/inc/theme/functions/content-functions.php';
require_once THEBE_THEME_DIR . '/inc/theme/functions/image-functions.php';
require_once THEBE_THEME_DIR . '/inc/theme/functions/parse-data-functions.php';

if( class_exists( 'WooCommerce' ) )
{
	define( 'THEBE_HAS_WOOCOMMERCE', true );

	require_once THEBE_THEME_DIR . '/inc/theme/functions/woocommerce-functions.php';
}
else
{
	define( 'THEBE_HAS_WOOCOMMERCE', false );
}

//--------------------------------------------


if ( ! isset( $content_width ) )
{
	$content_width = 1024;
}


/**
 * Only works in WordPress 4.8 or later.
 */
if( version_compare( $GLOBALS['wp_version'], '4.8-alpha', '<' ) )
{
	require THEBE_THEME_DIR . '/inc/back-compat.php';
}


/**
 * Theme setup
 */
add_action( 'after_setup_theme', 'thebe_theme_setup' );
if(!function_exists('thebe_theme_setup'))
{
	function thebe_theme_setup()
	{
		load_theme_textdomain( 'thebe', THEBE_THEME_DIR . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );
		
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary Menu', 'thebe' )
		));

		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
		));

		add_theme_support( 'post-formats', array() );

		add_theme_support( 'custom-header', array() );

		add_theme_support( 'custom-background', array() );

		add_editor_style( array( 'css/editor-style.css', thebe_editor_fonts_url() ) );

		//------------   custom styles   -------------
		require_once THEBE_THEME_DIR . '/inc/theme/styles.php';
	}
}


/**
 * Add specific CSS class by filter.
 **/
add_filter( 'body_class', 'thebe_handle_body_class');
function thebe_handle_body_class( $classes )
{
	$body_classes = array();

	global $thebe_global_site_vars;
	if( isset( $thebe_global_site_vars ) )
	{
		if( isset( $thebe_global_site_vars['template'] ) )
		{
			$body_classes[] = $thebe_global_site_vars['template'];
		}

		if( isset( $thebe_global_site_vars['portfolio_slider'] ) )
		{
			$body_classes[] = $thebe_global_site_vars['portfolio_slider'];
		}

		if( isset( $thebe_global_site_vars['use_widget'] ) && $thebe_global_site_vars['use_widget'] )
		{
			$body_classes[] = 'has-widget';
		}
	}

	//1: light, 2: dark
	$site_style_class = thebe_get_theme_option( 'thebe_site_style', '1' ) === '1' ? 'site-light' : 'site-dark';
	$body_classes[] = $site_style_class;

	if( thebe_get_theme_option( 'thebe_mobile_menu_fixed', '1' ) === '1' )
	{
		$body_classes[] = 'm-nav-fixed';
	}

    return array_merge( $classes, $body_classes );
}


/**
 * Add specific CSS class by filter - for admin.
 **/
add_filter( 'admin_body_class', 'thebe_handle_admin_body_class');
function thebe_handle_admin_body_class( $classes )
{
	global $pagenow;
	global $typenow;

	// Only run on add/edit screens
	if ( in_array( $pagenow, array('post.php', 'page.php', 'post-new.php', 'post-edit.php') ) && $typenow === 'page' ) 
	{
		$page_template_slug = get_page_template_slug();
		if( $page_template_slug !== false )
		{
			$template_name = preg_replace( '/(page-)|(\.php)/i', '', $page_template_slug );
			$template_name = preg_replace( '/-/', '', $template_name );

			if( !$template_name )
			{
				$template_name = 'default';
			}

			$classes .= ' page-template-'.$template_name;
			$classes .= ' init-page-template-'.$template_name;
		}
	}

    return $classes;
}


/**
 * sort by asc index
 */
if (!function_exists( 'thebe_sort_asc_by_index' ))
{
	function thebe_sort_asc_by_index($obj1, $obj2) 
	{
		return $obj1['index'] - $obj2['index'];
	}
}


/**
 * sort by desc index
 */
if (!function_exists( 'thebe_sort_desc_by_index' ))
{
	function thebe_sort_desc_by_index($obj1, $obj2) 
	{
		return $obj2['index'] - $obj1['index'];
	}
}


/**
 * Check to flush rewrite rules
 */
add_action( 'admin_init', 'thebe_load_plugin' );
function thebe_load_plugin() 
{
    if ( is_admin() && get_option( 'thebe_activated_plugin', '' ) === 'Thebe-Projects' )
    {
        delete_option( 'thebe_activated_plugin' );

        flush_rewrite_rules();
    }
}


/**
 * Set post type for search
 */
add_filter('pre_get_posts','thebe_exclude_page_from_search');
if(!function_exists('thebe_exclude_page_from_search'))
{
	function thebe_exclude_page_from_search($query)
	{
		if ($query->is_search) {
			$query->set('post_type', array( 'post', 'project', 'product', 'page' ));
		}
		return $query;
	}
}


/**
 * Join posts and postmeta tables
 */
function thebe_custom_search_join( $join ) {
    global $wpdb;

    if ( !is_admin() && is_search() ) {    
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}
add_filter( 'posts_join', 'thebe_custom_search_join' );


/**
 * Modify the search query with posts_where
 */
function thebe_custom_search_where( $where ) {
    global $pagenow, $wpdb;

    if ( !is_admin() && is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'thebe_custom_search_where' );


/**
 * Prevent duplicates
 */
function thebe_custom_search_distinct( $where ) {
    global $wpdb;

    if ( !is_admin() && is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'thebe_custom_search_distinct' );


/**
 * Add custom query variables
 */
add_filter( 'query_vars', 'thebe_add_query_vars_filter' );
if(!function_exists('thebe_add_query_vars_filter'))
{
	function thebe_add_query_vars_filter( $vars )
	{
		$vars[] = '_preset';
		$vars[] = '_cat';
		return $vars;
	}
}


/**
 * Get google font link for editor
 */
if(!function_exists('thebe_editor_fonts_url'))
{
	function thebe_editor_fonts_url()
	{
		$fonts_url = '';
		$fonts     = array();
		$subsets   = 'latin,latin-ext';

		if ( 'off' !== esc_html_x( 'on', 'Noto Sans font: on or off', 'thebe' ) ) {
			$fonts[] = 'Noto Sans:400italic,700italic,400,700';
		}

		if ( 'off' !== esc_html_x( 'on', 'Noto Serif font: on or off', 'thebe' ) ) {
			$fonts[] = 'Noto Serif:400italic,700italic,400,700';
		}

		if ( 'off' !== esc_html_x( 'on', 'Inconsolata font: on or off', 'thebe' ) ) {
			$fonts[] = 'Inconsolata:400,700';
		}

		$subset = esc_html_x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'thebe' );

		if ( 'cyrillic' == $subset ) {
			$subsets .= ',cyrillic,cyrillic-ext';
		} elseif ( 'greek' == $subset ) {
			$subsets .= ',greek,greek-ext';
		} elseif ( 'devanagari' == $subset ) {
			$subsets .= ',devanagari';
		} elseif ( 'vietnamese' == $subset ) {
			$subsets .= ',vietnamese';
		}

		if ( $fonts ) {
			$fonts_url = add_query_arg( array(
				'family' => urlencode( implode( '|', $fonts ) ),
				'subset' => urlencode( $subsets ),
			), '//fonts.googleapis.com/css' );
		}

		return $fonts_url;
	}
}


/**
 * Initialize widgets
 */
add_action('widgets_init', 'thebe_widgets_init');
if(!function_exists('thebe_widgets_init'))
{
	function thebe_widgets_init() 
	{
		register_sidebar( array(
			'name'          => esc_html__( 'Right Side Widget Area', 'thebe' ),
			'id'            => 'sidebar-right',
			'description'   => esc_html__( 'Add widgets here to appear in your sidebar (right).', 'thebe' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<div class="widget-title">',
			'after_title'   => '</div>'
		));

		register_sidebar( array(
			'name'          => esc_html__( 'Footer Widget Area', 'thebe' ),
			'id'            => 'sidebar-footer',
			'description'   => esc_html__( 'Add widgets here to appear in your footer.', 'thebe' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<div class="widget-title">',
			'after_title'   => '</div>'
		));
	}
}


/**
 * Parse part path
 */
if( !function_exists('thebe_parse_part_path') )
{
	function thebe_parse_part_path( $path )
	{
		if( file_exists( THEBE_STYLE_DIR . $path . '.php' ) )
		{
			return THEBE_STYLE_DIR . $path . '.php';
		}
		else
		{
			return THEBE_THEME_DIR . $path . '.php';
		}
	}
}


/**
 * SEO - Keyword and Desctiption
 */
if( ! function_exists( 'thebe_seo_action' ) )
{
	function thebe_seo_action()
	{
		if( thebe_get_theme_option('thebe_seo_enable', '1') === '1' )
		{
			global $thebe_global_site_vars;

			$seo_keywords = '';
			$seo_description = '';

			if( isset( $thebe_global_site_vars ) )
			{
				$seo_keywords = isset( $thebe_global_site_vars['seo_keywords'] ) ? $thebe_global_site_vars['seo_keywords'] : '';
				$seo_description = isset( $thebe_global_site_vars['seo_description'] ) ? $thebe_global_site_vars['seo_description'] : '';
			}
			
			// keywords
			if( !$seo_keywords )
			{
				$seo_keywords = thebe_get_theme_option('thebe_seo_keywords', '');
			}
			if( $seo_keywords )
			{
				?><meta name="keywords" content="<?php echo esc_attr( $seo_keywords ); ?>" /><?php
			}

			//description
			if( !$seo_description )
			{
				$seo_description = thebe_get_theme_option('thebe_seo_description', '');
			}
			if( $seo_description )
			{
				?><meta name="description" content="<?php echo esc_attr( $seo_description ); ?>" /><?php
			}
		}
	}
}
add_action( 'thebe_seo', 'thebe_seo_action' );

add_action( 'enqueue_block_editor_assets', function() {
	wp_add_inline_script(
		'wp-edit-post',
		"!( function( $ ) {
			wp.domReady( function() {
				var body = $( 'body' ),
				    templateSelectClass = '.editor-page-attributes__template select';

				body.on( 'change.set-editor-class', templateSelectClass, function() {
					var str = $( this ).val() || 'default';
					var pageTemplate = str.replace(/(page-)|(\.php)/ig, '').replace(/-/g, '');
					$( document ).trigger( 'block-editor.page-template-changed', [pageTemplate] );
				} )
				.find( templateSelectClass ).trigger( 'change.set-editor-class' );
			} );
		} )( jQuery );"
	);
} );