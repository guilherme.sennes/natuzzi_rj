	<?php
		$thebe_popup_img_url = '';
		$thebe_popup_img_w = 0;
		$thebe_popup_img_h = 0;
		$thebe_popup_img = thebe_get_theme_option('thebe_popup_img', '');
		$thebe_img_obj = thebe_get_image(array(
			'rid' => $thebe_popup_img,
			'max_width' => 720,
		));

		if( $thebe_img_obj )
		{
			$thebe_popup_img_url = $thebe_img_obj['thumb'];
			$thebe_popup_img_w = $thebe_img_obj['thumb_width'];
			$thebe_popup_img_h = $thebe_img_obj['thumb_height'];
		}

		$thebe_popup_title = thebe_get_theme_option('thebe_popup_title', '');
		$thebe_popup_content = thebe_get_theme_option('thebe_popup_content', '');

		$thebe_popop_is_empty = !$thebe_popup_img_url && !$thebe_popup_title && !$thebe_popup_content;

		$thebe_copyright_position = thebe_get_theme_option( 'thebe_copyright_position', '1' );

		$thebe_check_psw_required = get_post_type() === 'page' && post_password_required();
	?>
	</div>
	<?php

		if( !$thebe_check_psw_required )
		{
			?>
			<footer class="pt-footer">
				<div class="footer-left">
					<div class="pt-social">
						<ul>
							<?php
								$thebe_socials_names = array( 'facebook', 'twitter', 'pinterest', 'flickr', 'instagram', 'linkedin', 'whatsapp', 'behance', 'tumblr', 'googleplus', 'vimeo', 'youtube', 'dribbble', 'email' );
								$thebe_social_texts = array(
									'facebook' => '<i class="fa fa-facebook"></i>',
									'twitter' => '<i class="fa fa-twitter"></i>',
									'pinterest' => '<i class="fa fa-pinterest"></i>',
									'flickr' => '<i class="fa fa-flickr"></i>',
									'instagram' => '<i class="fa fa-instagram"></i>',
									'linkedin' => '<i class="fa fa-linkedin"></i>',
									'whatsapp' => '<i class="fa fa-whatsapp"></i>',
									'behance' => '<i class="fa fa-behance"></i>',
									'tumblr' => '<i class="fa fa-tumblr"></i>',
									'googleplus' => '<i class="fa fa-google-plus"></i>',
									'vimeo' => '<i class="fa fa-vimeo"></i>',
									'youtube' => '<i class="fa fa-youtube"></i>',
									'dribbble' => '<i class="fa fa-dribbble"></i>',
									'email' => '<i class="fa fa-envelope"></i>',
								);

								foreach( $thebe_socials_names as $thebe_social_item_name )
								{
									$thebe_social_url = thebe_get_theme_option('thebe_social_' . $thebe_social_item_name, '');
									if( $thebe_social_url != '' )
									{
										if( $thebe_social_item_name !== 'email' )
										{
											echo '<li><a href="'.esc_url( $thebe_social_url ).'" target="_blank">'.$thebe_social_texts[ $thebe_social_item_name ].'</a></li>';
										}
										else
										{
											echo '<li><a href="'.esc_attr( $thebe_social_url ).'" class="not-fade-link">'.$thebe_social_texts[ $thebe_social_item_name ].'</a></li>';
										}
									}
                                }
                                
                                $thebe_socials_group_value = thebe_get_theme_option( 'thebe_social_group', '' );
                                if( $thebe_socials_group_value )
                                {
                                    $thebe_socials_group_data = thebe_parse_group_data( $thebe_socials_group_value );
                                    foreach( $thebe_socials_group_data as $thebe_social_group_item_obj )
                                    {
                                        $thebe_social_link = $thebe_social_group_item_obj['link'];
                                        if( $thebe_social_link )
                                        {
                                            $thebe_social_icon = $thebe_social_group_item_obj['icon'];

                                            echo '<li><a href="'.esc_url( $thebe_social_link ).'" target="_blank"><i class="'.esc_attr( $thebe_social_icon ).'"></i></a></li>';
                                        }
                                    }
                                }
							?>
						</ul>
					</div>
					<?php
						if( !$thebe_popop_is_empty )
						{
							?><span class="call-popup"><?php echo esc_html( thebe_get_theme_option('thebe_popup_button', esc_html__('Popup', 'thebe')) ); ?></span><?php
						}
					?>
				</div>
				<div class="footer-right">
					<p class="copyright"><?php thebe_kses_content( thebe_get_theme_option( 'thebe_footer_copyright', '' ) ); ?></p>
					<?php
						$thebe_music_src = thebe_get_theme_option( 'thebe_music_src', '' );
						if( $thebe_music_src )
						{
							$thebe_music_src_arr = explode( ',', $thebe_music_src );
							foreach( $thebe_music_src_arr as $thebe_music_path )
							{
								?><div id="music-player" data-src="<?php echo esc_attr( $thebe_music_path ); ?>" data-volume="<?php echo esc_attr( thebe_get_theme_option( 'thebe_music_volume', '1' ) ); ?>" data-autoplay="<?php echo esc_attr( thebe_get_theme_option( 'thebe_music_autoplay', '0' ) ); ?>"></div><?php
							}
						}
					?>
				</div>
			</footer>
			<?php
		}

		if( is_active_sidebar( 'sidebar-footer' ) && !$thebe_check_psw_required )
		{
			$footer_widgets_wide_class = thebe_get_theme_option( 'thebe_footer_widgets_wide', '0' ) === '1' ? ' wide' : '';

			?>
			<div class="footer-widgets<?php echo esc_attr( $footer_widgets_wide_class ); ?>" data-header-color="<?php echo esc_attr( thebe_get_theme_option( 'thebe_footer_text_color', 'white' ) ); ?>">
				<div class="wrap">
				<?php
					get_sidebar('bottom');

					if( $thebe_copyright_position === '1' )
					{
						?><p class="copyright"><?php thebe_kses_content( thebe_get_theme_option( 'thebe_footer_copyright', '' ) ); ?></p><?php
					}
				?>
				</div>
				<?php
					$thebe_img_obj = thebe_get_image(array(
						'rid' => thebe_get_theme_option( 'thebe_footer_bg', '' ),
						'max_width' => 2200,
					));

					if( $thebe_img_obj )
					{
						?><div class="bg-full" data-bg="<?php echo esc_url( $thebe_img_obj['thumb'] ); ?>"></div><?php
					}
				?>
			</div>
			<?php
		}
	?>
	
	<?php
		if( !$thebe_popop_is_empty && !$thebe_check_psw_required )
		{
			?>
			<div class="pt-popup" data-text-color="<?php echo esc_attr( thebe_get_theme_option( 'thebe_popup_style', 'white' ) ); ?>" data-bg-color="<?php echo esc_attr( thebe_get_theme_option( 'thebe_popup_bg_color', '' ) ); ?>">
				<div class="wrap">
					<?php
						if( $thebe_popup_img_url )
						{
							?>
							<div class="img" data-w="<?php echo esc_attr( $thebe_popup_img_w ); ?>" data-h="<?php echo esc_attr( $thebe_popup_img_h ); ?>">
								<div class="bg-full" data-bg="<?php echo esc_url( $thebe_popup_img_url ); ?>"></div>
							</div>
							<?php
						}
					?>
					<div class="text">
						<h1><?php thebe_kses_content( $thebe_popup_title ); ?></h1>
						<div class="text-area"><?php echo do_shortcode( $thebe_popup_content ); ?></div>
						<?php
							if( $thebe_copyright_position === '2' )
							{
								?><p class="copyright"><?php thebe_kses_content( thebe_get_theme_option( 'thebe_footer_copyright', '' ) ); ?></p><?php
							}
						?>
					</div>
					
				</div>
			</div>
			<?php
		}
	?>

	<?php
		global $thebe_global_site_vars;

		if( !$thebe_check_psw_required && isset( $thebe_global_site_vars ) && isset( $thebe_global_site_vars['use_ajax'] ) && $thebe_global_site_vars['use_ajax'] && isset( $thebe_global_site_vars['template'] ) )
		{
			$thebe_single_type_class = $thebe_global_site_vars['template'] === 'page-blog' ? ' post' : ' project';
			$thebe_ajax_fullscreen_class = isset( $thebe_global_site_vars['ajax_fullscreen'] ) && $thebe_global_site_vars['ajax_fullscreen'] ? ' ajax-fullscreen' : '';
			?>
			<div class="ajax-content<?php echo esc_attr( $thebe_single_type_class.$thebe_ajax_fullscreen_class ); ?>">
				<div class="wrap">
					<i class="m-close-single"></i>
					<div class="close-single"></div>
					<div class="ajax-target"></div>
				</div>
			</div>
			<?php
		}
		else
		{
			?>
			<div class="ajax-content project">
				<div class="wrap">
					<i class="m-close-single"></i>
					<div class="close-single"></div>
					<div class="ajax-target"></div>
				</div>
			</div>
			<?php
		}
	?>
	
	<?php
		wp_footer();
	?>
</body>
</html>