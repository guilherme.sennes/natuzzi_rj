<?php
	$thebe_raw_proportion = thebe_get_theme_option( 'thebe_archives_img_raw', '1' ) === '1';
	$thebe_raw_class = $thebe_raw_proportion ? 'raw-proportion' : '';
?>
<div class="archive-list blog-list style-01 large-image <?php echo esc_attr( $thebe_raw_class ); ?>">
	<div class="blog-main">
		<div class="wrap">
			<?php

			if ( have_posts() )
			{
				while ( have_posts() )
				{
					the_post();
					
					$thebe_post_id = get_the_ID();
					$thebe_author_meta_id = get_the_author_meta('ID');
					$thebe_author_link = get_author_posts_url( $thebe_author_meta_id );
					$thebe_post_data = thebe_get_post_setting( $thebe_post_id );
					$thebe_post_title = get_the_title();

					$thebe_post_type = get_post_type();
					$thebe_prop_prefix = 'pt_'.$thebe_post_type.'_';

					$thebe_thumburl = '';
					$thebe_imageurl = '';

					$thebe_thumb_width = 500;
					$thebe_thumb_height = 500;
					
					if( has_post_thumbnail() )
					{
						if( !$thebe_raw_proportion )
						{
							$thebe_img_obj = thebe_get_image(array(
								'rid' => get_post_thumbnail_id(),
								'max_width' => 900,
								'crop_part_vertical' => thebe_get_property( $thebe_post_data, 'image_crop_part', 3, $thebe_prop_prefix )
							));
						}
						else
						{
							$thebe_img_obj = thebe_get_image(array(
								'rid' => get_post_thumbnail_id(),
								'max_width' => 900,
							));
						}

						if( $thebe_img_obj )
						{
							$thebe_thumburl = $thebe_img_obj['thumb'];
							$thebe_imageurl = $thebe_img_obj['image'];

							$thebe_thumb_width = $thebe_img_obj['thumb_width'];
							$thebe_thumb_height = $thebe_img_obj['thumb_height'];
						}
					}

					$thebe_permalink = get_permalink( $thebe_post_id );

					$thebe_banner_type = thebe_get_property( $thebe_post_data, 'banner_type', '1', $thebe_prop_prefix );
					$thebe_post_is_video = $thebe_banner_type === '3';

					$thebe_video_link = thebe_get_property( $thebe_post_data, 'video_link', '', $thebe_prop_prefix );
					$thebe_video_type = thebe_get_property( $thebe_post_data, 'video_type', '1', $thebe_prop_prefix );
					$thebe_video_volume = thebe_get_property( $thebe_post_data, 'video_volume', '1', $thebe_prop_prefix );

					$thebe_box_class = 'item';

					if( $thebe_post_is_video && $thebe_video_link )
					{
						$thebe_box_class .= ' v-post';
					}

					if( !$thebe_thumburl && !$thebe_post_is_video )
					{
						$thebe_box_class .= ' text-post';
					}

					if( is_sticky() )
					{
						$thebe_box_class .= ' sticky';
					}

					?>
					<div <?php post_class( $thebe_box_class ); ?> data-w="<?php echo esc_attr( $thebe_thumb_width) ;?>" data-h="<?php echo esc_attr( $thebe_thumb_height) ;?>">
						<div class="img">
							<?php
								if( $thebe_post_is_video && $thebe_video_link )
								{
									?>
									<div class="bg-full" data-bg="<?php echo esc_url( $thebe_thumburl ); ?>" data-src="<?php echo esc_attr( $thebe_video_link ); ?>" data-type="<?php echo esc_attr( $thebe_video_type ); ?>" data-volume="<?php echo esc_attr( $thebe_video_volume ); ?>"></div>
									<?php
								}
								else
								{
									?>
									<div class="bg-full" data-bg="<?php echo esc_url( $thebe_thumburl ); ?>"></div>
									<?php
								}
							?>
							<a class="full" data-href="<?php echo esc_url( $thebe_permalink ); ?>"></a>
						</div>
						<div class="text">
							<?php
								$thebe_time_link = get_the_title() != '' ? get_month_link( get_the_time('Y'), get_the_time('m'), get_the_time('d')) : $thebe_permalink;

								if( $thebe_post_title != '' )
								{
									?><h4><a href="<?php echo esc_url( $thebe_permalink ); ?>"><?php echo esc_html( $thebe_post_title ); ?></a></h4><?php
								}

								$thebe_description_length = $thebe_raw_proportion ? 45 : 22;
								$thebe_post_description = wp_trim_words( get_the_excerpt(), $thebe_description_length );

								if( !$thebe_post_description )
								{
									$thebe_shortcode_text = thebe_get_property( $thebe_post_data, 'shortCodeText', '', 'pt_sc_' );
									if( $thebe_shortcode_text )
									{
										$thebe_post_description = wp_strip_all_tags( do_shortcode( shortcode_unautop( wp_specialchars_decode( stripslashes( $thebe_shortcode_text ), ENT_QUOTES ) ) ) );
										$thebe_post_description = wp_trim_words( $thebe_post_description, $thebe_description_length );
									}
								}

								if( $thebe_post_description )
								{
									?><div class="intro"><?php echo esc_html( $thebe_post_description ); ?></div><?php
								}
							?>
							<div class="btn-wrap">
								<a class="btn" href="<?php echo esc_url( $thebe_permalink ); ?>"><span><?php esc_html_e('Read More', 'thebe'); ?></span></a>
							</div>
							<div class="list-meta">
								<div class="main-meta">
									<a class="avatar" href="<?php echo esc_url( $thebe_author_link ); ?>"><?php echo get_avatar( $thebe_author_meta_id, 100 ); ?></a>
									<div class="info">
										<a class="author" href="<?php echo esc_url( $thebe_author_link ); ?>"><?php echo get_the_author(); ?></a>
										<div class="date"><a href="<?php echo esc_url( $thebe_time_link ); ?>"><?php echo esc_html( get_the_date( get_option('date_format', 'F d Y') ) ); ?></a></div>
									</div>
								</div>
								<?php 
									if( $thebe_post_type === 'project' )
									{
										$thebe_categories = get_the_terms( $thebe_post_id, 'project_cat' );
										if( $thebe_categories )
										{
											?><div class="category"><?php

											foreach($thebe_categories as $thebe_cat)
											{
												?><a href="<?php echo esc_url( get_term_link( $thebe_cat->term_id ) ); ?>"><?php echo esc_html( $thebe_cat->name ); ?></a><?php
											}

											?></div><?php
										}
									}
									elseif( $thebe_post_type === 'product' )
									{
										$thebe_categories = get_the_terms( $thebe_post_id, 'product_cat' );
										if( $thebe_categories )
										{
											?><div class="category"><?php

											foreach($thebe_categories as $thebe_cat)
											{
												?><a href="<?php echo esc_url( get_term_link( $thebe_cat->term_id ) ); ?>"><?php echo esc_html( $thebe_cat->name ); ?></a><?php
											}

											?></div><?php
										}
									}
									else
									{
										$thebe_categories = get_the_category();
										if( $thebe_categories )
										{
											?><div class="category"><?php

											foreach($thebe_categories as $thebe_cat)
											{
												?><a href="<?php echo esc_url( get_category_link( $thebe_cat->term_id ) ); ?>"><?php echo esc_html( $thebe_cat->cat_name ); ?></a><?php
											}

											?></div><?php
										}
									}
								?>
							</div>
							
						</div>
					</div>
					<?php
				}
			}

			echo thebe_paging_nav();
		?>
		</div>
	</div>
	<?php
		if( is_active_sidebar('sidebar-right') )
		{
			?>
			<div class="pt-widget-list">
				<?php get_sidebar(); ?>
			</div>
			<?php
		}
	?>
</div>