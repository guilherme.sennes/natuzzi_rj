<?php
	$thebe_blog_category = thebe_get_page_setting_property( 'blog_category', '' );
	$thebe_current_cat = get_query_var('_cat');
	$thebe_page_link = get_permalink();

	$thebe_categories = array();

	if( !$thebe_blog_category )
	{
		$thebe_categories_items = get_categories();
		if( $thebe_categories_items )
		{
			foreach( $thebe_categories_items as $thebe_cat )
			{
				$thebe_categories[] = $thebe_cat->cat_name;
			}
		}
	}
	else
	{
		$thebe_categories = explode( ',', $thebe_blog_category );
	}

	if( $thebe_current_cat )
	{
		$thebe_search_category = $thebe_current_cat;
	}
	else
	{
		$thebe_search_category = $thebe_blog_category;
	}

?>
<div class="blog-main">
	<?php
		if( count( $thebe_categories ) > 1 )
		{
			?>
			<div class="category-nav">
				<div class="inner-wrap">
					<ul>
					<?php
		 
						if( !$thebe_current_cat )
						{
							?><li class="active"><a href="<?php echo esc_url( $thebe_page_link ); ?>"><?php esc_html_e( 'All', 'thebe' ); ?></a></li><?php
						}
						else
						{
							?><li><a href="<?php echo esc_url( $thebe_page_link ); ?>"><?php esc_html_e( 'All', 'thebe' ); ?></a></li><?php
						}

						foreach( $thebe_categories as $thebe_cat_name )
						{
							if( $thebe_current_cat === $thebe_cat_name )
							{
								?><li class="active"><a href="<?php echo esc_url( add_query_arg( '_cat', $thebe_cat_name, $thebe_page_link ) ); ?>"><?php echo esc_html( $thebe_cat_name ); ?></a></li><?php
							}
							else
							{
								?><li><a href="<?php echo esc_url( add_query_arg( '_cat', $thebe_cat_name, $thebe_page_link ) ); ?>"><?php echo esc_html( $thebe_cat_name ); ?></a></li><?php
							}
						}
					?>
					</ul>
				</div>
			</div>
			<?php
		}
	?>
	<div class="wrap">
	<?php

		$thebe_blog_num = thebe_get_page_setting_property( 'blog_num', '12' );
		$thebe_blog_per_page = intval( $thebe_blog_num );
		if( $thebe_blog_per_page == 0 )
		{
			$thebe_blog_per_page = 12;
		}

		if ( get_query_var('paged') ) {
		    $thebe_blog_paged = get_query_var('paged');
		} elseif ( get_query_var('page') ) {
		    $thebe_blog_paged = get_query_var('page');
		} else {
		    $thebe_blog_paged = 1;
        }

        $thebe_blog_query_data = array(
            'posts_per_page' => $thebe_blog_per_page,
            'paged' => $thebe_blog_paged,
            'post_type' => 'post',
            'suppress_filters' => 0,
        );

        $thebe_data_src_type = thebe_get_page_setting_property( 'blog_data_src_type', '1' );
        if( $thebe_data_src_type === '1' )
        {
            $thebe_blog_query_data['category_name'] = $thebe_blog_category;
        }
		elseif( $thebe_data_src_type === '2' )
        {
            $thebe_blog_query_data['post__in'] = explode( ',', thebe_get_page_setting_property( 'blog_data_ids', '' ) );
        }
		elseif( $thebe_data_src_type === '3' )
        {
            $thebe_query_src_obj = wp_parse_args( wp_specialchars_decode( stripslashes( thebe_get_page_setting_property( 'blog_data_query_str', '' ) ) ) );
            $thebe_blog_query_data = array_merge( $thebe_blog_query_data, $thebe_query_src_obj );
        }
        		
		$thebe_blog_query_result = new WP_Query( $thebe_blog_query_data );

		if( $thebe_blog_query_result->have_posts() )
		{
			while( $thebe_blog_query_result->have_posts() )
			{
				$thebe_blog_query_result->the_post();
				
				$thebe_post_id = get_the_ID();
				$thebe_author_meta_id = get_the_author_meta('ID');
				$thebe_author_link = get_author_posts_url( $thebe_author_meta_id );
				$thebe_post_data = thebe_get_post_setting( $thebe_post_id );
				$thebe_post_title = get_the_title();

				$thebe_thumburl = '';
				$thebe_imageurl = '';

				$thebe_thumb_width = 500;
				$thebe_thumb_height = 500;
				
				if( has_post_thumbnail() )
				{
					if( !$thebe_large_img )
					{
						$thebe_img_obj = thebe_get_image(array(
							'rid' => get_post_thumbnail_id(),
							'max_width' => 500,
						));
					}
					else
					{
						$thebe_img_obj = thebe_get_image(array(
							'rid' => get_post_thumbnail_id(),
							'max_width' => 900,
						));
					}

					if( $thebe_img_obj )
					{
						$thebe_thumburl = $thebe_img_obj['thumb'];
						$thebe_imageurl = $thebe_img_obj['image'];

						$thebe_thumb_width = $thebe_img_obj['thumb_width'];
						$thebe_thumb_height = $thebe_img_obj['thumb_height'];
					}
				}

				$thebe_permalink = get_permalink( $thebe_post_id );

				$thebe_banner_type = thebe_get_property( $thebe_post_data, 'banner_type', '1' );
				$thebe_post_is_video = $thebe_banner_type === '3';

				$thebe_video_link = thebe_get_property( $thebe_post_data, 'video_link', '' );
				$thebe_video_type = thebe_get_property( $thebe_post_data, 'video_type', '1' );
				$thebe_video_volume = thebe_get_property( $thebe_post_data, 'video_volume', '1' );

				$thebe_box_class = 'item';

				if( $thebe_post_is_video && $thebe_video_link )
				{
					$thebe_box_class .= ' v-post';
				}

				if( !$thebe_thumburl && !$thebe_post_is_video )
				{
					$thebe_box_class .= ' text-post';
				}

				if( is_sticky() )
				{
					$thebe_box_class .= ' sticky';
				}

				$thebe_ajax_bg_color = thebe_get_property( $thebe_post_data, 'ajax_bg_color', '' );
				$thebe_ajax_text_color = thebe_get_property( $thebe_post_data, 'ajax_text_color', 'white' );

				?>
				<div <?php post_class( $thebe_box_class ); ?> data-w="<?php echo esc_attr( $thebe_thumb_width) ;?>" data-h="<?php echo esc_attr( $thebe_thumb_height) ;?>" data-bg-color="<?php echo esc_attr( $thebe_ajax_bg_color ); ?>" data-text-color="<?php echo esc_attr( $thebe_ajax_text_color ); ?>">
					<div class="img">
						<?php
							if( $thebe_post_is_video && $thebe_video_link )
							{
								?>
								<div class="bg-full" data-bg="<?php echo esc_url( $thebe_thumburl ); ?>" data-src="<?php echo esc_attr( $thebe_video_link ); ?>" data-type="<?php echo esc_attr( $thebe_video_type ); ?>" data-volume="<?php echo esc_attr( $thebe_video_volume ); ?>"></div>
								<?php
							}
							else
							{
								?>
								<div class="bg-full" data-bg="<?php echo esc_url( $thebe_thumburl ); ?>"></div>
								<?php
							}
						?>
						<a class="full" data-href="<?php echo esc_url( $thebe_permalink ); ?>"></a>
					</div>
					<div class="text">
						<?php
							$thebe_time_link = get_the_title() != '' ? get_month_link( get_the_time('Y'), get_the_time('m'), get_the_time('d')) : $thebe_permalink;

							if( $thebe_post_title != '' )
							{
								?><h4><a href="<?php echo esc_url( $thebe_permalink ); ?>"><?php echo esc_html( $thebe_post_title ); ?></a></h4><?php
							}

							$thebe_description_length = $thebe_raw_proportion ? 45 : 22;
							$thebe_post_description = wp_trim_words( get_the_excerpt(), $thebe_description_length );

							if( !$thebe_post_description )
							{
								$thebe_shortcode_text = thebe_get_property( $thebe_post_data, 'shortCodeText', '', 'pt_sc_' );
								if( $thebe_shortcode_text )
								{
									$thebe_post_description = wp_strip_all_tags( do_shortcode( shortcode_unautop( wp_specialchars_decode( stripslashes( $thebe_shortcode_text ), ENT_QUOTES ) ) ) );
									$thebe_post_description = wp_trim_words( $thebe_post_description, $thebe_description_length );
								}
							}

							if( $thebe_post_description )
							{
								?><div class="intro"><?php echo esc_html( $thebe_post_description ); ?></div><?php
							}
						?>
						<div class="btn-wrap">
							<a class="btn" href="<?php echo esc_url( $thebe_permalink ); ?>"><span><?php esc_html_e('Read More', 'thebe'); ?></span></a>
						</div>
						<div class="list-meta">
							<div class="main-meta">
								<a class="avatar" href="<?php echo esc_url( $thebe_author_link ); ?>"><?php echo get_avatar( $thebe_author_meta_id, 100 ); ?></a>
								<div class="info">
									<a class="author" href="<?php echo esc_url( $thebe_author_link ); ?>"><?php echo get_the_author(); ?></a>
									<div class="date"><a href="<?php echo esc_url( $thebe_time_link ); ?>"><?php echo esc_html( get_the_date( get_option('date_format', 'F d Y') ) ); ?></a></div>
								</div>
							</div>
							<div class="category">
							<?php 
								$thebe_categories = get_the_category();
								if( $thebe_categories )
								{
									foreach($thebe_categories as $thebe_cat)
									{
										?><a href="<?php echo esc_url( get_category_link( $thebe_cat->term_id ) ); ?>"><?php echo esc_html( $thebe_cat->cat_name ); ?></a><?php
									}
								}
							?>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
		}

		echo thebe_paging_nav( $thebe_blog_query_result, $thebe_blog_paged );
	?>
	</div>
</div>
<?php
	if( is_active_sidebar('sidebar-right') && thebe_get_page_setting_property( 'blog_show_widgets', '1' ) === '1' )
	{
		?>
		<div class="pt-widget-list">
			<?php get_sidebar(); ?>
		</div>
		<?php
	}
?>