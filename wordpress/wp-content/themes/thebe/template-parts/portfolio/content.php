<?php
	$thebe_list_class_arr = array( 'pic-list' );

	if( $thebe_portfolio_type === '7' )
	{
		$thebe_list_class_arr[] = 'grid-list';

		if( thebe_get_page_setting_property( 'portfolio_wide', '0' ) === '1' )
		{
			$thebe_list_class_arr[] = 'wide';
		}
	}
	else
	{
		//
	}

	$thebe_img_size_rules = array(
		array(
			//
		),
		array(
			array( 'max_width' => 1240, 'max_height' => 0 ),
			array( 'max_width' => 1240, 'max_height' => 0 ),
		),
		array(
			array( 'max_width' => 2200, 'max_height' => 0 ),
			array( 'max_width' => 1240, 'max_height' => 0 ),
		),
		array(
			array( 'max_width' => 1240, 'max_height' => 0 ),
			array( 'max_width' => 560, 'max_height' => 0 ),
		),
		array(
			array( 'max_width' => 0, 'max_height' => 840 ),
			array( 'max_width' => 0, 'max_height' => 840 ),
		),
		array(
			array( 'max_width' => 1240, 'max_height' => 0 ),
			array( 'max_width' => 0, 'max_height' => 840 ),
		),
		array(
			array( 'max_width' => 640, 'max_height' => 0 ),
			array( 'max_width' => 640, 'max_height' => 0 ),
		),
	);

	$thebe_portfolio_category = thebe_get_page_setting_property( 'portfolio_category', '' );

	if( $thebe_use_ajax )
	{
		$thebe_list_class_arr[] = 'ajax';
	}

	if( thebe_get_page_setting_property( 'portfolio_large_text', '0' ) === '1' && $thebe_portfolio_type !== '7' )
	{
		$thebe_list_class_arr[] = 'large-text';
	}

	$thebe_thumb_type = thebe_get_page_setting_property( 'portfolio_thumb_type', 'type-h' );
	$thebe_columns = thebe_get_page_setting_property( 'portfolio_columns', 'col-auto' );
	$thebe_caption_pos = thebe_get_page_setting_property( 'portfolio_caption_pos', '1' );

	$thebe_list_class_arr[] = $thebe_thumb_type;
	$thebe_list_class_arr[] = $thebe_columns;
	$thebe_list_class_arr[] = 'caption-0'.$thebe_caption_pos;

?>
<div class="<?php echo esc_attr( implode(' ', $thebe_list_class_arr) ); ?>">
	<?php
		$thebe_portfolio_categories = get_categories(array(
			'taxonomy' => 'project_cat',
		));

		if( $thebe_portfolio_type !== '7' )
		{
			$thebe_portfolio_filter_enabled = false;
		}
		elseif( $thebe_portfolio_category == '' )
		{
			$thebe_portfolio_filter_enabled = true;
		}
		else
		{
			$thebe_portfolio_filter_comma = strpos( $thebe_portfolio_category, ',' );
			$thebe_portfolio_filter_enabled = $thebe_portfolio_filter_comma !== false && $thebe_portfolio_filter_comma > 0;
		}

		if( $thebe_portfolio_filter_enabled && count( $thebe_portfolio_categories ) > 1 )
		{
			?>
			<div class="filter">
				<i class="btn call-filter"><?php esc_html_e( 'Filter', 'thebe' ); ?></i>
				<ul class="filter-ctrl">
					<li class="active" data-filter="*"><i><?php esc_html_e( 'Show All', 'thebe' ); ?></i></li>
					<?php
						if( $thebe_portfolio_category == '' )
						{
							foreach( $thebe_portfolio_categories as $thebe_cat )
							{
								?><li data-filter=".filter-cat-<?php echo esc_attr( $thebe_cat->term_id ); ?>"><i><?php echo esc_html( $thebe_cat->name ); ?></i></li><?php
							}
						}
						else
						{
							$thebe_cats = explode( ',', $thebe_portfolio_category );
							foreach( $thebe_portfolio_categories as $thebe_cat )
							{
								if( in_array($thebe_cat->name, $thebe_cats) )
								{
									?><li data-filter=".filter-cat-<?php echo esc_attr( $thebe_cat->term_id ); ?>"><i><?php echo esc_html( $thebe_cat->name ); ?></i></li><?php
								}
							}
						}
					?>
				</ul>
			</div>
			<?php
		}
	?>
	<div class="wrap">
	<?php

		$thebe_portfolio_num = thebe_get_page_setting_property( 'portfolio_num', '12' );
		$thebe_portfolio_per_page = intval( $thebe_portfolio_num );
		if( $thebe_portfolio_per_page == 0 )
		{
			$thebe_portfolio_per_page = 12;
		}

		if ( get_query_var('paged') ) {
		    $thebe_portfolio_paged = get_query_var('paged');
		} elseif ( get_query_var('page') ) {
		    $thebe_portfolio_paged = get_query_var('page');
		} else {
		    $thebe_portfolio_paged = 1;
        }
        
        $thebe_portfolio_query_data = array(
            'posts_per_page' => $thebe_portfolio_per_page,
            'paged' => $thebe_portfolio_paged,
            'post_type' => 'project',
            'suppress_filters' => 0,
        );

        $thebe_data_src_type = thebe_get_page_setting_property( 'portfolio_data_src_type', '1' );
        if( $thebe_data_src_type === '1' )
        {
            $thebe_portfolio_query_data['project_cat'] = $thebe_portfolio_category;
        }
		elseif( $thebe_data_src_type === '2' )
        {
            $thebe_portfolio_query_data['post__in'] = explode( ',', thebe_get_page_setting_property( 'portfolio_data_ids', '' ) );
        }
		elseif( $thebe_data_src_type === '3' )
        {
            $thebe_query_src_obj = wp_parse_args( wp_specialchars_decode( stripslashes( thebe_get_page_setting_property( 'portfolio_data_query_str', '' ) ) ) );
            $thebe_portfolio_query_data = array_merge( $thebe_portfolio_query_data, $thebe_query_src_obj );
        }
		
		$thebe_portfolio_query_result = new WP_Query( $thebe_portfolio_query_data );

		if( $thebe_portfolio_query_result->have_posts() )
		{
			while( $thebe_portfolio_query_result->have_posts() )
			{
				$thebe_portfolio_query_result->the_post();
				
				$thebe_post_id = get_the_ID();
				$thebe_post_data = thebe_get_post_setting( $thebe_post_id );
				$thebe_post_title = get_the_title();

				$thebe_project_style = thebe_get_property( $thebe_post_data, 'project_style', '1', 'pt_project_' );

				$thebe_thumburl = '';
				$thebe_imageurl = '';

				$thebe_thumb_width = 0;
				$thebe_thumb_height = 0;

				$thebe_img_size_index = $thebe_portfolio_type === '1' ? intval( $thebe_project_style ) : intval( $thebe_portfolio_type ) - 1;
				$thebe_img_size_arr = $thebe_img_size_rules[$thebe_img_size_index];
				
				if( has_post_thumbnail() )
				{
					$thebe_img_obj = thebe_get_image(array(
						'rid' => get_post_thumbnail_id(),
						'max_width' => $thebe_img_size_arr[0]['max_width'],
						'max_height' => $thebe_img_size_arr[0]['max_height'],
					));

					if( $thebe_img_obj )
					{
						$thebe_thumburl = $thebe_img_obj['thumb'];
						$thebe_imageurl = $thebe_img_obj['image'];

						$thebe_thumb_width = $thebe_img_obj['thumb_width'];
						$thebe_thumb_height = $thebe_img_obj['thumb_height'];
					}
				}

				if( !$thebe_thumburl )
				{
					continue;
				}

				$thebe_custom_hyperlink = thebe_get_property( $thebe_post_data, 'custom_hyperlink', '', 'pt_project_' );
				
				$thebe_permalink = $thebe_custom_hyperlink ? $thebe_custom_hyperlink : get_permalink( $thebe_post_id );

				$thebe_additional_img_url = '';
				$thebe_additional_img_w = 0;
				$thebe_additional_img_h = 0;
				$thebe_additional_img_rid = thebe_get_property( $thebe_post_data, 'additional_img', '', 'pt_project_' );
				if( $thebe_additional_img_rid != '' )
				{
					$thebe_img_obj = thebe_get_image(array(
						'rid' => $thebe_additional_img_rid,
						'max_width' => $thebe_img_size_arr[1]['max_width'],
						'max_height' => $thebe_img_size_arr[1]['max_height'],
					));

					if( $thebe_img_obj )
					{
						$thebe_additional_img_url = $thebe_img_obj['thumb'];
						$thebe_additional_img_w = $thebe_img_obj['thumb_width'];
						$thebe_additional_img_h = $thebe_img_obj['thumb_height'];
					}
				}

				$thebe_banner_type = thebe_get_property( $thebe_post_data, 'banner_type', '1', 'pt_project_' );
				$thebe_post_is_video = $thebe_banner_type === '3';

				$thebe_video_link = thebe_get_property( $thebe_post_data, 'video_link', '', 'pt_project_' );
				$thebe_video_type = thebe_get_property( $thebe_post_data, 'video_type', '1', 'pt_project_' );
				$thebe_video_volume = thebe_get_property( $thebe_post_data, 'video_volume', '1', 'pt_project_' );

				$thebe_lightbox_mode = thebe_get_property( $thebe_post_data, 'lightbox_mode', '0', 'pt_project_' );

				$thebe_box_class = 'item';

				if( $thebe_portfolio_type === '1' )
				{
					$thebe_box_class .= ' p-style-0'.$thebe_project_style;
				}
				else if( $thebe_portfolio_type !== '7' )
				{
					$thebe_box_class .= ' p-style-0'.( intval( $thebe_portfolio_type ) - 1 );
				}

				if( $thebe_post_is_video && $thebe_video_link )
				{
					$thebe_box_class .= ' v-post';
				}

				if( $thebe_custom_hyperlink )
				{
					$thebe_box_class .= ' has-custom-link';
				}

				$thebe_categories = get_the_terms( $thebe_post_id, 'project_cat' );
				$thebe_filter_keys = array();
				if( $thebe_categories )
				{
					foreach( $thebe_categories as $thebe_cat )
					{
						$thebe_filter_keys[] = 'filter-cat-'.$thebe_cat->term_id;
					}
				}

				if( count($thebe_filter_keys) > 0 )
				{
					$thebe_box_class .= ' '.implode(' ', $thebe_filter_keys);
				}

				$thebe_ajax_bg_color = thebe_get_property( $thebe_post_data, 'ajax_bg_color', '', 'pt_project_' );
				$thebe_ajax_text_color = thebe_get_property( $thebe_post_data, 'ajax_text_color', 'white', 'pt_project_' );

				$thebe_header_color = thebe_get_property( $thebe_post_data, 'header_color', 'white', 'pt_project_' );
				$thebe_title_color = thebe_get_property( $thebe_post_data, 'title_color', '', 'pt_project_' );

				if( $thebe_lightbox_mode === '1' )
				{
					// thebe_generate_project_lightbox_data can be found at inc/theme/functions/content-functions.php
					$thebe_post_gallery_json = thebe_generate_project_lightbox_data( $thebe_post_data, $thebe_imageurl ); 
				}

				$thebe_has_lightbox_data = $thebe_lightbox_mode === '1' && count( $thebe_post_gallery_json['src'] ) > 0;

				if( $thebe_has_lightbox_data )
				{
					$thebe_box_class .= ' lightbox-item';
				}

				?>
				<div class="<?php echo esc_attr( $thebe_box_class ); ?>" data-w="<?php echo esc_attr( $thebe_thumb_width ); ?>" data-h="<?php echo esc_attr( $thebe_thumb_height ); ?>" data-bg-color="<?php echo esc_attr( $thebe_ajax_bg_color ); ?>" data-text-color="<?php echo esc_attr( $thebe_ajax_text_color ); ?>" data-header-color="<?php echo esc_attr( $thebe_header_color ); ?>" data-title-color="<?php echo esc_attr( $thebe_title_color ); ?>">
					<div class="inner-wrap">
						<?php
							if( $thebe_has_lightbox_data )
							{
								?><div class="img" data-src-type="json" data-src="<?php echo esc_attr( wp_json_encode( $thebe_post_gallery_json ) ); ?>"><?php
							}
							else
							{
								?><div class="img"><?php
							}
							?>
							<div class="img-main">
							<?php
								if( $thebe_post_is_video && $thebe_video_link )
								{
									?><div class="bg-full" data-bg="<?php echo esc_url( $thebe_thumburl ); ?>" data-src="<?php echo esc_url( $thebe_video_link ); ?>" data-type="<?php echo esc_attr( $thebe_video_type ); ?>" data-volume="<?php echo esc_attr( $thebe_video_volume ); ?>"></div><?php
								}
								else
								{
									?><div class="bg-full" data-bg="<?php echo esc_url( $thebe_thumburl ); ?>"></div><?php
								}

								if( !$thebe_has_lightbox_data )
								{
									?>
									<a class="full" data-href="<?php echo esc_url( $thebe_permalink ); ?>"></a>
									<?php
								}
							?>
							</div>
							<?php
								if( $thebe_additional_img_url )
								{
									?>
									<div class="img-addition" data-w="<?php echo esc_attr( $thebe_additional_img_w ); ?>" data-h="<?php echo esc_attr( $thebe_additional_img_h ); ?>">
										<div class="bg-full" data-bg="<?php echo esc_url( $thebe_additional_img_url ); ?>"></div>
										<?php
											if( !$thebe_has_lightbox_data )
											{
												?>
												<a class="full" data-href="<?php echo esc_url( $thebe_permalink ); ?>"></a>
												<?php
											}
										?>
									</div>
									<?php
								}
							?>
						</div>
						<div class="text">
							<div class="list-category">
							<?php
								if( $thebe_categories )
								{
									foreach( $thebe_categories as $thebe_cat )
									{
										?><a href="<?php echo esc_url( get_term_link( $thebe_cat->term_id ) ); ?>"><?php echo esc_html( $thebe_cat->name ); ?></a><?php
									}
								}
							?>
							</div>
							<?php
								if( $thebe_post_title != '' )
								{
									?>
									<h2>
										<span><?php echo esc_html( $thebe_post_title ); ?></span>
										<?php
											if( !$thebe_has_lightbox_data )
											{
												?>
												<a class="full" data-href="<?php echo esc_url( $thebe_permalink ); ?>"></a>
												<?php
											}
										?>
									</h2>
									<?php
								}
							?>
						</div>

					</div>
				</div>
				<?php
			}
		}

		if( !$thebe_portfolio_slider )
		{
			echo thebe_paging_nav( $thebe_portfolio_query_result, $thebe_portfolio_paged );
		}
	?>
	</div>
</div>