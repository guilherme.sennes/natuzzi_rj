<div class="page-other null">
	<div class="text">
		<?php
			$thebe_not_found_content_msg = thebe_get_theme_option('thebe_not_found_content_msg', '');
			if($thebe_not_found_content_msg == '')
			{
				?><h3><?php esc_html_e('It seems we can&rsquo;t find what you&rsquo;re looking for.', 'thebe')?></h3><?php
			}
			else
			{
				thebe_kses_content( $thebe_not_found_content_msg );
			}

		?>
		<h6><?php esc_html_e('Try search', 'thebe'); ?></h6>
		<?php 
			global $thebe_global_searth_form_type;
			$thebe_global_searth_form_type = '2';
			
			get_search_form();
		?>
		<div>
			<a class="btn" href="<?php echo esc_url( home_url('/') ); ?>"><?php esc_html_e('Back to homepage', 'thebe'); ?></a>
		</div>
	</div>
</div>