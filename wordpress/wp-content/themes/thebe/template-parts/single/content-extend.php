<?php
	$thebe_post_gallery_data = thebe_parse_image_group_data( thebe_get_property( $thebe_post_data, 'post_gallery', '', $thebe_prop_prefix ) );
	$thebe_post_gallery_items = $thebe_post_gallery_data['item'];

	if( count( $thebe_post_gallery_items ) > 0 )
	{
		?>
		<div class="single-extend">
		<?php
			foreach( $thebe_post_gallery_items as $thebe_post_gallery_item )
			{
				$thebe_extend_item_class = 'item';
				$thebe_post_gallery_vars = $thebe_post_gallery_item['variables'];

				if( intval( $thebe_post_gallery_vars['gap'] ) === 1 )
				{
					$thebe_extend_item_class .= ' gap';
				}

				$thebe_extend_item_class .= ' '.$thebe_post_gallery_vars['display'];

				$thebe_img_obj = thebe_get_image(array(
					'rid' => $thebe_post_gallery_item['rid'],
					'max_width' => $thebe_post_gallery_vars['display'] === 'boxed' ? 1000 : 1680,
				));
				
				$thebe_post_extend_thumb = '';
				$thebe_post_extend_img = '';
				if(	$thebe_img_obj )
				{
					$thebe_post_extend_thumb = $thebe_img_obj['thumb'];
					$thebe_post_extend_img = $thebe_img_obj['image'];
				}
				else
				{
					continue;
				}

				$thebe_use_video = intval( $thebe_post_gallery_vars['video_type'] ) !== 0;

				?>
				<div class="<?php echo esc_attr( $thebe_extend_item_class ); ?>" data-w="<?php echo esc_attr( $thebe_img_obj['thumb_width'] ); ?>" data-h="<?php echo esc_attr( $thebe_img_obj['thumb_height'] ); ?>">
					<div class="img">
					<?php
						if( $thebe_use_video )
						{
							?><div class="bg-full" data-bg="<?php echo esc_url( $thebe_post_extend_thumb ); ?>" data-src="<?php echo esc_url( $thebe_post_gallery_vars['video_link'] ); ?>" data-type="<?php echo esc_attr( $thebe_post_gallery_vars['video_type'] ); ?>" data-volume="<?php echo esc_attr( $thebe_post_gallery_vars['video_volume'] ); ?>"></div><?php
						}
						else
						{
							?><div class="bg-full" data-bg="<?php echo esc_url( $thebe_post_extend_thumb ); ?>"></div><?php
						}
					?>
					</div>
					<?php
						if( $thebe_post_gallery_vars['title'] || $thebe_post_gallery_vars['des'] )
						{
							?>
							<div class="text">
								<?php
									if( $thebe_post_gallery_vars['title'] )
									{
										?><h2><?php thebe_kses_content( $thebe_post_gallery_vars['title'] ); ?></h2><?php
									}

									if( $thebe_post_gallery_vars['des'] )
									{
										?><p><?php thebe_kses_content( $thebe_post_gallery_vars['des'] ); ?></p><?php
									}
								?>
							</div>
							<?php
						}
					?>
				</div>
				<?php
			}
		?>
		</div>
		<?php
	}
?>
