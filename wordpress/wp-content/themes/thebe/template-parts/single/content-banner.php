<?php

	$thebe_banner_class = 'single-header';

	$thebe_shape_type = thebe_get_property( $thebe_post_data, 'shape_type', 'type-h', $thebe_prop_prefix );

	if( !$thebe_post_is_video )
	{
		$thebe_rawsize = thebe_get_property( $thebe_post_data, 'rawsize', '1', $thebe_prop_prefix );
		$thebe_banner_class .= ' '.( $thebe_rawsize === '1' ? 'raw-proportion' : '' );
		
		$thebe_banner_class .= ' '.$thebe_shape_type;
	}

	$thebe_image_list = thebe_get_property( $thebe_post_data, 'image_list', '', $thebe_prop_prefix );
	$thebe_img_list_arr = array();

	if( !$thebe_post_is_video && ( $thebe_banner_type === '1' || ( $thebe_banner_type === '2' && $thebe_image_list ) ) )
	{
		$thebe_image_list_data = explode(',', $thebe_image_list);
		$thebe_images_count = count( $thebe_image_list_data );

		if( $thebe_banner_type === '2' )
		{
			if( $thebe_images_count < 2 )
			{
				$thebe_image_size = 1680;
			}
			elseif( $thebe_images_count < 3 )
			{
				$thebe_image_size = 890;
			}
			else
			{
				$thebe_image_size = 800;
			}
		}
		else
		{
			$thebe_image_size = 1680;
		}

		foreach( $thebe_image_list_data as $thebe_image_item_id )
		{
			if( $thebe_image_item_id === '' ) continue;

			$thebe_img_obj = thebe_get_image(array(
				'rid' => $thebe_image_item_id,
				'max_width' => $thebe_image_size,
			));
			
			if( $thebe_img_obj )
			{
				$thebe_img_list_arr[] = array('url' => $thebe_img_obj['thumb'], 'image' => $thebe_img_obj['image'], 'w' => $thebe_img_obj['thumb_width'], 'h' => $thebe_img_obj['thumb_height']);
			}
		}

		if( count( $thebe_img_list_arr ) === 0 && has_post_thumbnail() )
		{
			$thebe_image_rid = get_post_thumbnail_id();

			$thebe_img_obj = thebe_get_image(array(
				'rid' => $thebe_image_rid,
				'max_width' => $thebe_image_size,
			));

			if( $thebe_img_obj )
			{
				$thebe_img_list_arr[] = array('url' => $thebe_img_obj['thumb'], 'image' => $thebe_img_obj['image'], 'w' => $thebe_img_obj['thumb_width'], 'h' => $thebe_img_obj['thumb_height']);
			}
		}
	}
	else
	{
		$thebe_image_rid = '';
		if( $thebe_post_is_video )
		{
			$thebe_image_rid = thebe_get_property( $thebe_post_data, 'video_bg_img', '', $thebe_prop_prefix );
		}
		
		if( !$thebe_image_rid && has_post_thumbnail() )
		{
			$thebe_image_rid = get_post_thumbnail_id();
		}

		$thebe_img_obj = thebe_get_image(array(
			'rid' => $thebe_image_rid,
			'max_width' => 1680,
		));

		if( $thebe_img_obj )
		{
			$thebe_img_list_arr[] = array('url' => $thebe_img_obj['thumb'], 'image' => $thebe_img_obj['image'], 'w' => $thebe_img_obj['thumb_width'], 'h' => $thebe_img_obj['thumb_height']);
		}
	}

	$thebe_img_count = count( $thebe_img_list_arr );

	if( !$thebe_post_is_video && $thebe_img_count === 0 )
	{
		$thebe_banner_class .= ' only-text';
	}

	$thebe_single_last_row = '';
	if( $thebe_shape_type === 'type-auto' )
	{
		$thebe_single_last_row = 'style-0'.thebe_get_property( $thebe_post_data, 'last_row', '1', $thebe_prop_prefix );
	}

	$thebe_banner_autoplay = '';
	$thebe_banner_duration = '';

	if( $thebe_banner_type === '1' )
	{
		$thebe_banner_autoplay = thebe_get_property( $thebe_post_data, 'banner_autoplay', '0', $thebe_prop_prefix );
		$thebe_banner_duration = thebe_get_property( $thebe_post_data, 'banner_duration', '5000', $thebe_prop_prefix );
	}
	
?>
<div class="<?php echo esc_attr($thebe_banner_class); ?>" data-last-row="<?php echo esc_attr( $thebe_single_last_row ); ?>" data-autoplay="<?php echo esc_attr( $thebe_banner_autoplay ); ?>" data-duration="<?php echo esc_attr( $thebe_banner_duration ); ?>">
	<div class="wrap">
	<?php
		if( $thebe_img_count > 0 )
		{
			foreach( $thebe_img_list_arr as $thebe_img_obj )
			{
				if( $thebe_post_is_video && $thebe_video_link )
				{
					?>
					<div class="item" data-w="<?php echo esc_attr( $thebe_img_obj['w'] ); ?>" data-h="<?php echo esc_attr( $thebe_img_obj['h'] ); ?>">
						<div class="img">
							<div class="bg-full" data-bg="<?php echo esc_url( $thebe_img_obj['url'] ); ?>" data-src="<?php echo esc_url( $thebe_video_link ); ?>" data-type="<?php echo esc_attr( $thebe_video_type ); ?>" data-volume="<?php echo esc_attr( $thebe_video_volume ); ?>"></div>
						</div>
					</div>
					<?php
				}
				else
				{
					?>
					<div class="item" data-w="<?php echo esc_attr( $thebe_img_obj['w'] ); ?>" data-h="<?php echo esc_attr( $thebe_img_obj['h'] ); ?>">
						<div class="img">
							<div class="bg-full" data-bg="<?php echo esc_url( $thebe_img_obj['url'] ); ?>" data-src="<?php echo esc_url( $thebe_img_obj['image'] ); ?>"></div>
						</div>
					</div>
					<?php
				}
			}
		}
		else if( $thebe_post_is_video && $thebe_video_link )
		{
			?>
			<div class="item">
				<div class="img">
					<div class="bg-full" data-src="<?php echo esc_url( $thebe_video_link ); ?>" data-type="<?php echo esc_attr( $thebe_video_type ); ?>" data-volume="<?php echo esc_attr( $thebe_video_volume ); ?>"></div>
				</div>
			</div>
			<?php
		}
	?>
	</div>
</div>