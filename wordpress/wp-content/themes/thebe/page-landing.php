<?php
/*
 * Template Name: Landing
 */
	thebe_prepare_setting(
		array(
			'type' => 'page',
			'template' => 'landing',
		)
	);

	get_header();

	if ( post_password_required() )
	{
		?>
		<div class="page-password-required password-required-msg">
		<?php
			the_content();
		?>
		</div>
		<?php
	}
	else
	{
		?>
		<div class="landing"></div>
		<?php
	}
	
	get_footer(); 
?>