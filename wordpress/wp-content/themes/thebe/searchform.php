<?php
/**
 * Template for displaying search forms in thebe
 */
	global $thebe_global_searth_form_type;
	if( !isset($thebe_global_searth_form_type) )
	{
		$thebe_global_searth_form_type = '';
	}

	if( $thebe_global_searth_form_type == '' )
	{
		?>
		<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url('/') ); ?>">
			<label>
				<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'thebe' ); ?>" value="<?php echo esc_attr(get_search_query()); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'thebe' ); ?>" />
			</label>
			<button type="submit" class="search-submit"><span class="screen-reader-text"><?php echo esc_html_x( 'Search', 'submit button', 'thebe' ); ?></span></button>
		</form>
		<?php
	}
	elseif( $thebe_global_searth_form_type == '1' )
	{	
		?>
		<div class="header-search">
			<i class="call-search btn"><span><?php esc_html_e('Search', 'thebe'); ?></span></i>
			<form role="search" method="get" action="<?php echo esc_url( home_url('/') ); ?>">
				<div class="wrap">
					<input class="search" type="search" placeholder="<?php esc_attr_e('Digite sua busca', 'thebe'); ?>" name="s"/>
					<input type="submit" class="search-btn" value="<?php esc_html_e('Pesquisar', 'thebe'); ?>" />
					<i class="close-search btn"></i>
				</div>
			</form>
		</div>
		<?php
	}
	elseif ( $thebe_global_searth_form_type == '2' )
	{
		?>
		<form role="search" method="get" action="<?php echo esc_url( home_url('/') ); ?>">
			<input type="search" class="search-field" placeholder="<?php esc_attr_e('Search &hellip;', 'thebe'); ?>" name="s">
			<button type="submit" class="search-submit"><span class="screen-reader-text"><?php esc_attr_e('Search', 'thebe'); ?></span></button>
		</form>
		<?php
	}

	$thebe_global_searth_form_type = '';//clear global value
?>