<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<?php do_action('thebe_seo'); ?>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php
		if ( is_singular() && pings_open( get_queried_object() ) )
		{
			?><link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"><?php
		}

		wp_head();
	?>
</head>
<?php
	$thebe_loading_img_url = '';
	$thebe_loading_img = thebe_get_theme_option('thebe_loading_img', '');
	$thebe_img_obj = thebe_get_image(array(
		'rid' => $thebe_loading_img,
		'max_width' => 100,
	));

	if( $thebe_img_obj )
	{
		$thebe_loading_img_url = $thebe_img_obj['thumb'];
	}

	$thebe_site_style = thebe_get_theme_option('thebe_site_style', '1') === '1' ? 'light' : 'dark';
	$thebe_main_color = thebe_get_theme_option('thebe_main_color', '#05b14a');
?>
<body <?php body_class(); ?> data-color="<?php echo esc_attr( $thebe_main_color ); ?>">
	<div class="site-loader">
		<?php
			if( $thebe_loading_img_url )
			{
				?><img class="loader-img" src="<?php echo esc_url( $thebe_loading_img_url ); ?>" alt="<?php esc_attr_e('Loading', 'thebe'); ?>"/><?php
			}
		?>
		<div class="loader-icon"><i></i><i></i><i></i></div>
		<div class="loader-bg"></div>
	</div>
	<?php
		thebe_output_site_bg();

		$thebe_menu_style = thebe_get_theme_option('thebe_menu_style', '3');
		$thebe_menu_class = '';
		if( $thebe_menu_style === '1' )
		{
			$thebe_menu_class = 'menu-style-01 menu-open-mode-02';
		}
		elseif( $thebe_menu_style === '2' )
		{
			$thebe_menu_class = 'menu-style-02 menu-open-mode-01';
		}
		else if( $thebe_menu_style === '3' )
		{
			$thebe_menu_class = 'menu-style-01 menu-open-mode-02 outside-menu';
		}

		$thebe_menu_text_color = thebe_get_theme_option('thebe_menu_text_color', 'white');
		$thebe_menu_bg_color = thebe_get_theme_option('thebe_menu_bg_color', '');

		$thebe_additional_menu = thebe_get_theme_option('thebe_additional_menu', '');

		$thebe_additional_menu_class = $thebe_additional_menu ? '' : ' no-addition-menu';
	?>

	<header class="pt-header <?php echo esc_attr( $thebe_menu_class ); ?><?php echo esc_attr( $thebe_additional_menu_class ); ?>" data-text-color="<?php echo esc_attr( $thebe_menu_text_color ); ?>" data-bg-color="<?php echo esc_attr( $thebe_menu_bg_color ); ?>">
		<div class="wrap">

			<?php thebe_output_logo( $thebe_site_style ); ?>
			
			<div class="header-right">
			<?php
				if( thebe_get_theme_option('thebe_show_search_button', '1') == '1' )
				{
					global $thebe_global_searth_form_type;
					$thebe_global_searth_form_type = '1';
					
					get_search_form();
				}

				if( THEBE_HAS_WOOCOMMERCE )
				{
					?>
					<div class="pt-mini-cart">
						<i class="btn call-mini-cart">
							<span class="cart-count"></span>
						</i>
						<div class="widget_shopping_cart_content"></div>
					</div>
					<?php
				}

				?>
				<i class="call-menu btn"></i>
			</div>

			<div class="menu-wrap">
				<i class="close-menu btn"></i>

				<?php
					if( $thebe_menu_style !== '2' && $thebe_additional_menu )
					{
						?>
						<nav class="addition-menu">
							<div class="wrap">
								<ul>

								<?php
									$thebe_additional_menu_items = thebe_parse_group_data( $thebe_additional_menu, array(
										'title' => '',
										'link' => '',
										'src' => ''
									) );

									if( $thebe_menu_style === '3' )
									{
										$thebe_additional_img_size = 680;
									}
									else
									{
										$thebe_additional_img_size = count( $thebe_additional_menu_items ) < 3 ? 530 : 340;
									}

									foreach( $thebe_additional_menu_items as $thebe_additional_menu_item )
									{
										$thebe_additional_menu_img = '';

										$thebe_img_obj = thebe_get_image(array(
											'rid' => $thebe_additional_menu_item['src'],
											'width' => $thebe_additional_img_size,
											'height' => $thebe_additional_img_size,
										));

										if( $thebe_img_obj )
										{
											$thebe_additional_menu_img = $thebe_img_obj['thumb'];
										}

										?>
										<li>
											<div class="inner-wrap">
												<div class="bg-full" data-bg="<?php echo esc_url( $thebe_additional_menu_img ); ?>"></div>
												<div class="bg-color"></div>
												<p><?php thebe_kses_content( $thebe_additional_menu_item['title'] ); ?></p>
												<?php
													if( $thebe_additional_menu_item['link'] )
													{
														?><a class="full" href="<?php echo esc_url( $thebe_additional_menu_item['link'] ); ?>"></a><?php
													}
												?>
											</div>
										</li>
										<?php
									}
								?>

								</ul>
							</div>
						</nav>
						<?php
					}

					thebe_output_menu( 'primary', 'nav', 'main-menu-list', 'menu-list', 'main-menu', 3, true );
				?>				
			</div>
		</div>
	</header>
	
	<div class="main-content">
	<?php do_action( 'thebe_action_header_ready' ); ?>