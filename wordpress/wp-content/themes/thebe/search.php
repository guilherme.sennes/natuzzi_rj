<?php
	global $thebe_global_site_vars;
	if( !isset($thebe_global_site_vars) )
	{
		$thebe_global_site_vars = array();
	}

	$thebe_global_site_vars['use_widget'] = is_active_sidebar('sidebar-right');
	
	get_header();

	if( have_posts() )
	{
		global $wp_query;
		$thebe_found_posts_count = $wp_query->found_posts;

		?>
		<div class="title-group parallax style-03">
			<div class="text">
			<?php
				if( $thebe_found_posts_count > 1 )
				{
					?><h1><?php echo esc_html( $thebe_found_posts_count ); ?> <?php esc_html_e('Results for', 'thebe'); ?> <?php echo '"'.esc_html( get_search_query() ).'"'; ?></h1><?php
				}
				else
				{
					?><h1><?php echo esc_html( $thebe_found_posts_count ); ?> <?php esc_html_e('Result for', 'thebe'); ?> <?php echo '"'.esc_html( get_search_query() ).'"'; ?></h1><?php
				}
			?>
			</div>
		</div>
		<?php

		get_template_part('template-parts/content', 'archive'); 
	}
	else
	{
		get_template_part('template-parts/content', 'none'); 
	}

	get_footer();
?>