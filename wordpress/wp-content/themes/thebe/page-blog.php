<?php
/*
 * Template Name: Blog
 */
	thebe_prepare_setting(
		array(
			'type' => 'page',
			'template' => 'blog',
		)
	);

	global $thebe_global_site_vars;

	$thebe_global_site_vars['use_widget'] = is_active_sidebar('sidebar-right') && thebe_get_page_setting_property( 'blog_show_widgets', '1' ) === '1';
	
	$thebe_use_ajax = thebe_get_page_setting_property( 'blog_use_ajax', '0' ) === '1';

	if( $thebe_use_ajax )
	{
		$thebe_global_site_vars['use_ajax'] = true;
	}

	$thebe_global_site_vars['ajax_fullscreen'] = thebe_get_page_setting_property( 'blog_fullscreen', '0' ) === '1';

	get_header();

	if ( post_password_required() )
	{
		?>
		<div class="page-password-required password-required-msg">
		<?php
			the_content();
		?>
		</div>
		<?php
	}
	else
	{
		thebe_output_page_title();

		$thebe_raw_proportion = thebe_get_page_setting_property( 'blog_rawsize', '1' ) === '1';
		$thebe_raw_class = $thebe_raw_proportion ? ' raw-proportion' : '';

		$thebe_large_img = thebe_get_page_setting_property( 'blog_large_img', '0' ) === '1';
		$thebe_large_img_class = $thebe_large_img ? ' large-image' : '';

		$thebe_use_ajax_class = $thebe_use_ajax ? ' ajax' : '';

		$thebe_blog_style = ' style-0'.thebe_get_page_setting_property( 'blog_style', '1' );

		$thebe_list_class = 'blog-list'.$thebe_blog_style.$thebe_raw_class.$thebe_use_ajax_class.$thebe_large_img_class;
		?>
		<div class="<?php echo esc_attr( $thebe_list_class ); ?>">
		<?php
			require thebe_parse_part_path( '/template-parts/blog/content' );
		?>
		</div>
		<?php
	}
	
	get_footer(); 
?>