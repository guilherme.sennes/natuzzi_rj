<?php
	get_header(); 
	?>
	<div class="page-other p404">
		<div class="text">
		<?php
			$thebe_not_found_page_msg = thebe_get_theme_option('thebe_not_found_page_msg', '');
			if($thebe_not_found_page_msg == '')
			{
				?>
				<h1 class="large"><?php esc_html_e('Whoops!', 'thebe'); ?></h1>
				<h4><?php esc_html_e('We couldn&rsquo;t find the page you were looking for!', 'thebe'); ?></h4>
				<?php
			}
			else
			{
				thebe_kses_content( $thebe_not_found_page_msg );
			}
		?>
		</div>
	</div>
<?php get_footer(); ?>
